'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

var wiredep = require('wiredep').stream;
var _ = require('lodash');
var templateCache = require('gulp-angular-templatecache');

gulp.task('partials', function() {
    return gulp.src([
            path.join(conf.paths.src, '/core/**/*.html')
            // path.join(conf.paths.src, '/sample/**/*.html'),
            // path.join(conf.paths.tmp, '/serve/sample/**/*.html')
        ])
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe($.angularTemplatecache('templateCacheHtml.js', {
            module: conf.appName,
            root: 'core'
        }))
        .pipe(gulp.dest(conf.paths.tmp + '/partials'));
});

gulp.task('html', ['inject', 'partials'], function() {
    var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), {
        read: false
    });
    var partialsInjectOptions = {
        starttag: '<!-- inject:partials -->',
        ignorePath: path.join(conf.paths.tmp, '/partials'),
        addRootSlash: false
    };

    var htmlFilter = $.filter('*.html', {
        restore: true
    });
    var jsFilter = $.filter('**/*.js', {
        restore: true
    });
    var cssFilter = $.filter('**/*.css', {
        restore: true
    });
    var assets;

    return gulp.src(path.join(conf.paths.tmp, '/serve/sample/*.html'))
        .pipe($.inject(partialsInjectFile, partialsInjectOptions))
        .pipe(assets = $.useref.assets())
        .pipe($.rev())
        .pipe(jsFilter)
        .pipe($.sourcemaps.init())
        .pipe($.ngAnnotate())
        .pipe($.uglify({
            preserveComments: $.uglifySaveLicense
        })).on('error', conf.errorHandler('Uglify'))
        .pipe($.sourcemaps.write('maps'))
        .pipe(jsFilter.restore)
        .pipe(cssFilter)
        .pipe($.sourcemaps.init())
        .pipe($.replace('../../bower_components/material-design-iconfont/iconfont/', '../fonts/'))
        .pipe($.minifyCss({
            processImport: false
        }))
        .pipe($.sourcemaps.write('maps'))
        .pipe(cssFilter.restore)
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe($.revReplace())
        .pipe(htmlFilter)
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true,
            conditionals: true
        }))
        .pipe(htmlFilter.restore)
        .pipe(gulp.dest(path.join(conf.paths.dist, '/sample')))
        .pipe($.size({
            title: path.join(conf.paths.dist, '/sample'),
            showFiles: true
        }));
});

// Only applies for fonts from bower dependencies
// Custom fonts are handled by the "other" task
gulp.task('fonts', function() {
    return gulp.src($.mainBowerFiles().concat('bower_components/material-design-iconfont/iconfont/*'))
        .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe($.flatten())
        .pipe(gulp.dest(path.join(conf.paths.dist, '/fonts/')));
});

gulp.task('other', function() {
    var fileFilter = $.filter(function(file) {
        return file.stat.isFile();
    });

    return gulp.src([
            path.join(conf.paths.src, '/**/*'),
            path.join('!' + conf.paths.src, '/**/*.{html,css,js,scss}')
        ])
        .pipe(fileFilter)
        .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('clean', function() {
    return $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]);
});

//
// Custom
// 
gulp.task('partials-custom', function() {
    return gulp.src(path.join(conf.paths.src, '/core/**/*.html'))
        .pipe(templateCache('templateCacheHtml.js', {
            module: conf.appName,
            root: 'core'
        }))
        .pipe(gulp.dest(conf.paths.tmp + '/partials'));
});
gulp.task('lib-js', ['partials-custom'], function() {
    return gulp.src([
            path.join(conf.paths.src, '/core/**/*.module.js'),
            path.join(conf.paths.src, '/core/**/*.js'),
            path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'),
            path.join('!' + conf.paths.src, '/core/**/*.spec.js'),
            path.join('!' + conf.paths.src, '/core/**/*.mock.js')
        ])
        .pipe($.concat('mtda-kit.js'))
        .pipe($.size()).pipe(gulp.dest(path.join(conf.paths.src, '/')));
});
gulp.task('lib-css', [], function() {
    var sassOptions = {
        style: 'expanded'
    };

    var injectFiles = gulp.src([
        path.join(conf.paths.src, '/core/**/*.css'),
        path.join(conf.paths.src, '/core/**/*.scss'),
        path.join('!' + conf.paths.src, '/core/index.scss')
    ], {
        read: false
    });

    var injectOptions = {
        transform: function(filePath) {
            filePath = filePath.replace(conf.paths.src + '/core/', '');
            return '@import "' + filePath + '";';
        },
        starttag: '// injector',
        endtag: '// endinjector',
        addRootSlash: false
    };


    return gulp.src([
            path.join(conf.paths.src, '/core/index.scss')
        ])
        .pipe($.inject(injectFiles, injectOptions))
        .pipe(wiredep(_.extend({}, conf.wiredep)))
        .pipe($.sourcemaps.init())
        .pipe($.sass(sassOptions)).on('error', conf.errorHandler('Sass'))
        .pipe($.autoprefixer()).on('error', conf.errorHandler('Autoprefixer'))
        .pipe($.sourcemaps.write())
        .pipe($.concat('mtda-kit.css')).pipe($.size()).pipe(gulp.dest(path.join(conf.paths.src, '/')));
});
gulp.task('build-core', ['lib-js', 'lib-css']);
gulp.task('build', ['html', 'fonts', 'other', 'app', 'build-core']);