'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');

function isOnlyChange(event) {
    return event.type === 'changed';
}

gulp.task('watch', ['inject'], function() {

    gulp.watch([path.join(conf.paths.src, '/**/*.html'), 'bower.json'], ['inject-reload']);

    gulp.watch([
        path.join(conf.paths.src, '/**/*.css'),
        path.join(conf.paths.src, '/**/*.scss'),
        path.join('!' + conf.paths.src, '/mtda-kit.css'),
        path.join('!' + conf.paths.src, '/mtda-kit.min.scss')
    ], function(event) {
        if (isOnlyChange(event)) {
            gulp.start('styles-reload');
        } else {
            gulp.start('inject-reload');
        }
    });

    gulp.watch([
        path.join(conf.paths.src, '/**/*.js'),
        path.join('!' + conf.paths.src, '/mtda-kit.js'),
        path.join('!' + conf.paths.src, '/mtda-kit.min.js')
    ], function(event) {
        if (isOnlyChange(event)) {
            gulp.start(['scripts-reload', 'lib-js']);
        } else {
            gulp.start('inject-reload');
        }
    });

    gulp.watch(path.join(conf.paths.src, '/sample/**/*.html'), function(event) {
        browserSync.reload(event.path);
    });
});