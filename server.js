var express = require('express');
var compression = require('compression');
var prerender = require('prerender-node');
var base64url = require('b64url');
var bodyParser = require('body-parser');
var app = express();
var sm = require('sitemap'),
    currentSitemap;
// Here we require the prerender middleware that will handle requests from Search Engine crawlers
// We set the token only if we're using the Prerender.io service
// app.use(require('prerender-node')
//     .set('prerenderToken', 'iYoMs0u5XdiRFnv84x97')
//     // .set('prerenderServiceUrl', 'https://livejob-crawl.herokuapp.com')
//     //.set('prerenderServiceUrl', 'http://localhost:3000')
//     //.set('prerenderServiceUrl', 'https://service.prerender.io')
// );

// Here we require the prerender middleware that will handle requests from Search Engine crawlers 
// We set the token only if we're using the Prerender.io service 
app.use(require('prerender-node').set('prerenderToken', process.env.PRERENDER_TOKEN));
// app.use(app.router);

app.use(express.static(__dirname + '/dist'));
app.use(compression());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

//
// handle sitemap get
// 
app.get('/sitemap.xml', function(req, res) {
    //
    // generates sitemap
    // 
    sitemap(function(err, currentSitemap) {
        if (err)
            handleError(res, 'sitemap error');
        res.header('Content-Type', 'application/xml');
        res.send(currentSitemap.toString());
    });
});

//
// handle all gets
// 
app.get('/*', function(req, res) {
    res.sendFile(__dirname + '/dist/index.html');
});

app.listen(process.env.PORT || 3001);

function sitemap(cb) {
    var currentSitemap = sm.createSitemap({
        hostname: 'https://www.mtda.de',
        cacheTime: 600000, // 600 sec - cache purge period
        urls: [{
            url: '/',
            changefreq: 'daily',
            priority: 0.3
        }, {
            url: '/perfil/',
            changefreq: 'daily',
            priority: 0.3
        }, {
            url: '/servicos/',
            changefreq: 'daily',
            priority: 0.3
        }, {
            url: '/cases/',
            changefreq: 'daily',
            priority: 0.3
        }, {
            url: '/contato/',
            changefreq: 'daily',
            priority: 0.3
        }, {
            url: '/blog/',
            changefreq: 'daily',
            priority: 0.3
        }]
    });

    if (cb) return cb(err, currentSitemap);
}

// function sitemap(cb) {
//     var currentSitemap = sm.createSitemap({
//         hostname: 'https://www.mtda.de',
//         cacheTime: 600000, // 600 sec - cache purge period
//         urls: [{
//             url: '/',
//             changefreq: 'daily',
//             priority: 0.3
//         }]
//     });
//     Company.find({
//         active: true
//     }, function(err, companies) {
//         companies.forEach(function(company) {
//             currentSitemap.add({
//                 url: '/' + company.ref + '/',
//                 changefreq: 'daily',
//                 prioty: 0.7
//             });
//         })

//         if (cb) return cb(err, currentSitemap);
//     });
// }


function handleError(res, err) {
    return res.status(500).send(err);
}