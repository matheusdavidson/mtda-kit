(function() {
    'use strict';

    angular.module('mtda.app', [
        'ngAnimate',
        'ngCookies',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'ngStorage',
        'ui.router',
        'permission',
        'anim-in-out',
        'ngMaterial',
        'ngLodash',
        'toastr',
        'pascalprecht.translate',
        'angulartics',
        'angulartics.google.analytics',
        'angularMoment',
        'mtda.filepicker',
        'mtda.main',
        'mtda.env',
        'mtda.setting',
        'mtda.page',
        'mtda.services',
        'mtda.directives',
        'mtda.filters',
        'mtda.user',
        'mtda.account',
        'mtda.list',
        'mtda.banner',
        'mtda.category',
        'mtda.product',
        'mtda.cart',
        'mtda.checkout',
        'mtda.order',
        'angular-flexslider',
        'idf.br-filters',
        'djds4rce.angular-socialshare',
        'ngMask',
        'md.data.table',
        'validation.match'
    ]);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.account
     **/
    angular.module('mtda.account', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.banner
     **/
    angular.module('mtda.banner', [
        'ui.router',
        'ngFileUpload',
        'ngImgCrop'
    ]);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.cart
     **/
    angular.module('mtda.cart', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.admin
     * @requires mtda.env
     * @requires mtda.setting
     * @requires satellizer
     **/
    angular.module('mtda.category', [
        'ui.router',
        'slugifier'
    ]);
})();
(function() {
    'use strict';
    angular.module('mtda.directives', []);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.cart
     **/
    angular.module('mtda.filepicker', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';
    angular.module('mtda.filters', []);
})();
(function() {
    'use strict';
    angular.module('mtda.list', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';
    angular.module('mtda.main', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.admin
     **/
    angular.module('mtda.order', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';
    angular.module('mtda.page', []);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.product
     **/
    angular.module('mtda.product', [
        'ui.router',
        'slugifier',
        'froala',
        'ui.utils.masks',
        'mtda.cart'
    ]);
})();
(function() {
    'use strict';
    angular.module('mtda.services', []);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.user
     * @requires mtda.env
     * @requires mtda.setting
     * @requires satellizer
     **/
    angular.module('mtda.user', [
        'ui.router',
        'satellizer'
    ]);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.cart
     **/
    angular.module('mtda.checkout', [
        'ui.router'
    ]);
})();
(function() {
    'use strict';

    angular
        .module('mtda.app')
        .config(config);

    /** @ngInject */
    function config($logProvider, toastrConfig, $locationProvider, $mdThemingProvider, $authProvider, api, setting, enviroment, $httpProvider, $menuProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        // Set options third-party lib
        toastrConfig.allowHtml = true;
        toastrConfig.timeOut = 3000;
        toastrConfig.positionClass = 'toast-top-right';
        toastrConfig.preventDuplicates = true;
        toastrConfig.progressBar = true;

        $locationProvider.html5Mode(true);

        //
        // Theme options
        //
        $mdThemingProvider.theme('default').primaryPalette('indigo', {
            // 'hue-1': '600'
        }).accentPalette('red', {
            // 'hue-1': '600'
        });

        //
        // Dark theme
        //
        $mdThemingProvider.theme('darkness').primaryPalette('cyan').dark();

        //
        // Auth options
        //
        $authProvider.httpInterceptor = function() {
            return true;
        },
        $authProvider.withCredentials = false;
        $authProvider.tokenRoot = null;
        $authProvider.cordova = false;
        $authProvider.baseUrl = '/';
        $authProvider.loginUrl = api.url + 'auth/local/';
        $authProvider.signupUrl = api.url + 'api/users/';
        $authProvider.unlinkUrl = '/auth/unlink/';
        $authProvider.tokenName = 'token';
        $authProvider.tokenPrefix = setting.slug + '.session'; // Local Storage name prefix
        $authProvider.authHeader = 'Authorization';
        $authProvider.authToken = 'Bearer';
        $authProvider.storageType = 'localStorage';

        //
        // Debug handle
        //
        if (enviroment === 'production')
            $logProvider.debugEnabled(false);
        else
            $logProvider.debugEnabled(true);

        //
        // Intercept Http
        //
        $httpProvider.interceptors.push('HttpInterceptor');
    }

})();
(function() {
    'use strict';

    angular
        .module('mtda.app')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider, $mainProvider, $urlMatcherFactoryProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app', {
            abstract: true,
            views: {
                'app': {
                    templateUrl: /*@ngInject*/ function() {
                        return $mainProvider.layoutUrl();
                    }
                }
            }
        }).state('app.four-oh-four', {
            url: '/404/',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $mainProvider.fourOhFourUrl();
                    }
                }
            }
        });

        $urlRouterProvider.otherwise('/404/');

        //
        // Redirect Trailing Slash
        //
        // $urlMatcherFactoryProvider.strictMode(false);
        // $urlRouterProvider.otherwise(function($injector, $location) {
        //     // 
        //     // Not for iframes
        //     if ($location.hash() !== 'iframe') {

        //         // 
        //         // Inject lodash
        //         var _ = $injector.get('lodash');

        //         // 
        //         // Get current path
        //         var path = $location.url();

        //         // 
        //         // Normal cenario, but if this is still here, no route found, its a 404
        //         if (_.endsWith(path, '/') || _.endsWith(path, '/?')) {
        //             // 
        //             // If we are already /404/ or /404/? dont do nothing
        //             if (_.endsWith(path, '404/') || _.endsWith(path, '404/?')) return;

        //             // 
        //             // Send to 404 page
        //             $location.path('/404/');
        //             return;
        //         } else
        //         // 
        //         // Bad cenario, change ? for /?
        //         if (path.indexOf('?') > -1) {
        //             return path.replace('?', '/?');
        //         }

        //         // 
        //         // Bad cenario, no /, place /
        //         return path + '/';
        //     }
        // });
    }

})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountCtrl', /*@ngInject*/ function($state, $User, $account, $page, $user, activity, lodash, $mdDialog) {
        var vm = this;

        // 
        // Set logged in user
        var user = $user.getCurrent(),
            profileId = $state.params._id;

        // 
        // If no user id, add from the session
        if (!profileId) profileId = user.model._id;

        // 
        // Factory
        vm.factory = $account;
        vm.activityFactory = activity;

        // 
        // Enable editing if the logged in user is the same as the profile
        vm.editable = (profileId === user.model._id);

        // 
        // Update profile pic
        vm.updateProfilePicture = updateProfilePicture;

        //
        // Select account
        vm.factory.get(profileId).then(function(data) {

            if (!data) {
                $state.go('app.home');
            }

            //
            // Instantiate account
            vm.service = new $User.service({
                model: data
            });

        }, function() {
            $page.toast($page.msgNotDone);

            // 
            // Redirect user back to accounts
            $state.go('app.accounts');
        });

        // 
        // Feed list
        vm.params = {
            searchPlaceholder: 'Feed',
            source: getAllActivities,
            primaryIcon: 'edit',
            filters: {
                owner: true
            },
            getAnchorParams: getAnchorParams,
            disableTransition: true,
            limit: 6,
            noPoster: true,
            noPanel: true
        };

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // Update entry
            vm.service.update(false, true);
        }

        function getAllActivities(params) {
            // 
            // Add createdBy
            params.filters['createdBy'] = profileId;

            // 
            // Get entries
            return vm.activityFactory.getAll(params);
        }

        function getAnchorParams(item) {
            var params = {};

            // 
            // Project?
            if (lodash.hasIn(item, 'project._id')) {
                params['_id'] = item.project._id;
                params[item.type] = item.entry._id;
            } else {
                params['_id'] = item.entry._id;
            }

            return params;
        }

        function updateProfilePicture(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'core/filepicker/dialogFilepicker.tpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function(result) {
                vm.service.model.image = result;
            }.bind(this), function() {}.bind(this));
        }

        /*@ngInject*/
        function DialogController($scope, $user) {

            var cropperOptions = {
                aspectRatio: 4 / 4,
                resultImage: {
                    w: 420,
                    h: 420
                },
                viewMode: 1
            }

            $scope.qty = 1;
            $scope.cropperOptions = cropperOptions;
            $scope.result = false;
            $scope.bucket = 'pm-upload';
            $scope.prefix = 'user/profile/' + $user.current().model.account.owner._id + '/';
            $scope.useInstagram = false;

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.confirm = function() {
                // 
                // Get current user
                var user = $user.getCurrent();

                // 
                // Update current image
                user.model.image64 = $scope.result[0].img;

                // 
                // Save
                user.update().then(function(response) {
                    user.model.image = response.data.image;
                    $mdDialog.hide(user.model.image);
                });
            };
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').provider('$account',
        /**
         * @ngdoc object
         * @name mtda.account.$accountProvider
         **/
        /*@ngInject*/
        function $accountProvider() {
            var path = 'core/account/';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_config
             * @propertyOf mtda.account.$accountProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountsTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * url do template para listagem de contas
             **/
            this._accountsTemplateUrl = path + 'accounts.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountsFormTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for new account
             **/
            this._accountsFormTemplateUrl = path + 'accountsForm.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for account
             **/
            this._accountTemplateUrl = path + 'account.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountPasswordTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for account password
             **/
            this._accountPasswordTemplateUrl = path + 'password/account-password.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_publicAccountTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for public account
             **/
            this._publicAccountTemplateUrl = path + 'public/account.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountsListTitle
             * @propertyOf mtda.account.$accountProvider
             * @description
             * field to be used as title in accounts listing
             **/
            this._accountsListTitle = 'email';

            /**
             * @ngdoc object
             * @name mtda.banner.$accountProvider#_routePath
             * @propertyOf mtda.banner.$accountProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/';

            /**
             * @ngdoc object
             * @name mtda.banner.$accountProvider#_accountRoute
             * @propertyOf mtda.banner.$accountProvider
             * @description
             * Path for routes
             **/
            this._accountRoute = 'app.public-account.orders';

            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#$get
             * @propertyOf mtda.account.$accountProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($accounts) {
             *      console.log($accounts.accountsTemplateUrl);
             *      //prints the current accountsTemplateUrl of `mtda.account`
             *      //ex.: "mtda/account/accounts.tpl.html"
             *      console.log($accounts.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/accounts/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this._config,
                    accountsTemplateUrl: this._accountsTemplateUrl,
                    accountsFormTemplateUrl: this._accountsFormTemplateUrl,
                    accountTemplateUrl: this._accountTemplateUrl,
                    accountPasswordTemplateUrl: this._accountPasswordTemplateUrl,
                    publicAccountTemplateUrl: this._publicAccountTemplateUrl,
                    accountsListTitle: this._accountsListTitle,
                    accountRoute: this._accountRoute,
                    routePath: this.routePath,

                    // Factory
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    busy: busy,
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('get')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('get');
                    }
                }

                function remove(e, ids) {
                    // Are we busy?
                    if (this.busy.start('delete')) return NegativePromise.defer();

                    return $http.post(endpoint + 'destroy', {
                        ids: ids
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('delete');
                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('delete');
                    }
                }
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#config
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             *     $accountProvider.config('registerWelcome','Olá @firstName, você entrou para a @appName');
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountsTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.accountsTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.accountsTemplateUrl = function(val) {
                if (val) return this._accountsTemplateUrl = val;
                else return this._accountsTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountsFormTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter for update template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.accountsFormTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val template url
             **/
            this.accountsFormTemplateUrl = function(val) {
                if (val) return this._accountsFormTemplateUrl = val;
                else return this._accountsFormTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountsListTitle
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.accountsListTitle('account.company')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.accountsListTitle = function(val) {
                if (val) return this._accountsListTitle = val;
                else return this._accountsListTitle;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.accountsTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.accountTemplateUrl = function(val) {
                if (val) return this._accountTemplateUrl = val;
                else return this._accountTemplateUrl;
            }
            this.accountPasswordTemplateUrl = function(val) {
                if (val) return this._accountPasswordTemplateUrl = val;
                else return this._accountPasswordTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#publicAccountTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.publicAccountTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.publicAccountTemplateUrl = function(val) {
                if (val) return this._publicAccountTemplateUrl = val;
                else return this._publicAccountTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.banner.$accountProvider#logo
             * @methodOf mtda.banner.$accountProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        });
})();
(function() {
    'use strict';
    angular.module('mtda.account').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $accountProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.accounts', {
            url: $accountProvider.routePath() + 'accounts/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'accounts',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountsTemplateUrl();
                    },
                    controller: '$AccountsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                adminRequired: $userProvider.adminRequired
            }
        }).state('app.accounts-new', {
            url: $accountProvider.routePath() + 'accounts/new',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'accounts',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountsFormTemplateUrl();
                    },
                    controller: '$AccountsNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                adminRequired: $userProvider.adminRequired
            }
        }).state('app.accounts-edit', {
            url: $accountProvider.routePath() + 'accounts/edit/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'accounts',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountsFormTemplateUrl();
                    },
                    controller: '$AccountsEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                adminRequired: $userProvider.adminRequired
            }
        }).state('app.account-password', {
            url: $accountProvider.routePath() + 'accounts/password/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'account',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountPasswordTemplateUrl();
                    },
                    controller: '$AccountPasswordCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.account', {
            url: $accountProvider.routePath() + 'accounts/profile/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'account',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountTemplateUrl();
                    },
                    controller: '$AccountCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.public-account', {
            abstract: true,
            url: '/account/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            tabGroup: 'account',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.publicAccountTemplateUrl();
                    },
                    controller: '$AccountCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.public-account.profile', {
            url: 'profile/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            tabGroup: 'profile',
            innerTabGroup: 'profile',
            views: {
                'account-content': {
                    templateUrl: 'core/account/public/profile.tpl.html',
                    controller: '$PublicProfileCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsEditCtrl', /*@ngInject*/ function($state, $User, $account, $page) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $account;

        // 
        // Edit mode
        vm.edit = true;

        //
        // Select account
        vm.factory.get($state.params._id).then(function(data) {
            //
            // Instantiate account
            vm.service = new $User.service({
                model: data
            });

        }, function() {
            $page.toast($page.msgNotDone);

            // 
            // Redirect user back to accounts
            $state.go('app.accounts');
        });

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // Update entry
            vm.service.update(false, true);
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsNewCtrl', /*@ngInject*/ function($state, $User, $user, $page) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $user;

        //
        // Instantiate account
        vm.service = new $User.service({});

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // 
            // Set email as the password
            vm.service.model.password = vm.service.model.email;

            // Create entry, local provider and noLogin
            vm.service.create(false, true).then(success, fail);

            function success() {
                // Clean model
                vm.service.model = null;

                // Redirect listing
                $state.go('app.accounts');
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsCtrl', /*@ngInject*/ function($account, $state) {
        var vm = this;

        // // List
        // vm.params = {
        //     source: $account.getAll,
        //     secondaryActionType: 'delete',
        //     secondaryActionSource: $account.remove,
        //     addAction: 'app.accounts-new',
        //     primaryAction: primaryAction,
        //     extra: {
        //         listTitle: $account.accountsListTitle
        //     }
        // };

        // function primaryAction(item) {
        //     $state.go('app.accounts-edit', item, {
        //         inherit: false
        //     });
        // }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsItemCtrl', /*@ngInject*/ function($scope, $User) {
        var vm = this;

        vm.model = $scope.item;
        vm.service = new $User.service({
            model: vm.model
        });

        vm.toggleActive = function() {
            console.log(vm.model);
            vm.service.update();
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.banner').controller('$BannerCtrl', /*@ngInject*/ function($user, $List, $banner) {
        var vm = this;
        vm.user = $user;
        vm.currentUser = vm.user.current();

        // List
        vm.params = {
            source: $banner.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $banner.remove,
            addAction: 'app.banner-new',
            showAvatar: true,
            filters: {
                owner: true
            }
        };
    })
})();
(function() {
    'use strict';
    angular.module('mtda.banner').provider('$banner',
        /**
         * @ngdoc object
         * @name mtda.banner.$bannerProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $bannerProvider() {
            /**
             * @ngdoc object
             * @name mtda.banner.$bannerProvider#_config
             * @propertyOf mtda.banner.$bannerProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.banner.$bannerProvider#_routePath
             * @propertyOf mtda.banner.$bannerProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc function
             * @name mtda.banner.$bannerProvider#$get
             * @propertyOf mtda.banner.$bannerProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/banners/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this.config,
                    routePath: this.routePath,
                    // Factory
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    busy: busy
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (busy.start('bannerGetAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('bannerGetAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('bannerGetAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (busy.start('bannerGet')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('bannerGet');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('bannerGet');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (busy.start('bannerDelete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('bannerDelete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        busy.end('bannerDelete');
                    }
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$bannerProvider#config
             * @methodOf mtda.banner.$bannerProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($bannerProvider) {
             *     $bannerProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$bannerProvider#logo
             * @methodOf mtda.banner.$bannerProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($bannerProvider) {
             *     $bannerProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        }
    )
})();
(function() {
    'use strict';
    angular.module('mtda.banner').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $bannerProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.banner', {
            url: $bannerProvider.routePath() + 'banner/?term',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'banner',
            views: {
                'content': {
                    templateUrl: 'core/banner/banner.tpl.html',
                    controller: '$BannerCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.banner-new', {
            url: $bannerProvider.routePath() + 'banner/novo/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'banner',
            views: {
                'content': {
                    templateUrl: 'core/banner/bannerNew.tpl.html',
                    controller: '$BannerNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.banner').service('$Banner', /*@ngInject*/ function($http, $page, $state, $q, api, Busy) {

        var url = api.url + 'api/banners/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;

        function create() {
            // Are we busy?
            if (this.busy.start('create')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var captcha = this.captcha,
                model = this.model;

            return $http
                .post(url, {
                    model: model,
                    captcha: captcha
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgCreated);
                        this.busy.end('create');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('create');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid || (!this.model.image && !this.model.image64));
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.banner').controller('$BannerNewCtrl', /*@ngInject*/ function($state, $user, $Banner) {
        var vm = this;
        vm.user = $user;
        vm.currentUser = vm.user.current();

        vm.banner = new $Banner.service({
            model: {
                picFile: null
            }
        });

        vm.submit = submit;

        // ng-cropper
        vm.cropperOptions = {
            aspectRatio: 12 / 5,
            resultImage: {
                w: 1320,
                h: 550
            }
        }

        function submit() {

            // Send contact to server with recaptcha
            vm.banner.create().then(success, fail);

            function success() {
                // Clean model
                vm.banner.model = null;

                // Redirect listing
                $state.go('app.banner');
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartCtrl', /*@ngInject*/ function($log, $scope, $Cart, $cart, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $cart;
        vm.entries = vm.factory.entries;
    });
})();
(function() {
    'use strict';
    angular.module('mtda.cart').provider('$cart', $cartProvider);
    /**
     * @ngdoc object
     * @name mtda.cart.$cartProvider
     **/
    /*@ngInject*/
    function $cartProvider() {

        /**
         * @ngdoc object
         * @name mtda.cart.$cartProvider#entries
         * @propertyOf mtda.cart.$cartProvider
         * @description
         * store cart entries
         **/
        this.entries = [];

        /**
         * @ngdoc object
         * @name mtda.cart.$cartProvider#subtotal
         * @propertyOf mtda.cart.$cartProvider
         * @description
         * store cart subtotal
         **/
        this.subtotal = 0;

        /**
         * @ngdoc object
         * @name mtda.cart.$cartProvider#cartTemplateUrl
         * @propertyOf mtda.cart.$cartProvider
         * @description
         * cart template url
         **/
        this.cartTemplateUrl = 'core/cart/cart.tpl.html';

        this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, lodash, $localStorage) {
            var endpoint = api.url + 'api/carts/',
                busy = new Busy.service();

            return {
                entries: this.entries,
                subtotal: this.subtotal,
                remove: remove,
                updateSubtotal: updateSubtotal,
                clear: clear,
                hasEntries: hasEntries
            }

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#remove
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * remove entry
             **/
            function remove(index) {
                // 
                // Remove entry from local storage
                this.entries.splice(index, 1);

                // 
                // Notify user
                $page.toast($page.msgCartDeleted, 5000, 'top right');

                // 
                // Update subtotal
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#clear
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * remove all entries
             **/
            function clear() {
                // 
                // Remove all entries
                _.times(this.entries.length, function() {
                    // 
                    // Remove first element
                    this.entries.shift();
                }.bind(this));

                // 
                // Update totals
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#updateSubtotal
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * update subtotal
             **/
            function updateSubtotal() {
                // 
                // Calculate new subtotal
                var subtotal = _.reduce(this.entries, function(sum, obj) {
                    return sum + (obj.price * obj.qty);
                }, 0);

                // 
                // Set new subtotal
                this.subtotal = subtotal;
            };

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#hasEntries
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * check if cart has entries
             **/
            function hasEntries() {
                return (this.entries.length);
            }
        };
    }
})();
(function() {
    'use strict';
    angular.module('mtda.cart').config( /*@ngInject*/ function($stateProvider, $cartProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.cart', {
            url: '/cart/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $cartProvider.cartTemplateUrl;
                    },
                    controller: '$CartCtrl as vm'
                }
            }
        });
    })
})();
(function() {
    'use strict';

    angular.module('mtda.cart').run(runBlock);

    /** @ngInject */
    function runBlock($log, $localStorage, $cart, lodash) {
        // 
        // Se cart, restore from localStorage or start a new one
        $cart.entries = $localStorage.cart = (lodash.hasIn($localStorage, 'cart')) ? $localStorage.cart : [];

        // 
        // Update subtotal
        $cart.updateSubtotal();
    }
})();
(function() {
    'use strict';
    angular.module('mtda.cart').service('$Cart', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug, $localStorage, lodash, $cart, $mdSidenav, $timeout, $filter) {

        var url = api.url + 'api/carts/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.formStatus = formStatus;

        function create(ev) {
            // Start promise
            var defer = $q.defer();

            // Are we busy?
            if (this.busy.start('create')) defer.reject(response);

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;
            
            // 
            // Add entry to local storage
            $cart.entries.push(model);

            // 
            // Stop busy
            this.busy.end('create');

            // 
            // Notify user
            $page.showAlertConfirm(ev, $page.msgConfirmation, $page.msgCartCreated, $filter('translate')('KEEP_SHOPPING'), $filter('translate')('CHECKOUT'), function() {
                // 
                // Open cart
                $mdSidenav('cart').open();
            }, function() {
                // 
                // Go to checkout
                $state.go('app.checkout.identification');
            });

            // 
            // return promise
            defer.resolve();

            // Return promise
            return defer.promise;
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryCtrl', /*@ngInject*/ function($List, $category, $state) {
        var vm = this;

        // List
        vm.params = {
            source: $category.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $category.remove,
            addAction: 'app.category-new',
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            disableTransition: true,
            filters: {
                owner: true
            }
        };

        if ($state.params.term) vm.params.filters.term = $state.params.term;

        function primaryAction(item) {
            $state.go('app.category-edit', item, {
                inherit: false
            });
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').provider('$category',
        /**
         * @ngdoc object
         * @name mtda.banner.$categoryProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $categoryProvider() {
            /**
             * @ngdoc object
             * @name mtda.banner.$categoryProvider#_config
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.banner.$categoryProvider#_routePath
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc object
             * @name mtda.banner.$categoryProvider#publicCategoriesTemplateUrl
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * Url for public categories template
             **/
            this.publicCategoriesTemplateUrl = 'core/category/public/categories.tpl.html';

            /**
             * @ngdoc function
             * @name mtda.banner.$categoryProvider#$get
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, $state, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/categories/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this.config,
                    routePath: this.routePath,
                    // Factory
                    getAll: getAll,
                    get: get,
                    getMenu: getMenu,
                    remove: remove,
                    querySearch: querySearch,
                    slugValidation: slugValidation,
                    busy: busy
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (busy.start('categoryGetAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('categoryGetAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('categoryGetAll');
                    }
                }

                function getMenu(state) {
                    // Are we busy?
                    if (busy.start('getMenu')) return NegativePromise.defer();

                    return $http.get(endpoint + 'menu', {
                        params: {
                            category: state.params.category,
                            child: state.params.child
                        }
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('getMenu');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('getMenu');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (busy.start('categoryGet')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('categoryGet');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('categoryGet');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (busy.start('categoryDelete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('categoryDelete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        busy.end('categoryDelete');
                    }
                }

                function querySearch(query) {
                    if (!query) return false;

                    var deferred = $q.defer();
                    this.getAll({
                        filters: {
                            term: query,
                            owner: true
                        }
                    }).then(function(response) {
                        deferred.resolve(response.entries);
                    });

                    return deferred.promise;
                }

                function slugValidation(viewValue, id, parent) {
                    return $http.post(endpoint + 'slugValidation', {
                        slug: viewValue,
                        id: id,
                        parent: parent
                    }).then(
                        function(response) {
                            if (!response.data.valid) {
                                return $q.reject(response.data.errorMessage);
                            }

                            return true;
                        }
                    );
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$categoryProvider#config
             * @methodOf mtda.banner.$categoryProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($categoryProvider) {
             *     $categoryProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$categoryProvider#logo
             * @methodOf mtda.banner.$categoryProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($categoryProvider) {
             *     $categoryProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        }
    )
})();
(function() {
    'use strict';
    angular.module('mtda.category').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $categoryProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.categories', {
            url: $categoryProvider.routePath() + 'categories/?term',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'category',
            views: {
                'content': {
                    templateUrl: 'core/category/categories.tpl.html',
                    controller: '$CategoryCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.category-new', {
            url: $categoryProvider.routePath() + 'categories/new/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'category',
            views: {
                'content': {
                    templateUrl: 'core/category/categoryNew.tpl.html',
                    controller: '$CategoryNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.category-edit', {
            url: $categoryProvider.routePath() + 'categories/edit/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'category',
            views: {
                'content': {
                    templateUrl: 'core/category/categoryNew.tpl.html',
                    controller: '$CategoryEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.publicCategoriesEmpty', {
            url: '/categories/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $categoryProvider.publicCategoriesTemplateUrl;
                    },
                    controller: '$PublicCategoryCtrl as vm'
                }
            }
        }).state('app.publicCategories', {
            url: '/categories/:category/:child?term?page?sort',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $categoryProvider.publicCategoriesTemplateUrl;
                    },
                    controller: '$PublicCategoryCtrl as vm'
                }
            }
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').service('$Category', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug) {

        var url = api.url + 'api/categories/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;
        this.service.prototype.slugifyUrl = slugifyUrl;

        function create() {
            // Are we busy?
            if (this.busy.start('categoryCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgCreated);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this));
        }

        function update() {
            // Are we busy?
            if (this.busy.start('categoryCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(url + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }

        function slugifyUrl(value) {
            this.model.slug = Slug.slugify(value);
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryEditCtrl', /*@ngInject*/ function($state, $stateParams, $Category, $category) {
        var vm = this;

        vm.title = 'Editar';
        vm.subtitle = 'editar a';

        // Get category by id
        $category.get($state.params._id).then(function(data) {
            if (!data) $state.go('app.categories');

            vm.category = new $Category.service({
                model: data
            });
        }).catch(function() {
            $state.go('app.categories');
        });

        vm.submit = submit;

        // Autocomplete
        vm.querySearch = $category.querySearch.bind($category);

        function submit() {

            // Send contact to server with recaptcha
            vm.category.update().then(success, fail);

            function success() {}

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryNewCtrl', /*@ngInject*/ function($state, $stateParams, $Category, $category) {
        var vm = this;

        vm.title = 'Nova';
        vm.subtitle = 'adicionar uma nova';

        vm.category = new $Category.service();

        vm.submit = submit;

        // Autocomplete
        vm.querySearch = $category.querySearch.bind($category);

        function submit() {

            // Send contact to server with recaptcha
            vm.category.create().then(success, fail);

            function success() {
                // Clean model
                vm.category.model = null;

                // Redirect listing
                $state.go('app.categories');
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').directive('categorySlugValidator', /*@ngInject*/ function($category) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {

                scope.$watch(attrs.dependentModel, function (newVal, oldVal, scope) {
                    ngModel.$validate();
                });

                ngModel.$asyncValidators.slug = function(modelValue, viewValue) {
                    return $category.slugValidation(viewValue, scope.vm.category.model.id, scope.vm.category.model.parent);
                };
            }
        };
    });
})();
(function() {
    'use strict';

    angular.module('mtda.filepicker').controller('$FilepickerCtrl', /*@ngInject*/ function($scope, $log, $Filepicker, $filepicker, lodash, $timeout) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $filepicker;
        vm.entries = vm.factory.entries;

        console.log('freeAspect', vm.freeAspect);
        // 
        // Qty
        if (!vm.qty) vm.qty = 0;

        vm.service = new $Filepicker.service({
            qty: vm.qty,
            cropperOptions: vm.cropperOptions,
            result: vm.result
        });

        $scope.$watch('vm.service.result', function(nv, ov) {
            vm.result = nv;
        }, true);
    });
})();
(function() {
    'use strict';

    angular
        .module('mtda.filepicker')
        .directive('filepicker', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/filepicker/filepicker.tpl.html';
            },
            controller: '$FilepickerCtrl',
            controllerAs: 'vm',
            bindToController: {
                qty: '=',
                cropperOptions: '=',
                result: '=',
                bucket: '=',
                prefix: '=',
                useInstagram: '=',
                freeAspect: '='
            }
        };

        return directive;
    }

})();
(function() {
    'use strict';
    angular.module('mtda.filepicker').provider('$filepicker', $filepickerProvider);
    /**
     * @ngdoc object
     * @name mtda.filepicker.$filepickerProvider
     **/
    /*@ngInject*/
    function $filepickerProvider() {

        /**
         * @ngdoc object
         * @name mtda.filepicker.$filepickerProvider#entries
         * @propertyOf mtda.filepicker.$filepickerProvider
         * @description
         * store filepicker entries
         **/
        this.entries = [];

        /**
         * @ngdoc object
         * @name mtda.filepicker.$filepickerProvider#subtotal
         * @propertyOf mtda.filepicker.$filepickerProvider
         * @description
         * store filepicker subtotal
         **/
        this.subtotal = 0;

        /**
         * @ngdoc object
         * @name mtda.filepicker.$filepickerProvider#filepickerTemplateUrl
         * @propertyOf mtda.filepicker.$filepickerProvider
         * @description
         * filepicker template url
         **/
        this.filepickerTemplateUrl = 'core/filepicker/filepicker.tpl.html';

        this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, lodash, $localStorage) {
            var endpoint = api.url + 'api/filepickers/',
                busy = new Busy.service();

            return {
                entries: this.entries,
                subtotal: this.subtotal,
                remove: remove,
                updateSubtotal: updateSubtotal,
                clear: clear,
                hasEntries: hasEntries
            }

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#remove
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * remove entry
             **/
            function remove(index) {
                // 
                // Remove entry from local storage
                this.entries.splice(index, 1);

                // 
                // Notify user
                $page.toast($page.msgCartDeleted, 5000, 'top right');

                // 
                // Update subtotal
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#clear
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * remove all entries
             **/
            function clear() {
                // 
                // Remove all entries
                _.times(this.entries.length, function() {
                    // 
                    // Remove first element
                    this.entries.shift();
                }.bind(this));

                // 
                // Update totals
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#updateSubtotal
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * update subtotal
             **/
            function updateSubtotal() {
                // 
                // Calculate new subtotal
                var subtotal = _.reduce(this.entries, function(sum, obj) {
                    return sum + (obj.price * obj.qty);
                }, 0);

                // 
                // Set new subtotal
                this.subtotal = subtotal;
            };

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#hasEntries
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * check if filepicker has entries
             **/
            function hasEntries() {
                return (this.entries.length);
            }
        };
    }
})();
(function() {
    'use strict';
    angular.module('mtda.filepicker').service('$Filepicker', /*@ngInject*/ function($q, api, $page, Busy, lodash, Upload, $http, $filter) {

        var url = api.url + 'api/files/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            this.busy = new Busy.service();
            this.files = [];
            this.crop = false;
            this.qty = 0;
            this.selected = 0;
            this.view = 'main';
            this.filesOld = [];

            //
            // Instantiate
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.processFiles = processFiles;
        this.service.prototype.convertToBase64 = convertToBase64;
        this.service.prototype.cropWrapAndPush = cropWrapAndPush;
        this.service.prototype.cropImage = cropImage;
        this.service.prototype.remove = remove;
        this.service.prototype.startCrop = startCrop;
        this.service.prototype.saveCrop = saveCrop;
        this.service.prototype.cancelCrop = cancelCrop;
        this.service.prototype.updateSelectedCnt = updateSelectedCnt;
        this.service.prototype.toggleSelected = toggleSelected;
        this.service.prototype.updateResult = updateResult;
        this.service.prototype.pickOld = pickOld;
        this.service.prototype.cancelOld = cancelOld;
        this.service.prototype.saveOld = saveOld;

        function processFiles($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event, noBlob) {
            // Start promise
            var defer = $q.defer();

            // Are we busy?
            if (this.busy.start('processFiles')) defer.reject(response);

            var files = $newFiles;

            if (files && files.length) {

                // 
                // Transform images in base64
                this.convertToBase64(files, noBlob, function(urls) {
                    // 
                    // Create cropped image
                    // Wrap into an object
                    // Push to service files
                    this.cropWrapAndPush(urls, function() {
                        // 
                        // Stop busy
                        this.busy.end('processFiles');

                        // 
                        // Update cnt
                        this.updateSelectedCnt();

                        // 
                        // Update result
                        this.updateResult();

                        // 
                        // return promise
                        defer.resolve();

                    }.bind(this));
                }.bind(this));
            } else {
                // 
                // Stop busy
                this.busy.end('processFiles');

                // 
                // return promise
                defer.reject();
            }

            // Return promise
            return defer.promise;
        }

        function convertToBase64(files, noBlob, cb) {
            if (noBlob) {
                dataUrlArray(files, function(urls) {
                    cb(urls);
                });
            } else {
                // 
                // Convert to base64 using Upload
                Upload.base64DataUrl(files).then(function(urls) {
                    cb(urls);
                });
            }
        }

        function dataUrlArray(files, cb) {
            var processedFiles = [],
                cnt = files.length,
                x = 0;

            if (!files || !files.length) return cb([]);

            angular.forEach(files, function(file) {
                //
                // Convert blob to base64
                toDataUrl(file.url, 'image/jpeg', function(dataUrl) {
                    // 
                    // Add to processedFiles
                    processedFiles.push(dataUrl);

                    // 
                    // add cnt
                    x++;

                    // 
                    // Return if is the last one
                    if (x == cnt) cb(processedFiles);
                });
            });
        }

        function toDataUrl(url, outputFormat, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';

            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
            };
            img.src = url.replace(/^https:\/\//i, 'http://');
        }

        function cropWrapAndPush(urls, cb) {
            var x = 1,
                length = urls.length;

            // 
            // Set selected cnt for comparisons
            var selectedCnt = angular.copy(this.selected);

            // 
            // Adjust images, get cropped image and add to service files
            angular.forEach(urls, function(img) {
                // 
                // We need img
                if (!img || !img.length) return;

                // 
                // Crop the image
                this.cropImage(img, function(cropped) {

                    // 
                    // Set selected
                    // select only if we have less then qty
                    var selected = ((selectedCnt + x) <= this.qty) ? true : false;

                    // 
                    // Create obj
                    var obj = {
                        img: img,
                        cropped: cropped,
                        selected: selected
                    };

                    // 
                    // Push obj to service files
                    this.files.push(obj);

                    // 
                    // Return if is the last count
                    if (x === length) return cb(true);

                    // 
                    // Add to count
                    x++;
                }.bind(this));
            }.bind(this));
        }

        // 
        // Crop image using cropper js
        function cropImage(img, cb) {
            // 
            // Create a fake element
            var elem = angular.element('<img src="' + img + '" />');

            // 
            // Cropper options
            var resultImage = (lodash.hasIn(this.cropperOptions, 'resultImage')) ? this.cropperOptions.resultImage : {
                w: 540,
                h: 540
            };

            var cropperOptions = {
                aspectRatio: 4 / 4
            };

            // 
            // Extend with user options
            angular.extend(cropperOptions, this.cropperOptions);

            // 
            // Start crop and crop image
            var crop = elem.cropper(cropperOptions);

            var cropped = crop.cropper('getCroppedCanvas', resultImage).toDataURL("image/jpeg", 0.99);
            cb(cropped);
        }

        // 
        // Remove file
        function remove(index, item) {

            // 
            // Remove from files
            if (this.files && this.files[index]) this.files.splice(index, 1);

            // 
            // Update cnt
            this.updateSelectedCnt();

            // 
            // Update result
            this.updateResult();
        }

        // 
        // Start crop
        function startCrop(index, item) {
            // 
            // Create crop obj
            var crop = {};

            // 
            // Properties
            crop.img = item.img;
            crop.model = false;
            crop.index = index;

            this.crop = crop;
        }

        // 
        // Save crop
        function saveCrop() {
            // 
            // Create new obj
            var cropped = {};

            cropped.img = this.crop.model;
            cropped.cropped = this.crop.model;

            // 
            // Extend new obj with existing in files
            angular.extend(this.files[this.crop.index], cropped);

            // 
            // Disable crop
            this.crop = false;
        }

        // 
        // Cancel crop
        function cancelCrop() {
            // 
            // Disable crop
            this.crop = false;
        }

        // 
        // Calculate qty
        function updateSelectedCnt() {
            // 
            // How many we have
            var cnt = lodash.filter(this.files, 'selected').length;

            // 
            // Set new cnt
            this.selected = cnt;

            return this.selected;
        }

        // 
        // Update result
        function updateResult() {
            // 
            // Get selected files
            var result = lodash.filter(this.files, 'selected');
            this.result = result;
        }

        // 
        // Toggle selected
        function toggleSelected(file) {

            // 
            // Don't let select if we have reached qty
            if (file.selected && this.selected === this.qty) {
                file.selected = false;

                // 
                // Toast
                $page.toast('Limite de ' + this.qty + ' atingido');

                return;
            }

            // 
            // Update selected cnt
            this.updateSelectedCnt();

            // 
            // Update result
            this.updateResult();
        }

        function pickOld() {
            this.view = 'old';

            // 
            // Get files
            this.busy.start('getOld');

            // 
            // Get old files
            return $http
                .get(url, {})
                .success(
                    //
                    // success
                    //
                    function(response) {
                        this.busy.end('getOld');

                        // 
                        // Feed old files
                        this.filesOld = response;

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getOld');

                        return response;
                    }.bind(this));
        }

        function cancelOld() {
            this.view = 'main';
        }

        function saveOld() {
            // 
            // Get selected files
            var selected = lodash.filter(this.filesOld, 'selected');

            // 
            // Process files
            this.processFiles(false, false, selected, false, false, false, true).then(function() {
                // 
                // Change view
                this.view = 'main';
            }.bind(this));
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.filters').filter('attrsToString', /*@ngInject*/ function(lodash, $filter) {
        return function(value) {
            if (!value) return '';

            // 
            // Array and string
            var arr = [],
                string = '';

            // 
            // Loop through attrs to translate and join values
            lodash.forEach(value[0], function(val, key) {
                if (key !== 'colorValue') {

                    // 
                    // Value can be an array
                    if (typeof val === 'object') val = val.display;

                    // 
                    // Translate key, value and push to array
                    arr.push($filter('translate')(key) + ': ' + $filter('translate')(val));
                }
            });

            // 
            // Join array and set string
            string = arr.join(', ');

            return string;
        };
    })
})();
(function() {
    'use strict';
    angular.module('mtda.filters').filter('customMoment', /*@ngInject*/ function() {
        return function(value, format) {
            if (!value) return '';

            // 
            // Set format
            if (!format) format = 'DD/MM/YYYY HH:mm';

            // 
            // From now
            if (format === 'fromNow') {
                // 
                // Apply moment
                return moment(value).fromNow();
            } else {
                // 
                // Apply moment
                return moment(value).format(format);
            }
        };
    })
})();
(function() {
    'use strict';
    angular.module('mtda.filters').filter('dotToDash', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return '';
            return value.replace(/\./g, '-');
        };
    })
})();
(function() {
    'use strict';
    angular.module('mtda.filters').filter('nl2Br', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return '';
            return value.replace(/\n|\r\n|\r/g, '<br/>');
        };
    })
})();
(function() {
    'use strict';
    angular.module('mtda.filters').filter('shortYear', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return value;

            // 
            // Remove first two digits
            value = value.slice(2, 4);

            return value;
        };
    })
})();
'use strict';
angular.module('mtda.app').controller('FooterCtrl', /*@ngInject*/ function controller($scope, $timeout, $mdDialog, $state, $user) {
    var vm = this;

    vm.user = $user;
    vm.currentUser = vm.user.current();

});
(function() {
    'use strict';
    angular.module('mtda.app').directive('footer', /*@ngInject*/ function directive() {
        return {
            restrict: 'E',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/footer/footerAdmin.tpl.html';
            },
            controller: 'FooterCtrl',
            controllerAs: 'vm'
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.app').directive('scrollTo', /*@ngInject*/ function($location, $anchorScroll, $timeout, $state) {
        return function(scope, element, attrs) {

            element.bind('click', function(event) {
                var location = attrs.scrollTo;
                var currentState = $state.current.name;
                var state = attrs.scrollState ? attrs.scrollState : currentState;

                // Check if is the same route
                if (state == currentState) {
                    event.stopPropagation();
                    var off = scope.$on('$locationChangeStart', function(ev) {
                        off();
                        ev.preventDefault();
                    });

                    scroll(location);
                } else {
                    $state.go(state);

                    scope.$on('$stateChangeSuccess', function() {
                        $timeout(function() {
                            event.stopPropagation();
                            var off = scope.$on('$locationChangeStart', function(ev) {
                                off();
                                ev.preventDefault();
                            });

                            scroll(location);
                        }, 2000);
                    });
                }
            });

            function scroll(location) {
                $location.hash(location);
                $anchorScroll();
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.list').controller('ListCtrl', /*@ngInject*/ function($scope, $List) {
        var vm = this;

        vm.service = vm.instance ? vm.instance : new $List.service(vm.params);

        // Watch for changes in the filter
        $scope.$watch('vm.service.filters', filterWatch, true);

        function filterWatch(nv, ov) {
            if (nv != ov) {
                vm.service.filterUpdated();
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.list').directive('list', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListCtrl',
            controllerAs: 'vm',
            bindToController: {
                params: '=',
                instance: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/list/list.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.list').service('$List', /*@ngInject*/ function($http, $page, $state, $stateParams, $q, $location, api, Busy, lodash) {
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.entries = [];

            // Define if this is starting
            this.busy.start('starting');

            // Query params
            this.page = $stateParams.page && typeof $stateParams.page === 'string' ? Number($stateParams.page) : 1;
            this.limit = 12;
            this.firstQuery = true;
            this.filters = {
                term: '',
                sort: '-created'
            };

            // Route
            this.disableTransition = true;
            this.route = false;

            // Config
            this.loadMoreBtn = false;
            this.total = 0;
            this.totalDisplay = 0;
            this.searchPlaceholder = 'Pesquisar por título, descrição, etc..';
            this.showAvatar = false;
            this.primaryAction = false;
            this.primaryIcon = false;
            this.infinite = true;

            // Methods
            this.source = false;
            this.get = get;

            // Secondary action
            this.secondaryActionType = 'delete';
            this.secondaryActionSource = false;

            // Main actions
            this.addAction = false;

            // Set sort
            $stateParams.sort = this.filters.sort;

            // Set initial $stateParams on the filters
            angular.extend(this.filters, lodash.pick($stateParams, ['sort', 'term']));

            //
            // Extend filters first
            // angular.extend does not make deep extend
            //
            params.filters = angular.extend(this.filters, params.filters ? params.filters : {});

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);
        }

        this.service.prototype.search = search;
        this.service.prototype.updateTotals = updateTotals;
        this.service.prototype.updateLoadMoreBtn = updateLoadMoreBtn;
        this.service.prototype.filterUpdated = filterUpdated;
        this.service.prototype.updateQueryParams = updateQueryParams;
        this.service.prototype.entriesUpdated = entriesUpdated;
        this.service.prototype.resetList = resetList;
        this.service.prototype.goToNextPage = goToNextPage;
        this.service.prototype.goToPrevPage = goToPrevPage;

        this.service.prototype.get = get;
        this.service.prototype.getConfig = getConfig;
        this.service.prototype.getSuccess = getSuccess;

        this.service.prototype.secondaryAction = secondaryAction;

        this.service.prototype.negativePromise = negativePromise;

        function search() {
            // Reset main props
            this.resetList();

            // Update query params, silent redirect(no refresh)
            $state.go(this.route, this.updateQueryParams());
        }

        function updateTotals(total) {
            this.total = total;
            this.totalDisplay = this.entries.length;

            // var plus = this.infinite ? 0 : 1;

            // First page
            this.showingStart = this.page > 1 ? this.limit * (this.page - 1) + 1 : this.totalDisplay > 0 ? 1 : 0;
            this.showingEnd = this.page > 1 && !this.infinite ? (this.limit * (this.page - 1)) + this.totalDisplay : this.totalDisplay;

        }

        function updateLoadMoreBtn() {
            var result = (this.total > this.showingEnd);

            this.loadMoreBtn = result;
            return result;
        }

        function filterUpdated(noReset) {
            // Start loading
            if (this.busy.start('loading')) return false;

            // Reset main props
            if (!noReset) this.resetList();

            // Update query params
            var updatedQueryParams = this.updateQueryParams();

            // Silent redirect(no refresh) / required config on state: 'reloadOnSearch : false'
            if (!this.disableTransition)
                $location.search(updatedQueryParams);
            // $state.go(this.route, updatedQueryParams, {
            //     notify: false
            // });

            // Handle filter update
            this.get();
        }

        function updateQueryParams() {
            var obj = {
                page: this.page
            };

            // Add current filters
            angular.extend(obj, this.filters, {
                'category': undefined,
                'child': undefined
            }); // @todo: set this as a property

            return obj;
        }

        function entriesUpdated(data) {
            // Update totals
            this.updateTotals(data.total);

            // Update page
            this.page = data.nextPage ? data.nextPage : 1;

            // Update next button
            this.updateLoadMoreBtn();
        }

        // Verificar possibilidade de ter essas props aqui e manipular a lista por aqui mesmo,
        // no controller principal, teremos essas props tbm mas apenas para fins informativos(exibir para o usuário)
        function resetList() {
            // Reset props
            this.entries = [];
            this.total = 0;
            this.totalDisplay = 0;
            this.page = 1;
            this.loadMoreBtn = false;
        }

        function goToNextPage() {
            // Start loading
            if (this.busy.start('loadingMore')) return false;

            if (!this.disableTransition)
                $location.search(this.updateQueryParams());
            // $state.go('.', this.updateQueryParams(), {
            //     notify: false
            // });

            // Handle filter update
            this.get();
        }

        function goToPrevPage() {
            // Start loading
            if (this.busy.start('loadingMore')) return false;

            // Decrease page
            this.page = this.page > 2 ? this.page - 2 : 1;

            if (!this.disableTransition)
                $location.search(this.updateQueryParams());
            // $state.go('.', this.updateQueryParams(), {
            //     notify: false
            // });

            // Handle filter update
            this.get();
        }

        function get() {
            // Unset starting
            this.busy.end('starting');

            // Get from the source(http) and return promise
            return this.source(this.getConfig()).then(this.getSuccess.bind(this));
        }

        function getConfig() {
            // Transformar filtros em JSON
            var filters = this.filters ? angular.copy(this.filters) : false;

            return {
                user: false,
                firstQuery: this.firstQuery,
                page: this.page,
                limit: this.limit,
                filters: filters
            }
        }

        function getSuccess(data) {
            // Push new result
            this.entries = this.infinite ? this.entries.concat(data.entries) : data.entries;

            // Update totals and next button
            this.entriesUpdated(data);

            // Set to false after first time
            if (this.firstQuery) this.firstQuery = false;

            this.busy.end('loading');
            this.busy.end('loadingMore');

            // Return event
            return data;
        }

        function secondaryAction(e, item, index) {
            return this.secondaryActionSource(e, item).then(function() {
                if (this.secondaryActionType === 'delete') {
                    // Delete entry
                    this.entries.splice(index, 1);

                    // Update totals
                    this.total = this.total - 1;
                    this.totalDisplay = this.entries.length;

                    // If total displayed is 0 try to reload the query
                    if (!this.totalDisplay) this.filterUpdated();
                }
            }.bind(this), function() {}.bind(this))
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();
(function() {
    'use strict';

    angular
        .module('mtda.main')
        .controller('$MainCtrl', MainCtrl);

    /** @ngInject */
    function MainCtrl($scope, $state, $page, $auth, setting, enviroment, $user, $timeout, $mdSidenav, $mdMedia, $main, $cart) {
        var vm = this;
        vm.enviroment = enviroment;
        
        //
        // SEO
        //
        $page.title(setting.title);
        $page.description(setting.description);

        //
        // OPEN GRAPH
        //
        $page.ogLocale(setting.ogLocale);
        $page.ogSiteName(setting.ogSiteName);
        $page.ogTitle(setting.ogTitle);
        $page.ogDescription(setting.ogDescription);
        $page.ogUrl(setting.ogUrl);
        $page.ogImage(setting.ogImage);
        $page.ogSection(setting.ogSection);
        $page.ogTag(setting.ogTag);

        //
        // Scroll Top after transition
        // 
        var wdow = false;
        $scope.$on('$stateChangeSuccess', stateChangeSuccess);

        bootstrap();

        function bootstrap() {
            vm.config = $main;
            vm.user = $user;
            vm.$page = $page;
            vm.setting = setting;
            vm.year = moment().format('YYYY');
            vm.state = $state;
            vm.auth = $auth;
            vm.year = moment().format('YYYY');
            vm.mdMedia = $mdMedia;
            vm.cart = $cart;
        }

        function stateChangeSuccess() {
            // 
            // Toolbar and tabs
            // 
            $page.toolbar = $state.current.toolbar ? $state.current.toolbar : false;
            $page.tabs = $state.current.tabs ? $state.current.tabs : false;
            $page.footer = $state.current.footer ? $state.current.footer : false;

            // 
            // Set active tab according to route
            // 
            $page.config.set('tabGroup', $state.current.tabGroup ? $state.current.tabGroup : false);
            $page.config.set('innerTabGroup', $state.current.innerTabGroup ? $state.current.innerTabGroup : false);

            //
            // Auto Scroll to Top when state changed
            //
            $timeout(function() {
                // Close sidenav
                if($main.useAdmin) $mdSidenav('admin').close();
                if($main.useCart) $mdSidenav('cart').close();

                if (!wdow) wdow = angular.element('.app-content');

                // Animate to top
                wdow.animate({
                    scrollTop: 0
                }, 100);
            });
        }
    }
})();
(function() {
    'use strict';
    angular.module("mtda.env", []).constant("enviroment", "development").constant("api", {
        url: "http://localhost:9000/"
    });
})();
(function() {
    'use strict';
    angular.module('mtda.main').provider('$main',
        /**
         * @ngdoc object
         * @name mtda.main.$mainProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $mainProvider() {
            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_config
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_layoutUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url do template para layout
             **/
            this._layoutUrl = 'core/main/layout.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_toolbarUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url do template para toolbar
             **/
            this._toolbarUrl = 'core/directives/toolbar/toolbar.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_sidenavUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url do template para sidenav
             **/
            this._sidenavUrl = 'page/menu/sidenav.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_fourOhFourUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url for 404 template
             **/
            this._fourOhFourUrl = 'core/404/404.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_logo
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena logo
             **/
            this._logo = 'assets/images/logo.png';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_logoWhite
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena logo na versão branca
             **/
            this._logoWhite = 'assets/images/logo-white.png';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_logoWhite
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena logo na versão branca
             **/
            this.logoMtda = 'assets/images/mtda-yellow.png';

            this.useCart = true;
            this.useAdmin = true;

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#$get
             * @propertyOf mtda.main.$mainProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($window, setting) {
                return {
                    config: this._config,
                    layoutUrl: this._layoutUrl,
                    toolbarUrl: this._toolbarUrl,
                    sidenavUrl: this._sidenavUrl,
                    fourOhFourUrl: this._fourOhFourUrl,
                    logoWhite: this._logoWhite,
                    logoMtda: this._logoMtda,
                    logo: this._logo,
                    useCart: this.useCart,
                    useAdmin: this.useAdmin,
                }
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#config
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#logo
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para o path da logo
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.logo('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logo = function(value) {
                if (value) return this._logo = value;
                else return this._logo;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#logoWhite
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para o path da logo na versão branca
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.logoWhite('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logoWhite = function(value) {
                if (value) return this._logoWhite = value;
                else return this._logoWhite;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#logoMtda
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para o path da logo na versão branca
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.logoMtda('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logoMtda = function(value) {
                if (value) return this._logoMtda = value;
                else return this._logoMtda;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#layoutUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para url do layout
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.layoutUrl('core/layout/my-layout.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.layoutUrl = function(val) {
                if (val) return this._layoutUrl = val;
                else return this._layoutUrl;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#fourOhFourUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter 404 template url
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.fourOhFourUrl('core/layout/my-layout.html')
             * })
             * </pre>
             * @param {string} val - template url
             **/
            this.fourOhFourUrl = function(val) {
                if (val) return this._fourOhFourUrl = val;
                else return this._fourOhFourUrl;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#toolbarUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para url do toolbar
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.toolbarUrl('core/layout/my-toolbar.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.toolbarUrl = function(val) {
                if (val) return this._toolbarUrl = val;
                else return this._toolbarUrl;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#sidenavUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para url do sidenav
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.sidenavUrl('core/layout/my-sidenav.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.sidenavUrl = function(val) {
                if (val) return this._sidenavUrl = val;
                else return this._sidenavUrl;
            };
        }
    )
})();
(function() {
    angular.module("mtda.setting", []).constant("setting", {
        name: "mtda kit",
        slug: "mtda-kit",
        version: "0.0.0",
        title: "mtda kit",
        baseUrl: "",
        titleSeparator: " — ",
        description: "mtda-kit",
        copyright: "mtda-kit",
        google: {
            clientId: "",
            language: "pt-BR"
        },
        facebook: {
            scope: "email",
            appId: "",
            appSecret: "",
            language: "pt-BR"
        },
        https: [],
        redirWww: false,
        ogLocale: "pt_BR",
        ogSiteName: "mtda-kit",
        ogTitle: "mtda-kit",
        ogDescription: "mtda-kit",
        ogUrl: "",
        ogImage: "",
        ogSection: "mtda-kit",
        ogTag: "mtda-kit"
    });
})();
(function() {
    'use strict';
    angular.module('mtda.page').config( /** @ngInject */ function($translateProvider, $pageProvider) {
        $translateProvider.translations('en_GB', {
            // MESSAGES
            'MSG_CREATED': 'Entry created!',
            'MSG_UPDATED': 'Entry updated!',
            'MSG_DELETED': 'Entry deleted!',
            'MSG_NOT_DONE': 'Something wrong, try again!',
            'MSG_LOGIN': 'You´re logged in.',
            'MSG_CART_CREATED': 'Item added to the cart',
            'MSG_CART_DELETED': 'Item deleted from the cart',
            'MSG_FILL_ALL_FIELDS': 'Fill all required fields',
            'PAGE_NOT_FOUND': 'Page not found',
            'PAGE_NOT_FOUND_TEXT': 'The page you are looking for doesn\'t exist or an error occurred.<br /> <a ui-sref="/">Go back</a> to the main page.',

            'ITEM_DELETE': 'Delete item',
            'NO_ITEMS': 'No items',

            'CHANGE_STATUS': 'Change status',
            'BASIC_DETAILS': 'Basic info',
            'IMAGE_UPLOAD': 'Image',
            'OPEN_MENU': 'Open menu',

            'SPANISH': 'Spanish',

            'ALL': 'All',
            'TITLE': 'Title',
            'NAME': 'Name',
            'LASTNAME': 'Surname',
            'EMAIL': 'Email',
            'COMPANY': 'Company',
            'PRICE': 'Price',
            'PRICE_OFFER': 'Price offer',
            'FEATURED': 'Featured',
            'YES': 'Yes',
            'NO': 'No',
            'QTY': 'Quantity',
            'CHOOSE': 'Choose',
            'CHOOSE_FILE': 'Choose file',
            'IMAGE_SELECTED': 'Selected image',
            'CLICK_TO_CHANGE': 'Click to change',
            'SAVE': 'Save',
            'CANCEL': 'Cancel',
            'ORDER': 'Order',
            'DISABLED': 'Disabled',
            'SIZE': 'Size',
            'CEP': 'Post code',
            'STREET': 'Address line 1',
            'NUMBER': 'N#',
            'BAIRRO': 'Address line 2',
            'CITY': 'Town or City',
            'STATE': 'State',
            'PHONE': 'Phone',
            'MOBILE': 'Mobile',
            'TOTAL': 'Total',
            'IDENTIFICATION': 'Identification',
            'PAYMENT_DETAILS': 'Payment Details',
            'SHIPPING_DETAILS': 'Shipping Details',
            'SHIPPING_METHOD': 'Shipping method',
            'PAYMENT': 'Payment',
            'CONFIRMATION': 'Confirmation',
            'BACK': 'Back',
            'PREV': 'Previous',
            'NEXT': 'Next',
            'RESET': 'Reset',
            'LOADING': 'Loading..',
            'LOADING_RESULTS': 'Loading results..',
            'CLICK_ITEM_SEE_DETAILS': 'Click on the item to see more details',
            'NOT_AVAILABLE': 'Not available',
            'HOW_IT_WORKS': 'How it works',
            'ABOUT_US': 'About us',
            'NO_ENTRIES': 'No entries',
            'NO_RESULTS': 'No results',
            'SHOWING': 'Showing',
            'OF': 'of',
            'RESULT-S': 'result(s)',
            'ORDER_BY': 'Order by',
            'COPYRIGHT_MSG': 'All Right Reserved',
            'DEV_BY_MSG': 'Developed by',
            'ONLY': 'only',
            'SEARCH_FOR': 'Search for',
            'ALL_PRODUCTS': 'All products',
            'FROM': 'From',
            'FOR': 'For',
            'SHIPPING_AND_TAX': 'Shipping and taxes',
            'INTERESTED?': 'Interested?',
            'INTERESTED?_MSG': 'buy and customize right now',
            'ORDER_NOW': 'Order now',
            'ESTIMATE_SHIPPING': 'Estimate shipping costs',
            'TYPE_CEP': 'Type your postcode',
            'CALCULATE': 'Calculate',

            // CHECKOUT
            'CHECKOUT': 'Checkout',
            'KEEP_SHOPPING': 'Keep shopping',
            'WHO_IS_PAYING': 'Who is paying?',
            'BILLING_ADDRESS': 'Billing address',
            'WHO_IS_RECEIVING': 'Who is receiving?',
            'SHIPPING_ADDRESS': 'Shipping address',
            'SELECT_SHIP_METHOD': 'Select a shipping method:',
            'SHIP_FREE_SHIPPING': 'Free shipping',
            'SHIP_GET_IN_STORE': 'Collect in store',
            'SHIP_free_shipping': 'Free shipping',
            'SHIP_free': 'Free shipping',
            'SHIP_get_in_store': 'Collect in store',
            'CHOOSE_PAYMENT_METHOD': 'How do you want to pay',
            'CHECKOUT_WITH_PAYPAL': 'Pay with Paypal',
            'PAYPAL_REDIRECT_MSG': 'You\'ll be redirected to paypal website',
            'SAME_FOR_SHIPPING': 'Use the same details for shipping',
            'SHIPPING': 'Shipping',
            'CONFIRMATION_ERROR': 'Could not proccess your order',
            'CONFIRMATION_ERROR_RESTART': 'Select a product and try again',
            'BOLETO': 'Payment slip',
            'BOLETO_WARNING': 'Your order will be confirmed only after payment compensation',
            'DEBITO_BANCARIO': 'Bank transfer',
            'CREDIT_CARD': 'Credit card',
            'CHOOSE_YOUR_BANK': 'Choose your bank',
            'INSERT_CREDIT_CARD_INFORMATION': 'Insert credit card information',
            'PAY_WITH': 'Pay with',
            'CARD_NUMBER': 'Card number',
            'CARD_EXPIRY_DATE': 'Expiry (MM/YY)',
            'CARD_EXPIRY_FULLDATE': 'Expiry (MM/YYYY)',
            'CARD_SECURITY_NUMBER': 'CVV',
            'CARD_NAME': 'Name in the card',
            'CARD_BIRTHDAY': 'Birthday',
            'CARD_PHONE': 'Phone',
            'CARD_TYPE': 'Type',
            'PAY_BY_PAYPAL': 'Payment by Paypal',
            'PAY_BY_MOIP': 'Payment by MOIP',
            'CONF_ORDER_RECEIVED': 'You order has been received',
            'CONF_DEBIT_MAKE_THE_PAYMENT': 'make the payment',
            'CONF_DEBIT_DIRECTLY_BANK': 'directly through your bank',
            'CONF_DEBIT_TO_CONFIRM': 'to confirm!',
            'CONF_PAY_INVOICE': 'Pay the invoice',
            'CONF_PAY_PENDING': 'Your payment is pending, you\'ll receive an email when the payment is received.',
            'CONF_PAY_COMPLETED': 'We received your payment, your order is confirmed!',
            'CONF_ORDER_NUMBER': 'Order number',
            'CONF_ORDER_NUMBER_LABEL': 'You can keep up with your order by clicking in the order number.',
            'PRINT_INVOICE_FOR_PAYMENT': 'Invoice for payment',
            'FINISH_PAYMENT_YOUR_BANK': 'Proceed to my bank',

            // COLORS
            'COLOR': 'Color',
            'colors': 'Color',
            'sizes': 'Size',
            'BLUE': 'Blue',
            'GREEN': 'Green',
            'GREY': 'Grey',
            'YELLOW': 'Yellow',
            'ORANGE': 'Orange',
            'PURPLE': 'Purple',
            'RED': 'Red',
            'WHITE': 'White',
            'BLACK': 'Black',
            'SILVER': 'Silver',

            // CATEGORIES
            'CATEGORIES': 'Categories',
            'CAT_MENU': 'Categories',
            'CAT_FORM_PARENT_CAT': 'Parent category',

            // PRODUCTS
            'PRODUCT': 'Product',
            'PROD_MENU': 'Products',
            'PROD_LIST': 'Product list',
            'PROD_LIST_DESC': 'Here you can manage your products',
            'PROD_NEW_TITLE': 'New product',
            'PROD_EDIT_TITLE': 'Edit product',
            'PROD_NEW_SUBTITLE': 'Fill the form to add a new product',
            'PROD_EDIT_SUBTITLE': 'Manage product information',
            'PROD_FORM_CAT': 'Add categories..',
            'PROD_FORM_UP_DESC': 'Choose an image and crop it:',
            'PROD_FORM_VARIATIONS_DESC': 'Manage product variations',
            'PROD_FORM_VARIATIONS_ADD': 'Add variations..',
            'PROD_FORM_BASIC_DETAILS': 'Basic info',
            'PROD_FORM_CAT_DETAILS': 'Add categories',
            'PROD_FORM_PRICE_DETAILS': 'Prices and offers',
            'PROD_FORM_FEATURED_DETAILS': 'Featured product',
            'PROD_FORM_UPLOAD_DETAILS': 'Main product image',
            'PROD_FORM_VARIATION_STOCK': 'Variation and stock',
            'PROD_FORM_VARIATION_MANAGE': 'Manage variation',

            // VARIANTS
            'VARI_PRODUCT_VARIATION': 'Choose variations',

            'NAV_OPEN_STORE': 'open store',

            // CART
            'CART': 'Cart',
            'CART_ADD': 'Add to Cart',
            'CART_AVAI': 'available',
            'CART_AVAIS': 'available',
            'CART_AVAI_ERROR': 'limit exceeded',
            'CART_OPEN': 'Open cart',
            'CART_TOTAL': 'Total in the cart',
            'SEE_CART': 'Go to cart',

            // TABS
            'PROJ_TAB_LINK': 'My projects',
            'REP_TAB_LINK': 'Reports',
            'ACC_TAB_LINK': 'My account',
            'ACCS_TAB_LINK': 'Accounts',

            // ACCOUNT
            'ACC_FORM_COMPANY_DETAILS': 'Company details',

            // LOGIN
            'LOGIN_TITLE': 'Login',
            'LOGIN_BTN': 'Login',
            'LOGIN_FORGOT_LINK': 'Forgot my password',
            'LOGIN_SIGNUP_LINK': 'Sign up',
            'LOGIN_HAS_LINK': 'Login',
            'PASSWORD': 'Password',
            'REGISTER_TITLE': 'Sign up',
            'REGISTER_BTN': 'Register',
            'LOGOUT': 'Logout',
            'NEW_CUSTOMER': 'New customer',
            'RETURNING_CUSTOMER': 'Returning customer',

            // CEP
            'CEP_NOT_FOUND': 'No address found for this postcode, fill the form manually',

            // SERVER ERRORS
            'SERVER_FILL_FIELDS': 'Fill all required fields',
            'SERVER_ORDER_PAYMENT_ERROR': 'Error while making the payment',
            'SERVER_ORDER_EXISTING_PAYMENT': 'Payment already exists',
            'SERVER_ORDER_NO_ITEM': 'No items in the cart',
            'SERVER_ORDER_PAYMENT_NOT_DONE': 'Payment was not done, try again',
            'SERVER_ORDER_STOCK_CHECK_FAILED': 'It was not possibl to check products in stock, try again',
            'SERVER_ORDER_NO_STOCK': 'There is no stock for some items in the cart',

            // ORDER
            'ORDERS': 'Orders',
            'PAYMENT_TYPE': 'Payment type',
            'RESUME': 'Resume',
            'INTERNAL_MONITORING': 'Internal progress',
            'TRACKING_CODE': 'Tracking code',
            'CODE': 'Code',
            'INSERT_CODE': 'Insirt the code',
            'CUSTOMER': 'Customer',
            'CUSTOMERS': 'Customers',
            'PRODUCTS': 'Products',
            'PROG_0': 'Received',
            'PROG_1': 'In production',
            'PROG_2': 'Sent',
            'PROG_3': 'Delivered',

            // SORT
            'SORT_BY': 'Sort by',
            'SORT_RECENT': 'Recent',
            'SORT_NOT_RECENT': 'Not recent',
            'SORT_NAME_ASC': 'Name (A-Z)',
            'SORT_NAME_DESC': 'Name (Z-A)',
            'SORT_PRICE_ASC': 'Lower price',
            'SORT_PRICE_DESC': 'Higher price',

            // PAYMENT STATUS
            'PAY_STAT_-1': 'created',
            'PAY_STAT_0': 'pending',
            'PAY_STAT_1': 'approved',
            'PAY_STAT_2': 'failed',
            'PAY_STAT_3': 'canceled',
            'PAY_STAT_4': 'expired',
            'PAY_STAT_5': 'completed',
            'PAID_WITH': 'Paid with',
            'PAID_boleto': 'invoice',
            'PAID_debito_bancario': 'bank debit',
            'PAID_cc': 'credit card',
            'PAID_paypal_cc': 'credit card',
            'PAID_paypal_balance': 'paypal balance',

            // USER
            'USER': 'User',
            'USERS': 'Users',
            'PROFILE': 'Profile',
            'MY_ORDERS': 'My orders',
            'NAVIGATION': 'Navigation',
            'CONTACT': 'Contact',
            'YOUR_ACCOUNT': 'Your account',
            'WELCOME': 'Welcome',

            // HAPPYGRAM
            'HIW-STEP-1': 'See the categories and choose a product',
            'HIW-STEP-2': 'Import, crop and customize your pictures',
            'HIW-STEP-3': 'Make the payment and receive your products',

            // CONTACT
            'GET_IN_TOUCH': 'Get in touch',
            'GET_IN_TOUCH_MSG': 'Fill the form to get in touch with us',
            'ANTISPAN_CHECK': 'anti-spam control',
            'SUBJECT': 'Subject',
            'MESSAGE': 'Message',
            'SEND': 'Send',
            'CUSTOMER_SERVICE': 'Customer service',
            'OTHER': 'Other',
            'OTHER_CONTACTS': 'Other contacts',
            'ADDRESS': 'Address',
        });

        $translateProvider.translations('pt_BR', {
            // MESSAGES
            'MSG_CREATED': 'Criado com sucesso!',
            'MSG_UPDATED': 'Atualizado com sucesso!',
            'MSG_DELETED': 'Deletado com sucesso!',
            'MSG_NOT_DONE': 'Ação não realizada, tente novamente!',
            'MSG_LOGIN': 'Você está logado.',
            'MSG_CART_CREATED': 'O item foi adicionado ao carrinho',
            'MSG_CART_DELETED': 'O item foi removido do carrinho',
            'MSG_FILL_ALL_FIELDS': 'Preencha todos os campos obrigatórios',
            'PAGE_NOT_FOUND': 'Página não encontrada',
            'PAGE_NOT_FOUND_TEXT': 'A página que você visitou não existe ou algum erro pode ter acontecido. <br /> <a ui-sref="/">Volte</a> para a página principal.',

            'ITEM_DELETE': 'Deletar item',
            'NO_ITEMS': 'Nenhum item',

            'CHANGE_STATUS': 'Mudar status',
            'BASIC_DETAILS': 'Dados básicos',
            'IMAGE_UPLOAD': 'Imagem',
            'OPEN_MENU': 'Abrir menu',

            'SPANISH': 'Espanhol',

            'ALL': 'Todos',
            'TITLE': 'Título',
            'NAME': 'Nome',
            'LASTNAME': 'Sobrenome',
            'EMAIL': 'Email',
            'COMPANY': 'Empresa',
            'PRICE': 'Preço',
            'PRICE_OFFER': 'Preço oferta',
            'FEATURED': 'Destaque',
            'YES': 'Sim',
            'NO': 'Não',
            'QTY': 'Quantidade',
            'CHOOSE': 'Escolha',
            'CHOOSE_FILE': 'Escolher arquivo',
            'IMAGE_SELECTED': 'Imagem selecionada',
            'CLICK_TO_CHANGE': 'Clique para mudar',
            'SAVE': 'Salvar',
            'CANCEL': 'Cancelar',
            'ORDER': 'Ordem',
            'DISABLED': 'Desativado',
            'SIZE': 'Tamanho',
            'STREET': 'Logradouro',
            'NUMBER': 'Nº',
            'BAIRRO': 'Bairro',
            'CITY': 'Cidade',
            'STATE': 'UF',
            'PHONE': 'Telefone',
            'MOBILE': 'Celular',
            'TOTAL': 'Total',
            'IDENTIFICATION': 'Identificação',
            'PAYMENT_DETAILS': 'Detalhes de cobrança',
            'SHIPPING_DETAILS': 'Detalhes da entrega',
            'SHIPPING_METHOD': 'Forma de entrega',
            'PAYMENT': 'Pagamento',
            'CONFIRMATION': 'Confirmação',
            'BACK': 'Voltar',
            'PREV': 'Anterior',
            'NEXT': 'Próximo',
            'RESET': 'Resetar',
            'LOADING': 'Carregando..',
            'LOADING_RESULTS': 'Carregando resultados..',
            'CLICK_ITEM_SEE_DETAILS': 'Clique no item para ver mais detalhes',
            'NOT_AVAILABLE': 'Não disponível',
            'HOW_IT_WORKS': 'Como funciona',
            'ABOUT_US': 'Sobre nós',
            'NO_ENTRIES': 'Nenhuma entrada',
            'NO_RESULTS': 'Nenhum resultado',
            'SHOWING': 'Exibindo',
            'OF': 'de',
            'RESULT-S': 'resultado(s)',
            'ORDER_BY': 'Classificar por',
            'COPYRIGHT_MSG': 'Direitos reservados',
            'DEV_BY_MSG': 'Desenvolvido por',
            'ONLY': 'apenas',
            'SEARCH_FOR': 'Busca por',
            'ALL_PRODUCTS': 'Todos os produtos',
            'FROM': 'De',
            'FOR': 'Por',
            'SHIPPING_AND_TAX': 'Entrega e taxas',
            'INTERESTED?': 'Interessado?',
            'INTERESTED?_MSG': 'inicie a montagem agora mesmo',
            'ORDER_NOW': 'Fazer pedido',
            'ESTIMATE_SHIPPING': 'Calcular valor do frete',
            'TYPE_CEP': 'Digite seu CEP',
            'CALCULATE': 'Calcular',

            // CHECKOUT
            'CHECKOUT': 'Checkout',
            'KEEP_SHOPPING': 'Continuar comprando',
            'WHO_IS_PAYING': 'Quem está pagando?',
            'BILLING_ADDRESS': 'Endereço de cobrança',
            'WHO_IS_RECEIVING': 'Quem está recebendo?',
            'SHIPPING_ADDRESS': 'Endereço de entrega',
            'SELECT_SHIP_METHOD': 'Selecione um método de envio:',
            'SHIP_FREE_SHIPPING': 'Entrega grátis',
            'SHIP_GET_IN_STORE': 'Buscar na loja',
            'SHIP_free_shipping': 'Entrega grátis',
            'SHIP_free': 'Entrega grátis',
            'SHIP_get_in_store': 'Buscar na loja',
            'CHOOSE_PAYMENT_METHOD': 'Como gostaria de pagar?',
            'CHECKOUT_WITH_PAYPAL': 'Pagar com Paypal',
            'PAYPAL_REDIRECT_MSG': 'Você será redirecionado para o site do paypal',
            'SAME_FOR_SHIPPING': 'Usar os mesmos dados para a entrega',
            'SHIPPING': 'Entrega',
            'CONFIRMATION_ERROR': 'Houver um erro ao processar seu pagamento',
            'CONFIRMATION_ERROR_RESTART': 'Selecione um produto e tente novamente',
            'BOLETO': 'Boleto bancário',
            'BOLETO_WARNING': 'Seu pedido será liberado após a compensação do boleto',
            'DEBITO_BANCARIO': 'Débito bancário',
            'CREDIT_CARD': 'Cartão de crédito',
            'CHOOSE_YOUR_BANK': 'Selecione o seu banco',
            'INSERT_CREDIT_CARD_INFORMATION': 'Insira as informações do cartão',
            'PAY_WITH': 'Pagar pelo',
            'CARD_NUMBER': 'Número do cartão',
            'CARD_EXPIRY_DATE': 'Validade (MM/AA)',
            'CARD_EXPIRY_FULLDATE': 'Validade (MM/AAAA)',
            'CARD_SECURITY_NUMBER': 'CVC',
            'CARD_NAME': 'Nome impresso',
            'CARD_BIRTHDAY': 'Nascimento',
            'CARD_PHONE': 'Telefone',
            'CARD_TYPE': 'Instituição',
            'PAY_BY_PAYPAL': 'Pagamento feito pelo Paypal',
            'PAY_BY_MOIP': 'Pagamento feito pelo MOIP',
            'CONF_ORDER_RECEIVED': 'Seu pedido foi recebido com sucesso',
            'CONF_DEBIT_MAKE_THE_PAYMENT': 'efetue o pagamento',
            'CONF_DEBIT_DIRECTLY_BANK': 'diretamente pelo site do seu banco',
            'CONF_DEBIT_TO_CONFIRM': 'para confirmar!',
            'CONF_PAY_INVOICE': 'efetue o pagamento do boleto',
            'CONF_PAY_PENDING': 'Seu pagamento está em análise, você receberá um e-mail assim que o pagamento for recebido.',
            'CONF_PAY_COMPLETED': 'Recebemos seu pagamento, seu pedido está liberado!',
            'CONF_ORDER_NUMBER': 'Código do seu pedido',
            'CONF_ORDER_NUMBER_LABEL': 'Você pode acompanhar o seu pedido clicando no número da ordem acima.',
            'PRINT_INVOICE_FOR_PAYMENT': 'Boleto para pagamento',
            'FINISH_PAYMENT_YOUR_BANK': 'Prosseguir para o meu banco',

            // COLORS
            'COLOR': 'Cor',
            'colors': 'Cor',
            'sizes': 'Tamanho',
            'BLUE': 'Azul',
            'GREEN': 'Verde',
            'GREY': 'Cinza',
            'YELLOW': 'Amarelo',
            'ORANGE': 'Laranja',
            'PURPLE': 'Roxo',
            'RED': 'Vermelho',
            'WHITE': 'Branco',
            'BLACK': 'Preto',
            'SILVER': 'Prata',

            // CATEGORIES
            'CATEGORIES': 'Categorias',
            'CAT_MENU': 'Categorias',
            'CAT_FORM_PARENT_CAT': 'Categoria pai',
            'CART_TOTAL': 'Total do carrinho',
            'SEE_CART': 'Ver carrinho',

            // PRODUCTS
            'PRODUCT': 'Produto',
            'PROD_MENU': 'Produtos',
            'PROD_LIST': 'Lista de produtos',
            'PROD_LIST_DESC': 'Aqui você pode gerenciar seus produtos',
            'PROD_NEW_TITLE': 'Novo produto',
            'PROD_EDIT_TITLE': 'Editar produto',
            'PROD_NEW_SUBTITLE': 'Preencha o formulário abaixo para adicionar um novo produto',
            'PROD_EDIT_SUBTITLE': 'Altere os dados do produto',
            'PROD_FORM_CAT': 'Adicione categorias..',
            'PROD_FORM_UP_DESC': 'Escolha uma imagem e faça o recorte que deseja:',
            'PROD_FORM_VARIATIONS_DESC': 'Gerenciar variações do produto',
            'PROD_FORM_VARIATIONS_ADD': 'Adicione variações..',
            'PROD_FORM_BASIC_DETAILS': 'Dados básicos',
            'PROD_FORM_CAT_DETAILS': 'Adicione categorias',
            'PROD_FORM_PRICE_DETAILS': 'Valores e ofertas',
            'PROD_FORM_FEATURED_DETAILS': 'Destaque',
            'PROD_FORM_UPLOAD_DETAILS': 'Imagem principal do produto',
            'PROD_FORM_VARIATION_STOCK': 'Variações e estoque',
            'PROD_FORM_VARIATION_MANAGE': 'Gerenciar variações',

            // VARIANTS
            'VARI_PRODUCT_VARIATION': 'Escolha as variações',

            'NAV_OPEN_STORE': 'abrir loja',

            // CART
            'CART': 'Carrinho',
            'CART_ADD': 'Adicionar',
            'CART_AVAI': 'disponível',
            'CART_AVAIS': 'disponíveis',
            'CART_AVAI_ERROR': 'limite excedido',
            'CART_OPEN': 'Abrir carrinho',

            // TABS
            'PROJ_TAB_LINK': 'Meus projetos',
            'REP_TAB_LINK': 'Relatórios',
            'ACC_TAB_LINK': 'Minha conta',
            'ACCS_TAB_LINK': 'Contas',

            // ACCOUNT
            'ACC_FORM_COMPANY_DETAILS': 'Dados da empresa',

            // LOGIN
            'LOGIN_TITLE': 'acesse sua conta',
            'LOGIN_BTN': 'Entrar',
            'LOGIN_FORGOT_LINK': 'Esqueci minha senha',
            'LOGIN_SIGNUP_LINK': 'Não tenho cadastro',
            'LOGIN_HAS_LINK': 'Já tenho cadastro',
            'PASSWORD': 'Senha',
            'REGISTER_TITLE': 'crie sua conta',
            'REGISTER_BTN': 'Registrar',
            'LOGOUT': 'Sair',
            'NEW_CUSTOMER': 'Novo cliente',
            'RETURNING_CUSTOMER': 'Já sou cliente',

            // CEP
            'CEP_NOT_FOUND': 'Nenhum endereço encontrado com este cep, preencha manualmente',

            // SERVER ERRORS
            'SERVER_FILL_FIELDS': 'Preencha os campos corretamente',
            'SERVER_ORDER_PAYMENT_ERROR': 'Houve um erro no pagamento',
            'SERVER_ORDER_EXISTING_PAYMENT': 'Pagamento já existente',
            'SERVER_ORDER_NO_ITEM': 'Nenhum item no carrinho',
            'SERVER_ORDER_PAYMENT_NOT_DONE': 'O pagamento não foi realizado, tente novamente',
            'SERVER_ORDER_STOCK_CHECK_FAILED': 'Não foi possível verificar o estoque, tente novamente',
            'SERVER_ORDER_NO_STOCK': 'Não há estoque para algum dos items do carrinho',

            // ORDER
            'ORDERS': 'Pedidos',
            'PAYMENT_TYPE': 'Tipo de pagamento',
            'RESUME': 'Resumo',
            'INTERNAL_MONITORING': 'Acompanhamento interno',
            'TRACKING_CODE': 'Código de rastreamento',
            'CODE': 'Código',
            'INSERT_CODE': 'Insira o código',
            'CUSTOMER': 'Cliente',
            'CUSTOMERS': 'Clientes',
            'PRODUCTS': 'Produtos',
            'PROG_0': 'Recebido',
            'PROG_1': 'Em produção',
            'PROG_2': 'Enviado',
            'PROG_3': 'Entregue',

            // SORT
            'SORT_BY': 'Ordenar por',
            'SORT_RECENT': 'Recente',
            'SORT_NOT_RECENT': 'Não recente',
            'SORT_NAME_ASC': 'Nome (A-Z)',
            'SORT_NAME_DESC': 'Nome (Z-A)',
            'SORT_PRICE_ASC': 'Menor preço',
            'SORT_PRICE_DESC': 'Maior preço',

            // PAYMENT STATUS
            'PAY_STAT_-1': 'criado',
            'PAY_STAT_0': 'aguardando pagamento',
            'PAY_STAT_1': 'aprovado',
            'PAY_STAT_2': 'falhou',
            'PAY_STAT_3': 'cancelado',
            'PAY_STAT_4': 'expirado',
            'PAY_STAT_5': 'completo',
            'PAID_WITH': 'Pagou com',
            'PAID_boleto': 'boleto',
            'PAID_debito_bancario': 'débito bancário',
            'PAID_cc': 'cartão de crédito',
            'PAID_paypal_cc': 'cartão de crédito',
            'PAID_paypal_balance': 'saldo paypal',

            // USER
            'USER': 'Usuário',
            'USERS': 'Usuários',
            'PROFILE': 'Perfil',
            'MY_ORDERS': 'Meus pedidos',
            'NAVIGATION': 'Navegação',
            'CONTACT': 'Contato',
            'YOUR_ACCOUNT': 'Sua conta',
            'WELCOME': 'Seja bem vindo',

            // HAPPYGRAM
            'HIW-STEP-1': 'Veja as categorias e escolha um produto',
            'HIW-STEP-2': 'Importe, recorte e personalize suas fotos',
            'HIW-STEP-3': 'Faça o pagamento e receba seu produto em casa',

            // CONTACT
            'GET_IN_TOUCH': 'Entre em contato',
            'GET_IN_TOUCH_MSG': 'Preencha o formulário para falar conosco, faça seu pedido, tire dúvidas ou envie sugestões',
            'ANTISPAN_CHECK': 'controle anti-spam, marque a opção "não sou um robô" para enviar',
            'SUBJECT': 'Assunto',
            'MESSAGE': 'Mensagem',
            'SEND': 'Enviar',
            'CUSTOMER_SERVICE': 'Atendimento',
            'OTHER': 'Outro',
            'OTHER_CONTACTS': 'Outros contatos',
            'ADDRESS': 'Endereço',
        });

        $translateProvider.preferredLanguage($pageProvider.formLocale);
        // $translateProvider.useSanitizeValueStrategy('sanitize');
    });
})();
(function() {
    'use strict';
    angular.module('mtda.main').provider('$menu',
        /**
         * @ngdoc object
         * @name mtda.main.$menuProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $mainProvider() {
            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#_config
             * @propertyOf mtda.main.$menuProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#site
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu do site
             **/
            this.site = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#side
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu do side
             **/
            this.side = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#avatar
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu do avatar
             **/
            this.avatar = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#admin
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu admin
             **/
            this.admin = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#owner
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu owner
             **/
            this.owner = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#currentUser
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu para o usuário
             **/
            this.currentUser = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#userLoggedOut
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu de usuário não logado
             **/
            this.userLoggedOut = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#userLoggedIn
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu de usuário logado
             **/
            this.userLoggedIn = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#ownerLoggedIn
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu de usuário logado do role 'owner'
             **/
            this.ownerLoggedIn = [];

            /**
             * @ngdoc function
             * @name mtda.main.$menuProvider#$get
             * @propertyOf mtda.main.$menuProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($window, setting) {
                return {
                    config: this._config,
                    site: this.site,
                    side: this.side,
                    avatar: this.avatar,
                    admin: this.admin,
                    owner: this.owner,
                    currentUser: this.currentUser,
                    userLoggedOut: this.userLoggedOut,
                    userLoggedIn: this.userLoggedIn,
                    ownerLoggedIn: this.ownerLoggedIn
                }
            }

            /**
             * @ngdoc function
             * @name mtda.main.$menuProvider#config
             * @methodOf mtda.main.$menuProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.main.$menuProvider#set
             * @methodOf mtda.main.$menuProvider
             * @description
             * Adicionar um novo menu
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($menuProvider) {
             *     $menuProvider.set('admin', {
             *         name: 'Conta',
             *         type: 'link',
             *         icon: 'fa fa-at',
             *         url: '/account/',
             *         state: 'app.account'
             *     });
             * })
             * </pre>
             * @param {string} menu - slug do menu
             * @param {object} item - objeto contendo as propriedades do menu
             **/
            this.set = function(menu, items) {
                $.merge(this[menu], items);
            }
        }
    )
})();
(function() {
    'use strict';
    angular.module('mtda.order').controller('$OrderCtrl', /*@ngInject*/ function($List, $order, $Order, $state, $page) {
        var vm = this;

        // 
        // Set factory
        vm.factory = $order;

        // 
        // Set order id or redirect
        vm.order = $state.params._id;
        if (!vm.order) $state.go('app.orders');

        // 
        // Get order or redirect
        vm.factory.get(vm.order).then(success, failed);

        // 
        // Set form locale
        vm.formLocale = $page.formLocale;

        // 
        // Sucess, set service
        function success(data) {
            vm.service = new $Order.service({
                model: data
            });

            setFilterStatus();
        }

        // 
        // No order, redirect
        function failed() {
            $state.go('app.orders');
        }

        // 
        // Remove all from status select list
        function setFilterStatus() {
            // 
            // Get array from provider, but use angular.copy to remove bindings
            var entries = angular.copy(vm.factory.filterStatus);

            // 
            // Remove "all" options which is in 0 index
            entries.splice(0, 1);

            // 
            // Set
            vm.filterStatus = entries;
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.order').provider('$order',
        /**
         * @ngdoc object
         * @name mtda.banner.$orderProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $orderProvider() {
            /**
             * @ngdoc object
             * @name mtda.banner.$orderProvider#_config
             * @propertyOf mtda.banner.$orderProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.banner.$orderProvider#_routePath
             * @propertyOf mtda.banner.$orderProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc function
             * @name mtda.banner.$orderProvider#$get
             * @propertyOf mtda.banner.$orderProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, $state, api, Busy, NegativePromise, $filter, $timeout) {
                var endpoint = api.url + 'api/orders/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this.config,
                    routePath: this.routePath,
                    // Factory
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    querySearch: querySearch,
                    busy: busy,
                    sort: [{
                        name: $filter('translate')('SORT_NOT_RECENT'),
                        value: 'createdAt'
                    }, {
                        name: $filter('translate')('SORT_RECENT'),
                        value: '-createdAt'
                    }, {
                        name: $filter('translate')('SORT_NAME_ASC'),
                        value: 'userName'
                    }, {
                        name: $filter('translate')('SORT_NAME_DESC'),
                        value: '-userName'
                    }, {
                        name: $filter('translate')('SORT_PRICE_ASC'),
                        value: 'price'
                    }, {
                        name: $filter('translate')('SORT_PRICE_DESC'),
                        value: '-price'
                    }],
                    filterPaymode: [{
                        name: $filter('translate')('ALL'),
                        value: false
                    }, {
                        name: $filter('translate')('PAID_boleto'),
                        value: 'boleto'
                    }, {
                        name: $filter('translate')('PAID_debito_bancario'),
                        value: 'debito_bancario'
                    }, {
                        name: $filter('translate')('PAID_cc'),
                        value: 'cc'
                    }, {
                        name: $filter('translate')('PAID_paypal_balance'),
                        value: 'paypal_balance'
                    }, {
                        name: $filter('translate')('PAID_paypal_cc'),
                        value: 'paypal_cc'
                    }],
                    filterStatus: [{
                        name: $filter('translate')('ALL'),
                        value: false
                    }, {
                        name: $filter('translate')('PAY_STAT_-1'),
                        value: -1
                    }, {
                        name: $filter('translate')('PAY_STAT_0'),
                        value: 0
                    }, {
                        name: $filter('translate')('PAY_STAT_1'),
                        value: 1
                    }, {
                        name: $filter('translate')('PAY_STAT_2'),
                        value: 2
                    }, {
                        name: $filter('translate')('PAY_STAT_3'),
                        value: 3
                    }, {
                        name: $filter('translate')('PAY_STAT_4'),
                        value: 4
                    }, {
                        name: $filter('translate')('PAY_STAT_5'),
                        value: 5
                    }],
                    filterInternalStatus: [{
                        name: $filter('translate')('PROG_0'),
                        value: 0
                    }, {
                        name: $filter('translate')('PROG_1'),
                        value: 1
                    }, {
                        name: $filter('translate')('PROG_2'),
                        value: 2
                    }, {
                        name: $filter('translate')('PROG_3'),
                        value: 3
                    }]
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (busy.start('getAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this), onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (busy.start('get')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('get');

                        return response;
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (busy.start('delete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('delete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        busy.end('delete');
                    }
                }

                function querySearch(query) {
                    if (!query) return false;

                    var deferred = $q.defer();
                    this.getAll({
                        filters: {
                            term: query,
                            owner: true
                        }
                    }).then(function(response) {
                        deferred.resolve(response.entries);
                    });

                    return deferred.promise;
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$orderProvider#config
             * @methodOf mtda.banner.$orderProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($orderProvider) {
             *     $orderProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$orderProvider#logo
             * @methodOf mtda.banner.$orderProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($orderProvider) {
             *     $orderProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        }
    )
})();
(function() {
    'use strict';
    angular.module('mtda.order').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $orderProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.orders', {
            url: $orderProvider.routePath() + 'orders/?term?paymode?status',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'order',
            views: {
                'content': {
                    templateUrl: 'core/order/orders.tpl.html',
                    controller: '$OrdersCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.order', {
            url: $orderProvider.routePath() + 'orders/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'order',
            views: {
                'content': {
                    templateUrl: 'core/order/order.tpl.html',
                    controller: '$OrderCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.public-account.orders', {
            url: 'orders/?term?paymode?status',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'order',
            views: {
                'account-content': {
                    templateUrl: 'core/order/public/orders.tpl.html',
                    controller: '$PublicOrdersCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.public-account.order', {
            url: 'orders/:_id',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'order',
            views: {
                'account-content': {
                    templateUrl: 'core/order/public/order.tpl.html',
                    controller: '$PublicOrderCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.order').service('$Order', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug) {

        var url = api.url + 'api/orders/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;
            this.subtotal = 0;
            this.total = 0;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

            // 
            // Set subtotal
            this.setSubtotal();

            // 
            // Set total
            this.setTotal();
        }

        this.service.prototype.update = update;
        this.service.prototype.setSubtotal = setSubtotal;
        this.service.prototype.setTotal = setTotal;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;

        function update(email) {
            // Are we busy?
            if (this.busy.start('update')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            var model = this.model;

            // 
            // Post data
            var post = {
                model: model
            };

            // 
            // Send email?
            if (email) post.email = true;

            return $http
                .put(url + model.id, post)
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('update');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('update');

                        return response;
                    }.bind(this));
        }

        // 
        // Set subtotal
        // (cart.item x cart.qty)
        function setSubtotal() {
            // 
            // Entries validation
            if (!this.model.cart) return this.subtotal;

            // 
            // Calculate subtotal
            var subtotal = _.reduce(this.model.cart, function(sum, obj) {
                return sum + (obj.price * obj.qty);
            }, 0);

            // 
            // Set subtotal
            this.subtotal = subtotal;

            return subtotal;
        };

        // 
        // Set total
        // (subtotal x shipping)
        function setTotal() {
            // 
            // Entries validation
            if ((!this.subtotal && this.subtotal !== 0) || (!this.model.shipping.price && this.model.shipping.price !== 0)) return this.total;

            // 
            // Calculate total
            var total = this.subtotal + this.model.shipping.price;

            // 
            // Set subtotal
            this.total = total;

            return total;
        };

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.order').controller('$OrdersCtrl', /*@ngInject*/ function($List, $order, $state) {
        var vm = this;

        // 
        // Set factory
        vm.factory = $order;

        // List
        vm.params = {
            source: $order.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $order.remove,
            addAction: false,
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            filters: {
                owner: true,
                sort: '-createdAt',
                status: $state.params.status ? $state.params.status : false,
                paymode: $state.params.paymode ? $state.params.paymode : false
            },
            sortOptions: vm.factory.sort,
            filterPaymode: vm.factory.filterPaymode,
            filterStatus: vm.factory.filterStatus
        };

        function primaryAction(item) {
            $state.go('app.order', item, {
                inherit: false
            });
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.page').provider('$page',
        /**
         * @ngdoc object
         * @name core.page.$pageProvider
         * @description
         * Provém configurações/comportamentos/estados para página
         **/
        /*@ngInject*/
        function $pageProvider($translateProvider) {
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_config
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena configurações
             **/
            this.config = {
                // configuração para ativar/desativar a rota inicial
                'homeEnabled': true,
                set: _config.bind(this)
            };

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_title
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena o título
             **/
            this._title = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_description
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena a descrição
             **/
            this._description = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogSiteName
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph site name
             **/
            this._ogSiteName = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogTitle
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph title
             **/
            this._ogTitle = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogDescription
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph description
             **/
            this._ogDescription = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogUrl
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph url
             **/
            this._ogUrl = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogImage
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph image
             **/
            this._ogImage = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogSection
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph section
             **/
            this._ogSection = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogTag
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph tags
             **/
            this._ogTag = '';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_toolbar
             * @propertyOf core.page.$pageProvider
             * @description
             * toolbar que deve ser exibida
             **/
            this._toolbar = 'public';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_tabs
             * @propertyOf core.page.$pageProvider
             * @description
             * tabs que deve ser exibido
             **/
            this._tabs = 'public';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_footer
             * @propertyOf core.page.$pageProvider
             * @description
             * active footer
             **/
            this._footer = 'public';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_footer
             * @propertyOf core.page.$pageProvider
             * @description
             * active footer
             **/
            this.formLocale = 'pt_BR';

            this.locale = 'pt-br';

            this.protectedSlug = '/';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#brStates
             * @propertyOf core.page.$pageProvider
             * @description
             * all br states for select
             **/
            this.brStates = [{
                value: "AC",
                name: "Acre"
            }, {
                value: "AL",
                name: "Alagoas"
            }, {
                value: "AM",
                name: "Amazonas"
            }, {
                value: "AP",
                name: "Amapá"
            }, {
                value: "BA",
                name: "Bahia"
            }, {
                value: "CE",
                name: "Ceará"
            }, {
                value: "DF",
                name: "Distrito Federal"
            }, {
                value: "ES",
                name: "Espírito Santo"
            }, {
                value: "GO",
                name: "Goiás"
            }, {
                value: "MA",
                name: "Maranhão"
            }, {
                value: "MT",
                name: "Mato Grosso"
            }, {
                value: "MS",
                name: "Mato Grosso do Sul"
            }, {
                value: "MG",
                name: "Minas Gerais"
            }, {
                value: "PA",
                name: "Pará"
            }, {
                value: "PB",
                name: "Paraíba"
            }, {
                value: "PR",
                name: "Paraná"
            }, {
                value: "PE",
                name: "Pernambuco"
            }, {
                value: "PI",
                name: "Piauí"
            }, {
                value: "RJ",
                name: "Rio de Janeiro"
            }, {
                value: "RN",
                name: "Rio Grande do Norte"
            }, {
                value: "RO",
                name: "Rondônia"
            }, {
                value: "RS",
                name: "Rio Grande do Sul"
            }, {
                value: "RR",
                name: "Roraima"
            }, {
                value: "SC",
                name: "Santa Catarina"
            }, {
                value: "SE",
                name: "Sergipe"
            }, {
                value: "SP",
                name: "São Paulo"
            }, {
                value: "TO",
                name: "Tocantins"
            }];
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#$get
             * @propertyOf core.page.$pageProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($page) {
             *      console.log($page.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($mdToast, $filter, $mdDialog, $translate) {
                return {
                    config: this.config,
                    load: load(),
                    progress: progress(),
                    toast: toast($mdToast),
                    title: title,
                    description: description,
                    ogLocale: ogLocale,
                    ogSiteName: ogSiteName,
                    ogTitle: ogTitle,
                    ogDescription: ogDescription,
                    ogUrl: ogUrl,
                    ogImage: ogImage,
                    ogSection: ogSection,
                    ogTag: ogTag,
                    protectedSlug: this.protectedSlug,
                    toolbar: this._toolbar,
                    tabs: this._tabs,
                    footer: this._footer,
                    brStates: this.brStates,
                    msgConfirmation: false,
                    msgCreated: false,
                    msgUpdated: false,
                    msgDeleted: false,
                    msgNotDone: false,
                    msgLogin: false,
                    msgCartCreated: false,
                    msgCartDeleted: false,
                    msgFillAllFields: false,
                    msgCepNotFound: false,
                    setLocale: function(locale, tmhDynamicLocale, changeForm) {
                        $translate.use(locale);
                        moment.locale(locale);
                        this.locale = locale.toLowerCase().replace('_', '-');

                        tmhDynamicLocale.set(this.locale);

                        // 
                        // Change form?
                        if (changeForm) this.formLocale = locale;

                        // 
                        // Set system msgs
                        this.setLocaleMsgs();
                    },
                    locale: this.locale,
                    formLocale: this.formLocale,
                    setLocaleMsgs: function() {
                        this.msgConfirmation = $filter('translate')('CONFIRMATION');
                        this.msgCreated = $filter('translate')('MSG_CREATED');
                        this.msgUpdated = $filter('translate')('MSG_UPDATED');
                        this.msgDeleted = $filter('translate')('MSG_DELETED');
                        this.msgNotDone = $filter('translate')('MSG_NOT_DONE');
                        this.msgLogin = $filter('translate')('MSG_LOGIN');
                        this.msgCartCreated = $filter('translate')('MSG_CART_CREATED');
                        this.msgCartDeleted = $filter('translate')('MSG_CART_DELETED');
                        this.msgFillAllFields = $filter('translate')('MSG_FILL_ALL_FIELDS');
                        this.msgCepNotFound = $filter('translate')('CEP_NOT_FOUND');
                    },
                    showAlert: function(ev, title, text) {
                        // Appending dialog to document.body to cover sidenav in docs app
                        // Modal dialogs should fully cover application
                        // to prevent interaction outside of dialog
                        $mdDialog.show(
                            $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title(title)
                            .textContent(text)
                            .ariaLabel(title)
                            .ok('Ok')
                            .targetEvent(ev)
                        );
                    },
                    showAlertConfirm: function(ev, title, text, okTitle, cancelTitle, okCb, cancelCb) {
                        // Appending dialog to document.body to cover sidenav in docs app
                        // Modal dialogs should fully cover application
                        // to prevent interaction outside of dialog
                        var confirm = $mdDialog.confirm()
                            .title(title)
                            .htmlContent(text)
                            .ariaLabel(title)
                            .targetEvent(ev)
                            .ok(okTitle)
                            .cancel(cancelTitle);

                        $mdDialog.show(confirm).then(function() {
                            okCb();
                        }, function() {
                            cancelCb();
                        });

                    }
                }
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#config
             * @methodOf core.page.$pageProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config.set(function($pageProvider) {
             *     $pageProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            function _config(key, val) {
                if (key && (val || val === false)) {
                    return this.config[key] = val
                } else if (key) {
                    return this.config[key]
                } else {
                    return this.config
                }
            }

            /**
             * @ngdoc function
             * @name core.page.$pageProvider#title
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para meta tag título
             * @param {string} str título da página
             * @return {string} título da página
             **/
            function title(value) {
                if (value) return this._title = value;
                else return this._title;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#description
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para meta tag descrição
             * @param {string} value descrição da página
             **/
            function description(value) {
                if (value) return this._description = value;
                else return this._description;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogLocale
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph locale
             * @param {string} value locale
             **/
            function ogLocale(value) {
                if (value) return this._ogLocale = value;
                else return this._ogLocale;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogSiteName
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph site name
             * @param {string} value site name
             **/
            function ogSiteName(value) {
                if (value) return this._ogSiteName = value;
                else return this._ogSiteName;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogTitle
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph title
             * @param {string} value title
             **/
            function ogTitle(value) {
                if (value) return this._ogTitle = value;
                else return this._ogTitle;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogDescription
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph description
             * @param {string} value description
             **/
            function ogDescription(value) {
                if (value) return this._ogDescription = value;
                else return this._ogDescription;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogUrl
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph url
             * @param {string} value url
             **/
            function ogUrl(value) {
                if (value) return this._ogUrl = value;
                else return this._ogUrl;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogImage
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph image
             * @param {string} value image
             **/
            function ogImage(value) {
                if (value) return this._ogImage = value;
                else return this._ogImage;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogSection
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph section
             * @param {string} value section
             **/
            function ogSection(value) {
                if (value) return this._ogSection = value;
                else return this._ogSection;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogTag
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph tag
             * @param {string} value tag
             **/
            function ogTag(value) {
                if (value) return this._ogTag = value;
                else return this._ogTag;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#toolbar
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para toolbar
             * @param {string} value tag
             **/
            function toolbar(value) {
                if (value || value === false) return this._toolbar = value;
                else return this._toolbar;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#tabs
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph tag
             * @param {string} value tag
             **/
            function tabs(value) {
                if (value || value === false) return this._tabs = value;
                else return this._tabs;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#footer
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter active footer
             * @param {string} value tag
             **/
            function footer(value) {
                if (value || value === false) return this._footer = value;
                else return this._footer;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#load
             * @methodOf core.page.$pageProvider
             * @description
             * inicia e termina o carregamento da página
             * @return {object} com metodos de inicialização (init) e finalização (done)
             **/
            function load() {
                return {
                    init: function() {
                        this.status = true;
                        //console.log('loader iniciado...' + this.status);
                    },
                    done: function() {
                        this.status = false;
                        //console.log('loader finalizado...' + this.status);
                    }
                }
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#toast
             * @methodOf core.page.$pageProvider
             * @description
             * mostra uma mensagem de aviso
             * @param {string} msg mensagem
             * @param {integer} time tempo em milisegundos
             * @param {string} position posição do alerta. default: 'bottom right'
             **/
            function toast($mdToast) {
                return function(msg, time, position) {
                    time = time ? time : 5000;
                    $mdToast.show($mdToast.simple().content(msg).position(position ? position : 'bottom right').hideDelay(time));
                }
            }
            //another type of load
            function progress() {
                return {
                    init: function() {
                        this.status = true;
                        //console.log('progress iniciado...' + this.status);
                    },
                    done: function() {
                        this.status = false;
                        //console.log('progress finalizado...' + this.status);
                    }
                }
            }
        }
    )
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductCtrl', /*@ngInject*/ function($List, $product, $state) {
        var vm = this;

        // List
        vm.params = {
            source: $product.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $product.remove,
            addAction: 'app.products-new',
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            showAvatar: true,
            filters: {
                owner: true
            }
        };

        function primaryAction(item) {
            $state.go('app.product-form.edit', item, {
                inherit: false
            });
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').provider('$product',
        /**
         * @ngdoc object
         * @name mtda.banner.$productProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $productProvider() {

            /**
             * @ngdoc object
             * @name mtda.banner.$productProvider#_routePath
             * @propertyOf mtda.product.$productProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc object
             * @name mtda.banner.$productProvider#publicProductsTemplateUrl
             * @propertyOf mtda.product.$productProvider
             * @description
             * Url for public products template
             **/
            this.publicProductsTemplateUrl = 'core/product/public/products.tpl.html'; //'app/layout/pblicProducts.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.banner.$productProvider#publicProductTemplateUrl
             * @propertyOf mtda.product.$productProvider
             * @description
             * Url for public product template
             **/
            this.publicProductTemplateUrl = 'core/product/public/product.tpl.html'; //'app/layout/pblicProduct.tpl.html';

            this.variants = {
                colors: [{
                    value: 'blue',
                    display: 'BLUE',
                    en_GB: 'blue',
                    pt_BR: 'azul'
                }, {
                    value: 'green',
                    display: 'GREEN',
                    en_GB: 'green',
                    pt_BR: 'verde'
                }, {
                    value: 'grey',
                    display: 'GREY',
                    en_GB: 'grey',
                    pt_BR: 'cinza'
                }, {
                    value: 'yellow',
                    display: 'YELLOW',
                    en_GB: 'yellow',
                    pt_BR: 'amarelo'
                }, {
                    value: 'orange',
                    display: 'ORANGE',
                    en_GB: 'orange',
                    pt_BR: 'laranja'
                }, {
                    value: 'purple',
                    display: 'PURPLE',
                    en_GB: 'purple',
                    pt_BR: 'roxo'
                }, {
                    value: 'red',
                    display: 'RED',
                    en_GB: 'red',
                    pt_BR: 'vermelho'
                }, {
                    value: 'white',
                    display: 'WHITE',
                    en_GB: 'white',
                    pt_BR: 'branco'
                }, {
                    value: 'black',
                    display: 'BLACK',
                    en_GB: 'black',
                    pt_BR: 'preto'
                }, {
                    value: 'gold',
                    display: 'GOLD',
                    en_GB: 'gold',
                    pt_BR: 'dourado'
                }, {
                    value: 'silver',
                    display: 'SILVER',
                    en_GB: 'silver',
                    pt_BR: 'prata'
                }],
                sizes: '0, 1, 3, 5, 7, 9, 11, 13, 15, 17, xxs, xs, s, m, l, xl, xxl, xxxl, 44, 46, unique'.split(/, +/g).map(function(state) {
                    return {
                        value: state.toLowerCase(),
                        display: state
                    };
                })
            };

            this.froalaOptions = {
                toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', '-', 'insertLink', 'insertVideo', 'undo', 'redo']
            };

            this.cropperOptions = {
                aspectRatio: 11.2 / 15,
                resultImage: {
                    w: 352,
                    h: 384
                },
                viewMode: 1
            }

            //
            // Factory
            // 
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, $filter) {
                var endpoint = api.url + 'api/products/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();


                return {
                    getAll: getAll,
                    getList: getList,
                    get: get,
                    getPublic: getPublic,
                    remove: remove,
                    querySearch: querySearch,
                    slugValidation: slugValidation,
                    busy: busy,
                    variants: this.variants,
                    froalaOptions: this.froalaOptions,
                    cropperOptions: this.cropperOptions,
                    routePath: this.routePath,
                    transformChip: transformChip,
                    sort: [{
                        name: $filter('translate')('SORT_NAME_ASC'),
                        value: 'name'
                    }, {
                        name: $filter('translate')('SORT_NAME_DESC'),
                        value: '-name'
                    }, {
                        name: $filter('translate')('SORT_PRICE_ASC'),
                        value: 'price'
                    }, {
                        name: $filter('translate')('SORT_PRICE_DESC'),
                        value: '-price'
                    }]
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('productGetAll')) return NegativePromise.defer();

                    var getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    };

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('productGetAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productGetAll');
                    }
                }

                function getList(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    var getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    };

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint + 'list', {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('productGet')) return NegativePromise.defer();

                    var getParams = {
                        user: false
                    };

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('productGet');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productGet');
                    }
                }

                function getPublic(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('getPublic')) return NegativePromise.defer();

                    var getParams = {
                        user: false
                    };

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + 'public/' + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getPublic');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getPublic');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (this.busy.start('productDelete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('productDelete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        this.busy.end('productDelete');
                    }
                }

                function querySearch(query) {
                    if (!query) return false;

                    var deferred = $q.defer();

                    this.getAll({
                        filters: {
                            term: query
                        }
                    }).then(function(response) {
                        deferred.resolve(response.entries);
                    });

                    return deferred.promise;
                }

                function slugValidation(viewValue, id) {
                    return $http.post(endpoint + 'slugValidation', {
                        slug: viewValue,
                        id: id
                    }).then(
                        function(response) {
                            if (!response.data.valid) {
                                return $q.reject(response.data.errorMessage);
                            }

                            return true;
                        }
                    );
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$productProvider#logo
             * @methodOf mtda.banner.$productProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($productProvider) {
             *     $productProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }

            function transformChip(chip) {
                // If it is an object, it's already a known chip
                if (angular.isObject(chip)) {
                    return chip;
                }
                // Otherwise, create a new one
                return {
                    name: chip,
                    type: 'new'
                }
            }
        });
})();
(function() {
    'use strict';
    angular.module('mtda.product').config( /*@ngInject*/ function($stateProvider, $userProvider, $productProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.products', {
            url: $productProvider.routePath() + 'products/?term',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            views: {
                'content': {
                    templateUrl: 'core/product/products.tpl.html',
                    controller: '$ProductCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.products-new', {
            url: $productProvider.routePath() + 'products/novo',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            views: {
                'content': {
                    templateUrl: 'core/product/productNew.tpl.html',
                    controller: '$ProductNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form', {
            abstract: true,
            url: '',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            views: {
                'content': {
                    templateUrl: 'core/product/productFormPage.tpl.html',
                    controller: '$ProductFormPageCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.edit', {
            url: $productProvider.routePath() + 'products/editar/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'basic',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/productForm.tpl.html',
                    controller: '$ProductEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.variant', {
            url: $productProvider.routePath() + 'products/variant/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'variation',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/variant/variants.tpl.html',
                    controller: '$VariantsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.variant-new', {
            url: $productProvider.routePath() + 'products/variant/:_id/new',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'variation',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/variant/variantForm.tpl.html',
                    controller: '$VariantNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.variant-edit', {
            url: $productProvider.routePath() + 'products/variant/:_id/edit/:item',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'variation',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/variant/variantForm.tpl.html',
                    controller: '$VariantEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.productSearch', {
            url: '/products/?category?child?sort?term?page',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $productProvider.publicProductsTemplateUrl;
                    },
                    controller: '$PublicProductsCtrl as vm'
                }
            }
        }).state('app.product', {
            url: '/product/:slug/:id?category?child',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $productProvider.publicProductTemplateUrl;
                    },
                    controller: '$PublicProductCtrl as vm'
                }
            }
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').service('$Product', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug, $mdDialog) {

        var url = api.url + 'api/products/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = {
                image64: false
            };

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;
        this.service.prototype.slugifyUrl = slugifyUrl;
        this.service.prototype.chooseFile = chooseFile;

        function create() {
            // Are we busy?
            if (this.busy.start('productCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this));
        }

        function update() {
            // Are we busy?
            if (this.busy.start('productCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(url + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid || !this.model.categories.length);
        }

        function chooseFile(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'core/filepicker/dialogFilepicker.tpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function(result) {
                console.log('confirm', result);
                this.model.image64 = result[0].cropped;
            }.bind(this), function() {
                console.log('cancel');
            }.bind(this));
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }

        function slugifyUrl(value) {
            this.model.slug = Slug.slugify(value);
        }

        /*@ngInject*/
        function DialogController($scope, $product) {
            $scope.qty = 1;
            $scope.cropperOptions = $product.cropperOptions;
            $scope.result = false;

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.confirm = function() {
                $mdDialog.hide($scope.result);
            };
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductEditCtrl', /*@ngInject*/ function($state, $stateParams, $Product, $product, $category, lodash) {
        var vm = this;

        //
        // Set edit
        vm.edit = true;

        //
        // Reset iamge
        vm.resetImage = resetImage;

        // 
        // Product factory
        vm.factory = $product;

        //
        // Get and instantiate product
        vm.factory.get($state.params._id).then(function(data) {
            if (!data) $state.go('app.products');

            data.variants.attrs = data.variants.attrs ? data.variants.attrs : [];
            vm.variants = [];

            lodash.forEach(data.variants.attrs, function(val){
                vm.variants.push(val);
            });

            var hasColors = lodash.find(data.variants.attrs, ['name', 'colors']);
            var hasSizes = lodash.find(data.variants.attrs, ['name', 'sizes']);

            if (!hasColors) vm.variants.push({
                name: 'colors',
                status: false
            });

            if (!hasSizes) vm.variants.push({
                name: 'sizes',
                status: false
            });

            vm.product = new $Product.service({
                model: data
            });

        }).catch(function() {
            //
            // Send to list, we need product
            $state.go('app.products');
        });

        //
        // Submit and save
        vm.submit = submit;

        //
        // Set Autocomplete
        vm.querySearch = vm.factory.querySearch.bind($category);

        function resetImage() {
            vm.product.model.image = null;
        }

        function submit() {

            // Send contact to server with recaptcha
            vm.product.update().then(success, fail);

            function success() {}

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductFormPageCtrl', /*@ngInject*/ function($state) {
        var vm = this;
        vm.edit = $state.current.name === 'app.product-form.new' ? false : true;
        vm.params = $state.params;
        vm.productId = $state.params._id ? $state.params._id : undefined;

        vm.menu = [{
            label: 'BASIC_DETAILS',
            state: 'app.product-form.edit({_id: \''+vm.productId+'\'})',
            innerTabGroup: 'basic'
        }, {
            label: 'SPANISH',
            state: 'app.product-form.edit({_id: \''+vm.productId+'\'})',
            innerTabGroup: 'spanish'
        }, {
            label: 'PROD_FORM_VARIATION_STOCK',
            state: 'app.product-form.variant({_id: \''+vm.productId+'\'})',
            innerTabGroup: 'variation'
        }];
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductNewCtrl', /*@ngInject*/ function($state, $stateParams, $Product, $product, $category, $translate) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $product;

        //
        // Instantiate product
        vm.product = new $Product.service({
            model: {
                categories: [],
                variants: {
                    attrs: []
                },
                picFile: null
            }
        });

        vm.variants = [{
            name: 'colors',
            status: false
        }, {
            name: 'sizes',
            status: false
        }];
        
        //
        // Submit and save
        vm.submit = submit;

        //
        // Autocomplete
        vm.querySearch = vm.factory.querySearch.bind($category);

        function submit() {
            // Send contact to server with recaptcha
            vm.product.create().then(success, fail);

            function success() {
                // Clean model
                vm.product.model = null;

                // Redirect listing
                $state.go('app.products');
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').directive('productSlugValidator', /*@ngInject*/ function($product) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.slug = function(modelValue, viewValue) {
                    return $product.slugValidation(viewValue, scope.vm.service.model.id);
                };
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.product').provider('$variant',

        /*@ngInject*/
        function $variantProvider() {

            this.cropperOptions = {
                aspectRatio: '11 / 12',
                resultImage: {
                    w: 352,
                    h: 384
                }
            }

            //
            // Factory
            // 
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/variants/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    };

                return {
                    getAll: getAll,
                    get: get,
                    getOptions: getOptions,
                    remove: remove,
                    busy: new Busy.service(),
                    cropperOptions: this.cropperOptions
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível selecionar as variações');
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('get')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível selecionar o produto');
                        this.busy.end('get');
                    }
                }

                function getOptions(prod, item, queryParams) {
                    // Are we busy?
                    if (this.busy.start('getOptions')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + 'options/' + prod + '/' + item, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getOptions');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível selecionar as variações do produto');
                        this.busy.end('getOptions');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (this.busy.start('delete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry.id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('delete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível deletar a variação');
                        this.busy.end('delete');
                    }
                }
            }
        });
})();
(function() {
    'use strict';
    angular.module('mtda.services').service('Busy', /*@ngInject*/ function($page) {

        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this._status = {};

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

            return this;
        }

        this.service.prototype.status = status;
        this.service.prototype.start = start;
        this.service.prototype.end = end;

        function status(method, value) {
            if (value || value === false) return this._status[method] = value;
            else return this._status[method];
        }

        function start(method, dontChangePage) {
            //
            // Are we busy? return true so consuming code can stop
            // 
            if (this.status(method)) return true;

            //
            // We're not busy, make it
            // 
            this.status(method, true);

            //
            // Enable page load?
            // 
            if (!dontChangePage) $page.load.init();

            //
            // Return false so consuming code can continue
            // 
            return false;
        }

        function end(method, dontChangePage) {
            // Disable status
            this.status(method, false);

            // Disable page loading?
            if (!dontChangePage) $page.load.done();
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.services').factory('HttpInterceptor', /*@ngInject*/ function($q, $user, $localStorage, $location) {
        return {
            // optional method
            'request': function(config) {
                // do something on success
                return config;
            },
            // optional method
            'requestError': function(rejection) {
                // do something on error
                //if (canRecover(rejection)) {
                //return responseOrNewPromise
                //}
                return $q.reject(rejection);
            },
            // optional method
            'response': function(response) {
                // do something on success
                return response;
            },
            // optional method
            'responseError': function(rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    // 
                    // Gravar url para o caso de login
                    $localStorage.redirectOnLogin = $location.url();

                    // 
                    // Try to logout user
                    $user.logout();
                }
                // do something on error
                //if (canRecover(rejection)) {
                //return responseOrNewPromise
                //}
                return $q.reject(rejection);
            }
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.services').service('NegativePromise', /*@ngInject*/ function($q) {

        this.defer = defer;

        function defer(response) {

            if(!response) response = false;

            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(response);

            // Return promise
            return defer.promise;
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.services').service('NgCropper', /*@ngInject*/ function($timeout) {
        
    });
})();
(function() {
    'use strict';
    angular.module('mtda.user').directive('perm', /*@ngInject*/ function($log, Authorization, PermissionMap) {
        return {
            restrict: 'A',
            link: function($scope, $element, $attributes) {
                var only = $scope.$eval($attributes.only),
                    except = $scope.$eval($attributes.except);

                $scope.$watchGroup(['only', 'except'], function() {
                    try {
                        Authorization
                            .authorize(new PermissionMap({
                                only: only,
                                except: except
                            }), null)
                            .then(function() {
                                $scope.item.visible = true;
                                $element.removeClass('ng-hide');
                            })
                            .catch(function() {
                                $scope.item.visible = false;
                                $element.addClass('ng-hide');
                            });
                    } catch (e) {
                        $scope.item.visible = false;
                        $element.addClass('ng-hide');
                        $log.error(e.message);
                    }
                });
            }
        };
    })
})();
(function() {
    'use strict';

    angular.module('mtda.user').service('$RestoreUser',

        /*@ngInject*/
        function($auth, $q, $user, $User, $localStorage, lodash) {
            this.restore = function(data) {
                // Create promise
                var defer = $q.defer();

                // We must have authentication
                if (!$auth.isAuthenticated()) defer.reject(false)
                    // Check for data argument
                else if (data) setUser(data)
                    // We found one, who is this? get information
                else restoreFromStorage();

                // Return promise
                return defer.promise;

                function restoreFromStorage() {
                    setUser($localStorage.user, true);
                }
            }

            function setUser(data, restore) {
                // 
                // Set current account id if is not provided
                if (!lodash.hasIn(data.owner)) {
                    // 
                    // Define owner according to role
                    data.owner = (lodash.some(data.role, lodash.partial(lodash.eq, 'admin'))) ? false : data.account.owner;
                }

                // 
                // New User service instance in user factory _current method
                $user.set(new $User.service({
                    model: data
                }));
                
                if (restore) return;

                // 
                // Double check if we have user stored, if so, delete
                if (lodash.hasIn($localStorage, 'user'))
                    delete $localStorage.user;

                // 
                // Add to storage for reload
                $localStorage.user = data;
            }
        }
    );
})();
(function() {
    'use strict';
    angular.module('mtda.user').provider('$user',
        /**
         * @ngdoc object
         * @name mtda.user.$userProvider
         **/
        /*@ngInject*/
        function $userProvider() {
            var path = 'core/user/';

            this._current = null;

            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_config
             * @propertyOf mtda.user.$userProvider
             * @description
             * armazena configurações
             **/
            this._config = {};
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_loginTemplateUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url do template para a rota
             **/
            this._loginTemplateUrl = path + 'login/login.tpl.html';
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_registerTemplateUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url do template para novos cadastros
             **/
            this._registerTemplateUrl = path + 'register/register.tpl.html';
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_recoveryTemplateUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url do template para recuperação de senha
             **/
            this._recoveryTemplateUrl = path + 'recovery/recovery.tpl.html';
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_skipIfLoggedInUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url para método skipIfLoggedIn
             **/
            this._skipIfLoggedInUrl = '/admin/';

            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_roles
             * @propertyOf mtda.user.$userProvider
             * @description
             * user roles
             **/
            this._roles = ['admin', 'owner', 'user', 'operator'];
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#$get
             * @propertyOf mtda.user.$userProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($login) {
             *      console.log($login.templateUrl);
             *      //prints the current templateUrl of `mtda.login`
             *      //ex.: "mtda/login/login.tpl.html"
             *      console.log($login.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = /*@ngInject*/ function(lodash) {
                return {
                    config: this._config,
                    loginTemplateUrl: this._loginTemplateUrl,
                    registerTemplateUrl: this._registerTemplateUrl,
                    recoveryTemplateUrl: this._recoveryTemplateUrl,
                    skipIfLoggedInUrl: this._skipIfLoggedInUrl,
                    roles: this._roles,
                    getCurrent: function() {
                        // 
                        // Try to get current user
                        var user = this.current() ? this.current() : false;

                        return user;
                    },
                    current: function() {
                        return this._current;
                    },
                    set: function(data) {
                        this._current = data;
                        return data;
                    },
                    logout: function() {
                        // Logout
                        this._current.logout();

                        // Clean login data
                        this._current = null;
                    }
                }
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#config
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *     $userProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             *     $userProvider.config('registerWelcome','Olá @firstName, você entrou para a @appName');
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#templateUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.templateUrl('core/login/my-login.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.loginTemplateUrl = function(val) {
                if (val) return this._loginTemplateUrl = val;
                else return this._loginTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#recoveryTemplateUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para url do template para recuperação de senha
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.recoveryTemplateUrl('core/login/my-login-recovery.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.recoveryTemplateUrl = function(val) {
                if (val) return this._recoveryTemplateUrl = val;
                else return this._recoveryTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#registerTemplateUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para url do template de novos cadastros
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.registerTemplateUrl('core/login/my-register.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.registerTemplateUrl = function(val) {
                if (val) return this._registerTemplateUrl = val;
                else return this._registerTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#skipIfLoggedInUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter url for skipIfLoggedIn
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.skipIfLoggedInUrl('admin/')
             * })
             * </pre>
             * @param {string} val url to redirect user if is logged in
             **/
            this.skipIfLoggedInUrl = function(val) {
                if (val) return this._skipIfLoggedInUrl = val;
                else return this._skipIfLoggedInUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#roles
             * @methodOf mtda.user.$userProvider
             * @description
             * setter user roles
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.roles(['admin'])
             * })
             * </pre>
             * @param {array} user roles
             **/
            this.roles = function(val) {
                if (val) return this._roles = val;
                else return this._roles;
            }

            this.skipIfLoggedIn = /*@ngInject*/ function($q, $location, $auth, $user) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    $location.path($user.skipIfLoggedInUrl);
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            }

            this.skipIfLoggedInCheckout = /*@ngInject*/ function($q, $location, $auth, $user) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    $location.path('/checkout/payment-details/');
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            }

            this.loginRequired = /*@ngInject*/ function($q, $location, $auth, $localStorage) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {
                    // 
                    // Gravar url para o caso de login
                    $localStorage.redirectOnLogin = $location.url();

                    $location.path('/entrar/');
                }
                return deferred.promise;
            }

            this.loginCheckoutRequired = /*@ngInject*/ function($q, $location, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {
                    $location.path('/checkout/');
                }
                return deferred.promise;
            }

            this.editorRequired = /*@ngInject*/ function($q, $location, $user) {
                var deferred = $q.defer(),
                    user = $user.getCurrent();

                if (user && (user.model.roles.isEditor || user.model.roles.isOwner || user.model.roles.isAdmin)) {
                    deferred.resolve();
                } else {
                    $location.path('/');
                }
                return deferred.promise;
            }

            this.ownerRequired = /*@ngInject*/ function($q, $location, $user) {
                var deferred = $q.defer(),
                    user = $user.getCurrent();

                if (user && (user.model.roles.isOwner || user.model.roles.isAdmin)) {
                    deferred.resolve();
                } else {
                    $location.path('/');
                }
                return deferred.promise;
            }

            this.adminRequired = /*@ngInject*/ function($q, $location, $user) {
                var deferred = $q.defer(),
                    user = $user.getCurrent();

                if (user && user.model.roles.isAdmin) {
                    deferred.resolve();
                } else {
                    $location.path('/');
                }
                return deferred.promise;
            }
        });
})();
(function() {
    'use strict';
    angular.module('mtda.user').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $pageProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.login', {
            url: '/entrar/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.loginTemplateUrl()
                    },
                    controller: '$LoginCtrl as vm'
                }
            },
            resolve: {
                skipIfLoggedIn: $userProvider.skipIfLoggedIn
            }
        }).state('app.logout', {
            url: '/sair/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    controller: '$LogoutCtrl as vm'
                }
            }
        }).state('app.register', {
            url: '/registrar/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.registerTemplateUrl()
                    },
                    controller: '$RegisterCtrl as vm'
                },
                resolve: {
                    skipIfLoggedIn: $userProvider.skipIfLoggedIn
                }
            }
        }).state('app.recovery', {
            url: '/recuperar/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.recoveryTemplateUrl()
                    },
                    controller: '$RecoveryCtrl as vm'
                }
            },
            resolve: {
                authed: /*@ngInject*/ function($auth, $window, $user) {
                    if ($auth.isAuthenticated()) {
                        $window.location = $user.config.auth.loginSuccessRedirect //here we use $window to fix issue related with $location.hash (#) in url
                    }
                }
            }
        }).state('app.profile', {
            url: '/profile/',
            views: {
                'content': {
                    templateUrl: 'core/user/profile.tpl.html',
                    controller: '$ControllerCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        })

        // 
        // Users
        .state('app.users', {
            url: $pageProvider.protectedSlug + 'users/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'users',
            views: {
                'content': {
                    templateUrl: 'core/user/users/users.tpl.html',
                    controller: '$UsersCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.new-user', {
            url: $pageProvider.protectedSlug + 'new-user/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'users',
            views: {
                'content': {
                    templateUrl: 'core/user/users/userForm.tpl.html',
                    controller: '$UserNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.edit-user', {
            url: $pageProvider.protectedSlug + 'edit-user/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'users',
            views: {
                'content': {
                    templateUrl: 'core/user/users/userForm.tpl.html',
                    controller: '$UserEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        });
    })
})();
(function() {
    'use strict';

    angular.module('mtda.user').run(runBlock);

    /** @ngInject */
    function runBlock($log, PermissionStore, RoleStore, $user, $User, $RestoreUser, $auth, lodash) {
        // 
        // Restore user from local storage
        $RestoreUser.restore();

        // 
        // Permissions
        PermissionStore.definePermission('user', function(stateParams) {
            // 
            // Do we have current
            return !!$auth.isAuthenticated();
        });
        PermissionStore.definePermission('admin', function(stateParams) {
            // 
            // We need current model
            if (!lodash.hasIn($user, '_current') || !lodash.hasIn($user._current, 'model') || !lodash.hasIn($user._current.model, 'role')) return false;

            // 
            // Check for admin in user role array
            return lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'admin'));
        });
    }
})();
(function() {
    'use strict';
    angular.module('mtda.user').service('$User', /*@ngInject*/ function($auth, $http, $page, $state, $q, api, Busy, $location, $localStorage, lodash) {

        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.login = login;
        this.service.prototype.logout = logout;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;
        this.service.prototype.setRoles = setRoles;
        this.service.prototype.password = password;

        function create(provider, noLogin, redirect) {
            if (!provider) provider = 'local';

            // Are we busy?
            if (this.busy.start('save')) return this.negativePromise();

            // Default params
            var params = {
                model: {
                    provider: provider
                }
            };

            // Extend with model
            angular.extend(params.model, this.model);

            // Create user
            return $auth.signup(params).success(onSuccess.bind(this)).error(onError.bind(this));

            function onSuccess(response) {
                // 
                // Set roles
                response.user = this.setRoles(response.user);

                // 
                // Log in user after registration?
                if (!noLogin) {
                    // 
                    // Set $auth token
                    $auth.setToken(response.token);
                }

                // 
                // Confirmation msg
                $page.toast($page.msgCreated);

                // 
                // Unset busy
                this.busy.end('save');

                return response;
            }

            function onError(response) {
                $page.toast(response && response.error ? response.error : $page.msgNotDone);
                this.busy.end('save');

                return response;
            }
        }

        function update() {
            // Are we busy?
            if (this.busy.start('save')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(api.url + 'api/users/' + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('save');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('save');

                        return response;
                    }.bind(this));
        }

        function password() {
            // Are we busy?
            if (this.busy.start('password')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(api.url + 'api/users/' + model.id + '/password', {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast('Senha alterada com sucesso!');
                        this.busy.end('password');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('password');

                        return response;
                    }.bind(this));
        }

        function login(redirect) {

            // Are we busy?
            if (this.busy.start('login')) return this.negativePromise();

            return $auth.login({
                email: this.model.email,
                password: this.model.password
            }).then(onSuccess.bind(this), onError.bind(this));

            function onSuccess(response) {
                // 
                // Set roles
                response.data.user = this.setRoles(response.data.user);

                // 
                // Redirect
                if (!redirect) {
                    // 
                    // Any previus location registered?
                    if ($localStorage.redirectOnLogin) {
                        // 
                        // Redirect user to previus location
                        $location.path($localStorage.redirectOnLogin);

                        // 
                        // Delete previus location
                        delete $localStorage.redirectOnLogin;

                        // 
                        // Redirect for custmer
                    } else if (response.data.user.roles.isUser) $location.path('/customer/');

                    // 
                    // Redirect for admin
                    else if (response.data.user.roles.isAdmin) $location.path('/accounts/');

                    // 
                    // Redirect for owners
                    else if (response.data.user.roles.isOwner || response.data.user.roles.isEditor) $location.path('/admin/');

                    // 
                    // Redirect for any other situation
                    else $location.path('/customer/');
                } else {
                    // 
                    // Custom redirect from directive
                    $location.path(redirect);
                }

                // 
                // Confirmation msg
                $page.toast($page.msgLogin);

                // 
                // Unset busy
                this.busy.end('login');

                return response;
            }

            function onError(response) {
                $page.toast('Dados incorretos!');

                // 
                // Unset busy
                this.busy.end('login');

                return response;
            }
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }

        function logout() {
            // 
            // Remove user in local storage
            delete $localStorage.user;

            // 
            // Clear self model
            this.model = null;

            // 
            // Logout from $auth
            $auth.logout();

            // 
            // Redirect to login
            $state.go('app.login');
        }

        function setRoles(user) {
            var roles = {};

            // 
            // Is admin?
            roles.isAdmin = user.role.indexOf('admin') > -1 ? true : false;

            // 
            // Is owner?
            roles.isOwner = user.role.indexOf('owner') > -1 ? true : false;

            // 
            // Is editor?
            roles.isEditor = user.role.indexOf('editor') > -1 ? true : false;

            // 
            // Is operator?
            roles.isOperator = user.role.indexOf('operator') > -1 ? true : false;

            // 
            // Is user?
            roles.isUser = user.role.indexOf('user') > -1 ? true : false;

            // 
            // Set user
            user.roles = roles;

            return user;
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.user').provider('$userHttp',
        /**
         * @ngdoc object
         * @name mtda.user.$userHttpProvider
         **/
        /*@ngInject*/
        function $userHttpProvider() {
            /**
             * @ngdoc function
             * @name mtda.user.$userHttpProvider#$get
             * @propertyOf mtda.user.$userHttpProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($login) {
             *      console.log($login.templateUrl);
             *      //prints the current templateUrl of `mtda.login`
             *      //ex.: "mtda/login/login.tpl.html"
             *      console.log($login.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = /*@ngInject*/ function(lodash, api, Busy, $http, NegativePromise, $page) {
                var endpoint = api.url + 'api/users/',
                    busy = new Busy.service();

                return {
                    busy: busy,
                    // 
                    // Index
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    recoveryLink: recoveryLink,
                    recoveryPassword: recoveryPassword,
                    confirmHash: confirmHash
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    var getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    };

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('get')) return NegativePromise.defer();

                    var getParams = {
                        user: false
                    };

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('get');
                    }
                }

                function remove(e, ids) {
                    // Are we busy?
                    if (this.busy.start('delete')) return NegativePromise.defer();

                    return $http.post(endpoint + 'destroy', {
                        ids: ids
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('delete');
                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('delete');
                    }
                }

                /**
                 * recoveryLink
                 */
                function recoveryLink(email) {

                    // 
                    // Are we busy?
                    if (this.busy.start('recovery')) return NegativePromise.defer();

                    return $http.post(endpoint + "lost", {
                        email: email
                    }).success(onSuccess.bind(this)).error(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('recovery');
                        $page.toast(response.success);

                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('recovery');
                    }
                }

                /**
                 * recoveryPassword
                 */
                function recoveryPassword(hash, password) {

                    // 
                    // Are we busy?
                    if (this.busy.start('recoveryPassword')) return NegativePromise.defer();

                    return $http.put(endpoint + "lost/" + hash, {
                        password: password
                    }).success(onSuccess.bind(this)).error(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('recoveryPassword');
                        $page.toast(response.success);

                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('recoveryPassword');
                    }
                }

                /**
                 * confirmHash
                 */
                function confirmHash(hash) {

                    // 
                    // Are we busy?
                    if (this.busy.start('confirmHash')) return NegativePromise.defer();

                    return $http.get(endpoint + "hash/" + hash).success(onSuccess.bind(this)).error(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('confirmHash');
                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('confirmHash');
                    }
                }
            }
        });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash, $timeout, $localStorage, $cart, $state, $page, $window) {
        var vm = this;

        // 
        // Timeout vars
        var timeoutFocus = false;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        // 
        // Set user
        vm.user = $user.getCurrent();

        // 
        // Side menu with steps
        vm.menu = vm.factory.steps;

        // 
        // setCurrentStep
        setCurrentStep();

        // 
        // Restore or create model from local storage
        bootstrap();

        // 
        // Actions on state change
        $scope.$on('$stateChangeStart', stateChangeStart);
        $scope.$on('$stateChangeSuccess', stateChangeSuccess);

        // 
        // Clear checkout
        vm.clear = clear;

        //
        // Indicate that checkout is done!
        vm.completed = false;
        vm.isCompleted = isCompleted;

        // 
        // Indicate that user will be redirected to paypal
        vm.paypalRedirect = false;

        // 
        // Set form
        vm.form = {
            paymentDetails: {},
            shippingDetails: {},
            shippingMethod: {},
            payment: {}
        };

        // 
        // Check to see if form is available
        vm.hasForm = hasForm;

        // 
        // Callback for login success
        vm.cbLogin = cbLogin;

        // 
        // Callback for registration success
        vm.cbRegister = cbRegister;

        // 
        // Fix for the form validation
        // Sometimes when transitioning quickly trought routes, form becomes empty
        $scope.$watch('vm.form.paymentDetails', function(nv, ov) {
            if ($state.current.innerTabGroup === 'payment-details' && !nv && ov)
                vm.form.paymentDetails = ov;
        }, true);

        $scope.$watch('vm.form.shippingDetails', function(nv, ov) {
            if ($state.current.innerTabGroup === 'shipping-details' && !nv && ov)
                vm.form.shippingDetails = ov;
        }, true);

        $scope.$watch('vm.form.shippingMethod', function(nv, ov) {
            if ($state.current.innerTabGroup === 'shipping-method' && !nv && ov)
                vm.form.shippingMethod = ov;
        }, true);

        $scope.$watch('vm.form.payment', function(nv, ov) {
            if ($state.current.innerTabGroup === 'payment' && !nv && ov)
                vm.form.payment = ov;
        }, true);

        // 
        // Option to use payment details for shipping
        $scope.$watch('vm.service.model.shipping.method', function(nv, ov) {
            if (nv && nv !== ov) {
                // 
                // Get price for this 
                var price = $checkout.shippingMethods[nv].price;

                // 
                // Set shipping price
                vm.service.model.shipping.price = price;

                // *shipping price + total must be calculated in the server
            }
        }, true);

        // 
        // Update gateway according to payment.mode
        $scope.$watch('vm.service.model.payment.mode', function(nv, ov) {
            if (nv && nv !== ov) {
                // 
                // Set gateway
                var gateway = vm.service.model.payment.mode === 'paypal_balance' || vm.service.model.payment.mode === 'paypal_cc' ? 'paypal' : 'moip';

                // 
                // Set gateway
                vm.service.model.payment.gateway = gateway;

                // 
                // Clear old value if necessary
                if (ov === 'cc' || ov === 'debito-bancario' || ov === 'boleto') vm.service.model.moip = {};
                else vm.service.model.paypal = {};

                // 
                // Set some user details we already have on mode 'cc'
                if (nv === 'cc') {
                    // 
                    // Set card
                    if (!lodash.hasIn(vm.service.model, 'moip.card')) vm.service.model.moip.card = {};

                    // 
                    // Set name
                    vm.service.model.moip.card.name = vm.user.model.profile.firstName + ' ' + vm.user.model.profile.lastName;

                    // 
                    // Set contact phone
                    if (lodash.hasIn(vm.user.model, 'profile.contact.mobile') || lodash.hasIn(vm.user.model, 'profile.contact.phone'))
                        vm.service.model.moip.card.phone = vm.user.model.profile.contact.mobile || vm.user.model.profile.contact.phone;
                }

                // 
                // Try to focus first field, when we have additional form
                if (nv === 'cc' || nv === 'paypal_cc') {
                    focusFirstInput(vm.step.innerTabGroup, '.' + nv);
                }
            }
        }, true);

        // 
        // Toggle shipping address when check/uncheck vm.service.model.payment.same_for_shipping field
        $scope.$watch('vm.service.model.payment.same_for_shipping', function(nv, ov) {
            if (nv !== ov) {
                if (nv) {
                    // 
                    // Checked
                    // Copy payment.payer to shipping.to
                    angular.extend(vm.service.model.shipping.to, vm.service.model.payment.payer);

                    // 
                    // Copy payment.address to shipping.address
                    angular.extend(vm.service.model.shipping.address, vm.service.model.payment.address);
                } else {
                    // 
                    // Unchecked
                    // clear data o shipping.to
                    vm.service.model.shipping.to = {
                        firstName: '',
                        lastName: ''
                    };

                    // 
                    // clear data o shipping.address
                    vm.service.model.shipping.address = {
                        cep: '',
                        street: '',
                        num: '',
                        bairro: '',
                        city: '',
                        state: ''
                    };
                }
            }
        }, true);

        // 
        // Auto-fill card type
        $scope.$watch('vm.service.model.moip.card.number', function(nv, ov) {
            if (nv && nv !== ov) {

                // 
                // Get card
                var card = moip.creditCard.cardType(vm.service.model.moip.card.number);

                // 
                // Validate card
                if (!card || !card.brand) return;

                var cardCheck = {
                    'MASTERCARD': 'Mastercard',
                    'VISA': 'Visa',
                    'AMEX': 'AmericanExpress',
                    'DINERS': 'Diners',
                    'HIPERCARD': 'Hipercard',
                    'ELO': 'Elo',
                    'HIPER': 'Hiper'
                };

                // 
                // Set card type
                var type = cardCheck[card.brand] ? cardCheck[card.brand] : false;

                // 
                // Set form
                if (type) vm.service.model.moip.card.type = type;


            }
        }, true);

        // 
        // Auto-fill card type
        $scope.$watch('vm.service.model.paypal.card.number', function(nv, ov) {
            if (nv && nv !== ov) {

                // 
                // Get card
                var card = moip.creditCard.cardType(vm.service.model.paypal.card.number);

                // 
                // Validate card
                if (!card || !card.brand) return;

                var cardCheck = {
                    'MASTERCARD': 'Mastercard',
                    'VISA': 'Visa',
                    'AMEX': 'Amex'
                };

                // 
                // Set card type
                var type = cardCheck[card.brand] ? cardCheck[card.brand] : false;

                // 
                // Set form
                if (type) vm.service.model.paypal.card.type = type;


            }
        }, true);

        //
        // Navigation btn
        vm.prev = prev;
        vm.next = next;

        // 
        // Bootstrap controller
        // it will set a clear service or restore a model from localstorage
        // it will check completed steps in menu
        function bootstrap(clear) {
            // 
            // Set service
            setService(clear);

            // 
            // Execute payment in case of paypal confirmation
            if (vm.step.innerTabGroup === 'confirmation') {
                // 
                // We must have a payment
                if (!vm.service.model.payment || !vm.service.model.payment.mode) {
                    // 
                    // We are in confirmation but we don´t gave a payment
                    // Usually this is because the page was reloaded in confirmation page, after a payment was already done
                    // Take user to the beginning
                    $state.go('app.checkout.identification', {}, {
                        inherit: false
                    });
                } else {
                    // 
                    // In paypal balance, we need to execute the payment to actually pay
                    if (vm.service.model.payment.mode === 'paypal_balance') {
                        // 
                        // Add payment information from url to the model
                        vm.service.model.paypal.token = $state.params.token;
                        vm.service.model.paypal.payer = $state.params.PayerID;

                        // 
                        // Execute payment
                        vm.service.execute().then(executeSuccess.bind(this), executeFail.bind(this));
                    }
                }
            }

            // 
            // Send user to the right step
            validateSteps();
        }

        // 
        // Set a new service
        function setService(clear) {

            // 
            // Checkout entry sample 
            var entry = {
                model: {
                    user: vm.user ? vm.user.model : false,
                    payment: {
                        payer: {},
                        address: {}
                    },
                    shipping: {
                        to: {},
                        address: {}
                    }
                },
                stepStatus: {
                    'identification': false,
                    'payment-details': false,
                    'shipping-details': false,
                    'shipping-method': false,
                    'payment': false,
                    'confirmation': false
                }
            };
            console.log('entry', entry);
            // 
            // Extend user data
            extendModel(entry);

            // 
            // Localstorage data
            vm.checkout = $localStorage.checkout = clear ? entry : ((lodash.hasIn($localStorage, 'checkout')) ? $localStorage.checkout : entry);

            // 
            // Set cart in model
            vm.checkout.model.total = $cart.subtotal;
            vm.checkout.model.cart = $cart.entries;

            // 
            // Restore or create model from local storage
            var model = $localStorage.checkout.model = vm.checkout.model;

            // 
            // Instantiate a new checkout
            vm.service = new $Checkout.service({
                steps: vm.menu,
                model: model
            });
        }

        // 
        // Clear form
        // It will trigger bootstrap to clear the current service
        function clear() {
            // 
            // Set a new and clear service
            bootstrap(true);

            // 
            // Validate steps
            validateSteps();

            // 
            // Send user back to the first step
            // $state.go('app.checkout.identification');
        }

        // 
        // Return the current step 
        // find entries on vm.menu with the same innerTabGroup as the route
        function currentStep() {
            // 
            // Get current state
            var obj = lodash.find(vm.menu, ['innerTabGroup', $state.current.innerTabGroup]);

            if (!obj) return false;

            //
            // Set key
            obj.index = lodash.findIndex(vm.menu, ['innerTabGroup', $state.current.innerTabGroup]);

            return obj;
        }

        // 
        // Set vm.step with currentStep()
        function setCurrentStep() {
            // setCurrentStep
            vm.step = currentStep();

            // set prev and next btn
            vm.hasPrevBtn = (vm.step.index > 1 && vm.step.index <= 5);
            vm.hasNextBtn = (vm.step.index > 0 && vm.step.index < 5);
        }

        function stateChangeStart() {
            // 
            // Mark identification as completed after user has logged or registered
            if (vm.step.innerTabGroup === 'identification') {
                var user = $user.getCurrent();

                if ($user) {
                    // 
                    // Complete identification step
                    vm.step.checked = true;

                    // 
                    // Set user
                    vm.user = user.model;
                }
            }
        }
        // 
        // Actions on state change
        function stateChangeSuccess() {
            // 
            // Change current step
            setCurrentStep();
        }

        // 
        // Go To completedStep
        function validateSteps() {

            // 
            // last completed
            var lastIsCompleted = false,
                firstIncomplete = false,
                rightStep = false,
                rightStepSlug = false;

            // 
            // Loop trought all steps to check completed and redirect user to the right place
            angular.forEach(vm.service.steps, function(menu, key) {

                // 
                // current route
                var isCurrent = ($state.current.innerTabGroup === menu.innerTabGroup);

                // 
                // Identification validation
                if (menu.innerTabGroup === 'identification') {
                    // Check if is logged in
                    menu.checked = ($user.getCurrent());

                    // This is always enabled
                    menu.disabled = false;

                    // Update local storage step validation
                    vm.checkout.stepStatus[menu.innerTabGroup] = menu.checked;

                    // 
                    // Set first incompleted
                    firstIncomplete = menu.checked ? false : true;
                } else {
                    // 
                    // Validation for all other steps
                    var completed = (vm.checkout.stepStatus[menu.innerTabGroup] === true);

                    // toogle
                    menu.checked = (completed);
                    menu.disabled = isCurrent ? false : (!completed);

                    //
                    // If we have no firstIncomplete 
                    // and this is not completed 
                    // and its not the current route
                    if (!firstIncomplete && !completed && menu.innerTabGroup !== 'confirmation') {
                        if (!isCurrent) {
                            rightStep = 'app.checkout.' + menu.innerTabGroup;
                            rightStepSlug = menu.innerTabGroup;
                        }
                        firstIncomplete = true;
                    }
                }

                // 
                // Redirect user if we have lastCompletedStep and its not the current one
                if (rightStep && vm.step.innerTabGroup !== 'confirmation' && !vm.completed) {
                    // enable state menu
                    menu.disabled = false;

                    // Go to state
                    $state.go(rightStep, {}, {
                        inherit: false
                    });

                    // 
                    // Try to focus first input
                    focusFirstInput(rightStepSlug);
                } else {
                    // 
                    // Try to focus first input
                    focusFirstInput(menu.innerTabGroup);
                }

            }.bind(this));
        }

        function next(ev) {
            var redirect = true;

            // 
            // Payment details validation
            if (vm.step.innerTabGroup === 'payment-details') {
                // validate form
                if (vm.form.paymentDetails.$invalid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);

                    // 
                    // Trigger validation
                    vm.form.paymentDetails.$setSubmitted();

                    return false;
                }
            }

            // 
            // Shipping details validation
            if (vm.step.innerTabGroup === 'shipping-details') {
                // validate form
                if (vm.form.shippingDetails.$invalid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);

                    // 
                    // Trigger validation
                    vm.form.shippingDetails.$setSubmitted();

                    return false;
                }
            }

            // 
            // Shipping method validation
            if (vm.step.innerTabGroup === 'shipping-method') {
                // validate form
                if (vm.form.shippingMethod.$invalid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);
                    return false;
                }
            }

            // 
            // Shipping method validation
            if (vm.step.innerTabGroup === 'payment') {
                var valid = validateBeforeOrderSave();

                //
                // Validate all forms
                if (vm.form.payment.$invalid || !valid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);

                    // 
                    // Run step validation if other forms are not valid
                    if (!valid) {
                        validateSteps();
                    }

                    return false;
                } else {
                    // 
                    // Save order
                    vm.service.create().then(orderCreateSuccess.bind(this), orderCreateFail.bind(this));
                    redirect = false;
                }
            }

            // 
            // Mark current step as completed
            stepCompleted();

            // 
            // Get next step
            var nextStep = vm.menu[vm.step.index + 1],
                nextRoute = nextStep ? 'app.checkout.' + nextStep.innerTabGroup : false;

            // 
            // Redirect and enable next menu
            if (nextRoute && redirect) {
                //
                // enable next menu
                nextStep.disabled = false;

                //
                // go to next
                $state.go(nextRoute, {}, {
                    inherit: false
                });

                // 
                // Focus the first field inside checkout form
                // Use timeout to do this in the next digest
                focusFirstInput(nextStep.innerTabGroup);
            }
        }

        function prev() {
            // 
            // Get prev step
            var prevStep = vm.menu[vm.step.index - 1],
                prevRoute = prevStep ? 'app.checkout.' + prevStep.innerTabGroup : false;

            if (prevRoute) {
                //
                // go to prev
                $state.go(prevRoute, {}, {
                    inherit: false
                });

                // 
                // Focus the first field inside checkout form
                // Use timeout to do this in the next digest
                focusFirstInput(prevStep.innerTabGroup);
            }
        }

        function focusFirstInput(innerTabGroup, additionalClass) {

            if (timeoutFocus) $timeout.cancel(timeoutFocus);

            timeoutFocus = $timeout(function() {
                // 
                // Get current step form elem
                var elem = angular.element('.' + innerTabGroup);

                // 
                // Find additionalClass if provided
                if (additionalClass) elem = elem.find(additionalClass);

                // 
                // Find first input
                elem = elem.find(':input:first');

                // 
                // Focus elem
                if (elem) elem.focus();
            }, 100);
        }

        function stepCompleted() {
            // Change in menu
            vm.step.checked = true;

            // Change in localstorage
            vm.checkout.stepStatus[vm.step.innerTabGroup] = true;
        }

        function hasForm(form) {
            return (vm.form[form]);
        }

        function orderCreateSuccess(response) {
            // 
            // Update checkout with order object
            angular.extend(vm.service.model, response.data.order);

            // 
            // Already had an id, redirect to confirmation
            if (response.data.err) {
                if (vm.service.model.payment.mode === 'paypal_balance') {
                    $state.go('app.checkout.confirmation', {
                        paymentId: vm.service.model.paypal.payment,
                        token: vm.service.model.paypal.token,
                        PayerID: vm.service.model.paypal.payer
                    }, {
                        inherit: false
                    });

                    // 
                    // Execute payment
                    vm.service.execute().then(executeSuccess.bind(this), executeFail.bind(this));
                } else {
                    console.log('response.data.err', response.data.err);
                }
            } else {
                if (vm.service.model.payment.mode === 'paypal_balance') {
                    //
                    // Mark that we are reloading
                    vm.paypalRedirect = true;

                    // 
                    // Redirect user to paypal
                    $window.location = response.data.payment.redirectUrl;
                } else {
                    // 
                    // Complete checkout
                    completeCheckout(response.data.order);

                    // 
                    // Send user to confirmation
                    $state.go('app.checkout.confirmation');
                }
            }
        }

        function orderCreateFail() {}

        function executeSuccess(response) {
            // 
            // Update checkout with order object
            angular.extend(vm.service.model, response.data.order);

            completeCheckout(response.data.order);
        }

        function executeFail() {
            // 
            // Complete transaction with error
            vm.completed = {
                error: true
            };

            // 
            // Clear checkout
            finishCheckout();
        }

        function completeCheckout(order) {
            // 
            // Set as completed
            vm.completed = {
                error: false,
                order: order
            };

            // 
            // Clear checkout
            finishCheckout();
        }

        // 
        // Validate all fields before saving the order in payment route
        function validateBeforeOrderSave() {
            var valid = true;

            if (!vm.service.model.payment.payer.firstName || !vm.service.model.payment.payer.lastName || !vm.service.model.payment.address) {
                valid = false;

                // 
                // Remove completition
                vm.checkout.stepStatus['payment-details'] = false;
            }

            if (!vm.service.model.shipping.to.firstName || !vm.service.model.shipping.to.lastName || !vm.service.model.shipping.address) {
                valid = false;

                // 
                // Remove completition
                vm.checkout.stepStatus['shipping-details'] = false;
            }

            if (!vm.service.model.shipping.method || (!vm.service.model.shipping.price && vm.service.model.shipping.price !== 0)) {
                valid = false;

                // 
                // Remove completition
                vm.checkout.stepStatus['shipping-method'] = false;
            }

            if (vm.service.model.payment.mode === 'debito_bancario' && (!vm.service.model.moip || !vm.service.model.moip.bank)) {
                valid = false;
            }

            return valid;
        }

        function isCompleted() {
            return (vm.completed !== false && !vm.service.busy.status('execute'));
        }

        function finishCheckout() {
            $timeout(function() {
                // 
                // Clear checkout
                vm.checkout = false;
                delete $localStorage.checkout;

                // 
                // Set a new service
                setService(true);

                // 
                // Make all menu disable
                vm.service.steps[0].disabled = true;
                vm.service.steps[1].disabled = true;
                vm.service.steps[2].disabled = true;
                vm.service.steps[3].disabled = true;
                vm.service.steps[4].disabled = true;
                vm.service.steps[5].disabled = true;

                // 
                // Mark confirmation as completed
                vm.service.steps[5].checked = true;

                // 
                // Empty cart
                $cart.clear();
            });
        }

        // 
        // callback for login success
        // Try to add information from profile to checkout
        function cbLogin(user) {
            // 
            // Set user again
            vm.user = $user.getCurrent();

            //
            // Update service model
            vm.service.model.user = vm.user.model;

            // 
            // Extend user data
            extendUser(user);
        }

        // 
        // callback for registration success
        // Try to add information from profile to checkout
        function cbRegister(user) {
            // 
            // Set user again
            vm.user = $user.getCurrent();

            //
            // Update service model
            vm.service.model.user = vm.user.model;

            // 
            // Extend user data
            extendUser(user, true);
        }

        // 
        // Extend service with user address
        function extendUser(user, onlyName) {
            // 
            // Set user
            user = user ? user : vm.user.model;

            if (!onlyName) {
                // 
                // Extend profile data
                if (lodash.hasIn(user.profile, 'address.payment')) {
                    angular.extend(vm.service.model.payment.payer, user.profile.address.payment.payer);
                    angular.extend(vm.service.model.payment.address, user.profile.address.payment);
                }
                if (lodash.hasIn(user.profile, 'address.shipping')) {
                    angular.extend(vm.service.model.shipping.to, user.profile.address.shipping.to);
                    angular.extend(vm.service.model.shipping.address, user.profile.address.shipping);
                }
            }

            // 
            // Add first and last name if needed
            if (!vm.service.model.payment.payer.firstName) {
                vm.service.model.payment.payer.firstName = user.profile.firstName;
                vm.service.model.payment.payer.lastName = user.profile.lastName;
            }
            if (!vm.service.model.shipping.to.firstName) {
                vm.service.model.shipping.to.firstName = user.profile.firstName;
                vm.service.model.shipping.to.lastName = user.profile.lastName;
            }
        }

        // 
        // Extend entry with user address
        function extendModel(entry) {

            // User validation
            if (!vm.user) return entry;

            // 
            // Set user
            var user = vm.user.model;

            // 
            // Set user again
            if (!lodash.hasIn(user, 'profile')) {
                user = $user.getCurrent();
                user = user.model;
            }

            // 
            // Extend profile data
            if (lodash.hasIn(user.profile, 'address.payment')) {
                angular.extend(entry.model.payment.payer, user.profile.address.payment.payer);
                angular.extend(entry.model.payment.address, user.profile.address.payment);
            }
            if (lodash.hasIn(user.profile, 'address.shipping')) {
                angular.extend(entry.model.shipping.to, user.profile.address.shipping.to);
                angular.extend(entry.model.shipping.address, user.profile.address.shipping);
            }

            // 
            // Add first and last name if needed
            if (!entry.model.payment.payer.firstName) {
                entry.model.payment.payer.firstName = user.profile.firstName;
                entry.model.payment.payer.lastName = user.profile.lastName;
            }
            if (!entry.model.shipping.to.firstName) {
                entry.model.shipping.to.firstName = user.profile.firstName;
                entry.model.shipping.to.lastName = user.profile.lastName;
            }

            return entry;
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.checkout').provider('$checkout', $checkoutProvider);
    /**
     * @ngdoc object
     * @name mtda.checkout.$checkoutProvider
     **/
    /*@ngInject*/
    function $checkoutProvider() {

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#current
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * current checkout
         **/
        this.current = false;

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#subtotal
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * store checkout subtotal
         **/
        this.subtotal = 0;

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#checkoutTemplateUrl
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * checkout template url
         **/
        this.checkoutTemplateUrl = 'core/checkout/checkout.tpl.html';

        /**
         * @name mtda.checkout.$checkoutProvider#shippingMethods
         * @propertyOf mtda.checkout.$checkoutProvider
         * @ngdoc object
         * @description
         * shipping methods
         **/
        this.shippingMethods = {
            free: {
                slug: 'free',
                title: 'SHIP_FREE_SHIPPING',
                price: 0
            },
            get_in_store: {
                slug: 'get_in_store',
                title: 'SHIP_GET_IN_STORE',
                price: 0
            }
        };

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#gateways
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.gateways = {
            'paypal_balance': {
                status: true
            },
            'paypal_cc': {
                status: true
            },
            'boleto': {
                status: true
            },
            'debito_bancario': {
                status: true
            },
            'cc': {
                status: true
            }
        };

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#banks
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.banks = [{
            value: 'Itau',
            name: 'Itaú',
            image: '/assets/images/logo-itau.png'
        }, {
            value: 'BancoDoBrasil',
            name: 'Banco do Brasil',
            image: '/assets/images/logo-bb.png'
        }, {
            value: 'Bradesco',
            name: 'Bradesco',
            image: '/assets/images/logo-bradesco.png'
        }, {
            value: 'Banrisul',
            name: 'Banrisul',
            image: '/assets/images/logo-banrisul.png'
        }];

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#cardTypes
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.cardTypes = {
            moip: ['AmericanExpress', 'Diners', 'Mastercard', 'Hipercard', 'Hiper', 'Elo', 'Visa'],
            paypal: ['Visa', 'Mastercard', 'Discover', 'Amex']
        }

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#cardParcelas
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.cardParcelas = [1, 2, 3];

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#steps
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * steps
         **/
        this.steps = [{
            label: 'IDENTIFICATION',
            state: 'app.checkout.identification',
            innerTabGroup: 'identification',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_one'
            },
            checked: false,
            disabled: true
        }, {
            label: 'PAYMENT_DETAILS',
            state: 'app.checkout.payment-details',
            innerTabGroup: 'payment-details',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_two'
            },
            checked: false,
            disabled: true
        }, {
            label: 'SHIPPING_DETAILS',
            state: 'app.checkout.shipping-details',
            innerTabGroup: 'shipping-details',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_3'
            },
            checked: false,
            disabled: true
        }, {
            label: 'SHIPPING_METHOD',
            state: 'app.checkout.shipping-method',
            innerTabGroup: 'shipping-method',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_4'
            },
            checked: false,
            disabled: true
        }, {
            label: 'PAYMENT',
            state: 'app.checkout.payment',
            innerTabGroup: 'payment',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_5'
            },
            checked: false,
            disabled: true
        }, {
            label: 'CONFIRMATION',
            state: 'app.checkout.confirmation',
            innerTabGroup: 'confirmation',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_6'
            },
            checked: false,
            disabled: true
        }];

        this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, lodash, $localStorage) {
            var endpoint = api.url + 'api/checkout/',
                busy = new Busy.service();

            return {
                entries: this.entries,
                subtotal: this.subtotal,
                shippingMethods: this.shippingMethods,
                gateways: this.gateways,
                cardTypes: this.cardTypes,
                cardParcelas: this.cardParcelas,
                banks: this.banks,
                steps: this.steps,
                remove: remove,
                updateSubtotal: updateSubtotal
            }

            /**
             * @ngdoc object
             * @name mtda.checkout.$checkoutProvider#remove
             * @propertyOf mtda.checkout.$checkoutProvider
             * @description
             * remove entry
             **/
            function remove(index) {
                // 
                // Remove entry from local storage
                $localStorage.checkout.splice(index, 1);

                // 
                // Notify user
                $page.toast($page.msgCartDeleted);

                // 
                // Update subtotal
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.checkout.$checkoutProvider#updateSubtotal
             * @propertyOf mtda.checkout.$checkoutProvider
             * @description
             * update subtotal
             **/
            function updateSubtotal() {
                // 
                // Calculate new subtotal
                var subtotal = _.reduce(this.entries, function(sum, obj) {
                    return sum + (obj.price * obj.qty);
                }, 0);

                // 
                // Set new subtotal
                this.subtotal = subtotal;
            };
        };
    }
})();
(function() {
    'use strict';
    angular.module('mtda.checkout').config( /*@ngInject*/ function($stateProvider, $checkoutProvider, $userProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.checkout', {
            url: '/checkout/',
            abstract: true,
            innerTabGroup: 'identification',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $checkoutProvider.checkoutTemplateUrl;
                    },
                    controller: '$CheckoutCtrl as vm'
                }
            }
        }).state('app.checkout.identification', {
            url: '',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'identification',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/identification/identification.tpl.html'
                    },
                    // controller: '$CheckoutIdentificationCtrl as vm'
                }
            },
            resolve: {
                skipIfLoggedInCheckout: $userProvider.skipIfLoggedInCheckout
            }
        }).state('app.checkout.payment-details', {
            url: 'payment-details/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'payment-details',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/paymentDetails/paymentDetails.tpl.html'
                    },
                    // controller: '$CheckoutPaymentDetailsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.shipping-details', {
            url: 'shipping-details/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'shipping-details',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/shippingDetails/shippingDetails.tpl.html'
                    },
                    // controller: '$CheckoutShippingDetailsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.shipping-method', {
            url: 'shipping-method/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'shipping-method',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/shippingMethod/shippingMethod.tpl.html'
                    },
                    // controller: '$CheckoutShippingMethodCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.payment', {
            url: 'payment/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'payment',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/payment/payment.tpl.html'
                    },
                    // controller: '$CheckoutPaymentCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.confirmation', {
            url: 'confirmation/?method?paymentId?token?PayerID',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'confirmation',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/confirmation/confirmation.tpl.html'
                    },
                    // controller: '$CheckoutConfirmationCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.checkout').service('$Checkout', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug, $localStorage, lodash, $checkout, $mdSidenav, $timeout, $user, $filter) {

        var url = api.url + 'api/orders/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.execute = execute;
        this.service.prototype.formStatus = formStatus;

        function create() {
            // Are we busy?
            if (this.busy.start('create')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {

                        // 
                        // Remove loading
                        this.busy.end('create');

                        return response.data;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        // 
                        // Set error msg
                        var msg = (response && response.error && response.error.translate) ? $filter('translate')(response.error.translate) : $page.msgNotDone;
                        $page.showAlert(false, 'Opss..', msg);

                        // 
                        // Remove loading
                        this.busy.end('create');

                        return response.data;
                    }.bind(this));
        }

        function execute() {
            // Are we busy?
            if (this.busy.start('execute')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url + 'execute', {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {

                        // 
                        // Remove loading
                        this.busy.end('execute');

                        return response.data;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        // 
                        // Set error msg
                        var msg = (response && response.error && response.error.translate) ? $filter('translate')(response.error.translate) : $page.msgNotDone;
                        $page.showAlert(false, 'Opss..', msg);

                        // 
                        // Remove loading
                        this.busy.end('execute');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }
    });
})();
/*! Moip.js - build date: 2015-03-04 */
!function(a){function b(a){return a+="",a.replace(/[\s+|\.|\-]/g,"")}function c(){return this instanceof c?void 0:new c}var d=a.moip||{};a.moip=d,c.prototype={isValid:function(a){var d=c.prototype.cardType(a);if(a=b(a),d){if("HIPERCARD"===d.brand)return!0;for(var e=0,f=2-a.length%2;f<=a.length;f+=2)e+=parseInt(a.charAt(f-1),10);for(f=a.length%2+1;f<a.length;f+=2){var g=2*parseInt(a.charAt(f-1),10);e+=10>g?g:g-9}return e%10===0?!0:!1}return!1},cardType:function(a,c){var d={VISA:{matches:function(a){return/^4\d{15}$/.test(a)}},MASTERCARD:{matches:function(a){return/^5[1-5]\d{14}$/.test(a)}},AMEX:{matches:function(a){return/^3[4,7]\d{13}$/.test(a)}},DINERS:{matches:function(a){return/^3[0,6,8]\d{12}$/.test(a)}},HIPERCARD:{matches:function(a){return/^(606282\d{10}(\d{3})?)|(3841\d{15})$/.test(a)}},ELO:{matches:function(a){var b=["50670","50671","50672","50673","50674","50675","50676","50900","50901","50902","50903","50904","50905","50906","50907","401178","401179","431274","438935","451416","457393","457631","457632","504175","506699","506770","506771","506772","506773","506774","506775","506776","506777","506778","509080","509081","509082","509083","627780","636297"];return null===a||16!=a.length?!1:b.indexOf(a.substring(0,6))>-1||b.indexOf(a.substring(0,5))>-1}}},e={VISA:{matches:function(a){return/^4\d{3}\d*$/.test(a)}},MASTERCARD:{matches:function(a){return/^5[1-5]\d{4}\d*$/.test(a)}},AMEX:{matches:function(a){return/^3[4,7]\d{2}\d*$/.test(a)}},DINERS:{matches:function(a){return/^3(?:0[0-5]|[68][0-9])+\d*$/.test(a)}},HIPERCARD:{matches:function(a){return/^(606282|3841\d{2})\d*$/.test(a)}},ELO:{matches:function(a){var b=["50670","50671","50672","50673","50674","50675","50676","50900","50901","50902","50903","50904","50905","50906","50907","401178","401179","431274","438935","451416","457393","457631","457632","504175","506699","506770","506771","506772","506773","506774","506775","506776","506777","506778","509080","509081","509082","509083","627780","636297"];return null===a||a.length<6?!1:b.indexOf(a.substring(0,6))>-1||b.indexOf(a.substring(0,5))>-1}}};return a=b(a),c&&(d=e),d.ELO.matches(a)?{brand:"ELO"}:d.VISA.matches(a)?{brand:"VISA"}:d.MASTERCARD.matches(a)?{brand:"MASTERCARD"}:d.AMEX.matches(a)?{brand:"AMEX"}:d.HIPERCARD.matches(a)?{brand:"HIPERCARD"}:d.DINERS.matches(a)?{brand:"DINERS"}:null},isSecurityCodeValid:function(a,b){var c,e=d.creditCard.cardType(a);c="AMEX"===e.brand?4:3;var f=new RegExp("[0-9]{"+c+"}");return b.length===c&&f.test(b)},isExpiryDateValid:function(a,b){return a=parseInt(a,10),b=parseInt(b,10),1>a||a>12?!1:2!==(b+"").length&&4!==(b+"").length?!1:(2===(b+"").length&&(b=b>80?"19"+b:"20"+b),1e3>b||b>=3e3?!1:!c.prototype.isExpiredDate(a,b))},isExpiredDate:function(a,b){var c=new Date,d=("0"+(c.getMonth()+1)).slice(-2),e=c.getFullYear();if(a=("0"+a).slice(-2),2===b.toString().length){if(b>80)return!0;b="20"+b}var f=e+d,g=b+a;return parseInt(g,10)<parseInt(f,10)}},d.creditCard=c()}(window),function(a){var b=a.moip||{};a.moip=b,b.calculator={pricing:function(a){return this.buildJson(a)},buildJson:function(a){var b,c=[],d=[],e=[],f=[];if(b=this._calculateTransactionTax(a.amount,a.transaction_percentage,a.fixed),void 0!==a.antecipation_percentage&&void 0!==a.floating&&void 0!==a.installment){for(var g=0;11>=g;g++)c[g]=this._calculateInstallmentValue(a.amount,g+1),d[g]=this._calculateAntecipationPercentage(b,a,g,a.amount),e[g]=this._calculateTotalTax(d[g],b),f[g]=this._calculateLiquidValue(a.amount,e[g]);return{amount:a.amount,transaction_tax:b,antecipation_percentage:d,total_tax:e,liquid_value:f,installment_value:c}}return f=this._calculateLiquidValue(a.amount,b),{amount:a.amount,transaction_tax:b,liquid_value:f}},pricingWithInterest:function(a){for(var b=[],c=[],d=[],e=[],f=[],g=[],h=[],i=1;12>=i;i++)b[i-1]=this._calculateInterestRate(a.amount,a.interest_rate,i),c[i-1]=this._calculateAmount(b[i-1],i),d[i-1]=this._calculateTransactionTax(c[i-1],a.transaction_percentage,a.fixed),e[i-1]=this._calculateAntecipationPercentage(d[i-1],a,i-1,c[i-1]),f[i-1]=this._calculateAntecipationPercentageFromAmount(c[i-1],e[i-1]),g[i-1]=this._calculateTotalTax(e[i-1],d[i-1]),h[i-1]=this._calculateLiquidValue(c[i-1],g[i-1]);return{amount:c,transaction_tax:d,antecipation_percentage:f,total_tax:g,liquid_value:h,installment_value:b}},_calculateAntecipationPercentageFromAmount:function(a,b){return parseFloat(100*b/(a/100)).toFixed(2)},_calculateTransactionTax:function(a,b,c){return parseFloat(((a*(b/100)+c)/100).toFixed(2))},_calculateAntecipationPercentage:function(a,b,c,d){return parseFloat((parseFloat(b.antecipation_percentage/100/30*(30+15*c-b.floating))*parseFloat(d/100-a)).toFixed(2))},_calculateTotalTax:function(a,b){return parseFloat((a+b).toFixed(2))},_calculateLiquidValue:function(a,b){return parseFloat((a/100-parseFloat(b)).toFixed(2))},_calculateInstallmentValue:function(a,b){return parseFloat((a/b/100).toFixed(2))},_calculateInterestRate:function(a,b,c){return parseFloat(1===c?a/100:(this._coefficient(b,c)*(a/100)).toFixed(2))},_calculateAmount:function(a,b){return parseFloat(100*(a*b).toFixed(2))},_coefficient:function(a,b){return parseFloat((a/100/(1-1/Math.pow(a/100+1,b))).toFixed(10))}}}(window);

(function() {
    'use strict';
    angular.module('mtda.user').controller('$AccountListCtrl', /*@ngInject*/ function($scope, $account, lodash, $page) {
        var list = this,
            bookmark;

        // 
        // Set factory to get entries
        list.factory = $account;

        // 
        // Table options
        list.options = {
            rowSelection: true,
        };

        // 
        // Selected items
        list.selected = [];

        // 
        // Query
        list.query = {
            order: '-createdAt',
            limit: 5,
            page: 1,
            filters: ''
        };

        // 
        // Get all service
        list.getAll = getAll;

        // 
        // Remove service
        list.remove = remove;

        // 
        // Filters 
        list.filters = {
            show: false,
            setShow: setShow,
            options: {
                debounce: 500
            }
        };

        // 
        // Watch for changes in filter
        $scope.$watch('list.query.filters', watchQueryFilters);

        // 
        // Get entries
        list.getAll();

        // 
        // Success callback
        function success(data) {

            // 
            // Clean selected
            list.selected = [];

            // 
            // Set entries
            list.data = data;
        }

        // 
        // Get all service
        function getAll() {
            // 
            // Set promise and call
            list.promise = list.factory.getAll(list.query).then(success);
        };

        // 
        // Remove items
        function remove(e) {

            // 
            // Get array of ids
            var ids = _.map(list.selected, '_id');

            // 
            // Confirmation
            $page.showAlertConfirm(e, 'Excluir usuário', 'Deseja realmente excluir ' + ids.length + ' usuário(s)?', 'Excluir', 'Cancelar', function() {

                // 
                // Confirmed, exclude
                list.promise = list.factory.remove(e, ids).then(getAll);
            }, function() {

                // 
                // Canceled, clean selected
                list.selected = [];
            });
        }

        // 
        // Filters - setShow
        function setShow(status) {
            list.filters.show = status;
        }

        // 
        // Watch for query filter changes
        function watchQueryFilters(newValue, oldValue) {
            if (!oldValue) {
                bookmark = list.query.page;
            }

            if (newValue !== oldValue) {
                list.query.page = 1;
            }

            if (!newValue) {
                list.query.page = bookmark;
            }

            list.getAll();
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name app.project.directive:accountList
     * @restrict EA
     * @description
     * Listagem de contas
     * @element div
     **/
    angular.module('mtda.user').directive('accountList', /*@ngInject*/ function() {
        return {
            restrict: 'EA',
            templateUrl: 'core/account/account-list/account-list.tpl.html',
            controller: '$AccountListCtrl',
            controllerAs: 'list',
            bindToController: {}
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AddressFormContentCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // brState
        vm.states = $page.brStates;

        // 
        // Form locale
        vm.formLocale = $page.formLocale;
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').directive('addressFormContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$AddressFormContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/addressFormContent/addressFormContent.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$CompanyFormCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // brState
        vm.states = $page.brStates;
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').directive('companyForm', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$CompanyFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/companyForm/companyForm.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountPasswordCtrl', /*@ngInject*/ function($state, $User, $account, $page) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $account;

        //
        // Select account
        vm.factory.get($state.params._id).then(function(data) {
            //
            // Instantiate account
            vm.service = new $User.service({
                model: data
            });

        }, function() {
            $page.toast($page.msgNotDone);

            // 
            // Redirect user back to accounts
            vm.goToProfile();
        });

        //
        // Submit and save
        vm.submit = submit;

        // 
        // Go to profile
        vm.goToProfile = goToProfile;

        function submit() {
            // Update entry
            vm.service.password().then(function() {

                // 
                // Redirect user back to accounts
                vm.goToProfile();
            });
        }

        function goToProfile() {
            // 
            // Redirect user back to accounts
            $state.go('app.account', {
                _id: $state.params._id
            });
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$PersonFormContentCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // Form locale
        vm.formLocale = $page.formLocale;
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').directive('personFormContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$PersonFormContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/personFormContent/personFormContent.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$PhoneFormContentCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // Form locale
        vm.formLocale = $page.formLocale;

        // 
        // Flex size
        vm.flex = vm.setFlex ? vm.setFlex : '30';
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').directive('phoneFormContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$PhoneFormContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '=',
                setFlex: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/phoneFormContent/phoneFormContent.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$PublicProfileCtrl', /*@ngInject*/ function($account, $state, $menu, $user) {
        var vm = this;

        // 
        // User service
        vm.service = $user.getCurrent();
    })
})();
(function() {
    'use strict';
    angular.module('mtda.account').controller('$PublicAccount', /*@ngInject*/ function($account, $state, $menu, $user) {
        var vm = this;
        vm.menu = $menu;

        // 
        // User info
        vm.currentUser = $user.getCurrent();
    })
})();
(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartBtnCtrl', /*@ngInject*/ function($log, $mdSidenav, $Cart, $cart, lodash, $state) {
        var vm = this;

        vm.open = open;
        vm.show = show;

        vm.itemCount = function() {
            return $cart.entries.length;
        }

        function open() {
            $mdSidenav('cart').open();
        }

        function show() {
            return !($state.current.toolbar === 'admin');
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.cart').directive('cartBtn', /*@ngInject*/ function directive() {
        return {
            scope: true,
            controller: '$CartBtnCtrl',
            controllerAs: 'vm'
        };
    });
})();
(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartFormCtrl', /*@ngInject*/ function($log, $scope, $Cart, $cart, lodash) {
        var vm = this;

        //
        // New cart
        setCart();

        vm.variantsCompare = vm.product.variants.attrs;
        vm.submit = submit;

        //
        // Update variants quantities when attrs change
        vm.variantChange = variantChange;

        // 
        // Load directive and options
        bootstrap();

        //
        // load directive
        function bootstrap() {
            //
            // Set attrs options for the form
            angular.forEach(vm.product.variants.attrs, function(v, k) {
                // Get attr options
                var obj = v.options ? v.options : false;
                // Place in controller
                vm[v.name] = obj;
            });
            // 
            // Set product cnt
            vm.qtyAvailable = vm.product.variants.cnt;
        }
        // 
        // We need to adjust data for color
        function colorValue(value) {
            // Get color obj and set in the model
            vm.cart.model.attrs.colors = _.find(vm.colors, ['value', value]);
        }
        // 
        // We need to adjust variants quantities everytime a variant changes
        function variantChange(attrName, value, index) {
            if (attrName === 'colors') colorValue(value);

            var options = {},
                qty = 0;

            // $log.debug('variantChange', 'start');
            // $log.debug('attrName', attrName);
            // $log.debug('value', value);
            // $log.debug('index', index);
            // $log.debug('vm.variants', vm.variants);

            // 
            // Iterate over variants to make options
            _.forEach(vm.variants, function(variant, variantKey) {

                // $log.debug('variant', variant);

                // 
                // Filter variants by selected namespace
                if (!variant.name.match(attrName + ':' + value)) return;

                var skipVariant = false;

                // 
                // Get options for all attrs
                _.forEach(variant.attrs, function(attr, attrKey) {

                    // 
                    // Doublecheck variants before current
                    if (attrKey < index) {
                        var otherAttrName = vm.product.variants.attrs[attrKey].name,
                            otherAttrValue = lodash.hasIn(vm.cart.model.attrs[otherAttrName], 'value') ? vm.cart.model.attrs[otherAttrName].value : vm.cart.model.attrs[otherAttrName];

                        // $log.debug('vm.cart.model.attrs', vm.cart.model.attrs);
                        // $log.debug('variant.name', variant.name);
                        // $log.debug('otherAttrName', otherAttrName);
                        // $log.debug('otherAttrValue', otherAttrValue);

                        if (!variant.name.match(otherAttrName + ':' + otherAttrValue)) {
                            // $log.debug('skipVariant', true);

                            skipVariant = true;
                            return;
                        }
                    }

                    //
                    // attrKey must be higher then current index
                    if (attrKey <= index) return;

                    // 
                    // Create options array for this type if necessary
                    if (!_.hasIn(options, attr.name)) options[attr.name] = [];

                    // 
                    // Push option
                    options[attr.name].push(attr.value);
                });

                // 
                // Valid loop?
                if (!skipVariant) {
                    // 
                    // Increment to qty
                    qty = qty + variant.cnt;
                }
            });

            // 
            // Adjust options
            _.forEach(vm.product.variants.attrs, function(attr, attrKey) {
                //
                // attrKey must be higher then current index
                if (attrKey <= index) return;

                // $log.debug('vm.cart.model.attrs[attr.name]', vm.cart.model.attrs[attr.name]);

                // Update options in the form according to selection
                updateOptions(attr.name, options);

                // If is colors, we need to change colorValue as well
                if (attr.name === 'colors') updateOptions('colorValue', options);
            });

            // 
            // Set qty
            vm.qtyAvailable = qty;

            // $log.debug('variantChange', 'end');
        }

        function updateOptions(name, options) {
            // 
            // Reset attr model
            vm.cart.model.attrs[name] = null;

            // 
            // Empty options for attr not found
            if (!_.hasIn(options, name)) {
                _.forEach(vm[name], function(o) {
                    o.disabled = true;
                });

                return;
            }

            // 
            // Disable options not found
            _.forEach(vm[name], function(o) {
                o.disabled = (_.includes(options[name], o.value)) ? false : true;
            });
        }

        function submit(ev) {
            vm.cart.create(ev).then(onSuccess, onError);

            function onSuccess() {
                // 
                // Update subtotal
                $cart.updateSubtotal();

                // 
                // Reset the form with a new cart
                setCart();

                // 
                // Reset validation
                vm.form.$setPristine();
            }

            function onError() {}
        }

        function setCart() {
            vm.cart = new $Cart.service({
                model: {
                    product: vm.product,
                    price: vm.product.priceOffer ? vm.product.priceOffer : vm.product.price
                }
            });
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.cart').directive('cartForm', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$CartFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                product: '=',
                variants: '=',
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/cart/cartForm/cartForm.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartSidenavCtrl', controller);

    /*@ngInject*/
    function controller($scope, $cart, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $cart;
        vm.entries = vm.factory.entries;
    }
})();
(function() {
    'use strict';

    angular.module('mtda.cart').directive('cartSidenav', cartSidenav);

    /** @ngInject */
    function cartSidenav() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/cart/cartSidenav/cartSidenav.tpl.html';
            },
            controller: '$CartSidenavCtrl',
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

})();
(function() {
    'use strict';
    angular.module('mtda.category').controller('CategoryItemDCtrl', /*@ngInject*/ function() {
        var vm = this;
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').directive('categoryItem', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: 'CategoryItemDCtrl',
            controllerAs: 'vm',
            bindToController: {
                entries: '=',
                category: '=',
                child: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/category/item/item.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.category').controller('$PublicCategoryCtrl', /*@ngInject*/ function($List, $state, $location, $category, $product, $filter) {
        var vm = this;

        vm.category = $category;
        vm.changeCategory = changeCategory;
        vm.breadcrumbs = {};

        // Change breadcrumbs
        changeBreadcrumbs();

        if (!$state.params.category) {
            // $state.go('app.home');
        }

        //
        // Get all categories for menu
        //
        vm.category.getMenu($state).then(function(data) {
            vm.categoryTitle = data.title ? data.title : $filter('translate')('ALL_PRODUCTS');

            // List
            vm.params = {
                infinite: false,
                source: $product.getList,
                categoryMenu: data.entries,
                disableTransition: true,
                filters: {
                    sort: '-price',
                    category: $state.params.category
                },
                sortOptions: $product.sort
            };

            vm.service = new $List.service(vm.params);
        }).catch(function() {
            $state.go('app.home');
        });

        function primaryAction(item) {
            $state.go('app.category-edit', item);
        }

        function changeCategory(category, child, categoryTitle, childTitle) {
            // Create new title
            var title = categoryTitle;
            if (childTitle) title = childTitle + ' - ' + title;

            // Create new url
            var url = category;
            if (child) url += '/' + child;

            // Change the title
            vm.categoryTitle = title;

            // Create filter obj with new categories
            var filters = {
                category: category,
                child: child
            };

            // Change breadcrumbs
            changeBreadcrumbs(filters);

            $state.go('app.publicCategories', filters, {
                notify: false
            });

            // Reset the list
            vm.service.resetList();

            // Change the filter to trigger product entries reload
            angular.extend(vm.service.filters, filters);
        }

        function changeBreadcrumbs(filters) {
            var params = filters ? filters : $state.params;

            // Change breadcrumbs
            vm.breadcrumbs = {
                category: params.category,
                child: params.child
            }

            if (!params.category)
                vm.breadcrumbs['title'] = $filter('translate')('ALL_PRODUCTS');
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryTabsCtrl', /*@ngInject*/ function($category) {
        var vm = this;

        getCategories();

        function getCategories() {
            vm.factory = $category;
            vm.factory.getAll().then(function(data) {
                vm.entries = data.entries;
            });
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.category').directive('categoryTabs', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$CategoryTabsCtrl',
            controllerAs: 'vm',
            bindToController: {},
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/category/tabs/categoryTabs.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.directives.directive:backgroundImage
     * @restrict EA
     * @description
     * Setar background image
     **/
    angular.module('mtda.directives').directive('backgroundImage', /*@ngInject*/ function() {
        return function(scope, element, attrs) {
            attrs.$observe('backgroundImage', function(value) {
                if (!value) return;

                element.css({
                    'background-image': 'url(' + value + ')'
                });
            });
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$CeperCtrl', /*@ngInject*/ function($http, $page, api, lodash) {
        var vmCep = this;
        vmCep.busy = false;
        vmCep.get = get;

        // 
        // Set address obj if is not provided
        if (!lodash.hasIn(vmCep, 'address')) vmCep.address = {};

        function get() {
            var cep = vmCep.model;

            if (vmCep.region === 'uk') {
                getPostcode(cep);
            } else {
                getCep(cep);
            }
        }

        function getCep(cep) {
            if (cep && cep.toString().length === 8) {
                var url = api.url + 'api/cep/';

                // 
                // Set busy
                vmCep.busy = true;

                // 
                // Request addr
                request(url, cep);
            }
        }

        function getPostcode(cep) {
            var url = api.url + 'api/postcode/';

            // 
            // Set busy
            vmCep.busy = true;

            // 
            // Request addr
            request(url, cep);
        }

        function request(url, cep) {
            // 
            // Hide all elements with hide-on-cep-search
            angular.element('.hide-on-cep-search').addClass('searching-cep');
            return $http.get(url + cep, {}).then(onSuccess, onError);
        }

        function onSuccess(response) {
            var cep = response.data;

            // 
            // Unset busy
            vmCep.busy = false;

            // 
            // Unhide elements
            angular.element('.hide-on-cep-search').removeClass('searching-cep');

            // 
            // Empty current address
            vmCep.address.street = '',
            vmCep.address.num = '',
            vmCep.address.bairro = '',
            vmCep.address.city = '',
            vmCep.address.state = '',

            // 
            // Extend found cep with model
            angular.extend(vmCep.address, cep);

            // 
            // Focus on number input if we have street
            if (cep.street) angular.element('.number-input').focus();
            else angular.element('.street-input').focus();
        }

        function onError(response) {
            // 
            // Unset busy
            vmCep.busy = false;

            // 
            // Unhide elements
            angular.element('.hide-on-cep-search').removeClass('searching-cep');

            // 
            // Show msg
            $page.toast($page.msgCepNotFound, 10000, 'bottom left');
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$BarAvatarCtrl', /*@ngInject*/ function($user, $menu, lodash) {
        var vm = this;

        vm.factory = $user;
        vm.menu = $menu;
        vm.logout = logout;

        // 
        // Current user
        vm.currentUser = vm.factory.getCurrent();

        // 
        // Show menu validation
        vm.avatarMenu = avatarMenu();

        function logout() {
            vm.factory.logout();
        }

        function avatarMenu() {

            // 
            // Set initial vars
            var menu = false,
                currentUser = vm.factory.getCurrent();

            // 
            // Validate currentUser
            if (!currentUser) return [];

            // 
            // Add user roles
            var userRoles = currentUser.model.role;

            // 
            // Filter sidemenu and check for allowed
            menu = lodash.filter(vm.menu.avatar, function(value) {

                // 
                // Get difference menu.role x userRoles
                var diff = _.difference(value.role, userRoles);

                // 
                // Menu is allowed when diff length is lower then value.role length
                return (diff.length < value.role.length);
            });

            // 
            // Return filtered menu
            return menu;
        }
    })
})();
(function() {
    'use strict';

    angular.module('mtda.directives').directive('barAvatar', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$BarAvatarCtrl',
            controllerAs: 'vm',
            bindToController: {
                pictureOrder: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/barAvatar/barAvatar.tpl.html';
            }
        };
    });

})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('BreadcrumbsCtrl', /*@ngInject*/ function($scope) {
        var vm = this;

        $scope.$watch('vm.data', function(nv, ov) {
            bootstrap();
        }, true);

        function bootstrap() {
            vm.data.slugTitle = vm.data.slug ? vm.data.slug.split('-').join(' ') : false;
            vm.data.categoryTitle = vm.data.category ? vm.data.category.split('-').join(' ') : (vm.data.title ? vm.data.title : false);
            vm.data.childTitle = vm.data.child ? vm.data.child.split('-').join(' ') : false;

            if (!vm.data.category) vm.data.category = undefined;
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('breadcrumbs', /*@ngInject*/ function($filter) {
        return {
            restrict: 'A',
            templateUrl: "core/directives/breadcrumbs/breadcrumbs.tpl.html",
            controller: 'BreadcrumbsCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                data: '='
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$CeperCtrl', /*@ngInject*/ function($http, $page, api, lodash) {
        var vmCep = this;
        vmCep.busy = false;
        vmCep.get = get;

        // 
        // Set address obj if is not provided
        if (!lodash.hasIn(vmCep, 'address')) vmCep.address = {};

        function get() {
            var cep = vmCep.model;

            if (vmCep.region === 'uk') {
                getPostcode(cep);
            } else {
                getCep(cep);
            }
        }

        function getCep(cep) {
            if (cep && cep.toString().length === 8) {
                var url = api.url + 'api/cep/';

                // 
                // Set busy
                vmCep.busy = true;

                // 
                // Request addr
                request(url, cep);
            }
        }

        function getPostcode(cep) {
            var url = api.url + 'api/postcode/';

            // 
            // Set busy
            vmCep.busy = true;

            // 
            // Request addr
            request(url, cep);
        }

        function request(url, cep) {
            // 
            // Hide all elements with hide-on-cep-search
            angular.element('.hide-on-cep-search').addClass('searching-cep');
            return $http.get(url + cep, {}).then(onSuccess, onError);
        }

        function onSuccess(response) {
            var cep = response.data;

            // 
            // Unset busy
            vmCep.busy = false;

            // 
            // Unhide elements
            angular.element('.hide-on-cep-search').removeClass('searching-cep');

            // 
            // Empty current address
            vmCep.address.street = '',
            vmCep.address.num = '',
            vmCep.address.bairro = '',
            vmCep.address.city = '',
            vmCep.address.state = '',

            // 
            // Extend found cep with model
            angular.extend(vmCep.address, cep);

            // 
            // Focus on number input if we have street
            if (cep.street) angular.element('.number-input').focus();
            else angular.element('.street-input').focus();
        }

        function onError(response) {
            // 
            // Unset busy
            vmCep.busy = false;

            // 
            // Unhide elements
            angular.element('.hide-on-cep-search').removeClass('searching-cep');

            // 
            // Show msg
            $page.toast($page.msgCepNotFound, 10000, 'bottom left');
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.directives.directive:ceper
     * @restrict EA
     * @description
     * Input para auto busca de cep
     * @element div
     * @param {object} model - model qye representa o campo numerico do cep
     * @param {object} address - model que representa os campos de endereço (street, district, city, state)
     * @param {string} endereço do server que deverá responder o json no formato esperado
     **/
    angular.module('mtda.directives').directive('ceper', /*@ngInject*/ function() {
        return {
            restrict: 'EA',
            templateUrl: 'core/directives/ceper/ceper.tpl.html',
            controller: '$CeperCtrl',
            controllerAs: 'vmCep',
            bindToController: {
                model: '=',
                address: '=',
                icon: '=',
                required: '=',
                region: '='
            }
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$CropperCtrl', /*@ngInject*/ function($scope, $element, $filter, $timeout, Upload) {
        var vm = this;

        // Default params
        vm.imageElem = false;
        vm.dataUrl = false;
        vm.cropper = false;
        vm.cropTimeout = false;

        // Cropper default options
        vm.options = {
            aspectRatio: 16 / 9,
            resultImage: {
                w: 160,
                h: 90
            },
            viewMode: 3,
            autoCropArea: 1,
            crop: function(data) {

                // Cancel timeout
                $timeout.cancel(vm.cropTimeout);

                // Timeout for setting result image model
                vm.cropTimeout = $timeout(function() {

                    // jQuery Cropped image
                    vm.resultImage = vm.imageElem.cropper('getCroppedCanvas', {
                        width: vm.options.resultImage.w,
                        height: vm.options.resultImage.h
                    }).toDataURL("image/jpeg", 0.99);

                    // JS Cropper image
                    // vm.resultImage = vm.cropper.getCroppedCanvas({
                    //     width: vm.options.resultImage.w,
                    //     height: vm.options.resultImage.h
                    // }).toDataURL("image/jpeg", 0.99);

                }, 200);
            }
        };

        // Extend default options with user options
        angular.extend(vm.options, vm.cropperOptions);

        // Watch for changes in the image
        $scope.$watch('vm.image', function(nv, ov) {
            if (nv || nv !== ov) {
                // We need some value
                if (!nv) return false;

                // 
                // Check if element is already a base64
                if (typeof nv === 'string' && nv.indexOf('data:image') != -1) return setDataAndStart(nv);

                //
                // Convert blob to base64
                Upload.dataUrl(nv, true).then(function(dataUrl) {
                    setDataAndStart(dataUrl);
                });
            }
        });

        function setDataAndStart(dataUrl) {
            // Set dataUrl
            vm.dataUrl = dataUrl;

            // Wait next angular digest
            $timeout(function() {
                // Start cropper
                start();
            });
        }

        function start(dataUrl) {
            // Set image element
            vm.imageElem = $element.find('img');

            // Destroy the jQuery Cropper
            if (vm.cropper) vm.cropper('destroy');

            // Destroy the JS Cropper
            // if (vm.cropper) vm.cropper.destroy();

            // Start the jQuery Cropper
            vm.cropper = vm.imageElem.cropper(vm.options);

            // Start the JS Cropper
            // vm.cropper = new Cropper(vm.imageElem[0], vm.options);
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('ngCropper', /*@ngInject*/ function($filter) {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/cropper/cropper.tpl.html",
            controller: '$CropperCtrl',
            controllerAs: 'vm',
            scope: {},
            bindToController: {
                image: '=',
                resultImage: '=',
                cropperOptions: '='
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('InnerLoadingCtrl', /*@ngInject*/ function() {
        var vm = this;
    })
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('innerLoading', /*@ngInject*/ function($filter) {
        return {
            restrict: 'A',
            templateUrl: "core/directives/innerLoading/innerLoading.tpl.html",
            controller: 'InnerLoadingCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                condition: '='
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('loader', /*@ngInject*/ function() {
        return {
            restrict: 'E',
            templateUrl: "core/directives/loader/loader.tpl.html"
        }
    })
})();
(function() {
    'use strict';

    angular
        .module('mtda.directives')
        .directive('sideMenu', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/sideMenu/sideMenu.tpl.html';
            },
            controller: '$SidenavMenuCtrl',
            controllerAs: 'vm',
            bindToController: {
                items: '='
            }
        };

        return directive;
    }

})();
(function() {
    'use strict';
    angular
        .module('mtda.directives')
        .directive('sidenav', sidenav);

    /** @ngInject */
    function sidenav() {
        var directive = {
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/sidenav/sidenav.tpl.html';
            },
            controller: NavController,
            controllerAs: 'vm',
            bindToController: true
        };
        return directive;

        /** @ngInject */
        function NavController($scope, $element, $mdSidenav, $state) {
            var vm = this;

            vm.adminIsLockedOpen = adminIsLockedOpen;

            // 
            // Admin is locked open
            function adminIsLockedOpen() {
                return ($state.current.toolbar === 'admin');
            }
        }
    }
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$SidenavMenuCtrl', controller);
    /*@ngInject*/
    function controller(lodash, $page, $user, $menu) {
        var vm = this;

        // 
        // Active menu
        vm.activeTab = activeTab;

        // 
        // set User
        vm.user = $user;

        // 
        // Set menu
        vm.menu = $menu;
        vm.sideMenu = sideMenu();
        // // 
        // // for ng-if
        // vm.showOwner = showOwner;
        // vm.showAdmin = showAdmin;

        function activeTab(tabGroup) {
            // 
            // Check against page config('tabGroup')
            return (tabGroup === $page.config.innerTabGroup);
        }

        // function toggleMenu() {
        //     vm.closed = vm.closed ? false : true;
        // }

        // function showOwner() {
        //     var user = vm.user.getCurrent();
        //     if (!user) return false;

        //     return (user.model.roles.isOwner || user.model.roles.isAdmin);
        // }

        // function showAdmin() {
        //     var user = vm.user.getCurrent();
        //     if (!user) return false;

        //     return (user.model.roles.isAdmin);
        // }

        function sideMenu() {

            // 
            // Set initial vars
            var menu = false,
                currentUser = vm.user.getCurrent();

            // 
            // Validate currentUser
            if (!currentUser) return [];

            // 
            // Add user roles
            var userRoles = currentUser.model.role;

            // 
            // Filter sidemenu and check for allowed
            menu = lodash.filter(vm.menu.side, function(value) {

                // 
                // Get difference menu.role x userRoles
                var diff = _.difference(value.role, userRoles);

                // 
                // Menu is allowed when diff length is lower then value.role length
                return (diff.length < value.role.length);
            });

            // 
            // Return filtered menu
            return menu;
        }
    }
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('sidenavMenu', /*@ngInject*/ function($filter) {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: "core/directives/sidenavMenu/sidenavMenu.tpl.html",
            controller: '$SidenavMenuCtrl',
            controllerAs: 'vm',
            bindToController: {
                area: '='
            }
        }
    })
})();
(function() {
    'use strict';

    angular.module('mtda.directives').directive('sidenavUserToolbar', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/sidenavUserToolbar/sidenavUserToolbar.tpl.html';
            },
            controller: NavController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function NavController($scope, $user, lodash) {
            var vm = this;
            vm.user = $user;
        }
    }

})();
(function() {
    'use strict';

    angular.module('mtda.directives').controller('$ToolbarCtrl', controller);

    /** @ngInject */
    function controller($scope, $element, $mdSidenav, $state, $timeout, $window, $user) {
        var vm = this;

        vm.searchMode = false;
        vm.sidenav = $mdSidenav;
        vm.toogleSearch = toogleSearch;
        vm.goToSearch = goToSearch;
        vm.term = '';
        vm.user = $user;
        vm.activeTab = activeTab;

        // $scope.$watch('vm.categories', watchCategories, true);
        vm.openMenu = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        //
        // scrollTo
        //
        vm.scrollTo = scrollTo;

        //
        // Open menu
        // 
        vm.openMenu = openMenu;

        //
        // Fixed on scroll
        // 
        var wdow = angular.element('.app-content'),
            currentState = false;

        // Bind window scroll if fixedOnScroll is set
        if (vm.fixedOnScroll) wdow.bind('scroll', navbarPosition);

        // Fix / unfix navbar
        function navbarPosition() {
            var elem = angular.element('md-toolbar.toolbar-default');

            if (!elem.length) {
                // 
                // Remove fixed from toolbar and wrapper
                elem.removeClass('fixed');
                wdow.removeClass('toolbar-fixed');

                // 
                // Reset currentstate
                currentState = false;

                // 
                // Remove binding
                wdow.unbind('scroll');

                return false;
            }

            var point = angular.element(".fixed-point"),
                pointTop = point.offset().top,
                pos = (pointTop - 0);

            if (currentState === false && pos <= 0) {
                currentState = 'add';
                elem.hide().slideDown('slow').addClass('fixed');
                wdow.addClass('toolbar-fixed');
            } else if (currentState === 'add' && pos > 0) {
                currentState = false;
                elem.hide().show().removeClass('fixed');
                wdow.removeClass('toolbar-fixed');
            }
        }

        function scrollTo(view) {
            var target = angular.element('#' + view);
            if (target.length) {
                wdow.animate({
                    scrollTop: target.offset().top
                }, 800, 'swing');
            }
        }

        function openMenu($mdOpenMenu, ev) {
            $mdOpenMenu(ev);
        }

        function toogleSearch(value) {
            // Focus product search input in the page for productSearch route
            if (value && $state.current.name === 'app.productSearch') {
                // Focus search input
                focusSearchPageInput();

                return;
            }

            vm.searchMode = value;

            if (value) {
                vm.term = '';

                $timeout(function() {
                    var input = $element.find('.search');
                    input.focus();
                }, 200);
            }
        }

        function goToSearch() {
            $state.go('app.productSearch', {
                term: vm.term
            });

            // Close search and focus product search page input
            $timeout(function() {
                // Clear search
                vm.term = '';

                // Close search
                vm.toogleSearch(false);

                // Focus search input
                focusSearchPageInput();
            }, 100);
        }

        function focusSearchPageInput() {
            var input = angular.element('.product-search form').find('input');
            input.focus();
        }

        function goToBanner(url) {
            $window.location.href = url;
        }

        function activeTab(slug) {
            return (
                (
                    $state.current.name == 'app.publicCategories' ||
                    $state.current.name == 'app.publicCategoriesEmpty'
                ) &&
                $state.params.category == slug
            );
        }
    }
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('toolbar', /*@ngInject*/ function($main) {
        return {
            scope: {},
            controller: '$ToolbarCtrl',
            controllerAs: 'vm',
            bindToController: {
                currentState: '=',
                categories: '=',
                fixedOnScroll: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : $main.toolbarUrl;
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$ToolbarAvatarCtrl', /*@ngInject*/ function($user, $menu) {
        var vm = this;

        vm.factory = $user;
        vm.menu = $menu;
        vm.logout = logout;

        // 
        // Current user
        vm.currentUser = vm.factory.getCurrent();

        // 
        // Show menu validation
        vm.showMenu = showMenu;

        function logout() {
            vm.factory.logout();
        }

        function showMenu(roles) {
            var invalid = false;

            // 
            // User must be logged in
            if (!vm.currentUser) return false;

            // 
            // Check each role
            if (roles) {
                angular.forEach(roles, function(val) {
                    // 
                    // This role can't be false
                    if (!vm.currentUser.model.roles[val]) invalid = true;
                });
            }

            // 
            // is invalid?
            if (invalid) return false;

            // 
            // Return true to show, since no other validation failed
            return true;
        }
    })
})();
(function() {
    'use strict';

    angular.module('mtda.directives').directive('toolbarAvatar', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$ToolbarAvatarCtrl',
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/toolbarAvatar/toolbarAvatar.tpl.html';
            }
        };
    });

})();
(function() {
    'use strict';
    angular.module('mtda.directives').controller('$ToolbarTabsCtrl', controller);
    /*@ngInject*/
    function controller($menu, $user, lodash, $page, $state, $timeout) {
        var vm = this;
        vm.menu = $menu;
        vm.user = lodash.hasIn($user, '_current') ? $user._current : false;
        vm.activeTab = activeTab;
        vm.goTo = goTo;
        vm.init = false;

        $timeout(function() {
            vm.init = true;
        }, 100);

        // 
        // Permission check
        vm.permissions = {
            admin: vm.user && lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'admin')) ? true : false,
            owner: vm.user && lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'owner')) ? true : false,
            user: vm.user && lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'user')) ? true : false,
        }

        function activeTab(tabGroup) {
            // 
            // Check against page config('tabGroup')
            return (tabGroup === $page.config.tabGroup);
        }

        function goTo(route) {
            if (!vm.init) return false;

            $state.go(route, {
                inherit: false
            });
            // // 
            // // Cancel previus timeout
            // if (vm.goToTimeout) $timeout.cancel(vm.goToTimeout);

            // // 
            // // Check if this is not the current route already
            // vm.goToTimeout = $timeout(function() {
            //     if ($state.current.name !== route)
            //         $state.go(route, {
            //             inherit: false
            //         });
            // }, 100);
        }
    }
})();
(function() {
    'use strict';
    angular.module('mtda.directives').directive('toolbarTabs', /*@ngInject*/ function($filter) {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/toolbarTabs/toolbarTabs.tpl.html",
            controller: '$ToolbarTabsCtrl',
            controllerAs: 'vm',
            bindToController: true
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.list').controller('ListContentCtrl', /*@ngInject*/ function(setting, api) {
        var vm = this;

        // Get entries
        vm.service.filterUpdated(true);
        vm.uploadFolder = api.url + 'images/uploads/';
    })
})();
(function() {
    'use strict';
    angular.module('mtda.list').directive('listContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                service: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/content/listContent.tpl.html';
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.list').controller('ListFilterBoxCtrl', /*@ngInject*/ function() {})
})();
(function() {
    'use strict';
    angular.module('mtda.list').directive('listFilterBox', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListFilterBoxCtrl',
            controllerAs: 'vm',
            bindToController: {
                listFilters: '=',
                listBrStates: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/filter-box/listFilterBox.tpl.html';
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.list').controller('ListLoadMoreCtrl', /*@ngInject*/ function() {});
})();
(function() {
    'use strict';
    angular.module('mtda.list').directive('listLoadMore', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListLoadMoreCtrl',
            controllerAs: 'vm',
            bindToController: {
                service: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/load-more/listLoadMore.tpl.html';
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.list').controller('ListSearchBoxCtrl', /*@ngInject*/ function() {
    });
})();
(function() {
    'use strict';
    angular.module('mtda.list').directive('listSearchBox', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListSearchBoxCtrl',
            controllerAs: 'vm',
            bindToController: {
                service: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/search-box/listSearchBox.tpl.html';
            }
        };
    });
})();
(function() {
    'use strict';

    angular
        .module('mtda.directives')
        .directive('addrDisplay', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/order/addrDisplay/addrDisplay.tpl.html';
            },
            controller: $addrDisplayCtrl,
            controllerAs: 'vm',
            bindToController: {
                address: '=',
                person: '=',
                formLocale: '='
            }
        };

        return directive;

        function $addrDisplayCtrl() {

        }
    }

})();
(function() {
    'use strict';
    angular.module('mtda.order').controller('$PublicOrderCtrl', /*@ngInject*/ function($List, $order, $Order, $state, $page, $user) {
        var vm = this;

        // 
        // Set user
        vm.user = $user.getCurrent();

        // 
        // Set factory
        vm.factory = $order;

        // 
        // Set order id or redirect
        vm.order = $state.params._id;
        if (!vm.order) $state.go('app.orders');

        // 
        // Get order or redirect
        vm.factory.get(vm.order, {
            user: vm.user.model._id
        }).then(success).catch(failed);

        // 
        // Set form locale
        vm.formLocale = $page.formLocale;

        // 
        // Sucess, set service
        function success(data) {
            // 
            // User validation
            if (data.user._id !== vm.user.model._id) {
                $state.go('app.public-account.orders');
                return false;
            }

            // 
            // Set service
            vm.service = new $Order.service({
                model: data
            });

            setFilterStatus();
        }

        // 
        // No order, redirect
        function failed() {
            $state.go('app.public-account.orders');
        }

        // 
        // Remove all from status select list
        function setFilterStatus() {
            // 
            // Get array from provider, but use angular.copy to remove bindings
            var entries = angular.copy(vm.factory.filterStatus);

            // 
            // Remove "all" options which is in 0 index
            entries.splice(0, 1);

            // 
            // Set
            vm.filterStatus = entries;
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.order').controller('$PublicOrdersCtrl', /*@ngInject*/ function($List, $order, $state, $user) {
        var vm = this;

        // 
        // Set factory
        vm.factory = $order;

        // List
        vm.params = {
            source: source,
            secondaryActionType: 'delete',
            secondaryActionSource: $order.remove,
            addAction: false,
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            filters: {
                owner: true,
                sort: '-createdAt',
                status: $state.params.status ? $state.params.status : false,
                paymode: $state.params.paymode ? $state.params.paymode : false
            },
            sortOptions: vm.factory.sort,
            filterPaymode: vm.factory.filterPaymode,
            filterStatus: vm.factory.filterStatus
        };

        function primaryAction(item) {
            $state.go('app.public-account.order', item, {
                inherit: false
            });
        }

        function source(params) {
            // 
            // Current user
            vm.user = $user.getCurrent();

            // 
            // Add user
            params.user = vm.user.model._id;

            return $order.getAll(params);
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProdFormCtrl', /*@ngInject*/ function($state, $stateParams, $Product, $product, $category) {
        var vm = this;

        // 
        // Set edit
        vm.edit = $stateParams.id ? $stateParams.id : false;

        // 
        // Product factory
        vm.factory = $product;

        //
        // Instantiate product
        setService();

        //
        // Submit and save
        vm.submit = submit;

        //
        // Autocomplete
        vm.querySearch = vm.factory.querySearch.bind($category);

        // 
        // Set product service
        function setService() {
            // 
            // create mode
            if (!vm.edit) {
                // 
                // new model
                newModel();
            }
            // 
            // update mode
            else {

            }
        }

        // 
        // New model
        function newModel() {
            return vm.service = new $Product.service({
                model: {
                    categories: [],
                    variants: {
                        attrs: []
                    },
                    picFile: null
                }
            });
        }

        // 
        // Submit form
        function submit() {
            // Send contact to server with recaptcha
            vm.service.create().then(success, fail);

            function success() {
                // Clean model
                vm.service.model = null;

                // Redirect listing
                $state.go('app.products');
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').directive('prodForm', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$ProdFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                id: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/prodForm/prodForm.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductSliderCtrl', /*@ngInject*/ function($scope, lodash) {
        var vm = this;
        vm.selected = 0;

        vm.value = function(key) {
            console.log('key', key);
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').directive('productSlider', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$ProductSliderCtrl',
            controllerAs: 'vm',
            bindToController: {
                images: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/productSlider/productSlider.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProdViewCtrl', /*@ngInject*/ function() {
        var vm = this;
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').directive('prodView', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$ProdViewCtrl',
            controllerAs: 'vm',
            bindToController: {
                entries: '=',
                category: '=',
                child: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/prodView/prodView.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$PublicProductCtrl', /*@ngInject*/ function($scope, $user, $List, $product, $stateParams, $state) {
        var vm = this;
        if (!$stateParams.id) $state.go('app.home');

        vm.breadcrumbs = {
            slug: $stateParams.slug,
            slugId: $stateParams.id,
            category: $stateParams.category,
            child: $stateParams.child
        };

        vm.factory = $product;
        vm.product = false;

        vm.url = window.location;

        vm.factory.getPublic($stateParams.id).then(function(data) {
            vm.product = data.product;
            vm.variants = data.variants;
        });
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$PublicProductsCtrl', /*@ngInject*/ function($List, $product, $state) {
        var vm = this;

        // List
        vm.params = {
            infinite: false,
            source: $product.getList,
            filters: {
                sort: '-price',
                term: $state.params.term
            },
            sortOptions: [{
                name: 'Nome (A-Z)',
                value: 'name'
            }, {
                name: 'Nome (Z-A)',
                value: '-name'
            }, {
                name: 'Maior preço',
                value: '-price'
            }, {
                name: 'Menor preço',
                value: 'price'
            }]
        };

        vm.service = new $List.service(vm.params);
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').service('$Variant', /*@ngInject*/ function($http, $page, $state, $q, api, Busy) {

        var url = api.url + 'api/variants/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = {
                attrs: []
            };

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;

        function create() {
            // Are we busy?
            if (this.busy.start('create')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgCreated);
                        this.busy.end('create');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('create');

                        return response;
                    }.bind(this));
        }

        function update() {
            // Are we busy?
            if (this.busy.start('update')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(url + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('update');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('update');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantEditCtrl', /*@ngInject*/ function($scope, $Variant, $variant, $product, $state, lodash, $page) {
        var vm = this;

        vm.factory = $variant;
        vm.product = false;

        //
        // Submit and save
        vm.submit = submit;

        $scope.$watch('vm.service.model.attrs', watchVariants, true);

        vm.factory.getOptions($state.params._id, $state.params.item).then(function(data) {
            //
            // Need product
            if (!data.entry) {
                $page.toast('Um dos atributos não existe mais, adicione novamente ou exclua esta variação');
                $state.go('app.product-form.variant', {
                    inherit: false
                });
            }

            //
            // Set product
            vm.product = data.entry;

            // 
            // Set variant
            vm.service = new $Variant.service({
                model: data.item
            });
        });

        function watchVariants(nv, ov) {
            var name = '';
            var check = false;

            if (nv !== ov) {
                var value = lodash.chain(nv).mapKeys(function(value, key) {
                    return value.name;
                }).mapValues(function(o) {

                    if (check) {
                        name += ',';
                    } else {
                        check = true
                    }

                    name += o.name + ':' + o.value;
                    return o.value;
                }).value();

                if (lodash.hasIn(vm.service.model, 'name')) vm.service.model.name = name;
            }
        }

        function submit() {
            // Send contact to server with recaptcha
            vm.service.update().then(success, fail);

            function success() {}

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantNewCtrl', /*@ngInject*/ function($scope, $Variant, $variant, $product, $state, lodash) {
        var vm = this;

        vm.factory = $variant;
        vm.service = new $Variant.service();
        vm.product = false;

        //
        // Submit and save
        vm.submit = submit;

        $scope.$watch('vm.service.model.attrs', watchVariants, true);

        vm.factory.getOptions($state.params._id, false).then(function(data) {
            vm.product = data.entry;
            vm.service.model.product = vm.product;
            vm.service.model.name = null;

            angular.forEach(vm.product.variants.attrs, function(value, key) {
                vm.service.model.attrs.push({
                    name: value.name,
                    value: null
                });
            });
        });

        function watchVariants(nv, ov) {
            var name = '';
            var check = false;

            if (nv !== ov) {
                var value = lodash.chain(nv).mapKeys(function(value, key) {
                    return value.name;
                }).mapValues(function(o) {

                    if (check) {
                        name += ',';
                    } else {
                        check = true
                    }

                    name += o.name + ':' + o.value;
                    return o.value;
                }).value();

                if (lodash.hasIn(vm.service.model, 'name')) vm.service.model.name = name;
            }
        }

        function submit() {
            // Send contact to server with recaptcha
            vm.service.create().then(success, fail);

            function success() {
                // Clean model
                vm.service.model = null;

                // Redirect listing
                $state.go('app.product-form.variant', {
                    _id: $state.params._id
                });
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantsCtrl', /*@ngInject*/ function($List, $variant, $state) {
        var vm = this;

        vm.factory = $variant;
        vm.productId = $state.params._id;

        // List
        vm.params = {
            source: getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: vm.factory.remove,
            addAction: 'app.product-form.variant-new',
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            showAvatar: false,
            productId: vm.productId
        };

        function primaryAction(item) {
            $state.go('app.product-form.variant-edit', {
                _id: vm.productId,
                item: item._id
            }, {
                inherit: false
            });
        }

        function getAll(params) {
            params.product = $state.params._id;
            return vm.factory.getAll(params);
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantFormCtrl', /*@ngInject*/ function($scope, lodash, $translate) {
        var vm = this;

        vm.selected = [];
        vm.querySearchVariations = querySearchVariations;
        vm.transformChip = transformChip;
        vm.loading = true;

        // Load if we have data
        if (lodash.hasIn(vm.model, 0)) {
            var model = lodash.chain(vm.model).find({
                name: vm.name
            }).pick(['name', 'options', 'status']).value();

            if (model) {
                vm.status = vm.status === false ? false : true;
                vm.selected = model.options;
            }
        }

        // Set loading to false
        vm.loading = false;

        $scope.$watch('vm.status', function(nv, ov) {
            if (nv !== ov && !vm.loading) {
                // Selected
                if (nv) {
                    vm.selected = vm.selected ? vm.selected : [];
                    vm.model.splice(vm.position, 0, {
                        name: vm.name,
                        options: vm.selected
                    });
                } else {
                    vm.model.splice(vm.position, 1);
                    vm.selected = [];
                }
            }
        }, true);

        function transformChip(chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }
            // Otherwise, create a new one
            return {
                name: chip,
                type: 'new'
            }
        }

        function querySearchVariations(query) {
            var arr = vm.entries;

            if (!Array.isArray(arr)) return false;

            var results = query ? arr.filter(createFilterFor(query)) : [];
            return results;
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(value) {

                var lang = $translate.use();

                return (value.value.indexOf(lowercaseQuery) === 0) ||
                    (value.value.indexOf(lowercaseQuery) === 0) ||
                    (value[lang] && value[lang].indexOf(lowercaseQuery) === 0);
            };
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.product').directive('variantForm', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$VariantFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                name: '@',
                entries: '=',
                model: '=',
                position: '@',
                lang: '=',
                title: '@',
                status: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/variantForm/variantForm.tpl.html";
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$LoginCtrl', /*@ngInject*/ function() {
        var vm = this;
    })
})();
(function() {
    'use strict';

    angular.module('mtda.user').controller('$RecoveryCtrl', /*@ngInject*/ function($userHttp, $location, $state) {
        var vm = this;

        // 
        // Set hash
        vm.hash = $location.hash() ? $location.hash() : false;
        vm.confirmed = false;

        // 
        // Factory
        vm.factory = $userHttp;

        // 
        // submitRecoveryLink
        vm.submitRecoveryLink = submitRecoveryLink;

        // 
        // submitRecoveryPassword
        vm.submitRecoveryPassword = submitRecoveryPassword;

        // 
        // Validate hash if provided
        if (vm.hash) confirmHash();

        // 
        // Submit recovery link
        // send recovery link to email
        function submitRecoveryLink() {
            vm.factory.recoveryLink(vm.email);
        }

        // 
        // Submit recovery password
        // change password
        function submitRecoveryPassword() {
            vm.factory.recoveryPassword(vm.hash, vm.password).then(function() {

                // 
                // Send to login
                $state.go('app.login');
            });
        }

        // 
        // Confirm hash
        function confirmHash() {
            vm.factory.confirmHash(vm.hash).then(function(data) {

                // 
                // Set confirmed
                vm.confirmed = true;

            }, function(error) {

                // 
                // Set confirmed
                vm.confirmed = false;

                // 
                // Redirect user to login
                $state.go('app.login');
            });
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$RegisterCtrl', /*@ngInject*/ function($auth, $page, $User, setting, api, $location, $user, $RestoreUser) {
        // @todo - colocar isso na rota
        if ($auth.isAuthenticated()) $location.path('/');

        var vm = this;
    })
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserListCtrl', /*@ngInject*/ function($scope, $userHttp, lodash, $page) {
        var list = this,
            bookmark;

        // 
        // Set factory to get entries
        list.factory = $userHttp;

        // 
        // Table options
        list.options = {
            rowSelection: true,
        };

        // 
        // Selected items
        list.selected = [];

        // 
        // Query
        list.query = {
            order: '-createdAt',
            limit: 5,
            page: 1,
            filters: '',
            owner: true
        };

        // 
        // Get all service
        list.getAll = getAll;

        // 
        // Remove service
        list.remove = remove;

        // 
        // Filters 
        list.filters = {
            show: false,
            setShow: setShow,
            options: {
                debounce: 500
            }
        };

        // 
        // is owner
        list.isOwner = isOwner;

        // 
        // Watch for changes in filter
        $scope.$watch('list.query.filters', watchQueryFilters);

        // 
        // Get entries
        list.getAll();

        // 
        // Success callback
        function success(data) {

            // 
            // Clean selected
            list.selected = [];

            // 
            // Set entries
            list.data = data;
        }

        // 
        // Get all service
        function getAll() {
            // 
            // Set promise and call
            list.promise = list.factory.getAll(list.query).then(success);
        };

        // 
        // Remove items
        function remove(e) {

            // 
            // Get array of ids
            var ids = _.map(list.selected, '_id');

            // 
            // Confirmation
            $page.showAlertConfirm(e, 'Excluir usuário', 'Deseja realmente excluir ' + ids.length + ' usuário(s)?', 'Excluir', 'Cancelar', function() {

                // 
                // Confirmed, exclude
                list.promise = list.factory.remove(e, ids).then(getAll);
            }, function() {

                // 
                // Canceled, clean selected
                list.selected = [];
            });
        }

        // 
        // Filters - setShow
        function setShow(status) {
            list.filters.show = status;
        }

        // 
        // Watch for query filter changes
        function watchQueryFilters(newValue, oldValue) {
            if (!oldValue) {
                bookmark = list.query.page;
            }

            if (newValue !== oldValue) {
                list.query.page = 1;
            }

            if (!newValue) {
                list.query.page = bookmark;
            }

            list.getAll();
        }

        // 
        // Check if is owner
        function isOwner(role) {
            return lodash.find(role, function(o) {
                return o === 'owner';
            });
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name app.project.directive:userList
     * @restrict EA
     * @description
     * Listagem de proejtos
     * @element div
     **/
    angular.module('mtda.user').directive('userList', /*@ngInject*/ function() {
        return {
            restrict: 'EA',
            templateUrl: 'core/user/user-list/user-list.tpl.html',
            controller: '$UserListCtrl',
            controllerAs: 'list',
            bindToController: {}
        }
    });
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserEditCtrl', /*@ngInject*/ function($state, $stateParams, $User, $userHttp, $element, lodash) {
        var vm = this;

        //
        // Set edit
        vm.edit = true;

        // 
        // Project factory
        vm.factory = $userHttp;

        // 
        // Current url
        vm.redirectUrl = ($state.current.name === 'app.edit-user') ? 'app.users' : 'app.config.users';

        //
        // Get and instantiate
        vm.factory.get($state.params._id).then(function(data) {
            // 
            // Check data
            if (!data) $state.go(vm.redirectUrl);

            // 
            // Service
            vm.service = new $User.service({
                model: data
            });
        });

        //
        // Submit and save
        vm.submit = submit;

        // 
        // Multiselect de grupos
        vm.groups = [{
            label: 'Usuário',
            value: 'user'
        }, {
            label: 'Editor',
            value: 'editor'
        }];

        vm.isEditorOrUser = isEditorOrUser;

        function submit() {
            // Update entry
            vm.service.update(false, true);
        }

        function isEditorOrUser(model) {
            return !model.role.length || lodash.find(model.role, function(o) {
                console.log('o', o);
                return o === 'user' || o === 'editor';
            });
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserNewCtrl', /*@ngInject*/ function($state, $stateParams, $User, $user) {
        var vm = this;

        // 
        // Project factory
        vm.factory = $user;

        //
        // Instantiate product
        vm.service = new $User.service({
            model: {
                account: {
                    owner: $user.current().model
                },
                role: ['user']
            }
        });

        // 
        // Current url
        vm.redirectUrl = ($state.current.name === 'app.new-user') ? 'app.users' : 'app.config.users';

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // 
            // Create entry, local provider and noLogin
            vm.service.create(false, true, false).then(success, fail);

            function success() {
                // 
                // Clean model
                vm.service.model = null;

                // 
                // Redirect listing
                $state.go(vm.redirectUrl);
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserNewDialogCtrl', /*@ngInject*/ function($state, $stateParams, $user, $User, $mdDialog, $timeout) {
        var vm = this;

        // 
        // Focus first element
        $timeout(function() {
            var elem = angular.element('.project-type-form-dialog input:first');
            elem.focus();
        }, 100);

        // 
        // Factory
        vm.factory = $user;

        //
        // Instantiate product
        vm.service = new $User.service({
            model: {
                account: {
                    owner: vm.factory.current().model
                }
            }
        });

        vm.cancel = cancel;
        vm.save = save;

        function cancel() {
            $mdDialog.cancel();
        }

        function save() {
            // 
            // Create entry, local provider and noLogin
            vm.service.create(false, true, false).then(success, fail);

            function success(response) {
                // Clean model
                vm.service.model = null;

                // Close dialog
                $mdDialog.hide(response.data);
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';

    angular.module('mtda.user').controller('$UsersCtrl', /*@ngInject*/ function() {
        var vm = this;
    });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutConfirmationCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutIdentificationCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutPaymentCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutPaymentDetailsCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutShippingDetailsCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();
(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutShippingMethodCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$LoginFormCtrl', /*@ngInject*/ function($auth, $page, $user, $User, setting, api, $RestoreUser) {
        var vm = this;
        vm.user = new $User.service();

        vm.submit = submit;

        function submit() {
            // Send contact to server with recaptcha
            vm.user.login(vm.redirect).then(success, fail);

            function success(response) {
                // 
                // Retora user data
                $RestoreUser.restore(response.data.user);

                // 
                // cb?
                if (vm.cb && typeof vm.cb === 'function') vm.cb(response.data.user);
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.user').directive('loginForm', /*@ngInject*/ function() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: "core/user/login/form/loginForm.tpl.html",
            controller: '$LoginFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                redirect: '=',
                cb: '='
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.user').controller('$RegisterFormCtrl', /*@ngInject*/ function($User, $location, $user, $RestoreUser, $localStorage) {
        var vm = this;

        vm.sign = {};

        vm.user = new $User.service();
        vm.user.provider = 'local';

        vm.submit = submit;

        if (vm.setRole) {
            if (!vm.user.model) vm.user.model = {};
            vm.user.model.role = vm.setRole;
        }

        function submit() {
            // Send contact to server with recaptcha
            vm.user.create('local', false, vm.redirect).then(success, fail);

            function success(response) {
                // 
                // Redirect
                if (!vm.redirect) {
                    // 
                    // Any previus location registered?
                    if ($localStorage.redirectOnLogin) {
                        // 
                        // Redirect user to previus location
                        $location.path($localStorage.redirectOnLogin);

                        // 
                        // Delete previus location
                        delete $localStorage.redirectOnLogin;

                        // 
                        // Redirect for custmer
                    } else if (response.data.user.roles.isUser) $location.path('/customer/');

                    // 
                    // Redirect for admin
                    else if (response.data.user.roles.isAdmin) $location.path('/accounts/');

                    // 
                    // Redirect for owners
                    else if (response.data.user.roles.isOwner || response.data.user.roles.isEditor) $location.path('/admin/');

                    // 
                    // Redirect for any other situation
                    else $location.path('/customer/');
                } else {
                    // 
                    // Custom redirect from directive
                    $location.path(vm.redirect);
                }

                // 
                // Retora user data
                $RestoreUser.restore(response.data.user);

                // 
                // cb?
                if (vm.cb && typeof vm.cb === 'function') vm.cb(response.data.user);

                return response.data.user;
            }

            function fail() {}
        }
    })
})();
(function() {
    'use strict';
    angular.module('mtda.user').directive('registerForm', /*@ngInject*/ function() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: "core/user/register/form/registerForm.tpl.html",
            controller: '$RegisterFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                redirect: '=',
                cb: '=',
                setRole: '='
            }
        }
    })
})();
angular.module("mtda.app").run(["$templateCache", function($templateCache) {$templateCache.put("core/404/404.tpl.html","<div class=\"four-oh-four\">\r\n	<div class=\"icon\">\r\n		<md-icon md-font-set=\"material-icons\" md-font-content=\"info_outline\"></md-icon>\r\n	</div>\r\n	<div class=\"title\">404</div>\r\n	<div class=\"subtitle\">Página não encontrada, retorne para a página principal e tente novamente.</div>\r\n	<md-button class=\"md-raised\" href=\"/\"><md-icon md-font-set=\"material-icons\" md-font-content=\"home\"></md-icon> Página principal</md-button>\r\n</div>");
$templateCache.put("core/account/account.tpl.html","<div class=\"wrapper account-profile panel\" loading=\"{{vm.factory.busy.status(\'get\')}}\" editable=\"{{vm.editable}}\">\r\n	<div class=\"loading\" layout layout-align=\"center center\">\r\n		<md-progress-circular md-mode=\"indeterminate\" class=\"md-accent md-hue-3\"></md-progress-circular>\r\n	</div>\r\n	<div class=\"prof-content\">\r\n		<div class=\"prof-header\">\r\n			<div layout layout-align=\"start center\">\r\n				<div\r\n					class=\"prof-picture\"\r\n					ng-click=\"vm.editable ? vm.updateProfilePicture() : false\"\r\n					accept=\"image/*\"\r\n					md-ink-ripple>\r\n					\r\n					<img\r\n					class=\"md-avatar\"\r\n					alt=\"{{ vm.service.model.profile.firstName }}\"\r\n					ng-src=\"{{vm.service.model.image.thumb ? vm.service.model.image.thumb : \'../assets/images/user-avatar.jpg\' }}\">\r\n					\r\n					<md-icon md-font-set=\"material-icons\" md-font-content=\"camera_alt\" ng-if=\"vm.editable\"></md-icon>\r\n				</div>\r\n				\r\n				<span class=\"flex-sep\"></span>\r\n				\r\n				<div class=\"prof-header-details\" layout=\"column\" layout-align=\"center start\" flex>\r\n					<h3 class=\"md-title\">{{vm.service.model.profile.fullName}}</h3>\r\n					<p class=\"md-caption\">\r\n						adicionado\r\n						<span am-time-ago=\"vm.service.model.createdAt\"></span>\r\n						<md-tooltip>\r\n						{{vm.service.model.createdAt | customMoment:\'DD/MM/YYYY HH:mm:ss\'}}\r\n						</md-tooltip>\r\n					</p>\r\n					\r\n					<md-button class=\"md-fab md-mini\" ui-sref=\"app.edit-user(vm.service.model)\" aria-label=\"Editar perfil\">\r\n						<md-icon md-font-set=\"material-icons\">edit</md-icon>\r\n					</md-button>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"prof-details\">\r\n			<md-list>\r\n				<md-list-item>\r\n					<md-icon md-font-set=\"material-icons\" md-font-content=\"face\"></md-icon>\r\n					<p>{{vm.service.model.profile.fullName}}</p>\r\n				</md-list-item>\r\n				\r\n				<md-list-item>\r\n					<md-icon md-font-set=\"material-icons\" md-font-content=\"mail\"></md-icon>\r\n					<p>{{vm.service.model.email}}</p>\r\n				</md-list-item>\r\n				\r\n				<md-list-item>\r\n					<md-icon md-font-set=\"material-icons\" md-font-content=\"phone\"></md-icon>\r\n					<p>\r\n						<span ng-if=\"vm.service.model.profile.contact.mobile\">{{vm.service.model.profile.contact.mobile | brPhoneNumber}}</span>\r\n						<span ng-if=\"vm.service.model.profile.contact.mobile && vm.service.model.profile.contact.mobile\"> - </span>\r\n						<span ng-if=\"vm.service.model.profile.contact.phone\">{{vm.service.model.profile.contact.phone | brPhoneNumber}}</span>\r\n					</p>\r\n				</md-list-item>\r\n				\r\n				<md-list-item ng-if=\"vm.editable\">\r\n					<md-icon class=\"danger\" md-font-set=\"material-icons\" md-font-content=\"lock_outline\"></md-icon>\r\n					<p>\r\n						<a class=\"danger\" ui-sref=\"app.account-password({_id: vm.service.model._id})\">Alterar minha senha</a>\r\n					</p>\r\n				</md-list-item>\r\n			</md-list>\r\n		</div>\r\n		<div class=\"act-feed\" layout>\r\n			<list\r\n				params=\"vm.params\"\r\n				template-url=\"app/activity/list.tpl.html\"\r\n				class=\"list-default list-manual list-activity\"\r\n				flex flex-xs=\"none\">\r\n			</list>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/account/accounts.tpl.html","<div class=\"wrapper\">\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">{{\'ACCS_TAB_LINK\' | translate}}</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n	<div account-list></div>\r\n</div>");
$templateCache.put("core/account/accountsForm.tpl.html","<div class=\"wrapper\">\r\n	\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">{{\'ACCS_TAB_LINK\' | translate}}</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n	\r\n	<div class=\"panel\">\r\n		<div class=\"panel-heading\">\r\n			<div class=\"panel-title\">\r\n				<md-icon md-font-set=\"material-icons\" md-font-content=\"supervisor_account\"></md-icon> {{vm.edit ? \'Editar\' : \'Nova\'}} conta\r\n			</div>\r\n		</div>\r\n		\r\n		<!-- LOADING -->\r\n		<div class=\"loading\" ng-show=\"vm.factory.busy.status(\'get\')\">\r\n			<p layout layout-align=\"center center\">\r\n				<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n			</p>\r\n		</div>\r\n		\r\n		<!-- FORM -->\r\n		<form name=\"form\" class=\"form form-product wrapper content\" ng-if=\"!vm.edit || vm.edit && vm.service.model\">\r\n			<div class=\"panel-body\">\r\n				\r\n				<p class=\"title\">Informações de login</p>\r\n				<div person-form-content model=\"vm.service.model.profile\"></div>\r\n				<div layout layout-sm=\"column\">\r\n					<md-input-container flex flex-xs=\"none\">\r\n						<label>{{\'EMAIL\' | translate}}</label>\r\n						<md-icon md-font-set=\"material-icons\">mail</md-icon>\r\n						<input ng-model=\"vm.service.model.email\" required>\r\n					</md-input-container>\r\n				</div>\r\n				\r\n				<input type=\"hidden\" ng-model=\"vm.service.model.password\">\r\n				\r\n				<div company-form model=\"vm.service.model.company\"></div>\r\n				\r\n				<p class=\"title\">Plano atual</p>\r\n				<div layout layout-sm=\"column\">\r\n					<md-input-container>\r\n						<label>Usuários</label>\r\n						<md-icon md-font-set=\"material-icons\">phonelink_setup</md-icon>\r\n						<input ng-model=\"vm.service.model.account.users\" type=\"number\" required>\r\n					</md-input-container>\r\n\r\n					<md-input-container>\r\n						<label>Situação</label>\r\n						<md-select ng-model=\"vm.service.model.account.active\" required>\r\n							<md-option ng-value=\"true\" ng-selected=\"vm.service.model.account.active === true\">\r\n								Ativo\r\n							</md-option>\r\n							<md-option ng-value=\"false\" ng-selected=\"vm.service.model.account.active === false\">\r\n								Inativo\r\n							</md-option>\r\n						</md-select>\r\n					</md-input-container>\r\n				</div>\r\n\r\n				<div layout layout-align=\"start center\">\r\n					<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'save\')\"></md-progress-circular>\r\n					<md-button ng-disabled=\"vm.service.formStatus(\'save\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"clean-left md-raised md-primary\" ng-if=\"!vm.service.busy.status(\'save\')\">\r\n						{{\'SAVE\' | translate}}\r\n						\r\n						<md-tooltip>\r\n						{{\'SAVE\' | translate}}\r\n						</md-tooltip>\r\n					</md-button>\r\n					<md-button ng-disabled=\"vm.service.busy.status(\'save\')\" ui-sref=\"app.accounts\" class=\"md-accent\">\r\n						{{\'CANCEL\' | translate}}\r\n						\r\n						<md-tooltip>\r\n						{{\'CANCEL\' | translate}}\r\n						</md-tooltip>\r\n					</md-button>\r\n				</div>\r\n			</div>\r\n		</form>\r\n		<!-- /FORM -->\r\n	</div>\r\n</div>");
$templateCache.put("core/account/list.tpl.html","<div class=\"md-whiteframe-1dp account\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/account/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/account/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<div class=\"list-content no-action hide-loading hide-starting\">\r\n    <div ng-repeat=\"item in vm.service.entries\" ng-controller=\"$AccountsItemCtrl as itemCtrl\">\r\n        <div class=\"list-item\" layout=\"column\" layout-sm=\"row\" layout-gt-sm=\"row\">\r\n            <div class=\"list-inner-content\" layout=\"row\" flex-sm flex-gt-sm>\r\n                <p>{{ item[vm.service.extra.listTitle] }}</p>\r\n            </div>\r\n            <div class=\"list-actions\" layout layout-align=\"end center\" layout-align-xs=\"start center\">\r\n                <md-button ng-click=\"vm.service.primaryAction(item, $event)\">Editar</md-button>\r\n                <md-switch ng-model=\"itemCtrl.model.account.active\" ng-change=\"itemCtrl.toggleActive()\" aria-label=\"{{\'CHANGE_STATUS\' | translate}}\"></md-switch>\r\n                <md-button aria-label=\"Delete\" class=\"md-icon-button\">\r\n                    <md-icon md-font-set=\"material-icons\" ng-click=\"vm.service.secondaryAction($event, item, $index)\" aria-label=\"Open Chat\" class=\"md-hue-3\">delete</md-icon>\r\n                </md-button>\r\n            </div>\r\n        </div>\r\n        \r\n        <md-divider></md-divider>\r\n    </div>\r\n    <div\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </div>\r\n</div>");
$templateCache.put("core/banner/banner.tpl.html","<div class=\"wrapper\">\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">Banner</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n	\r\n	<list\r\n		params=\"vm.params\"\r\n		class=\"list-default\">\r\n	</list>\r\n</div>");
$templateCache.put("core/banner/bannerNew.tpl.html","<br>\r\n<!-- FORM -->\r\n<form name=\"form\" class=\"form wrapper\">\r\n	<div class=\"form-box basic\">\r\n		<div>\r\n			<p class=\"title\">{{\'BASIC_DETAILS\' | translate}}</p>\r\n		</div>\r\n		<div class=\"box\">\r\n			<div layout layout-xs=\"column\">\r\n				<md-input-container flex>\r\n					<label>{{\'TITLE\' | translate}}</label>\r\n					<input ng-model=\"vm.banner.model.title\" required>\r\n				</md-input-container>\r\n				<md-input-container flex>\r\n					<label>URL</label>\r\n					<input ng-model=\"vm.banner.model.url\" required>\r\n				</md-input-container>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class=\"form-box basic\">\r\n		<div>\r\n			<p class=\"title\">{{\'IMAGE_UPLOAD\' | translate}}</p>\r\n		</div>\r\n		<div class=\"box\">\r\n			<div layout=\"column\">\r\n				\r\n				<md-button\r\n					class=\"md-raised clean-left\"\r\n					ngf-select\r\n					ng-model=\"vm.banner.model.picFile\"\r\n					accept=\"image/*\">\r\n					{{\'CHOOSE_FILE\' | translate}}\r\n				</md-button>\r\n				\r\n				<br />\r\n				\r\n				<div\r\n					ngf-drop\r\n					ng-model=\"vm.banner.model.picFile\"\r\n					ngf-pattern=\"image/*\"\r\n					class=\"cropArea\"\r\n					ng-show=\"vm.banner.model.picFile\">\r\n					\r\n					<div ng-cropper image=\"vm.banner.model.picFile\" result-image=\"vm.banner.model.image64\" cropper-options=\"vm.cropperOptions\"></div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div layout layout-align=\"start center\">\r\n		<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.banner.busy.status(\'create\')\"></md-progress-circular>\r\n		<md-button ng-disabled=\"vm.banner.formStatus(\'create\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary clean-left\" ng-if=\"!vm.banner.busy.status(\'create\')\">\r\n			{{\'SAVE\' | translate}}\r\n			\r\n			<md-tooltip>\r\n			{{\'SAVE\' | translate}}\r\n			</md-tooltip>\r\n		</md-button>\r\n		\r\n		<md-button ng-disabled=\"vm.service.busy.status(\'create\')\" ui-sref=\"app.banner\">\r\n			{{\'CANCEL\' | translate}}\r\n			\r\n			<md-tooltip>\r\n			{{\'CANCEL\' | translate}}\r\n			</md-tooltip>\r\n		</md-button>\r\n	</div>\r\n</form>\r\n<!-- /FORM -->");
$templateCache.put("core/cart/cart.tpl.html","<div class=\"wrapper\">\n    <div class=\"title-border\">\n        <h1 class=\"page-title title\">{{\'CART\' | translate}}</h1>\n        <div class=\"border\"></div>\n    </div>\n    \n    <div class=\"md-whiteframe-1dp list-custom cart-list\" has-entries=\"{{!!vm.entries.length}}\">\n        <div class=\"hide-entries no-results\">\n            <md-icon md-font-set=\"material-icons\">info_outline</md-icon> {{\'NO_ITEMS\' | translate}}\n        </div>\n        <div class=\"listing show-entries\">\n            <div class=\"head\">\n                <table>\n                    <tr>\n                        <td colspan=\"2\">\n                            <h3>{{\'PROD_MENU\' | translate}}</h3>\n                        </td>\n                        <td class=\"content total\">\n                            <h3>{{\'TOTAL\' | translate}}</h3>\n                        </td>\n                        <td class=\"actions\"></td>\n                    </tr>\n                </table>\n            </div>\n            <md-divider></md-divider>\n            <table>\n                <tr ng-repeat=\"item in vm.entries\">\n                    <td class=\"img\">\n                        <img alt=\"{{ item.product.name }}\" ng-src=\"{{item.product.image.thumb}}\" class=\"md-avatar\" />\n                    </td>\n                    <td class=\"content\">\n                        <h3> <a ui-sref=\"app.product({slug: item.product.slug, id:item.product.id})\">{{ item.product.name }}</a> </h3>\n                        <h4> {{ item.product.priceOffer ? item.product.priceOffer : item.product.price | currency}} x {{item.qty}}</h4>\n                    </td>\n                    <td class=\"content total\">\n                        <h3>{{ (item.product.priceOffer ? item.product.priceOffer : item.product.price) * item.qty | currency}}</h3>\n                    </td>\n                    <td class=\"actions\">\n                        <md-button class=\"md-icon-button\" aria-label=\"Delete\">\n                            <md-icon md-font-set=\"material-icons\" ng-click=\"vm.factory.remove($index)\" aria-label=\"{{\'DELETE_ITEM\' | translate}}\">delete</md-icon>\n                        </md-button>\n                    </td>\n                </tr>\n            </table>\n            <md-divider></md-divider>\n            <div class=\"foot\">\n                <table>\n                    <tr>\n                        <td colspan=\"2\">\n                            <h3>{{\'CART_TOTAL\' | translate}}</h3>\n                        </td>\n                        <td class=\"content total\">\n                            <h3>{{ vm.factory.subtotal | currency}}</h3>\n                        </td>\n                        <td class=\"actions\"></td>\n                    </tr>\n                </table>\n            </div>\n        </div>\n    </div>\n    \n    <br>\n\n    <div layout layout-align=\"end center\">\n        <md-button class=\"md-primary md-raised btn-checkout\" ui-sref=\"app.checkout.identification\" ng-disabled=\"!vm.factory.hasEntries()\">\n            Checkout\n        </md-button>\n    </div>\n</div>");
$templateCache.put("core/category/categories.tpl.html","<div class=\"wrapper\">\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">{{\'CATEGORIES\' | translate}}</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n\r\n	<list\r\n		params=\"vm.params\"\r\n		template-url=\"core/category/list.tpl.html\"\r\n		class=\"list-default\">\r\n	</list>\r\n</div>");
$templateCache.put("core/category/categoryNew.tpl.html","<br>\r\n\r\n<!-- FORM -->\r\n<form name=\"form\" class=\"form wrapper\">\r\n	<div class=\"form-box basic\">\r\n		<div>\r\n			<p class=\"title\">{{\'BASIC_DETAILS\' | translate}}</p>\r\n		</div>\r\n		<div class=\"box\">\r\n			<div layout layout-sm=\"column\">\r\n				<md-input-container flex>\r\n					<label>{{\'TITLE\' | translate}}</label>\r\n					<input ng-model=\"vm.category.model.title\" required>\r\n				</md-input-container>\r\n				<md-input-container flex>\r\n					<label>URL</label>\r\n					<input ng-model=\"vm.category.model.slug\" ng-blur=\"vm.category.slugifyUrl(vm.category.model.slug)\" required category-slug-validator dependent-model=\"vm.category.model.parent\" edit={{vm.category.model._id}}>\r\n				</md-input-container>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div class=\"form-box basic\">\r\n		<div>\r\n			<p class=\"title\">{{\'CAT_FORM_PARENT_CAT\' | translate}}</p>\r\n		</div>\r\n		<div class=\"box\">\r\n			<div layout layout-sm=\"column\">\r\n				<md-autocomplete\r\n					md-input-name=\"parent\"\r\n					md-selected-item=\"vm.category.model.parent\"\r\n					md-search-text=\"vm.searchText\"\r\n					md-items=\"item in vm.querySearch(vm.searchText)\"\r\n					md-item-text=\"item.title\"\r\n					md-min-length=\"0\"\r\n					md-delay=\"1000\"\r\n					flex>\r\n					<md-item-template>\r\n						<span md-highlight-text=\"vm.searchText\" md-highlight-flags=\"^i\">{{item.title}}</span>\r\n					</md-item-template>\r\n					<md-not-found>\r\n						Nenhuma categoria encontrada para \"{{vm.searchText}}\"\r\n					</md-not-found>\r\n				</md-autocomplete>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div layout layout-align=\"start center\">\r\n		<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.category.busy.status(\'categoryCreate\')\"></md-progress-circular>\r\n		<md-button ng-disabled=\"vm.category.formStatus(\'create\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary clean-left\" ng-if=\"!vm.category.busy.status(\'categoryCreate\')\">\r\n			{{\'SAVE\' | translate}}\r\n			\r\n			<md-tooltip>\r\n			{{\'SAVE\' | translate}}\r\n			</md-tooltip>\r\n		</md-button>\r\n		<md-button ng-disabled=\"vm.service.busy.status(\'productCreate\')\" ui-sref=\"app.categories\" class=\"md-accent\">\r\n			{{\'CANCEL\' | translate}}\r\n			\r\n			<md-tooltip>\r\n			{{\'CANCEL\' | translate}}\r\n			</md-tooltip>\r\n		</md-button>\r\n	</div>\r\n</form>\r\n<!-- /FORM -->");
$templateCache.put("core/category/list.tpl.html","<div class=\"md-whiteframe-1dp\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/category/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/category/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content hide-loading hide-starting\">\r\n    <md-list-item ng-repeat=\"item in vm.service.entries\" ng-click=\"vm.service.primaryAction ? vm.service.primaryAction(item, $event) : \'\'\" ng-class=\"{\'no-action\': !vm.service.primaryAction}\">\r\n        <md-icon md-font-set=\"material-icons\" ng-if=\"vm.service.primaryIcon\">{{vm.service.primaryIcon}}</md-icon>\r\n        <img alt=\"{{ item.title }}\" ng-src=\"{{vm.uploadFolder + item.imgThumb}}\" class=\"md-avatar\" ng-if=\"vm.service.showAvatar\" />\r\n        <p>{{ item.parentTitle }}</p>\r\n        <md-icon md-font-set=\"material-icons\" ng-click=\"vm.service.secondaryAction($event, item, $index)\" aria-label=\"Open Chat\" class=\"md-secondary md-hue-3\">delete</md-icon>\r\n\r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/checkout/checkout-nav.tpl.html","<md-button class=\"md-fab md-primary\" aria-label=\"{{\'PREV\' | translate}}\" ng-click=\"vm.prev($event)\" ng-if=\"vm.hasPrevBtn\">\n	<md-icon md-font-set=\"material-icons\">navigate_before</md-icon>\n	\n	<md-tooltip>\n	{{\'PREV\' | translate}}\n	</md-tooltip>\n</md-button>\n<md-button class=\"md-fab md-primary\" aria-label=\"{{\'NEXT\' | translate}}\" ng-click=\"vm.next($event)\" ng-if=\"vm.hasNextBtn\">\n	<md-icon md-font-set=\"material-icons\">navigate_next</md-icon>\n\n	<md-tooltip>\n	{{\'NEXT\' | translate}}\n	</md-tooltip>\n</md-button>\n<md-button class=\"md-fab clear-checkout\" aria-label=\"{{\'RESET\' | translate}}\" ng-click=\"vm.clear()\">\n	<md-icon md-font-set=\"material-icons\">cached</md-icon>\n\n	<md-tooltip>\n	{{\'RESET\' | translate}}\n	</md-tooltip>\n</md-button>");
$templateCache.put("core/checkout/checkout.tpl.html","<div class=\"wrapper\">\n    <br>\n    <div layout-gt-sm layout=\"column\">\n        <div\n            side-menu\n            items=\"vm.menu\"\n            class=\"side-menu\"\n            layout=\"row\"\n            hide-sm hide-xs>\n        </div>\n\n        <div class=\"checkout-content\" step=\"{{vm.step.innerTabGroup}}\" flex=\"no-shrink\">\n            <div ui-view=\"checkout-form\" class=\"anim-in-out anim-slide-below-fade\" data-anim-sync=\"true\"></div>\n        </div>\n    </div>\n</div>");
$templateCache.put("core/filepicker/dialogFilepicker.tpl.html","<md-dialog aria-label=\"{{\'CHOOSE_FILE\' | translate}}\" ng-cloak flex>\r\n	<form>\r\n		<md-toolbar>\r\n			<div class=\"md-toolbar-tools\">\r\n				<h2>{{\'CHOOSE_FILE\' | translate}}</h2>\r\n				<span flex></span>\r\n				<md-button class=\"md-icon-button\" ng-click=\"cancel()\">\r\n					<md-icon md-font-set=\"material-icons\" md-font-content=\"close\" aria-label=\"Close dialog\"></md-icon>\r\n				</md-button>\r\n			</div>\r\n		</md-toolbar>\r\n		<md-dialog-content>\r\n			<div class=\"md-dialog-content\">\r\n				<div filepicker qty=\"qty\" cropper-options=\"cropperOptions\" result=\"result\" bucket=\"bucket\" prefix=\"prefix\" free-aspect=\"freeAspect\" use-instagram=\"useInstagram\"></div>\r\n			</div>\r\n		</md-dialog-content>\r\n		<md-dialog-actions layout=\"row\">\r\n			<span flex></span>\r\n			<md-button ng-click=\"cancel()\">\r\n				{{\'CANCEL\' | translate}}\r\n			</md-button>\r\n			<md-button ng-click=\"confirm()\" style=\"margin-right:20px;\">\r\n				{{\'SAVE\' | translate}}\r\n			</md-button>\r\n		</md-dialog-actions>\r\n	</form>\r\n</md-dialog>");
$templateCache.put("core/filepicker/filepicker.tpl.html","<div class=\"header\" ng-if=\"vm.service.view === \'main\'\">\r\n	<div layout layout-xs=\"column\">\r\n		<div flex-sm flex-gt-sm>Selecione <b>{{vm.qty}}</b> fotos</div>\r\n		<div>{{vm.service.selected}}/{{vm.service.qty}}</div>\r\n	</div>\r\n</div>\r\n\r\n<div class=\"file-selection\" layout=\"column\" ng-if=\"vm.service.view === \'main\'\">\r\n	<div ng-if=\"vm.service.busy.status(\'processFiles\')\">\r\n		<p layout layout-align=\"start center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando imagens..\r\n		</p>\r\n	</div>\r\n	\r\n	<div ng-if=\"!vm.service.busy.status(\'processFiles\')\">\r\n		<div ngf-drop ngf-select ng-model=\"vm.dropArea\" ngf-change=\"vm.service.processFiles($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event)\" class=\"drop-box\"\r\n			ngf-drag-over-class=\"\'dragover\'\" ngf-multiple=\"true\" ngf-allow-dir=\"true\"\r\n			accept=\"image/*,application/pdf\"\r\n		ngf-pattern=\"\'image/*,application/pdf\'\">Arraste e solte os arquivos aqui ou clique para fazer upload</div>\r\n		<div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>\r\n	</div>\r\n</div>\r\n\r\n<div class=\"file-others\" ng-class=\"{\'file-use-instagram\': vm.useInstagram}\" layout=\"row\" ng-if=\"vm.service.view === \'main\'\">\r\n	<md-button class=\"btn-instagram\" flex>Instagram</md-button>\r\n	<md-button ng-click=\"vm.service.pickOld()\" flex>Existente</md-button>\r\n</div>\r\n\r\n<div class=\"file-old\" ng-if=\"vm.service.view === \'old\'\">\r\n	<div>Arquivos existentes - selecione os arquivos</div>\r\n	\r\n	<div ng-if=\"vm.service.busy.status(\'getOld\')\">\r\n		<p layout layout-align=\"start center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando imagens..\r\n		</p>\r\n	</div>\r\n	\r\n	<div class=\"file-list\" ng-if=\"!vm.service.busy.status(\'getOld\')\">\r\n		<div class=\"file-grid\" flex>\r\n			<div class=\"file-item\" ng-repeat=\"file in vm.service.filesOld track by $index\">\r\n				<div class=\"file-image-wrapper\" ng-class=\"{\'active\': file.selected}\">\r\n					<img ng-src=\"{{file.url}}\" />\r\n					\r\n					<md-checkbox aria-label=\"file selection\" class=\"md-primary\" ng-change=\"vm.service.toggleSelectedOld(file)\" ng-model=\"file.selected\">\r\n					</md-checkbox>\r\n				</div>\r\n				<div class=\"file-controls\" layout>\r\n					<a class=\"color-primary\" ng-click=\"vm.service.removeOld($index, file)\" md-ink-ripple flex>\r\n						<md-icon md-font-set=\"material-icons\" md-font-content=\"delete\"></md-icon>\r\n					</a>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div class=\"actions\">\r\n		<div class=\"file-controls\" layout>\r\n			<md-button class=\"md-primary\" ng-click=\"vm.service.saveOld()\">\r\n				{{\'SAVE\' | translate}}\r\n			</md-button>\r\n			\r\n			<md-button class=\"\" ng-click=\"vm.service.cancelOld()\">\r\n				{{\'CANCEL\' | translate}}\r\n			</md-button>\r\n		</div>\r\n	</div>\r\n</div>\r\n\r\n<div class=\"file-crop\" ng-if=\"vm.service.crop\">\r\n	<div class=\"text-center\">\r\n		<h4>Faça o corte e clique em salvar</h4>\r\n	</div>\r\n	\r\n	<div class=\"file-crop-wrapper\">\r\n		<div ng-cropper image=\"vm.service.crop.img\" result-image=\"vm.service.crop.model\" cropper-options=\"vm.cropperOptions\"></div>\r\n		<div class=\"file-controls\" layout>\r\n			<md-button class=\"md-primary\" ng-click=\"vm.service.saveCrop()\">\r\n				{{\'SAVE\' | translate}}\r\n			</md-button>\r\n			\r\n			<md-button class=\"\" ng-click=\"vm.service.cancelCrop()\">\r\n				{{\'CANCEL\' | translate}}\r\n			</md-button>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div ng-class=\"{\'file-free-aspect\': vm.freeAspect}\">\r\n	<div class=\"file-list\" ng-if=\"vm.service.view == \'main\' && !vm.service.crop\">\r\n		<div class=\"file-grid\" flex>\r\n			<div class=\"file-item\" ng-repeat=\"file in vm.service.files track by $index\">\r\n				<div class=\"file-image-wrapper\" ng-class=\"{\'active\': file.selected}\">\r\n					<img ng-src=\"{{file.cropped}}\" />\r\n					\r\n					<md-checkbox aria-label=\"file selection\" class=\"md-primary\" ng-change=\"vm.service.toggleSelected(file)\" ng-model=\"file.selected\">\r\n					</md-checkbox>\r\n				</div>\r\n				<div class=\"file-controls\" layout>\r\n					<a class=\"color-primary\" ng-click=\"vm.service.startCrop($index, file)\" md-ink-ripple flex>\r\n						<md-icon md-font-set=\"material-icons\" md-font-content=\"crop\"></md-icon>\r\n					</a>\r\n					<a class=\"color-primary\" ng-click=\"vm.service.remove($index, file)\" md-ink-ripple flex>\r\n						<md-icon md-font-set=\"material-icons\" md-font-content=\"delete\"></md-icon>\r\n					</a>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/footer/footerAdmin.tpl.html","<div class=\"wrapper\">\r\n	<div layout=\"row\" class=\"copyright\" flex>\r\n		<div flex>\r\n			Copyright  © 2015, O Cotidiano, Direitos reservados.\r\n		</div>\r\n		<div flex class=\"text-right mtda\">\r\n			<a href=\"http://www.mtda.de\" target=\"_blank\">\r\n				desenvolvido por <img src=\"assets/images/mtda-yellow.png\" alt=\"\">\r\n			</a>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/layout/404.tpl.html","<div class=\"fourOhFour\" layout layout-align=\"center center\">\r\n	<md-icon md-font-set=\"material-icons\">thumb_down</md-icon>\r\n	404\r\n	{{\'PAGE_NOT_FOUND\' | translate}}\r\n	{{\'PAGE_NOT_FOUND_TEXT\' | translate}}\r\n</div>");
$templateCache.put("core/layout/layout.tpl.html","<!-- loader -->\r\n<loader></loader>\r\n<!-- /loader -->\r\n\r\n<div class=\"acme-navbar\" acme-navbar current-state=\"main.state.current.name\" categories=\"main.categoryBar\" layout=\"column\" flex=\"none\"></div>\r\n\r\n<div\r\n	ui-view=\"content\"\r\n	class=\"anim-in-out anim-slide-below-fade\"\r\n	data-anim-sync=\"true\"\r\n	layout=\"column\" flex=\"noshrink\">\r\n</div>\r\n\r\n<footer template-url=\"core/footer/footerAdmin.tpl.html\" flex=\"noshrink\"></footer>");
$templateCache.put("core/list/list.tpl.html","<div class=\"md-whiteframe-1dp\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    \r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/main/layout.tpl.html","<!-- loader -->\r\n<loader></loader>\r\n<!-- /loader -->\r\n\r\n<div class=\"toolbar\" toolbar current-state=\"main.state.current.name\" categories=\"main.categoryBar\" layout=\"column\" flex=\"none\"></div>\r\n\r\n<div\r\n	ui-view=\"content\"\r\n	class=\"anim-in-out anim-slide-below-fade\"\r\n	data-anim-sync=\"true\"\r\n	layout=\"column\" flex=\"noshrink\">\r\n</div>\r\n\r\n<footer class=\"hide-admin\" flex=\"noshrink\"></footer>");
$templateCache.put("core/order/list.tpl.html","<div class=\"md-whiteframe-1dp order-list\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <div layout class=\"custom-filters\" flex>\r\n        \r\n        <div layout=\"row\" layout-align=\"start center\">\r\n            <div hide-xs>\r\n                <md-icon md-fon-set=\"material-icons\">sort</md-icon>\r\n            </div>\r\n            <md-input-container>\r\n                <md-select ng-model=\"vm.service.filters.sort\" aria-label=\"{{\'SORT_BY\' | translate}}\">\r\n                    <md-option ng-repeat=\"sort in vm.service.sortOptions\" value=\"{{sort.value}}\">\r\n                        {{sort.name}}\r\n                    </md-option>\r\n                </md-select>\r\n            </md-input-container>\r\n        </div>\r\n        \r\n        <div class=\"flex-sep\"></div>\r\n        \r\n        <div layout=\"row\" layout-align=\"start center\">\r\n            <div hide-xs>\r\n                <md-icon md-fon-set=\"material-icons\">attach_money</md-icon>\r\n            </div>\r\n            <md-input-container>\r\n                <md-select ng-model=\"vm.service.filters.paymode\" aria-label=\"{{\'PAYMENT_TYPE\' | translate}}\">\r\n                    <md-option ng-repeat=\"sort in vm.service.filterPaymode\" value=\"{{sort.value}}\">\r\n                        {{sort.name}}\r\n                    </md-option>\r\n                </md-select>\r\n            </md-input-container>\r\n        </div>\r\n        \r\n        <div class=\"flex-sep\"></div>\r\n        \r\n        <div layout=\"row\" layout-align=\"start center\">\r\n            <div hide-xs>\r\n                <md-icon md-fon-set=\"material-icons\">lens</md-icon>\r\n            </div>\r\n            <md-input-container>\r\n                <md-select ng-model=\"vm.service.filters.status\" aria-label=\"{{\'PAYMENT_TYPE\' | translate}}\">\r\n                    <md-option ng-repeat=\"sort in vm.service.filterStatus\" value=\"{{sort.value}}\">\r\n                        {{sort.name}}\r\n                    </md-option>\r\n                </md-select>\r\n            </md-input-container>\r\n        </div>\r\n        \r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/order/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/order/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content hide-loading hide-starting\">\r\n    <md-list-item\r\n        ng-repeat=\"item in vm.service.entries\"\r\n        ng-click=\"vm.service.primaryAction ? vm.service.primaryAction(item, $event) : \'\'\">\r\n        <div layout layout-xs=\"column\"\r\n            layout-align-sm=\"start center\" layout-align-gt-sm=\"start center\"\r\n            class=\"item\" flex>\r\n            <div class=\"status {{\'status-\' + item.status}}\">\r\n                <md-icon md-font-set=\"material-icons\">lens</md-icon>\r\n            </div>            \r\n            <div flex-sm=\"50\" flex-gt-sm=\"50\">\r\n                <p>{{item.userName}}</p>\r\n                <p class=\"md-caption\">{{ \'PAY_STAT_\' + item.status | translate }}</p>\r\n            </div>\r\n            <div flex-sm=\"20\" flex-gt-sm=\"20\">\r\n                <p>{{item.createdAt | customMoment}}</p>\r\n                <p class=\"md-caption\">#{{item.number}}</p>\r\n            </div>\r\n            <div flex-sm flex-gt-sm layout=\"column\" layout-align-sm=\"center end\" layout-align-gt-sm=\"center end\">\r\n                <p>{{item.total | currency}}</p>\r\n                <p class=\"md-caption\">{{\'PAID_\' + item.payment.mode | translate}}</p>\r\n            </div>\r\n        </div>\r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/order/order.tpl.html","<div class=\"order\">\r\n	<div class=\"inner-loading\" layout=\"row\" layout-align=\"center center\" ng-if=\"vm.factory.busy.status(\'get\')\">\r\n		<p layout layout-align=\"center center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n		</p>\r\n	</div>\r\n	<div class=\"wrapper\" ng-if=\"!vm.factory.busy.status(\'get\') && vm.service\">\r\n		<div class=\"title-box\" layout=\"row\">\r\n			<div>\r\n				<h2 class=\"hm-title\"><span>{{\'ORDER\' | translate}}</span> <span class=\"color-primary\">#{{vm.service.model.number}}</span></h2>\r\n			</div>\r\n		</div>\r\n		<br>\r\n		\r\n		<div class=\"order-content md-whiteframe-1dp\" layout layout-xs=\"column\">\r\n			<!-- COLUNA RESUMO -->\r\n			<div class=\"resume column\">\r\n				<!-- RESUMO VALORES -->\r\n				<div class=\"value-info\">\r\n					<span class=\"md-caption color-primary\">{{\'RESUME\' | translate}}</span>\r\n					\r\n					<div layout>\r\n						<p flex class=\"md-body-1\">Subtotal</p>\r\n						<p class=\"md-body-1\">{{vm.service.subtotal | currency}}</p>\r\n					</div>\r\n					<div layout>\r\n						<p flex class=\"md-body-1\">{{\'SHIPPING\' | translate}}</p>\r\n						<p class=\"md-body-1\">{{vm.service.model.shipping.price | currency}}</p>\r\n					</div>\r\n					<div layout class=\"total\">\r\n						<p flex class=\"md-body-2\">{{\'TOTAL\' | translate}}</p>\r\n						<p class=\"md-body-2\">{{vm.service.total | currency}}</p>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- INFORMAÇÕES DE PAGAMENTO -->\r\n				<div class=\"payment-info\">\r\n					<span class=\"md-caption color-primary\">{{\'PAYMENT\' | translate}}</span>\r\n					\r\n					<div>\r\n						<div layout>\r\n							<p flex class=\"md-body-1\">{{\'PAID_WITH\' | translate}}</p>\r\n							<p class=\"md-body-1\">{{\'PAID_\' + vm.service.model.payment.mode | translate}}</p>\r\n						</div>\r\n						<div layout ng-if=\"vm.service.model.moip || vm.service.model.paypal\">\r\n							<p flex class=\"md-body-1\">Gateway</p>\r\n							<p class=\"md-body-1\" ng-if=\"vm.service.model.moip\">MOIP</p>\r\n							<p class=\"md-body-1\" ng-if=\"vm.service.model.paypal\">Paypal</p>\r\n						</div>\r\n						<div layout layout-align=\"start center\">\r\n							<p flex class=\"md-body-1\">Status</p>\r\n							<md-select ng-model=\"vm.service.model.status\" aria-label=\"{{\'PAYMENT_TYPE\' | translate}}\">\r\n								<md-option ng-repeat=\"sort in vm.filterStatus\" value=\"{{sort.value}}\">\r\n									{{sort.name}}\r\n								</md-option>\r\n							</md-select>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- STATUS INTERNO -->\r\n				<div class=\"internal-info\">\r\n					<span class=\"md-caption color-primary\">{{\'INTERNAL_MONITORING\' | translate}}</span>\r\n					\r\n					<div>\r\n						<div layout layout-align=\"start center\">\r\n							<p flex class=\"md-body-1\">Status</p>\r\n							<md-select ng-model=\"vm.service.model.internalStatus\" aria-label=\"{{\'PAYMENT_TYPE\' | translate}}\">\r\n								<md-option ng-repeat=\"sort in vm.factory.filterInternalStatus\" value=\"{{sort.value}}\">\r\n									{{sort.name}}\r\n								</md-option>\r\n							</md-select>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- RASTREAMENTO -->\r\n				<div>\r\n					<span class=\"md-caption color-primary\">{{\'TRACKING_CODE\' | translate}}</span>\r\n					\r\n					<div>\r\n						<div layout layout-align=\"start center\">\r\n							<p flex class=\"md-body-1\">{{\'CODE\' | translate}}</p>\r\n							<md-input-container md-no-float>\r\n								<input ng-model=\"vm.service.model.shipping.tracking\" placeholder=\"{{\'INSERT_CODE\' | translate}}\">\r\n							</md-input-container>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<div layout layout-align=\"end start\">\r\n					<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'update\')\"></md-progress-circular>\r\n					<md-button class=\"md-primary\" ng-click=\"vm.service.update(true)\" ng-if=\"!vm.service.busy.status(\'update\')\">\r\n						{{\'SAVE\' | translate}}\r\n\r\n						<md-tooltip>\r\n						{{\'SAVE\' | translate}}\r\n						</md-tooltip>\r\n					</md-button>\r\n				</div>\r\n			</div>\r\n			\r\n			<!-- COLUNA DADOS -->\r\n			<div class=\"full-info column\" flex-sm flex-gt-sm>\r\n				<!-- DADOS DO CLIENTES -->\r\n				<div>\r\n					<span class=\"md-caption color-primary\">{{\'CUSTOMER\' | translate}}</span>\r\n					\r\n					<div layout layout-xs=\"column\">\r\n						<div flex-sm flex-gt-sm>\r\n							<p>{{vm.service.model.user.profile.firstName+ \' \' +vm.service.model.user.profile.lastName}}</p>\r\n							<p>{{vm.service.model.user.email}}</p>\r\n						</div>\r\n						\r\n						<div layout=\"column\" layout-align-sm=\"center end\" layout-align-gt-sm=\"start end\">\r\n							<p class=\"md-caption\" ng-if=\"vm.formLocale !== \'pt_BR\'\">\r\n								<span ng-if=\"vm.service.model.user.profile.contact.phone\">{{vm.service.model.user.profile.contact.phone}}</span>\r\n							</p>\r\n							<p class=\"md-caption\" ng-if=\"vm.formLocale !== \'pt_BR\'\">\r\n								<span ng-if=\"vm.service.model.user.profile.contact.mobile\">{{vm.service.model.user.profile.contact.mobile}}</span>\r\n							</p>\r\n							<p class=\"md-caption\" ng-if=\"vm.formLocale === \'pt_BR\'\">\r\n								<span ng-if=\"vm.service.model.user.profile.contact.phone\">{{vm.service.model.user.profile.contact.phone | brPhoneNumber}}</span>\r\n							</p>\r\n							<p class=\"md-caption\" ng-if=\"vm.formLocale === \'pt_BR\'\">\r\n								<span ng-if=\"vm.service.model.user.profile.contact.mobile\">{{vm.service.model.user.profile.contact.mobile | brPhoneNumber}}</span>\r\n							</p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- DADOS DO COBRANÇA E ENTREGA -->\r\n				<div layout layout-xs=\"column\">\r\n					<!-- COBRANÇA -->\r\n					<div flex-sm flex-gt-sm>\r\n						<span class=\"md-caption color-primary\">{{\'PAYMENT_DETAILS\' | translate}}</span>\r\n						\r\n						<div addr-display address=\"vm.service.model.payment.address\" person=\"vm.service.model.payment.payer\" form-locale=\"vm.formLocale\"></div>\r\n					</div>\r\n					\r\n					<!-- ENTREGA -->\r\n					<div flex-sm flex-gt-sm>\r\n						<span class=\"md-caption color-primary\">{{\'SHIPPING_DETAILS\' | translate}}</span>\r\n						\r\n						<div addr-display address=\"vm.service.model.shipping.address\" person=\"vm.service.model.shipping.to\" form-locale=\"vm.formLocale\"></div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- PRODUTOS -->\r\n				<div class=\"products\">\r\n					<span class=\"md-caption color-primary\">{{\'PRODUCTS\' | translate}}</span>\r\n					\r\n					<div class=\"list\">\r\n						<div class=\"item\" ng-repeat=\"item in vm.service.model.cart\" layout layout-xs=\"column\">\r\n							<div class=\"image\" layout layout-align=\"start center\">\r\n								<img ng-src=\"{{item.product.image.thumb}}\" alt=\"{{vm.service.model.product.name}}\">\r\n							</div>\r\n							<div flex-sm flex-gt-sm>\r\n								<p>{{item.product.name}}</p>\r\n								<p class=\"md-caption\">{{item.attrs | attrsToString}}</p>\r\n							</div>\r\n							<div layout=\"column\" layout-align=\"start end\">\r\n								<p>{{item.qty * item.price | currency}}</p>\r\n								<p class=\"md-caption\">{{item.qty}} x {{item.price | currency}}</p>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- TIPO ENTREGA -->\r\n				<div>\r\n					<span class=\"md-caption color-primary\">{{\'SHIPPING_METHOD\' | translate}}</span>\r\n					\r\n					<div layout layout-xs=\"column\">\r\n						<div flex-sm flex-gt-sm>\r\n							<p>{{\'SHIP_\' + vm.service.model.shipping.method | translate}}</p>\r\n						</div>\r\n						<div>\r\n							<p>{{vm.service.model.shipping.price | currency}}</p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/order/orders.tpl.html","<div class=\"wrapper\">\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">{{\'ORDERS\' | translate}}</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n\r\n	<list\r\n		params=\"vm.params\"\r\n		template-url=\"core/order/list.tpl.html\"\r\n		class=\"list-default\">\r\n	</list>\r\n</div>");
$templateCache.put("core/product/list.tpl.html","<div class=\"md-whiteframe-1dp product-admin-list\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/product/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/product/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content no-action hide-loading hide-starting\">\r\n    <md-list-item ng-repeat=\"item in vm.service.entries\" layout=\"row\">\r\n        <div layout=\"row\" flex>\r\n            <div layout=\"row\" flex>\r\n                <img alt=\"{{ item.name }}\" ng-src=\"{{item.image.thumb}}\" class=\"md-avatar\" ng-if=\"vm.service.showAvatar\" />\r\n                <p>{{ item.name }}</p>\r\n            </div>\r\n            <div layout layout-align=\"center\">\r\n                <md-button ng-click=\"vm.service.primaryAction(item, $event)\">Editar</md-button>\r\n                <md-button aria-label=\"Delete\">\r\n                    <md-icon md-font-set=\"material-icons\" ng-click=\"vm.service.secondaryAction($event, item, $index)\" aria-label=\"Open Chat\" class=\"md-hue-3\">delete</md-icon>\r\n                </md-button>\r\n            </div>\r\n        </div>\r\n        \r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/product/productForm.tpl.html","<div class=\"panel\">\r\n	<!-- LOADING -->\r\n	<div class=\"loading\" ng-show=\"vm.factory.busy.status(\'productGet\')\">\r\n		<p layout layout-align=\"center center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n		</p>\r\n	</div>\r\n	\r\n	<!-- FORM -->\r\n	<form name=\"form\" class=\"form form-product content\" ng-if=\"!vm.edit || vm.edit && vm.product.model\">\r\n		<div class=\"panel-body clean-bottom\">\r\n			<p class=\"title\">{{\'PROD_FORM_BASIC_DETAILS\' | translate}}</p>\r\n			\r\n			<div layout layout-sm=\"column\">\r\n				<md-input-container flex>\r\n					<label>{{\'NAME\' | translate}}</label>\r\n					<input ng-model=\"vm.product.model.name\" required>\r\n				</md-input-container>\r\n				<md-input-container flex>\r\n					<label>URL</label>\r\n					<input ng-model=\"vm.product.model.slug\" ng-blur=\"vm.product.slugifyUrl(vm.product.model.slug)\" required product-slug-validator edit={{vm.product.model._id}}>\r\n				</md-input-container>\r\n			</div>\r\n			\r\n			<textarea froala=\"vm.factory.froalaOptions\" ng-model=\"vm.product.model.desc\"></textarea>\r\n			\r\n			<br><br>\r\n			\r\n			<div class=\"form-box categories\">\r\n				<div>\r\n					<p class=\"title\">{{\'PROD_FORM_CAT_DETAILS\' | translate}}</p>\r\n				</div>\r\n				<div class=\"box\">\r\n					<md-chips\r\n						class=\"category-chips\"\r\n						ng-model=\"vm.product.model.categories\"\r\n						md-autocomplete-snap\r\n						md-transform-chip=\"vm.factory.transformChip($chip)\"\r\n						md-require-match=\"true\">\r\n						<md-autocomplete\r\n							md-selected-item=\"vm.selectedItem\"\r\n							md-search-text=\"vm.searchText\"\r\n							md-items=\"item in vm.querySearch(vm.searchText)\"\r\n							md-item-text=\"item.title\"\r\n							md-min-length=\"1\"\r\n							md-delay=\"1000\"\r\n							placeholder=\"{{\'PROD_FORM_CAT\' | translate}}\">\r\n							<span md-highlight-text=\"vn.searchText\">{{item.parentTitle}}</span>\r\n						</md-autocomplete>\r\n						<md-chip-template>\r\n							<span>\r\n								<strong>{{$chip.parentTitle}}</strong>\r\n							</span>\r\n						</md-chip-template>\r\n					</md-chips>\r\n				</div>\r\n			</div>\r\n			\r\n			<div class=\"form-box\">\r\n				<div>\r\n					<p class=\"title\">{{\'PROD_FORM_PRICE_DETAILS\' | translate}}</p>\r\n				</div>\r\n				<div class=\"box\">\r\n					<div layout>\r\n						<md-input-container flex-xs flex-sm=\"35\" flex-gt-sm=\"20\">\r\n							<label>{{\'PRICE\' | translate}}</label>\r\n							<input ng-model=\"vm.product.model.price\" ui-money-mask=\"2\" required>\r\n						</md-input-container>\r\n						<md-input-container flex-xs flex-sm=\"35\" flex-gt-sm=\"20\">\r\n							<label>{{\'PRICE_OFFER\' | translate}}</label>\r\n							<input ng-model=\"vm.product.model.priceOffer\" ui-money-mask=\"2\">\r\n						</md-input-container>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			\r\n			<div class=\"form-box\">\r\n				<div>\r\n					<p class=\"title\">{{\'PROD_FORM_FEATURED_DETAILS\' | translate}}</p>\r\n				</div>\r\n				<div class=\"box\">\r\n					<div layout>\r\n						<md-input-container flex-xs flex-sm=\"35\" flex-gt-sm=\"20\">\r\n							<label>{{\'FEATURED\' | translate}}</label>\r\n							<md-select ng-model=\"vm.product.model.featured.status\" required>\r\n								<md-option value=\"1\">\r\n									{{\'YES\' | translate}}\r\n								</md-option>\r\n								<md-option value=\"0\">\r\n									{{\'NO\' | translate}}\r\n								</md-option>\r\n							</md-select>\r\n						</md-input-container>\r\n						<md-input-container flex-xs flex-sm=\"35\" flex-gt-sm=\"20\">\r\n							<label>{{\'ORDER\' | translate}}</label>\r\n							<input type=\"number\" ng-model=\"vm.product.model.featured.order\">\r\n						</md-input-container>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			\r\n			<div class=\"form-box variants\" layout=\"column\">\r\n				<div>\r\n					<p class=\"title\">{{\'PROD_FORM_VARIATIONS_DESC\' | translate}}</p>\r\n				</div>\r\n				<div class=\"box\" layout=\"column\">\r\n					<div variant-form ng-repeat=\"item in vm.variants\" status=\"item.status\" name=\"{{item.name}}\" title=\"{{item.name | translate}}\" entries=\"vm.factory.variants[item.name]\" model=\"vm.product.model.variants.attrs\" position=\'{{$index}}\' lang></div>\r\n					<!-- COLOR VARIATION -->\r\n					<!-- <div variant-form name=\"colors\" title=\"COLOR\" entries=\"vm.factory.variants.colors\" model=\"vm.product.model.variants.attrs\" position=\'0\' lang></div>\r\n					\r\n					<md-divider></md-divider> -->\r\n					\r\n					<!-- SIZE VARIATION -->\r\n					<!-- <div variant-form name=\"sizes\" title=\"SIZE\" entries=\"vm.factory.variants.sizes\" model=\"vm.product.model.variants.attrs\" position=\'1\' lang></div> -->\r\n				</div>\r\n			</div>\r\n			\r\n			<div class=\"form-box\">\r\n				<div>\r\n					<p class=\"title\">{{\'PROD_FORM_UPLOAD_DETAILS\' | translate}}</p>\r\n				</div>\r\n				<div class=\"box\">\r\n					<div layout=\"column\">\r\n						<div>\r\n							<p>{{\'PROD_FORM_UP_DESC\' | translate}}</p>\r\n						</div>\r\n						<div layout=\"column\">\r\n							<md-button\r\n								class=\"md-raised clean-left\"\r\n								ng-click=\"vm.product.chooseFile()\"\r\n								accept=\"image/*\">\r\n								{{\'CHOOSE_FILE\' | translate}}\r\n							</md-button>\r\n							\r\n							<div class=\"result-img\">\r\n								<img ng-if=\"vm.product.model.image.file && !vm.product.model.image64\" ng-src=\"{{vm.product.model.image.thumb}}\">\r\n								<img ng-if=\"vm.product.model.image64\" ng-src=\"{{vm.product.model.image64}}\">\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			\r\n			<div layout layout-align=\"start center\">\r\n				<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.product.busy.status(\'productCreate\')\"></md-progress-circular>\r\n				<md-button ng-disabled=\"vm.product.formStatus(\'productCreate\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary clean-left\" ng-if=\"!vm.product.busy.status(\'productCreate\')\">\r\n					{{\'SAVE\' | translate}}\r\n					\r\n					<md-tooltip>\r\n					{{\'SAVE\' | translate}}\r\n					</md-tooltip>\r\n				</md-button>\r\n				<md-button ng-disabled=\"vm.product.busy.status(\'productCreate\')\" ui-sref=\"app.products\">\r\n					{{\'CANCEL\' | translate}}\r\n					\r\n					<md-tooltip>\r\n					{{\'CANCEL\' | translate}}\r\n					</md-tooltip>\r\n				</md-button>\r\n			</div>\r\n		</div>\r\n	</form>\r\n	<!-- /FORM -->\r\n	\r\n</div>");
$templateCache.put("core/product/productFormPage.tpl.html","<div class=\"wrapper-full\">\r\n	<br>\r\n	<div layout layout-xs=\"column\">\r\n		<div\r\n			side-menu\r\n			items=\"vm.menu\"\r\n			class=\"side-menu\"\r\n			ng-if=\"vm.edit\">\r\n		</div>\r\n\r\n		<div flex=\"no-shrink\">\r\n			<div ui-view=\"admin-content\" class=\"anim-in-out anim-slide-below-fade\" data-anim-sync=\"true\"></div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/product/productNew.tpl.html","<div class=\"wrapper\">\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">\r\n			Novo produto\r\n			</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n	<div class=\"panel\" prod-form></div>\r\n</div>");
$templateCache.put("core/product/products.tpl.html","<div class=\"wrapper\">\r\n	<div class=\"title-box\" layout=\"row\">\r\n		<div>\r\n			<h2 class=\"hm-title\">{{\'PROD_LIST\' | translate}}</h2>\r\n		</div>\r\n	</div>\r\n	<br>\r\n	<list\r\n		params=\"vm.params\"\r\n		template-url=\"core/product/list.tpl.html\"\r\n		class=\"list-default\">\r\n	</list>\r\n</div>");
$templateCache.put("core/account/account-list/account-list.tpl.html","<div class=\"card md-whiteframe-1dp account-table\">\r\n	<md-toolbar class=\"md-table-toolbar md-default\" ng-hide=\"list.selected.length || list.filters.show\">\r\n		<div class=\"md-toolbar-tools\">\r\n			<span>Contas</span>\r\n			<span flex></span>\r\n			<md-button class=\"md-icon-button\" ng-click=\"list.filters.setShow(true)\">\r\n				<md-icon>filter_list</md-icon>\r\n			</md-button>\r\n			<md-button class=\"md-icon-button\" ui-sref=\"app.accounts-new\">\r\n				<md-icon>add</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</md-toolbar>\r\n	\r\n	<md-toolbar class=\"md-table-toolbar md-default\" ng-show=\"list.filters.show && !list.selected.length\">\r\n		<div class=\"md-toolbar-tools\">\r\n			<md-icon>search</md-icon>\r\n			<form flex name=\"list.filters.form\">\r\n				<input class=\"md-table-search-input\" type=\"text\" ng-model=\"list.query.filters\" ng-model-options=\"list.filters.options\" placeholder=\"Pesquisa..\">\r\n			</form>\r\n			<md-button class=\"md-icon-button\" ng-click=\"list.filters.setShow(false)\">\r\n				<md-icon>close</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</md-toolbar>\r\n	\r\n	<md-toolbar class=\"md-table-toolbar alternate\" ng-show=\"list.options.rowSelection && list.selected.length\">\r\n		<div class=\"md-toolbar-tools\">\r\n			<span>{{list.selected.length}} {{list.selected.length > 1 ? \'items selecionados\' : \'item selecionado\'}}</span>\r\n			<span flex></span>\r\n			<md-button class=\"md-icon-button\" ng-click=\"list.remove($event)\">\r\n				<md-icon>delete</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</md-toolbar>\r\n	\r\n	<!-- exact table from live demo -->\r\n	<md-table-container>\r\n		<table md-table md-row-select=\"list.options.rowSelection\" multiple=\"{{list.options.multiSelect}}\" ng-model=\"list.selected\" md-progress=\"list.promise\">\r\n			<thead md-head md-order=\"list.query.order\" md-on-reorder=\"list.getAll\">\r\n				<tr md-row>\r\n					<th md-column class=\"cell-avatar\" md-numeric></th>\r\n					<th md-column md-order-by=\"email\"><span>Email</span></th>\r\n					<th md-column class=\"cell-created-at\" md-order-by=\"createdAt\"><span>Criação</span></th>\r\n					<th md-column class=\"cell-account-active\" md-numeric md-order-by=\"account.active\"></th>\r\n					<th md-column class=\"cell-action\"></th>\r\n				</tr>\r\n			</thead>\r\n			<tbody md-body ng-hide=\"list.data.total === 0\">\r\n				<tr md-row md-select=\"item\" md-select-id=\"name\" md-auto-select=\"false\" ng-repeat=\"item in list.data.entries\">\r\n					<td md-cell>\r\n						<img alt=\"{{ item.profile.fullName }}\" ng-src=\"{{item.image.thumb ? item.image.thumb : \'assets/images/user-avatar.jpg\'}}\" class=\"md-avatar\" />\r\n					</td>\r\n					<td md-cell>{{item.email}}</td>\r\n					<td md-cell>{{item.createdAt | customMoment}}</td>\r\n					<td md-cell ng-controller=\"$AccountsItemCtrl as itemCtrl\">\r\n						<md-switch ng-model=\"itemCtrl.model.account.active\" ng-change=\"itemCtrl.toggleActive()\" aria-label=\"{{\'CHANGE_STATUS\' | translate}}\"></md-switch>\r\n					</td>\r\n					<td md-cell class=\"cell-action\">\r\n						<md-button class=\"md-icon-button\" ui-sref=\"app.accounts-edit({_id: item._id})\">\r\n							<md-icon>edit</md-icon>\r\n						</md-button>\r\n					</td>\r\n				</tr>\r\n			</tbody>\r\n			<tbody ng-show=\"list.data.total === 0\">\r\n				<tr>\r\n					<td colspan=\"6\" class=\"md-table-no-results\">\r\n						<md-icon>block</md-icon> Nenhum resultado\r\n					</td>\r\n				</tr>\r\n			</tbody>\r\n		</table>\r\n	</md-table-container>\r\n	\r\n	<md-table-pagination\r\n		md-limit=\"list.query.limit\"\r\n		md-limit-options=\"[5, 10, 15]\"\r\n		md-page=\"list.query.page\"\r\n		md-total=\"{{list.data.total}}\"\r\n		md-on-paginate=\"list.getAll\"\r\n		md-label=\"{page: \'Página:\', rowsPerPage: \'Linhas por página:\', of: \'de\'}\"\r\n		md-page-select>\r\n	</md-table-pagination>\r\n</div>");
$templateCache.put("core/account/addressFormContent/addressFormContent.tpl.html","<div layout layout-xs=\"column\" ng-if=\"vm.formLocale !== \'pt_BR\'\">\n    <div ceper icon=\"true\" model=\"vm.model.cep\" address=\"vm.model\" required=\"true\" region=\"\'uk\'\"></div>\n</div>\n<div layout layout-xs=\"column\">\n    <div ceper icon=\"true\" model=\"vm.model.cep\" address=\"vm.model\" required=\"true\" ng-if=\"vm.formLocale === \'pt_BR\'\"></div>\n    <md-input-container class=\"hide-on-cep-search\" flex-xs=\"none\" flex ng-class=\"{\'md-has-icon\':vm.formLocale !== \'pt_BR\'}\">\n        <label>{{\'STREET\' | translate}}</label>\n        <input ng-model=\"vm.model.street\" class=\"street-input\" required>\n    </md-input-container>\n    <md-input-container class=\"hide-on-cep-search\">\n        <label>{{\'NUMBER\' | translate}}</label>\n        <input ng-model=\"vm.model.num\" class=\"number-input\" required>\n    </md-input-container>\n</div>\n\n<div class=\"hide-on-cep-search\" layout layout-xs=\"column\">\n    <md-input-container flex-xs=\"none\" flex>\n        <label>{{\'BAIRRO\' | translate}}</label>\n        <md-icon md-font-set=\"material-icons\"></md-icon>\n        <input ng-model=\"vm.model.bairro\" ng-required=\"vm.formLocale === \'pt_BR\'\">\n    </md-input-container>\n    <md-input-container flex-xs=\"none\" flex ng-if=\"vm.formLocale === \'pt_BR\'\">\n        <label>{{\'CITY\' | translate}}</label>\n        <input ng-model=\"vm.model.city\" required>\n    </md-input-container>\n    <md-input-container ng-if=\"vm.formLocale !== \'pt_BR\'\">\n        <label>{{\'CITY\' | translate}}</label>\n        <input ng-model=\"vm.model.city\" required>\n    </md-input-container>\n    <md-input-container ng-if=\"vm.formLocale === \'pt_BR\'\">\n        <label>{{\'STATE\' | translate}}</label>\n        <md-select ng-model=\"vm.model.state\" required required=\"true\">\n            <md-option ng-repeat=\"opt in vm.states\" ng-value=\"opt.value\">{{opt.name}}</md-option>\n        </md-select>\n    </md-input-container>\n</div>");
$templateCache.put("core/account/companyForm/companyForm.tpl.html","<p class=\"title\">{{\'ACC_FORM_COMPANY_DETAILS\' | translate}}</p>\n\n<div layout layout-xs=\"column\">\n    <md-input-container flex>\n        <label>Contato na empresa</label>\n        <md-icon md-font-set=\"material-icons\">face</md-icon>\n        <input type=\"tel\" ng-model=\"vm.model.company.responsible\">\n    </md-input-container>\n</div>\n\n<div layout layout-xs=\"column\">\n    <md-input-container flex-sm=\"30\" flex-gt-sm=\"30\">\n        <label>CNPJ</label>\n        <md-icon md-font-set=\"material-icons\">store</md-icon>\n        <input type=\"tel\" ng-model=\"vm.model.profile.doc.cnpj\" ui-br-cnpj-mask>\n    </md-input-container>\n    \n    <md-input-container flex-xs=\"none\" flex>\n        <label>{{\'COMPANY\' | translate}}</label>\n        <input ng-model=\"vm.model.company.name\" required>\n    </md-input-container>\n</div>\n\n<!-- ADDRESS DIRECTIVE -->\n<div address-form-content model=\"vm.model.address\"></div>\n\n<!-- PHONE DIRECTIVE -->\n<div phone-form-content model=\"vm.model.contact\"></div>");
$templateCache.put("core/account/personFormContent/personFormContent.tpl.html","<div layout layout-xs=\"column\">\n    <md-input-container flex flex-xs=\"none\">\n        <label>{{\'NAME\' | translate}}</label>\n        <md-icon md-font-set=\"material-icons\">face</md-icon>\n        <input ng-model=\"vm.model.firstName\" required>\n    </md-input-container>\n    <md-input-container flex flex-xs=\"none\">\n        <label>{{\'LASTNAME\' | translate}}</label>\n        <input ng-model=\"vm.model.lastName\" required>\n    </md-input-container>\n</div>");
$templateCache.put("core/account/password/account-password.tpl.html","<div class=\"wrapper panel account-password-wrapper\">\r\n	<div class=\"panel-heading\">\r\n		<div class=\"panel-title\">\r\n			<md-icon md-font-set=\"material-icons\" md-font-content=\"lock_outline\"></md-icon> Alterar senha\r\n		</div>\r\n	</div>\r\n	\r\n	<!-- LOADING -->\r\n	<div class=\"loading\" ng-show=\"vm.factory.busy.status(\'get\')\">\r\n		<p layout layout-align=\"center center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n		</p>\r\n	</div>\r\n	\r\n	<form class=\"project-type-form form has-icon\" name=\"form\" ng-if=\"!vm.factory.busy.status(\'get\') && vm.service.model\">\r\n		<div class=\"panel-body\">\r\n			<div layout layout-xs=\"column\" class=\"nome\">\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>Senha antiga</label>\r\n					<md-icon md-font-set=\"material-icons\">history</md-icon>\r\n					<input ng-model=\"vm.service.model.oldPassword\" type=\"password\" required>\r\n				</md-input-container>\r\n			</div>\r\n			<div layout layout-xs=\"column\" class=\"senha\">\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>Nova senha</label>\r\n					<md-icon md-font-set=\"material-icons\">done</md-icon>\r\n					<input ng-model=\"vm.service.model.newPassword\" type=\"password\" required>\r\n				</md-input-container>\r\n			</div>\r\n			<div layout layout-xs=\"column\" class=\"nova-senha\">\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>Repetir senha</label>\r\n					<md-icon md-font-set=\"material-icons\">done_all</md-icon>\r\n					<input ng-model=\"vm.service.model.confirmPassword\" match=\"vm.service.model.newPassword\" type=\"password\" required>\r\n				</md-input-container>\r\n			</div>\r\n			\r\n			<br>\r\n			\r\n			<div layout layout-align=\"start center\">\r\n				<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'password\')\"></md-progress-circular>\r\n				<md-button ng-disabled=\"vm.service.formStatus(\'password\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"clean-left md-primary\" ng-if=\"!vm.service.busy.status(\'password\')\">\r\n					Alterar\r\n					\r\n					<md-tooltip>\r\n					Alterar senha\r\n					</md-tooltip>\r\n				</md-button>\r\n				<md-button ng-disabled=\"vm.service.busy.status(\'save\')\" ng-click=\"vm.goToProfile()\">\r\n					{{\'CANCEL\' | translate}}\r\n					\r\n					<md-tooltip>\r\n					{{\'CANCEL\' | translate}}\r\n					</md-tooltip>\r\n				</md-button>\r\n			</div>\r\n			\r\n		</div>\r\n	</form>\r\n</div>");
$templateCache.put("core/account/phoneFormContent/phoneFormContent.tpl.html","<div layout layout-xs=\"column\" ng-if=\"vm.formLocale === \'pt_BR\'\">\n    <md-input-container flex-sm=\"vm.flex\" flex-gt-sm=\"vm.flex\">\n        <label>{{\'PHONE\' | translate}}</label>\n        <md-icon md-font-set=\"material-icons\">smartphone</md-icon>\n        <input type=\"tel\" ng-model=\"vm.model.phone\" ui-br-phone-number >\n    </md-input-container>\n    <md-input-container flex-sm=\"vm.flex\" flex-gt-sm=\"vm.flex\">\n        <label>{{\'MOBILE\' | translate}}</label>\n        <input type=\"tel\" ng-model=\"vm.model.mobile\" ui-br-phone-number required>\n    </md-input-container>\n</div>\n<div layout layout-xs=\"column\" ng-if=\"vm.formLocale !== \'pt_BR\'\">\n    <md-input-container flex-sm=\"vm.flex\" flex-gt-sm=\"vm.flex\">\n        <label>{{\'PHONE\' | translate}}</label>\n        <md-icon md-font-set=\"material-icons\">smartphone</md-icon>\n        <input type=\"tel\" ng-model=\"vm.model.phone\">\n    </md-input-container>\n    <md-input-container flex-sm=\"vm.flex\" flex-gt-sm=\"vm.flex\">\n        <label>{{\'MOBILE\' | translate}}</label>\n        <input type=\"tel\" ng-model=\"vm.model.mobile\" required>\n    </md-input-container>\n</div>");
$templateCache.put("core/account/public/profile.tpl.html","<div class=\"profile\">\r\n    <!-- FORM -->\r\n    <form name=\"form\" class=\"form form-profile wrapper content\" ng-if=\"!vm.edit || vm.edit && vm.service.model\">\r\n        <div class=\"form-box has-icon basic\">\r\n            <div>\r\n                <p class=\"title\"><md-icon md-font-set=\"material-icons\" md-font-content=\"account_circle\"></md-icon> {{\'BASIC_DETAILS\' | translate}}</p>\r\n            </div>\r\n            <div class=\"box\">\r\n                <div person-form-content model=\"vm.service.model.profile\"></div>\r\n                <div layout layout-sm=\"column\">\r\n                    <md-input-container flex flex-xs=\"none\">\r\n                        <label>{{\'EMAIL\' | translate}}</label>\r\n                        <md-icon md-font-set=\"material-icons\">mail</md-icon>\r\n                        <input ng-model=\"vm.service.model.email\" required>\r\n                    </md-input-container>\r\n                </div>\r\n                \r\n                <div phone-form-content model=\"vm.service.model.profile.contact\" set-flex=\"50\"></div>\r\n            </div>\r\n        </div>\r\n        \r\n        <div class=\"form-box has-icon basic\">\r\n            <div>\r\n                <p class=\"title\"><md-icon md-font-set=\"material-icons\" md-font-content=\"payment\"></md-icon> {{\'PAYMENT_DETAILS\' | translate}}</p>\r\n            </div>\r\n            \r\n            <div class=\"box\">\r\n                <span class=\"md-caption\">{{\'WHO_IS_PAYING\' | translate}}</span>\r\n                <!-- ADDRESS DIRECTIVE -->\r\n                <div person-form-content model=\"vm.service.model.profile.address.payment.payer\"></div>\r\n                \r\n                <span class=\"md-caption\">{{\'BILLING_ADDRESS\' | translate}}</span>\r\n                <!-- ADDRESS DIRECTIVE -->\r\n                <div address-form-content model=\"vm.service.model.profile.address.payment\"></div>\r\n            </div>\r\n        </div>\r\n        \r\n        <div class=\"form-box has-icon basic\">\r\n            <div>\r\n                <p class=\"title\"><md-icon md-font-set=\"material-icons\" md-font-content=\"local_shipping\"></md-icon> {{\'SHIPPING_DETAILS\' | translate}}</p>\r\n            </div>\r\n            \r\n            <div class=\"box\">\r\n                <span class=\"md-caption\">{{\'WHO_IS_RECEIVING\' | translate}}</span>\r\n                <!-- ADDRESS DIRECTIVE -->\r\n                <div person-form-content model=\"vm.service.model.profile.address.shipping.to\"></div>\r\n                \r\n                <span class=\"md-caption\">{{\'SHIPPING_ADDRESS\' | translate}}</span>\r\n                <!-- ADDRESS DIRECTIVE -->\r\n                <div address-form-content model=\"vm.service.model.profile.address.shipping\"></div>\r\n            </div>\r\n        </div>\r\n\r\n        <div layout layout-align=\"start center\">\r\n            <md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'save\')\"></md-progress-circular>\r\n            <md-button ng-disabled=\"vm.service.formStatus(\'save\', form.$invalid)\" ng-click=\"vm.service.update()\" class=\"clean-left md-raised md-primary\" ng-if=\"!vm.service.busy.status(\'save\')\">\r\n                {{\'SAVE\' | translate}}\r\n                \r\n                <md-tooltip>\r\n                {{\'SAVE\' | translate}}\r\n                </md-tooltip>\r\n            </md-button>\r\n        </div>\r\n    </form>\r\n    <!-- /FORM -->\r\n</div>");
$templateCache.put("core/account/public/publicAccount.tpl.html","<div class=\"wrapper account-public\">\r\n    <div class=\"title-border\">\r\n        <h1 class=\"page-title title\">{{\'YOUR_ACCOUNT\' | translate}}</h1>\r\n        <div class=\"border\"></div>\r\n    </div>\r\n    <br>\r\n    <div layout-gt-sm layout=\"column\">\r\n        <div class=\"account-menu\">\r\n            <div class=\"user-details\">\r\n                <div class=\"user-details-box\" layout layout-align=\"start center\">\r\n                    <div class=\"pic\"></div>\r\n                    \r\n                    <div flex>\r\n                        <p class=\"username\">{{vm.currentUser.model.profile.fullName}}</p>\r\n                        <p>{{vm.currentUser.model.email}}</p>\r\n                    </div>\r\n                </div>\r\n\r\n                <md-divider></md-divider>\r\n            </div>\r\n            \r\n            <div\r\n                side-menu\r\n                items=\"vm.menu.userLoggedIn\"\r\n                class=\"side-menu\"\r\n                layout=\"row\"\r\n                hide-sm hide-xs>\r\n            </div>\r\n        </div>\r\n        \r\n        <div class=\"account-content\" view=\"{{vm.step.innerTabGroup}}\" flex=\"no-shrink\">\r\n            <div ui-view=\"account-content\" class=\"anim-in-out anim-slide-below-fade\" data-anim-sync=\"true\"></div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/cart/cartForm/cartForm.tpl.html","<form name=\"vm.form\" class=\"cart-form\">\r\n	<div class=\"form-colors row\" ng-repeat=\"item in vm.product.variants.attrs\">\r\n		<div ng-if=\"item.name === \'colors\'\" layout>\r\n			<div class=\"label\">\r\n				{{\'COLOR\' | translate}}\r\n			</div>\r\n			\r\n			<md-select ng-model=\"vm.cart.model.attrs.colorValue\" ng-change=\"vm.variantChange(\'colors\', vm.cart.model.attrs.colorValue, $index)\" placeholder=\"{{\'CHOOSE\' | translate}}\" required flex=\"40\">\r\n				<md-option ng-repeat=\"item in vm.colors\" value=\"{{item.value}}\" ng-disabled=\"item.disabled\"><span class=\"cart-color\" ng-class=\"item.value\"></span>{{item.display | translate}}</md-option>\r\n			</md-select>\r\n		</div>\r\n		<div ng-if=\"item.name === \'sizes\'\" layout>\r\n			<div class=\"label\">\r\n				{{\'SIZE\' | translate}}\r\n			</div>\r\n			\r\n			<md-select ng-model=\"vm.cart.model.attrs.sizes\" ng-change=\"vm.variantChange(\'sizes\', vm.cart.model.attrs.sizes, $index)\" placeholder=\"{{\'CHOOSE\' | translate}}\" required flex=\"40\">\r\n				<md-option ng-repeat=\"item in vm.sizes\" value=\"{{item.value}}\" ng-disabled=\"item.disabled\">{{item.display | translate}}</md-option>\r\n			</md-select>\r\n		</div>\r\n	</div>\r\n	\r\n	<div class=\"form-qty row\" layout>\r\n		<div class=\"label\">\r\n			{{\'QTY\' | translate}}\r\n		</div>\r\n		\r\n		<md-input-container md-no-float>\r\n			<input type=\"number\" min=\"0\" max=\"{{vm.qtyAvailable}}\" name=\"qty\" aria-label=\"{{\'CART_AVAI_ERROR\' | translate}}\" ng-model=\"vm.cart.model.qty\" required>\r\n			<div class=\"hint\">{{vm.qtyAvailable}} {{\'CART_AVAI\' | translate}}</div>\r\n			<div ng-messages=\"vm.form.qty.$error\">\r\n				<div ng-message=\"max\">{{\'CART_AVAI_ERROR\' | translate}}</div>\r\n			</div>\r\n		</md-input-container>\r\n	</div>\r\n	\r\n	<div class=\"form-action row\" layout layout-align=\"start center\">\r\n		<div class=\"label\"></div>\r\n		<md-button class=\"md-primary md-raised\" ng-click=\"vm.submit($event)\"><md-icon md-font-set=\"material-icons\">add_shopping_cart</md-icon> {{\'CART_ADD\' | translate}}</md-button>\r\n	</div>\r\n</form>");
$templateCache.put("core/cart/cartSidenav/cartSidenav.tpl.html","<md-toolbar flex=\"none\">\r\n    <div class=\"md-toolbar-tools\">\r\n        <div layout=\"column\" flex>\r\n            {{\'CART\' | translate}}\r\n        </div>\r\n    </div>\r\n</md-toolbar>\r\n\r\n<md-content class=\"cart-sidenav-content\" has-entries=\"{{!!vm.entries.length}}\" md-scroll-y flex=\"no-shrink\">\r\n    <div class=\"hide-entries no-results\">\r\n        <md-icon md-font-set=\"material-icons\">info_outline</md-icon> {{\'NO_ITEMS\' | translate}}\r\n    </div>\r\n    <ul class=\"list show-entries\" ng-if=\"vm.entries.length\">\r\n        <li layout layout-align=\"start center\" ng-repeat=\"item in vm.entries\">\r\n            <div class=\"image\">\r\n                <img ng-src=\"{{item.product.image.thumb}}\" class=\"md-avatar\" alt=\"{{item.product.name}}\" />\r\n            </div>\r\n            <div class=\"content\" layout=\"column\" flex>\r\n                <h3>{{item.product.name}}</h3>\r\n                <h4>{{item.product.priceOffer ? item.product.priceOffer : item.product.price | currency}} x {{item.qty}}</h4>\r\n            </div>\r\n            <md-button class=\"md-icon-button md-primary\" ng-click=\"vm.factory.remove($index)\">\r\n                <md-icon md-font-set=\"material-icons\">close</md-icon>\r\n            </md-button>\r\n        </li>\r\n    </ul>\r\n</md-content>\r\n\r\n<md-divider></md-divider>\r\n\r\n<div class=\"subtotal\" flex=\"none\">\r\n    Subtotal: {{ vm.factory.subtotal | currency}}\r\n</div>\r\n\r\n<md-divider></md-divider>\r\n\r\n<div class=\"actions\" layout layout-align=\"center center\" flex=\"none\">\r\n    <md-button class=\"md-primary\" ui-sref=\"app.cart\">{{\'SEE_CART\' | translate}}</md-button>\r\n    <md-button class=\"\" ui-sref=\"app.checkout.identification\" ng-disabled=\"!vm.factory.hasEntries()\">Checkout</md-button>\r\n</div>");
$templateCache.put("core/category/item/item.tpl.html","<div class=\"products item-wrapper\">\r\n	<div\r\n		class=\"product md-whiteframe-1dp\"\r\n		ng-repeat=\"product in vm.entries\"\r\n		ui-sref=\"app.product({id: product._id, slug: product.slug, category: vm.category, child: vm.child})\">\r\n		\r\n		<a class=\"image-wrapper\" md-ink-ripple>\r\n			<img ng-src=\"{{product.image.thumb}}\" class=\"md-card-image\" alt=\"{{product.name}}\">\r\n			\r\n			<div class=\"title\">\r\n				<p class=\"md-title fade\">{{product.name}}</p>\r\n			</div>\r\n		</a>\r\n		\r\n		<div layout=\"row\" class=\"details\" layout-align=\"end start\">\r\n			<div flex layout=\"column\">\r\n				<span class=\"price\" ng-class=\"{\'hide\': !product.priceOffer}\">{{product.price | currency}}</span>\r\n				<span class=\"price no-offer\" ng-class=\"{\'hide\': product.priceOffer}\">only</span>\r\n				<span class=\"price-discount\">{{product.priceOffer ? product.priceOffer : product.price | currency}}</span>\r\n			</div>\r\n			\r\n			<div>\r\n				<md-button class=\"md-icon-button\" ui-sref=\"app.product({id: product._id, slug: product.slug, category: vm.category, child: vm.child})\">\r\n					<i class=\"material-icons\">add_shopping_cart</i>\r\n				</md-button>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/category/tabs/categoryTabs.tpl.html","<md-tabs md-dynamic-height class=\"md-primary wrapper\" md-center-tabs=\"true\">\r\n	<md-tab\r\n		ng-repeat=\"item in vm.entries\"\r\n		label=\"{{item.title}} ({{item.cnt > 0 ? item.cnt : 0}})\"\r\n		ui-sref=\"app.publicCategories({category:item.slug})\"\r\n		md-active=\"vm.activeTab(item.slug)\">\r\n	</md-tab>\r\n</md-tabs>");
$templateCache.put("core/category/public/categories.tpl.html","<div\r\n	class=\"inner-breadcrumbs\"\r\n	breadcrumbs\r\n	data=\"vm.breadcrumbs\">\r\n</div>\r\n<div class=\"wrapper public category-public product-view\" ng-if=\"vm.service\">\r\n	\r\n	<div class=\"title-border\">\r\n		<h1 class=\"page-title title\">{{ vm.categoryTitle }}</h1>\r\n		<div class=\"border\"></div>\r\n	</div>\r\n	\r\n	<div class=\"filters\" layout=\"row\" layout-align=\"start center\">\r\n		<div layout=\"row\" layout-align=\"start center\" flex>\r\n			<div flex>\r\n				{{\'SHOWING\' | translate}}: {{vm.service.showingStart}} - {{vm.service.showingEnd}} {{\'OF\' | translate}} {{vm.service.total}} {{\'RESULT-S\' | translate}}\r\n			</div>\r\n			<div layout=\"row\" layout-align=\"end center\" flex>\r\n				<div hide-xs>Classificar por:</div>\r\n				<md-input-container>\r\n					<md-select ng-model=\"vm.service.filters.sort\" aria-label=\"classificar por\">\r\n						<md-option ng-repeat=\"sort in vm.service.sortOptions\" value=\"{{sort.value}}\">\r\n							{{sort.name}}\r\n						</md-option>\r\n					</md-select>\r\n				</md-input-container>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div layout=\"column\" layout-sm=\"row\" layout-gt-sm=\"row\">\r\n		<div class=\"category-list\" flex-sm flex-gt-sm>\r\n			<list\r\n				params=\"vm.params\"\r\n				instance=\"vm.service\"\r\n				template-url=\"core/category/public/list.tpl.html\">\r\n			</list>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/category/public/list.tpl.html","<!-- Listing -->\r\n<div class=\"listing\" layout=\"column\" layout-gt-md=\"row\"\r\n	is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n	is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n	has-entries=\"{{!!vm.service.entries.length}}\">\r\n	\r\n	<!-- Filters -->\r\n	\r\n	<!-- Entries -->\r\n	<div\r\n		list-content\r\n		service=\"vm.service\"\r\n		template-url=\"core/category/public/products.tpl.html\"\r\n		class=\"list-content\"\r\n		flex-gt-md>\r\n	</div>\r\n</div>");
$templateCache.put("core/category/public/products.tpl.html","<div class=\"category-list-wrapper\">\r\n	\r\n	<div class=\"no-results\" ng-show=\"!vm.service.entries.length && !vm.service.busy.status(\'loading\') && !vm.service.starting\">\r\n		<p>\r\n			<md-icon md-font-set=\"material-icons\">error_outline</md-icon> {{\'NO_RESULTS\' | translate}}\r\n		</p>\r\n	</div>\r\n	\r\n	<div class=\"loading\" ng-show=\"vm.service.busy.status(\'loading\') || vm.service.starting\">\r\n		<p layout layout-align=\"start center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> {{\'LOADING_RESULTS\' | translate}}\r\n		</p>\r\n	</div>\r\n	\r\n	<div\r\n		class=\"list-content category-item\"\r\n		ng-show=\"vm.service.entries.length && !vm.service.busy.status(\'loading\') && !vm.service.starting\"\r\n		category-item \r\n		entries=\"vm.service.entries\"\r\n		category=\"vm.service.filters.category\"\r\n		child=\"vm.service.filters.child\">\r\n	</div>\r\n	\r\n	<div layout=\"row\" ng-if=\"vm.service.entries.length\">\r\n		<md-button class=\"md-primary list-more\" ng-click=\"vm.service.goToPrevPage()\" ng-disabled=\"!(vm.service.page > 2)\">\r\n			<md-icon md-font-set=\"material-icons\">navigate_before</md-icon> {{\'PREV\' | translate}}\r\n		</md-button>\r\n		<md-button class=\"md-primary list-more\" ng-click=\"vm.service.goToNextPage()\" ng-disabled=\"!vm.service.loadMoreBtn\">\r\n			{{\'NEXT\' | translate}} <md-icon md-font-set=\"material-icons\">navigate_next</md-icon>\r\n		</md-button>\r\n	</div>\r\n</div>");
$templateCache.put("core/checkout/confirmation/confirmation.tpl.html","<!-- FORM -->\n<div class=\"form confirmation content\" is-completed=\"{{vm.isCompleted()}}\">\n    <div class=\"form-box basic\">\n        <div>\n            <p class=\"title\">\n                <md-icon md-font-set=\"material-icons\" ng-if=\"!vm.completed.error\">done</md-icon>\n                <md-icon md-font-set=\"material-icons\"  ng-if=\"vm.completed.error\">error_outline</md-icon>\n                {{\'CONFIRMATION\' | translate}}\n            </p>\n        </div>\n        <div class=\"box\">\n            <div class=\"show-loading loading\" layout layout-align=\"center center\">\n                <md-progress-circular md-mode=\'indeterminate\' md-diameter=\"40\"></md-progress-circular> {{\'LOADING\' | translate}}\n            </div>\n            \n            <div class=\"completed text-center hide-loading\" ng-if=\"!vm.completed.error\" layout=\"column\" layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\">\n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"success-msg\">\n                    <md-icon md-font-set=\"material-icons\">done</md-icon>\n                    <div>\n                        {{\'CONF_ORDER_RECEIVED\' | translate}}\n                        <span ng-if=\"vm.completed.order.payment.mode === \'paypal_balance\' || vm.completed.order.payment.mode === \'paypal_cc\' || vm.completed.order.payment.mode === \'cc\'\">!</span>\n                        <span ng-if=\"vm.completed.order.payment.mode === \'debito_bancario\'\">, {{\'CONF_DEBIT_MAKE_THE_PAYMENT\' | translate}} <a href=\"{{vm.completed.order.moip.bankDebit}}\" target=\"_blank\">{{\'CONF_DEBIT_DIRECTLY_BANK\' | translate}}</a> {{\'CONF_DEBIT_TO_CONFIRM\' | translate}}</span>\n                        <span ng-if=\"vm.completed.order.payment.mode === \'boleto\'\">, <a href=\"{{vm.completed.order.moip.invoice}}\" target=\"_blank\">{{\'CONF_PAY_INVOICE\' | translate}}</a> {{\'CONF_DEBIT_TO_CONFIRM\' | translate}}</span>\n                    </div>\n                </div>\n                \n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"payment-status-msg text-center\">\n                    <div ng-if=\"vm.completed.order.status === 0\">{{\'CONF_PAY_PENDING\' | translate}}</div>\n                    <div ng-if=\"vm.completed.order.status === 1\">{{\'CONF_PAY_COMPLETED\' | translate}}</div>\n                </div>\n                \n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"text-center\">\n                    <a ui-sref=\"app.order-payments\" class=\"order-link\">{{\'CONF_ORDER_NUMBER\' | translate}} #{{vm.completed.order.number}}</a>\n                </div>\n                \n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"payment-status-msg text-center\">\n                    <div>{{\'CONF_ORDER_NUMBER_LABEL\' | translate}}</div>\n                </div>\n                \n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"payment-button hide-loading\" ng-if=\"vm.completed.order.payment.mode === \'boleto\'\">\n                    <md-button class=\"md-raised md-primary\" href=\"{{vm.completed.order.moip.invoice}}\" target=\"_blank\">{{\'PRINT_INVOICE_FOR_PAYMENT\' | translate}}</md-button>\n                </div>\n                \n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"payment-button hide-loading\" ng-if=\"vm.completed.order.payment.mode === \'debito_bancario\'\">\n                    <md-button class=\"md-raised md-primary\" href=\"{{vm.completed.order.moip.bankDebit}}\" target=\"_blank\">{{\'FINISH_PAYMENT_YOUR_BANK\' | translate}}</md-button>\n                </div>\n            </div>\n            \n            <div class=\"error hide-loading\" ng-if=\"vm.completed.error\" layout=\"column\" layout-align=\"center center\">\n                <div layout layout-align-sm=\"center center\" layout-align-gt-sm=\"center center\" class=\"success-msg\">\n                    <md-icon md-font-set=\"material-icons\">error_outline</md-icon>\n                    {{\'CONFIRMATION_ERROR\' | translate}}\n                </div>\n                \n                <p class=\"md-caption\">{{\'CONFIRMATION_ERROR_RESTART\' | translate}}</p>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- /FORM -->");
$templateCache.put("core/checkout/identification/identification.tpl.html","<!-- FORM -->\n<div class=\"form identification content\" ng-if=\"!vm.edit || vm.edit && vm.product.model\">\n    <div class=\"form-box basic\">\n        <div>\n            <p class=\"title\"><md-icon md-font-set=\"material-icons\">fingerprint</md-icon> {{\'IDENTIFICATION\' | translate}}</p>\n        </div>\n        <div class=\"box login-default\">\n            <md-tabs md-dynamic-height md-border-bottom>\n                <md-tab label=\"{{\'NEW_CUSTOMER\' | translate}}\">\n                    <md-content>\n                        <br>\n                        <div register-form class=\"register-form\" redirect=\"\'/checkout/payment-details/\'\" cb=\"vm.cbRegister\" set-role=\"[\'user\']\"></div>\n                    </md-content>\n                </md-tab>\n                <md-tab label=\"{{\'RETURNING_CUSTOMER\' | translate}}\">\n                    <md-content>\n                        <br>\n                        <div login-form class=\"login-form\" redirect=\"\'/checkout/payment-details/\'\" cb=\"vm.cbLogin\"></div>\n                        \n                        <div class=\"help\" layout=\"row\">\n                            <a flex ui-sref=\"app.recovery\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_FORGOT_LINK\' | translate}}</a>\n                        </div>\n                    </md-content>\n                </md-tab>\n            </md-tabs>\n        </div>\n    </div>\n</div>\n<!-- /FORM -->");
$templateCache.put("core/checkout/payment/payment.tpl.html","<!-- FORM -->\n<form name=\"vm.form.payment\" class=\"form payment content\" is-loading=\"{{!!vm.service.busy.status(\'create\')}}\" is-redirecting=\"{{!!vm.paypalRedirect}}\">\n    <div class=\"form-box basic\">\n        <div>\n            <p class=\"title\"><md-icon md-font-set=\"material-icons\">payment</md-icon> {{\'PAYMENT\' | translate}}</p>\n        </div>\n        \n        <div class=\"box\">\n            <span class=\"md-caption hide-loading\">{{\'CHOOSE_PAYMENT_METHOD\' | translate}}</span>\n            \n            <md-radio-group class=\"hide-loading hide-redirecting\" ng-model=\"vm.service.model.payment.mode\" required>\n                <md-radio-button\n                    ng-value=\"\"\n                    aria-label=\"null\"\n                    class=\"hide\">\n                </md-radio-button>\n                <md-radio-button\n                    ng-value=\"\'paypal_balance\'\"\n                    aria-label=\"{{\'CHECKOUT_WITH_PAYPAL\' | translate}}\"\n                    class=\"md-primary radio-row\"\n                    ng-if=\"vm.factory.gateways.paypal_balance.status\">\n                    \n                    <div layout=\"row\" layout-align=\"start center\">\n                        <div flex>\n                            {{\'CHECKOUT_WITH_PAYPAL\' | translate}}\n                            <br>\n                            <span class=\"desc\">{{\'PAYPAL_REDIRECT_MSG\' | translate}}</span>\n                        </div>\n                        \n                        <a href=\"https://www.paypal.com.br\" target=\"_blank\" class=\"gateway-logo\">\n                            <img src=\"assets/images/paypal.png\" alt=\"{{\'PAY_BY_PAYPAL\' | translate}}\">\n                            \n                            <md-tooltip>\n                            {{\'PAY_BY_PAYPAL\' | translate}}\n                            </md-tooltip>\n                        </a>\n                    </div>\n                </md-radio-button>\n                <md-radio-button\n                    ng-value=\"\'paypal_cc\'\"\n                    aria-label=\"{{\'CREDIT_CARD\' | translate}}\"\n                    class=\"md-primary radio-row\"\n                    ng-if=\"vm.factory.gateways.paypal_cc.status\">\n                    \n                    <div layout=\"row\" layout-align=\"start center\">\n                        <div flex>\n                            {{\'CREDIT_CARD\' | translate}}\n                        </div>\n                        \n                        <a href=\"https://www.paypal.com.br\" target=\"_blank\" class=\"gateway-logo\">\n                            <img src=\"assets/images/paypal.png\" alt=\"{{\'PAY_BY_PAYPAL\' | translate}}\">\n                            \n                            <md-tooltip>\n                            {{\'PAY_BY_PAYPAL\' | translate}}\n                            </md-tooltip>\n                        </a>\n                    </div>\n                </md-radio-button>\n                <md-radio-button\n                    ng-value=\"\'boleto\'\"\n                    aria-label=\"{{\'BOLETO\' | translate}}\"\n                    class=\"md-primary radio-row\"\n                    ng-if=\"vm.factory.gateways.boleto.status\">\n                    \n                    <div layout=\"row\" layout-align=\"start center\">\n                        <div flex>\n                            {{\'BOLETO\' | translate}}\n                            <br>\n                            <span class=\"desc\">{{\'BOLETO_WARNING\' | translate}}</span>\n                        </div>\n                        \n                        <a href=\"https://www.moip.com.br\" target=\"_blank\" class=\"gateway-logo moip\">\n                            <img src=\"assets/images/moip.png\" alt=\"{{\'PAY_BY_MOIP\' | translate}}\">\n                            \n                            <md-tooltip>\n                            {{\'PAY_BY_MOIP\' | translate}}\n                            </md-tooltip>\n                        </a>\n                    </div>\n                </md-radio-button>\n                <md-radio-button\n                    ng-value=\"\'debito_bancario\'\"\n                    aria-label=\"{{\'DEBITO_BANCARIO\' | translate}}\"\n                    class=\"md-primary radio-row\"\n                    ng-if=\"vm.factory.gateways.debito_bancario.status\">\n                    \n                    <div layout=\"row\" layout-align=\"start center\">\n                        <div flex>\n                            {{\'DEBITO_BANCARIO\' | translate}}\n                        </div>\n                        \n                        <a href=\"https://www.moip.com.br\" target=\"_blank\" class=\"gateway-logo moip\">\n                            <img src=\"assets/images/moip.png\" alt=\"{{\'PAY_BY_MOIP\' | translate}}\">\n                            \n                            <md-tooltip>\n                            {{\'PAY_BY_MOIP\' | translate}}\n                            </md-tooltip>\n                        </a>\n                    </div>\n                </md-radio-button>\n                <md-radio-button\n                    ng-value=\"\'cc\'\"\n                    aria-label=\"{{\'CREDIT_CARD\' | translate}}\"\n                    class=\"md-primary radio-row\"\n                    ng-if=\"vm.factory.gateways.cc.status\">\n                    \n                    <div layout=\"row\" layout-align=\"start center\">\n                        <div flex>\n                            {{\'CREDIT_CARD\' | translate}}\n                        </div>\n                        \n                        <a href=\"https://www.moip.com.br\" target=\"_blank\" class=\"gateway-logo moip\">\n                            <img src=\"assets/images/moip.png\" alt=\"{{\'PAY_BY_MOIP\' | translate}}\">\n                            \n                            <md-tooltip>\n                            {{\'PAY_BY_MOIP\' | translate}}\n                            </md-tooltip>\n                        </a>\n                    </div>\n                </md-radio-button>\n            </md-radio-group>\n            \n            <!-- DEBITO BANCARIO FORM -->\n            <div class=\"debito-bancario gateway-form hide-loading\" ng-if=\"vm.service.model.payment.mode === \'debito_bancario\'\">\n                <span class=\"md-caption hide-loading\">{{\'CHOOSE_YOUR_BANK\' | translate}}</span>\n                \n                <div class=\"bank\">\n                    <a ng-repeat=\"bank in vm.factory.banks\" ng-click=\"vm.service.model.moip.bank = bank.value\" ng-class=\"{\'active\':vm.service.model.moip.bank===bank.value}\">\n                        <img ng-src=\"{{bank.image}}\" id=\"{{bank.value}}\" alt=\"{{bank.name}}\">\n                        <md-tooltip>\n                        {{\'PAY_WITH\' | translate}} {{bank.name}}\n                        </md-tooltip>\n                    </a>\n                </div>\n            </div>\n            \n            <!-- PAYPAL CC FORM -->\n            <div class=\"paypal_cc gateway-form hide-loading\" ng-if=\"vm.service.model.payment.mode === \'paypal_cc\'\">\n                <span class=\"md-caption hide-loading\">{{\'INSERT_CREDIT_CARD_INFORMATION\' | translate}}</span>\n                \n                <div layout layout-xs=\"column\">\n                    <md-input-container flex-sm flex-gt-sm>\n                        <label>{{\'CARD_NUMBER\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.paypal.card.number\" ng-model-options=\"{ updateOn: \'blur\' }\" type=\"number\" ng-minlength=\"13\" ng-maxlength=\"19\" required>\n                    </md-input-container>\n                    <md-input-container>\n                        <label>{{\'CARD_EXPIRY_FULLDATE\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.paypal.card.expiry\" mask=\"12/2199\" ng-minlength=\"7\" ng-maxlength=\"7\" required>\n                    </md-input-container>\n                    <md-input-container>\n                        <label>{{\'CARD_SECURITY_NUMBER\' | translate}}</label>\n                        <input type=\"number\" ng-model=\"vm.service.model.paypal.card.cvv2\" ng-minlength=\"3\" ng-maxlength=\"4\" required>\n                    </md-input-container>\n                </div>\n                \n                <div layout=\"row\" layout-xs=\"column\">\n                    <md-select ng-model=\"vm.service.model.paypal.card.type\" placeholder=\"{{\'CARD_TYPE\' | translate}}\" flex-sm flex-gt-sm required>\n                        <md-option ng-value=\"opt\" ng-repeat=\"opt in vm.factory.cardTypes.paypal\">{{ opt }}</md-option>\n                    </md-select>\n                </div>\n            </div>\n            \n            <!-- CC FORM -->\n            <div class=\"cc gateway-form hide-loading\" ng-if=\"vm.service.model.payment.mode === \'cc\'\">\n                <span class=\"md-caption\">{{\'INSERT_CREDIT_CARD_INFORMATION\' | translate}}</span>\n                \n                <div layout layout-xs=\"column\">\n                    <md-input-container flex-sm flex-gt-sm>\n                        <label>{{\'CARD_NUMBER\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.moip.card.number\" ng-model-options=\"{ updateOn: \'blur\' }\" type=\"number\" ng-minlength=\"13\" ng-maxlength=\"19\" required>\n                    </md-input-container>\n                    <md-input-container>\n                        <label>{{\'CARD_EXPIRY_DATE\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.moip.card.expiry\" mask=\"19/99\" ng-maxlength=\"5\" required>\n                    </md-input-container>\n                    <md-input-container>\n                        <label>{{\'CARD_SECURITY_NUMBER\' | translate}}</label>\n                        <input type=\"number\" ng-model=\"vm.service.model.moip.card.cvv2\" ng-minlength=\"3\" ng-maxlength=\"4\" required>\n                    </md-input-container>\n                </div>\n                <div layout layout-xs=\"column\">\n                    <md-input-container flex-sm flex-gt-sm>\n                        <label>{{\'CARD_NAME\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.moip.card.name\" required>\n                    </md-input-container>\n                    <md-input-container flex-sm flex-gt-sm>\n                        <label>{{\'CPF\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.moip.card.cpf\" ui-br-cpf-mask required>\n                    </md-input-container>\n                    <md-input-container flex-sm flex-gt-sm>\n                        <label>{{\'CARD_BIRTHDAY\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.moip.card.birthday\" mask=\"39/19/9999\" required>\n                    </md-input-container>\n                    <md-input-container flex-sm flex-gt-sm>\n                        <label>{{\'CARD_PHONE\' | translate}}</label>\n                        <input ng-model=\"vm.service.model.moip.card.phone\" ui-br-phone-number required>\n                    </md-input-container>\n                </div>\n                \n                <div layout layout-xs=\"column\">\n                    <md-select ng-model=\"vm.service.model.moip.card.type\" placeholder=\"{{\'CARD_TYPE\' | translate}}\" flex-sm flex-gt-sm required>\n                        <md-option ng-value=\"opt\" ng-repeat=\"opt in vm.factory.cardTypes.moip\">{{ opt }}</md-option>\n                    </md-select>\n                    <md-select ng-model=\"vm.service.model.moip.card.parcelas\" placeholder=\"Parcelas\" flex-sm flex-gt-sm required>\n                        <md-option ng-value=\"opt\" ng-repeat=\"opt in vm.factory.cardParcelas\">{{ opt }}</md-option>\n                    </md-select>\n                </div>\n            </div>\n            \n            <!-- LOADING MSGS -->\n            <div class=\"show-loading loading\" layout layout-align=\"start center\">\n                <md-progress-circular md-mode=\'indeterminate\' md-diameter=\"40\"></md-progress-circular> {{\'LOADING\' | translate}}\n            </div>\n            <div class=\"show-redirecting loading\" layout layout-align=\"start center\">\n                <md-progress-circular md-mode=\'indeterminate\' md-diameter=\"40\"></md-progress-circular> {{\'PAYPAL_REDIRECT_MSG\' | translate}}..\n            </div>\n        </div>\n        \n        <div ng-include=\"\'core/checkout/checkout-nav.tpl.html\'\" class=\"btn-group hide-loading\"></div>\n    </div>\n</form>\n<!-- /FORM -->");
$templateCache.put("core/checkout/paymentDetails/paymentDetails.tpl.html","<!-- FORM -->\n<form name=\"vm.form.paymentDetails\" class=\"form payment-details content\">\n    <div class=\"form-box has-icon basic\">\n        <div>\n            <p class=\"title\"><md-icon md-font-set=\"material-icons\">payment</md-icon> {{\'PAYMENT_DETAILS\' | translate}}</p>\n        </div>\n        \n        <div class=\"box\">\n            <span class=\"md-caption\">{{\'WHO_IS_PAYING\' | translate}}</span>\n            <!-- ADDRESS DIRECTIVE -->\n            <div person-form-content model=\"vm.service.model.payment.payer\"></div>\n            \n            <span class=\"md-caption\">{{\'BILLING_ADDRESS\' | translate}}</span>\n            <!-- ADDRESS DIRECTIVE -->\n            <div address-form-content model=\"vm.service.model.payment.address\"></div>\n            \n            <!-- SAME ADDRESS FOR SHIPPING -->\n            <span class=\"md-caption\">{{\'SHIPPING\' | translate}}</span>\n            <br><br>\n            <div>\n                <md-checkbox ng-model=\"vm.service.model.payment.same_for_shipping\" aria-label=\"{{\'SAME_FOR_SHIPPING\' | translate}}\">\n                {{\'SAME_FOR_SHIPPING\' | translate}}\n                </md-checkbox>\n            </div>\n\n            <br><br>\n        </div>\n        \n        <div ng-include=\"\'core/checkout/checkout-nav.tpl.html\'\" class=\"btn-group\"></div>\n    </div>\n</form>\n<!-- /FORM");
$templateCache.put("core/checkout/shippingDetails/shippingDetails.tpl.html","<!-- FORM -->\n<form name=\"vm.form.shippingDetails\" class=\"form shipping-details content\">\n    <div class=\"form-box has-icon basic\">\n        <div>\n            <p class=\"title\"><md-icon md-font-set=\"material-icons\">local_shipping</md-icon> {{\'SHIPPING_DETAILS\' | translate}}</p>\n        </div>\n        \n        <div class=\"box\">\n            <span class=\"md-caption\">{{\'WHO_IS_RECEIVING\' | translate}}</span>\n            <!-- ADDRESS DIRECTIVE -->\n            <div person-form-content model=\"vm.service.model.shipping.to\"></div>\n    \n            <span class=\"md-caption\">{{\'SHIPPING_ADDRESS\' | translate}}</span>\n            <!-- ADDRESS DIRECTIVE -->\n            <div address-form-content model=\"vm.service.model.shipping.address\"></div>\n        </div>\n\n        <div ng-include=\"\'core/checkout/checkout-nav.tpl.html\'\" class=\"btn-group\"></div>\n    </div>\n</form>\n<!-- /FORM -->");
$templateCache.put("core/checkout/shippingMethod/shippingMethod.tpl.html","<!-- FORM -->\n<form name=\"vm.form.shippingMethod\" class=\"form shipping-method content\">\n    <div class=\"form-box basic\">\n        <div>\n            <p class=\"title\"><md-icon md-font-set=\"material-icons\">local_shipping</md-icon> {{\'SHIPPING_METHOD\' | translate}}</p>\n        </div>\n        \n        <div class=\"box\">\n            <span class=\"md-caption\">{{\'SELECT_SHIP_METHOD\' | translate}}</span>\n            \n            <md-radio-group ng-model=\"vm.service.model.shipping.method\" required>\n                <md-radio-button\n                    ng-value=\"\"\n                    aria-label=\"null\"\n                    class=\"hide\">\n                </md-radio-button>\n                <md-radio-button\n                    ng-repeat=\"item in vm.factory.shippingMethods\"\n                    ng-value=\"item.slug\"\n                    aria-label=\"{{item.title | translate}}\"\n                    class=\"md-primary radio-row\">\n                    \n                    <div layout=\"row\">\n                        <div flex>\n                            {{item.title | translate}}\n                        </div>\n                        \n                        <div>\n                            {{item.price | currency}}\n                        </div>\n                    </div>\n                </md-radio-button>\n            </md-radio-group>\n        </div>\n        \n        <div ng-include=\"\'core/checkout/checkout-nav.tpl.html\'\" class=\"btn-group\"></div>\n    </div>\n</form>\n<!-- /FORM");
$templateCache.put("core/directives/backgroundImage/ceper.tpl.html","<md-input-container class=\"ceper\" ng-class=\"{\'md-has-icon\': vmCep.icon}\">\r\n	<label>{{\'CEP\' | translate}}</label>\r\n	<md-icon md-font-set=\"material-icons\" ng-if=\"vmCep.icon\">place</md-icon>\r\n	<md-progress-circular class=\"load\" md-mode=\"indeterminate\" md-diameter=\"18\" ng-show=\"vmCep.busy\"></md-progress-circular>\r\n	<input type=\"tel\" ng-model=\"vmCep.model\" ng-change=\"vmCep.get()\" ui-br-cep-mask ng-required=\"vmCep.required\" ng-if=\"vmCep.region !== \'uk\'\">\r\n	<input ng-model=\"vmCep.model\" ng-change=\"vmCep.get()\" ng-model-options=\"{ updateOn: \'blur\' }\" ng-required=\"vmCep.required\" ng-if=\"vmCep.region === \'uk\'\">\r\n</md-input-container>");
$templateCache.put("core/directives/barAvatar/barAvatar.tpl.html","<md-menu>\n    <md-button md-no-ink class=\"md-primary\" aria-label=\"{{\'OPEN_USER_MENU\' | translate}}\" data-badge=\"{{!!vm.factory.getCurrent()}}\" ng-click=\"$mdOpenMenu()\">\n        <div layout=\"row\" layout-align=\"start center\">\n            <div class=\"avatar\" background-image=\"{{vm.currentUser.model.image.thumb ? vm.currentUser.model.image.thumb : \'assets/images/user-avatar.jpg\'}}\" flex-order=\"{{vm.pictureOrder ? vm.pictureOrder : 0}}\"></div>\n            <div>{{vm.currentUser.model.profile.fullName}}</div>\n        </div>\n    </md-button>\n    <md-menu-content width=\"4\">\n        \n        <md-menu-item ng-repeat=\"item in vm.avatarMenu\" ng-if=\"vm.factory.getCurrent()\">\n            <md-button ui-sref=\"{{item.href}}\" ng-if=\"item.href\">\n                <md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\n                {{item.title | translate}}\n            </md-button>\n        </md-menu-item>\n        \n        <md-menu-divider ng-if=\"vm.factory.getCurrent()\"></md-menu-divider>\n        \n        <md-menu-item ng-if=\"vm.factory.getCurrent()\">\n            <md-button ng-click=\"vm.logout()\">\n                <md-icon md-font-set=\"material-icons\">power_settings_new</md-icon>\n                {{\'LOGOUT\' | translate}}\n            </md-button>\n        </md-menu-item>\n        \n        <md-menu-item ng-if=\"!vm.factory.getCurrent()\">\n            <md-button ui-sref=\"app.login\">\n                <md-icon md-font-set=\"material-icons\">person</md-icon>\n                {{\'LOGIN_BTN\' | translate}}\n            </md-button>\n        </md-menu-item>\n    </md-menu-content>\n</md-menu>");
$templateCache.put("core/directives/breadcrumbs/breadcrumbs.tpl.html","<div class=\"breadcrumbs md-caption\">\r\n	<div class=\"wrapper\">\r\n		<span><a ui-sref=\"app.home\">Home</a></span>\r\n		<span ng-if=\"vm.data.category\"> / <a ui-sref=\"app.publicCategories({category:vm.data.category, child: undefined})\" ui-sref-opts=\"{inherit:false}\" ui-sref-active>{{vm.data.categoryTitle}}</a></span>\r\n		<span ng-if=\"!vm.data.category && vm.data.categoryTitle\"> / <a ui-sref=\"app.publicCategoriesEmpty()\" ui-sref-opts=\"{inherit:false}\" ui-sref-active>{{vm.data.categoryTitle}}</a></span>\r\n		<span ng-if=\"vm.data.child\"> / <a ui-sref=\"app.publicCategories({category:vm.data.category, child:vm.data.child})\" ui-sref-opts=\"{inherit:false}\" ui-sref-active>{{vm.data.childTitle}}</a></span>\r\n		<span ng-if=\"vm.data.slug && vm.data.slugId\"> / <a ui-sref=\"app.product({slug:vm.data.slug, id: vm.data.slugId, category:vm.data.category, child:vm.data.child})\" ui-sref-opts=\"{inherit:false}\" ui-sref-active>{{vm.data.slugTitle}}</a></span>\r\n	</div>\r\n</div>");
$templateCache.put("core/directives/ceper/ceper.tpl.html","<md-input-container class=\"ceper\" ng-class=\"{\'md-has-icon\': vmCep.icon}\">\r\n	<label>{{\'CEP\' | translate}}</label>\r\n	<md-icon md-font-set=\"material-icons\" ng-if=\"vmCep.icon\">place</md-icon>\r\n	<md-progress-circular class=\"load\" md-mode=\"indeterminate\" md-diameter=\"18\" ng-show=\"vmCep.busy\"></md-progress-circular>\r\n	<input type=\"tel\" ng-model=\"vmCep.model\" ng-change=\"vmCep.get()\" ui-br-cep-mask ng-required=\"vmCep.required\" ng-if=\"vmCep.region !== \'uk\'\">\r\n	<input ng-model=\"vmCep.model\" ng-change=\"vmCep.get()\" ng-model-options=\"{ updateOn: \'blur\' }\" ng-required=\"vmCep.required\" ng-if=\"vmCep.region === \'uk\'\">\r\n</md-input-container>");
$templateCache.put("core/directives/cropper/cropper.tpl.html","<img ng-src=\"{{vm.dataUrl}}\" alt=\"\" ng-if=\"vm.dataUrl\">");
$templateCache.put("core/directives/innerLoading/innerLoading.tpl.html","<div class=\"inner-loading\" ng-show=\"vm.condition\" layout=\"row\" layout-align=\"center center\">\r\n	<p layout layout-align=\"center center\">\r\n		<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n	</p>\r\n</div>");
$templateCache.put("core/directives/loader/loader.tpl.html","<div class=\"page-loader\" ng-class=\"{\'show\':main.$page.load.status}\">\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent md-hue-3\"></md-progress-linear>\n</div>");
$templateCache.put("core/directives/sideMenu/sideMenu.tpl.html","<div flex>\n	<div class=\"content\" ng-class=\"{\'closed\': vm.closed}\">\n		<button\n		ng-repeat=\"item in vm.items\"\n		ui-sref=\"{{item.state ? item.state : item.href}}\"\n		class=\"menu-item\"\n		ng-class=\"{\'active\': vm.activeTab(item.innerTabGroup)}\"\n		md-ink-ripple\n		layout layout-align=\"start center\"\n		ng-disabled=\"item.disabled\">\n		<div flex>\n			<md-icon md-font-set=\"material-icons\" ng-if=\"!item.icon\">chevron_right</md-icon>\n			<md-icon md-font-set=\"{{item.icon.mdFontSet ? item.icon.mdFontSet : \'material-icons\'}}\" ng-if=\"item.icon && !item.icon.right\">{{item.icon.content ? item.icon.content : item.icon}}</md-icon>\n			{{item.label ? item.label : item.title | translate}}\n		</div>\n		<md-icon md-font-set=\"{{item.icon.mdFontSet ? item.icon.mdFontSet : \'material-icons\'}}\" ng-if=\"item.icon && item.icon.right\">{{item.icon.content ? item.icon.content : item.icon}}</md-icon>\n		<md-icon md-font-set=\"material-icons\" ng-if=\"item.checked\">done</md-icon>\n		</button>\n	</div>\n	\n	<div class=\"toggle-menu\" hide-gt-sm>\n		<a ng-click=\"vm.toggleMenu()\" md-ink-ripple>\n			<md-icon md-font-set=\"material-icons\" show-xs show-sm>menu</md-icon>\n		</a>\n	</div>\n</div>");
$templateCache.put("core/directives/sidenav/sidenav.tpl.html","<div sidenav-user-toolbar class=\"sidenav-user-toolbar\" ng-if=\"!vm.currentUser\"></div>\n\n<md-content flex layout=\"column\">\n    <div class=\"sidenav-menu\" sidenav-menu flex=\"noshrink\"></div>\n    \n    <!-- footer -->\n    <div class=\"page-footer\" layout=\"column\" layout-align=\"center center\" flex=\"noshrink\">\n        <p class=\"copyright\">\n            {{ main.setting.copyright }} © {{ main.year }}\n        </p>\n    </div>\n    <!-- /footer -->\n</md-content>");
$templateCache.put("core/directives/sidenavMenu/sidenavMenu.tpl.html","<ul class=\"app-menu admin-menu\">\r\n	<!-- <div ng-if=\"vm.menu.site.length && vm.area === \'public\'\">\r\n		<p class=\"label\">{{\'NAVIGATION\' | translate}}</p>\r\n		<li\r\n			class=\"parent-list-item\"\r\n			ng-repeat=\"item in vm.menu.site\"\r\n			md-active=\"vm.activeTab(item.tabGroup)\">\r\n			\r\n			<button class=\"md-button md-default-theme\" ui-sref=\"{{item.href}}\" md-ink-ripple>\r\n			<md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\r\n			{{item.title | translate}}\r\n			</button>\r\n			\r\n		</li>\r\n	</div>\r\n	\r\n	<div ng-if=\"vm.user.getCurrent() && vm.menu.userLoggedIn.length && vm.area === \'public\'\">\r\n		<p class=\"label\">{{\'USER\' | translate}}</p>\r\n		<li\r\n			class=\"parent-list-item\"\r\n			ng-repeat=\"item in vm.menu.userLoggedIn\"\r\n			ui-sref-active>\r\n			\r\n			<button class=\"md-button md-default-theme\" ui-sref=\"{{item.href}}\" md-ink-ripple>\r\n			<md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\r\n			{{item.title | translate}}\r\n			</button>\r\n			\r\n		</li>\r\n	</div>\r\n\r\n	<div ng-if=\"vm.showOwner() && vm.menu.owner.length\">\r\n		<p class=\"label\">Administrador</p>\r\n		<li\r\n			class=\"parent-list-item\"\r\n			ng-repeat=\"item in vm.menu.owner\"\r\n			ui-sref-active>\r\n			\r\n			<button class=\"md-button md-default-theme\" ui-sref=\"{{item.href}}\" md-ink-ripple>\r\n			<md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\r\n			{{item.title | translate}}\r\n			</button>\r\n			\r\n		</li>\r\n	</div> -->\r\n	\r\n	<div>\r\n		<p class=\"label\">Menu de acesso</p>\r\n		<li\r\n			class=\"parent-list-item\"\r\n			ng-repeat=\"item in vm.sideMenu\"\r\n			ui-sref-active>\r\n			\r\n			<button class=\"md-button md-default-theme\" ui-sref=\"{{item.href}}\" md-ink-ripple>\r\n			<md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\r\n			{{item.title | translate}}\r\n			</button>\r\n			\r\n		</li>\r\n	</div>\r\n\r\n	<div ng-if=\"vm.showAdmin() && vm.menu.admin.length\">\r\n		<p class=\"label\">Sistema</p>\r\n		<li\r\n			class=\"parent-list-item\"\r\n			ng-repeat=\"item in vm.menu.admin\"\r\n			ng-if=\"vm.showAdmin()\"\r\n			ui-sref-active>\r\n			\r\n			<button class=\"md-button md-default-theme\" ui-sref=\"{{item.href}}\" md-ink-ripple>\r\n			<md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\r\n			{{item.title | translate}}\r\n			</button>\r\n			\r\n		</li>\r\n	</div>\r\n<!-- \r\n	<div ng-if=\"vm.user.getCurrent() && vm.menu.currentUser.length\">\r\n		<p class=\"label\">{{vm.user.getCurrent().model.profile.firstName}}</p>\r\n		<li\r\n			class=\"parent-list-item\"\r\n			ng-repeat=\"item in vm.menu.currentUser\"\r\n			ui-sref-active>\r\n			\r\n			<button class=\"md-button md-default-theme\" ui-sref=\"{{item.href}}\" md-ink-ripple>\r\n			<md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\r\n			{{item.title | translate}}\r\n			</button>\r\n			\r\n		</li>\r\n	</div> -->\r\n</ul>");
$templateCache.put("core/directives/sidenavUserToolbar/sidenavUserToolbar.tpl.html","<md-toolbar class=\"md-tall\" ng-if=\"vm.user._current\">\n    <div class=\"md-toolbar-tools md-toolbar-tools-bottom\">\n        <div layout=\"column\" flex>\n            <div class=\"pic\"></div>\n            <div class=\"user\">{{vm.user._current.model.profile.fullName}}</div>\n            <div class=\"email\">{{vm.user._current.model.email}}</div>\n        </div>\n    </div>\n</md-toolbar>");
$templateCache.put("core/directives/toolbar/toolbar.tpl.html","<md-toolbar class=\"md-toolbar-admin\" layout layout-align=\"start center\" ng-class=\"{\'search-mode\': vm.searchMode}\">\n	<div class=\"md-toolbar-tools wrapper\">\n		Admin\n		<span flex></span>\n		\n		<md-button class=\"md-icon-button\" aria-label=\"logout\" ng-click=\"vm.user.logout()\">\n			<md-icon md-font-set=\"material-icons\">power_settings_new</md-icon>\n		</md-button>\n		\n		<!-- MENU MOBILE -->\n		<md-button class=\"md-icon-button\" ng-click=\"vm.sidenav(\'admin\').open()\" aria-label=\"Abrir menu lateral\">\n			<i class=\"material-icons\">menu</i>\n		</md-button>\n	</div>\n</md-toolbar>");
$templateCache.put("core/directives/toolbarAvatar/toolbarAvatar.tpl.html","<md-menu>\n    <md-button class=\"md-icon-button md-primary\" aria-label=\"{{\'OPEN_USER_MENU\' | translate}}\" data-badge=\"{{!!vm.factory.getCurrent()}}\" ng-click=\"$mdOpenMenu()\" show-gt-sm hide-xs>\n        <i class=\"material-icons\">person</i>\n    </md-button>\n    <md-menu-content width=\"4\">\n        <md-menu-item ng-if=\"vm.factory.getCurrent() && vm.currentUser.model.roles.isOwner\">\n            <md-menu>\n                <md-button ng-click=\"$mdOpenMenu()\">\n                    <md-icon md-font-set=\"material-icons\">lock</md-icon>\n                    Admin\n                </md-button>\n                <md-menu-content>\n                    <md-menu-item ng-repeat=\"item in vm.menu.ownerLoggedIn\">\n                        <md-button ui-sref=\"{{item.href}}\">\n                            <md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\n                            {{item.title | translate}}\n                        </md-button>\n                    </md-menu-item>\n                </md-menu-content>\n            </md-menu>\n        </md-menu-item>\n        \n        <md-menu-divider ng-if=\"vm.factory.getCurrent() && vm.currentUser.model.roles.isOwner && vm.menu.userLoggedIn\"></md-menu-divider>\n        \n        <md-menu-item ng-repeat=\"item in vm.menu.userLoggedIn\" ng-if=\"vm.factory.getCurrent() && vm.menu.userLoggedIn\">\n            <md-button ui-sref=\"{{item.href}}\" ng-if=\"item.href\">\n                <md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\n                {{item.title | translate}}\n            </md-button>\n        </md-menu-item>\n        \n        <md-menu-item ng-repeat=\"item in vm.menu.userLoggedOut\" ng-if=\"!vm.factory.getCurrent() && vm.menu.userLoggedOut\">\n            <md-button ui-sref=\"{{item.href}}\" ng-if=\"item.href\">\n                <md-icon md-font-set=\"material-icons\">{{item.icon}}</md-icon>\n                {{item.title | translate}}\n            </md-button>\n        </md-menu-item>\n        \n        <md-menu-divider ng-if=\"vm.factory.getCurrent()\"></md-menu-divider>\n        \n        <md-menu-item ng-if=\"vm.factory.getCurrent()\">\n            <md-button ng-click=\"vm.logout()\">\n                <md-icon md-font-set=\"material-icons\">power_settings_new</md-icon>\n                {{\'LOGOUT\' | translate}}\n            </md-button>\n        </md-menu-item>\n        \n        <md-menu-item ng-if=\"!vm.factory.getCurrent()\">\n            <md-button ui-sref=\"app.login\">\n                <md-icon md-font-set=\"material-icons\">person</md-icon>\n                {{\'LOGIN_BTN\' | translate}}\n            </md-button>\n        </md-menu-item>\n    </md-menu-content>\n</md-menu>");
$templateCache.put("core/directives/toolbarTabs/toolbarTabs.tpl.html","<md-tabs class=\"wrapper\" md-dynamic-height>\r\n	<md-tab ng-repeat=\"item in vm.menu.owner\" ng-if=\"vm.permissions.owner || vm.permissions.admin\" ng-click=\"vm.goTo(item.href)\" md-active=\"vm.activeTab(item.tabGroup)\" label=\"{{item.title | translate}}\"></md-tab>\r\n	<md-tab ng-repeat=\"item in vm.menu.admin\" ng-if=\"vm.permissions.admin\" ng-click=\"vm.goTo(item.href)\" md-active=\"vm.activeTab(item.tabGroup)\" label=\"{{item.title | translate}}\"></md-tab>\r\n</md-tabs>");
$templateCache.put("core/list/content/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content hide-loading hide-starting\">\r\n    <md-list-item ng-repeat=\"item in vm.service.entries\">\r\n        <md-icon md-font-set=\"material-icons\" ng-if=\"vm.service.primaryIcon\">{{vm.service.primaryIcon}}</md-icon>\r\n        <img alt=\"{{ item.title }}\" ng-src=\"{{item.image.thumb}}\" class=\"md-avatar\" ng-if=\"vm.service.showAvatar\" />\r\n        <p>{{ item.title }}</p>\r\n        <md-icon md-font-set=\"material-icons\" ng-click=\"vm.service.secondaryAction($event, item, $index)\" aria-label=\"Open Chat\" class=\"md-secondary md-hue-3\">delete</md-icon>\r\n\r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/list/filter-box/listFilterBox.tpl.html","<form name=\"vm.form\" layout-padding>\r\n	<div class=\"group\" ng-if=\"vm.listBrStates\">\r\n		<h4 class=\"title\"><md-icon md-font-set=\"material-icons\">place</md-icon> Localização</h4>\r\n		<md-select\r\n			placeholder=\"Estado\"\r\n			class=\"state\"\r\n			ng-model=\"vm.listFilters.state\">\r\n			<md-option ng-value=\"opt.value\" ng-repeat=\"opt in vm.listBrStates\">\r\n				{{ opt.name }}\r\n			</md-option>\r\n		</md-select>\r\n	</div>\r\n	<div class=\"group\">\r\n		<h4 class=\"title\"><md-icon md-font-set=\"material-icons\">event</md-icon> Periodo</h4>\r\n		<md-input-container>\r\n			<label>Data inicial</label>\r\n			<input mask=\"39/19/2999\" ng-model=\"vm.listFilters.startDate\" ng-model-options=\"{ updateOn: \'blur\' }\">\r\n		</md-input-container>\r\n		<md-input-container>\r\n			<label>Data final</label>\r\n			<input mask=\"39/19/2999\" ng-model=\"vm.listFilters.endDate\" ng-model-options=\"{ updateOn: \'blur\' }\">\r\n		</md-input-container>\r\n	</div>\r\n</form>");
$templateCache.put("core/list/load-more/listLoadMore.tpl.html","<div class=\"md-list-item-text list-more-wrapper\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\" ng-if=\"vm.service.busy.status(\'loadingMore\')\"></md-progress-linear>\r\n	<md-button class=\"md-primary list-more\" ng-click=\"vm.service.goToNextPage()\">\r\n		<md-icon md-font-set=\"material-icons\">loop</md-icon> Carregar mais resultados\r\n	</md-button>\r\n</div>");
$templateCache.put("core/list/search-box/listSearchBox.tpl.html","<form name=\"form\">\r\n    <md-input-container md-no-float>\r\n        <md-icon md-icon-set=\"material-icons\">search</md-icon>\r\n        <input ng-model=\"vm.service.filters.term\" ng-model-options=\"{ updateOn: \'blur\' }\" ng-keyup=\"cancel($event)\" placeholder={{vm.service.searchPlaceholder}}>\r\n    </md-input-container>\r\n</form>");
$templateCache.put("core/order/addrDisplay/addrDisplay.tpl.html","<p class=\"person\">{{vm.person.firstName+ \' \'+vm.person.lastName}}</p>\n<p>\n	<span ng-if=\"vm.formLocale !== \'pt_BR\'\">{{vm.address.num}} </span>\n	<span>{{vm.address.street}}</span>\n	<span ng-if=\"vm.formLocale === \'pt_BR\'\">, {{vm.address.num}}</span>\n</p>\n\n<p><span>{{vm.address.bairro}}</span></p>\n<p>\n	<span>{{vm.address.city}}</span>\n	<span ng-if=\"vm.formLocale === \'pt_BR\'\">, {{vm.address.state}}</span>\n</p>\n<p>\n	<span ng-if=\"vm.formLocale !== \'pt_BR\'\">{{vm.address.cep}}</span>\n	<span ng-if=\"vm.formLocale === \'pt_BR\'\">{{vm.address.cep | brCep}}</span>\n</p>");
$templateCache.put("core/order/public/list.tpl.html","<div class=\"md-whiteframe-1dp order-list\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <div layout class=\"custom-filters\" flex>\r\n        \r\n        <div layout=\"row\" layout-align=\"start center\">\r\n            <div hide-xs>\r\n                <md-icon md-fon-set=\"material-icons\">sort</md-icon>\r\n            </div>\r\n            <md-input-container>\r\n                <md-select ng-model=\"vm.service.filters.sort\" aria-label=\"{{\'SORT_BY\' | translate}}\">\r\n                    <md-option ng-repeat=\"sort in vm.service.sortOptions\" value=\"{{sort.value}}\">\r\n                        {{sort.name}}\r\n                    </md-option>\r\n                </md-select>\r\n            </md-input-container>\r\n        </div>\r\n        \r\n        <div class=\"flex-sep\"></div>\r\n        \r\n        <div layout=\"row\" layout-align=\"start center\">\r\n            <div hide-xs>\r\n                <md-icon md-fon-set=\"material-icons\">attach_money</md-icon>\r\n            </div>\r\n            <md-input-container>\r\n                <md-select ng-model=\"vm.service.filters.paymode\" aria-label=\"{{\'PAYMENT_TYPE\' | translate}}\">\r\n                    <md-option ng-repeat=\"sort in vm.service.filterPaymode\" value=\"{{sort.value}}\">\r\n                        {{sort.name}}\r\n                    </md-option>\r\n                </md-select>\r\n            </md-input-container>\r\n        </div>\r\n        \r\n        <div class=\"flex-sep\"></div>\r\n        \r\n        <div layout=\"row\" layout-align=\"start center\">\r\n            <div hide-xs>\r\n                <md-icon md-fon-set=\"material-icons\">lens</md-icon>\r\n            </div>\r\n            <md-input-container>\r\n                <md-select ng-model=\"vm.service.filters.status\" aria-label=\"{{\'PAYMENT_TYPE\' | translate}}\">\r\n                    <md-option ng-repeat=\"sort in vm.service.filterStatus\" value=\"{{sort.value}}\">\r\n                        {{sort.name}}\r\n                    </md-option>\r\n                </md-select>\r\n            </md-input-container>\r\n        </div>\r\n        \r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/order/public/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/order/public/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content hide-loading hide-starting\">\r\n    <md-subheader class=\"md-no-sticky\">{{\'CLICK_ITEM_SEE_DETAILS\' | translate}}</md-subheader>\r\n    <md-list-item\r\n        ng-repeat=\"item in vm.service.entries\"\r\n        ng-click=\"vm.service.primaryAction ? vm.service.primaryAction(item, $event) : \'\'\">\r\n        <div layout layout-xs=\"column\"\r\n            layout-align-sm=\"start center\" layout-align-gt-sm=\"start center\"\r\n            class=\"item\" flex>\r\n            <div class=\"status {{\'status-\' + item.status}}\">\r\n                <md-icon md-font-set=\"material-icons\">lens</md-icon>\r\n            </div>\r\n            <div flex-sm=\"50\" flex-gt-sm=\"50\">\r\n                <p>{{\'ORDER\' | translate}} #{{item.number}}</p>\r\n                <p class=\"md-caption\">{{ \'PAY_STAT_\' + item.status | translate }}</p>\r\n            </div>\r\n            <div flex-sm=\"20\" flex-gt-sm=\"20\">\r\n                <p>{{item.createdAt | customMoment}}</p>\r\n                <p class=\"md-caption\">#{{item.number}}</p>\r\n            </div>\r\n            <div flex-sm flex-gt-sm layout=\"column\" layout-align-sm=\"center end\" layout-align-gt-sm=\"center end\">\r\n                <p>{{item.total | currency}}</p>\r\n                <p class=\"md-caption\">{{\'PAID_\' + item.payment.mode | translate}}</p>\r\n            </div>\r\n        </div>\r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/order/public/order.tpl.html","<div class=\"order-public\">\r\n	<div class=\"inner-loading\" layout=\"row\" layout-align=\"center center\" ng-if=\"vm.factory.busy.status(\'get\')\">\r\n		<p layout layout-align=\"center center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n		</p>\r\n	</div>\r\n	<div ng-if=\"!vm.factory.busy.status(\'get\') && vm.service\">\r\n		<div class=\"order-content md-whiteframe-1dp\" layout=\"column\">\r\n			<!-- COLUNA DADOS -->\r\n			<div class=\"full-info column\" layout layout-xs=\"column\" layout-align-sm=\"start center\" layout-align-gt-sm=\"start center\" flex-sm flex-gt-sm>\r\n				<h2 class=\"md-title\" flex-sm flex-gt-sm><span>{{\'ORDER\' | translate}}</span> <span class=\"color-primary\">#{{vm.service.model.number}}</span></h2>\r\n				<span class=\"md-body-1\">{{vm.service.model.createdAt | customMoment}}</span>\r\n			</div>\r\n			<!-- COLUNA RESUMO -->\r\n			<div class=\"resume column\">\r\n				<!-- RESUMO VALORES -->\r\n				<div class=\"value-info\">\r\n					<span class=\"md-caption color-primary\">{{\'RESUME\' | translate}}</span>\r\n					\r\n					<div layout>\r\n						<p flex class=\"md-body-1\">Subtotal</p>\r\n						<p class=\"md-body-1\">{{vm.service.subtotal | currency}}</p>\r\n					</div>\r\n					<div layout>\r\n						<p flex class=\"md-body-1\">{{\'SHIPPING\' | translate}}</p>\r\n						<p class=\"md-body-1\">{{vm.service.model.shipping.price | currency}}</p>\r\n					</div>\r\n					<div layout class=\"total\">\r\n						<p flex class=\"md-body-2\">{{\'TOTAL\' | translate}}</p>\r\n						<p class=\"md-body-2\">{{vm.service.total | currency}}</p>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- INFORMAÇÕES DE PAGAMENTO -->\r\n				<div class=\"payment-info\">\r\n					<span class=\"md-caption color-primary\">{{\'PAYMENT\' | translate}}</span>\r\n					\r\n					<div>\r\n						<div layout>\r\n							<p flex class=\"md-body-1\">{{\'PAID_WITH\' | translate}}</p>\r\n							<p class=\"md-body-1\">{{\'PAID_\' + vm.service.model.payment.mode | translate}}</p>\r\n						</div>\r\n						<div layout ng-if=\"vm.service.model.moip || vm.service.model.paypal\">\r\n							<p flex class=\"md-body-1\">Gateway</p>\r\n							<p class=\"md-body-1\" ng-if=\"vm.service.model.moip\">MOIP</p>\r\n							<p class=\"md-body-1\" ng-if=\"vm.service.model.paypal\">Paypal</p>\r\n						</div>\r\n						<div layout layout-align=\"start center\">\r\n							<p flex class=\"md-body-1\">Status</p>\r\n							<span class=\"md-body-2\">{{\'PAY_STAT_\' + vm.service.model.status | translate}}</span>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- STATUS INTERNO -->\r\n				<div class=\"internal-info\">\r\n					<span class=\"md-caption color-primary\">{{\'INTERNAL_MONITORING\' | translate}}</span>\r\n					\r\n					<div>\r\n						<div layout layout-align=\"start center\">\r\n							<p flex class=\"md-body-1\">Status</p>\r\n\r\n							<span class=\"md-body-2\">{{\'PROG_\' + vm.service.model.internalStatus | translate}}</span>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- RASTREAMENTO -->\r\n				<div class=\"tracking-info\">\r\n					<span class=\"md-caption color-primary\">{{\'TRACKING_CODE\' | translate}}</span>\r\n					\r\n					<div>\r\n						<div layout layout-align=\"start center\">\r\n							<p flex class=\"md-body-1\">{{\'CODE\' | translate}}</p>\r\n							<span class=\"md-body-2\">{{vm.service.model.shipping.tracking ? vm.service.model.shipping.tracking : \'NOT_AVAILABLE\' | translate}}</span>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			\r\n			<!-- COLUNA DADOS -->\r\n			<div class=\"full-info column\" flex-sm flex-gt-sm>\r\n				<!-- DADOS DO COBRANÇA E ENTREGA -->\r\n				<div layout layout-xs=\"column\">\r\n					<!-- COBRANÇA -->\r\n					<div flex-sm flex-gt-sm>\r\n						<span class=\"md-caption color-primary\">{{\'PAYMENT_DETAILS\' | translate}}</span>\r\n						\r\n						<div addr-display address=\"vm.service.model.payment.address\" person=\"vm.service.model.payment.payer\" form-locale=\"vm.formLocale\"></div>\r\n					</div>\r\n					\r\n					<!-- ENTREGA -->\r\n					<div flex-sm flex-gt-sm>\r\n						<span class=\"md-caption color-primary\">{{\'SHIPPING_DETAILS\' | translate}}</span>\r\n						\r\n						<div addr-display address=\"vm.service.model.shipping.address\" person=\"vm.service.model.shipping.to\" form-locale=\"vm.formLocale\"></div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- PRODUTOS -->\r\n				<div class=\"products\">\r\n					<span class=\"md-caption color-primary\">{{\'PRODUCTS\' | translate}}</span>\r\n					\r\n					<div class=\"list\">\r\n						<div class=\"item\" ng-repeat=\"item in vm.service.model.cart\" layout layout-xs=\"column\">\r\n							<div class=\"image\" layout layout-align=\"start center\">\r\n								<img ng-src=\"{{item.product.image.thumb}}\" alt=\"{{vm.service.model.product.name}}\">\r\n							</div>\r\n							<div flex-sm flex-gt-sm>\r\n								<p>{{item.product.name}}</p>\r\n								<p class=\"md-caption\">{{item.attrs | attrsToString}}</p>\r\n							</div>\r\n							<div layout=\"column\" layout-align=\"start end\">\r\n								<p>{{item.qty * item.price | currency}}</p>\r\n								<p class=\"md-caption\">{{item.qty}} x {{item.price | currency}}</p>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n				<md-divider></md-divider>\r\n				\r\n				<!-- TIPO ENTREGA -->\r\n				<div>\r\n					<span class=\"md-caption color-primary\">{{\'SHIPPING_METHOD\' | translate}}</span>\r\n					\r\n					<div layout layout-xs=\"column\">\r\n						<div flex-sm flex-gt-sm>\r\n							<p>{{\'SHIP_\' + vm.service.model.shipping.method | translate}}</p>\r\n						</div>\r\n						<div>\r\n							<p>{{vm.service.model.shipping.price | currency}}</p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/order/public/orders.tpl.html","<div class=\"public-orders\">\r\n	<list\r\n		params=\"vm.params\"\r\n		template-url=\"core/order/public/list.tpl.html\"\r\n		class=\"list-default\">\r\n	</list>\r\n</div>");
$templateCache.put("core/product/prodForm/prodForm.tpl.html","<!-- LOADING -->\r\n<div class=\"loading\" ng-show=\"vm.factory.busy.status(\'productGet\')\">\r\n	<p layout layout-align=\"center center\">\r\n		<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n	</p>\r\n</div>\r\n\r\n<!-- FORM -->\r\n<form name=\"form\" class=\"form form-product content\" ng-if=\"!vm.edit || (vm.edit && vm.service.model)\">\r\n	<div class=\"panel-body clean-bottom\">\r\n		<p class=\"title\">{{\'PROD_FORM_BASIC_DETAILS\' | translate}}</p>\r\n		\r\n		<div layout=\"column\" layout-gt-sm=\"row\">\r\n			<md-input-container flex>\r\n				<label>{{\'NAME\' | translate}}</label>\r\n				<input ng-model=\"vm.service.model.name\" required>\r\n			</md-input-container>\r\n			<md-input-container flex>\r\n				<label>URL</label>\r\n				<input ng-model=\"vm.service.model.slug\" ng-blur=\"vm.service.slugifyUrl(vm.service.model.slug)\" required product-slug-validator edit={{vm.service.model._id}}>\r\n			</md-input-container>\r\n		</div>\r\n		\r\n		<textarea froala=\"vm.factory.froalaOptions\" ng-model=\"vm.service.model.desc\"></textarea>\r\n		\r\n		<br><br>\r\n		\r\n		<p class=\"title\">{{\'PROD_FORM_CAT_DETAILS\' | translate}}</p>\r\n		<div class=\"categories\">\r\n			<md-chips\r\n				class=\"category-chips\"\r\n				ng-model=\"vm.service.model.categories\"\r\n				md-autocomplete-snap\r\n				md-transform-chip=\"vm.factory.transformChip($chip)\"\r\n				md-require-match=\"true\">\r\n				<md-autocomplete\r\n					md-selected-item=\"vm.selectedItem\"\r\n					md-search-text=\"vm.searchText\"\r\n					md-items=\"item in vm.querySearch(vm.searchText)\"\r\n					md-item-text=\"item.title\"\r\n					md-min-length=\"1\"\r\n					md-delay=\"1000\"\r\n					placeholder=\"{{\'PROD_FORM_CAT\' | translate}}\">\r\n					<span md-highlight-text=\"vn.searchText\">{{item.parentTitle}}</span>\r\n				</md-autocomplete>\r\n				<md-chip-template>\r\n					<span>\r\n						<strong>{{$chip.parentTitle}}</strong>\r\n					</span>\r\n				</md-chip-template>\r\n			</md-chips>\r\n		</div>\r\n		\r\n		<br><br>\r\n		\r\n		<p class=\"title\">{{\'PROD_FORM_PRICE_DETAILS\' | translate}}</p>\r\n		\r\n		<div layout=\"column\" layout-gt-sm=\"row\">\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>Preço real</label>\r\n				<input ng-model=\"vm.service.model.defaultVariant.price.retail\" ui-money-mask=\"2\" required>\r\n			</md-input-container>\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>Preço listagem</label>\r\n				<input ng-model=\"vm.service.model.defaultVariant.price.list\" ui-money-mask=\"2\">\r\n			</md-input-container>\r\n		</div>\r\n		\r\n		<p class=\"title\">Detalhes de entrega</p>\r\n		\r\n		<div layout=\"column\" layout-gt-sm=\"row\">\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>Altura(cm)</label>\r\n				<input ng-model=\"vm.service.model.defaultVariant.shipping.dimensions.height\" ui-number-mask=\"3\" required>\r\n			</md-input-container>\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>Comprimento(cm)</label>\r\n				<input ng-model=\"vm.service.model.defaultVariant.shipping.dimensions.length\" ui-number-mask=\"3\">\r\n			</md-input-container>\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>Largura(cm)</label>\r\n				<input ng-model=\"vm.service.model.defaultVariant.shipping.dimensions.width\" ui-number-mask=\"3\">\r\n			</md-input-container>\r\n		</div>\r\n		\r\n		<div layout=\"column\" layout-gt-sm=\"row\">\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>Peso(kg)</label>\r\n				<input ng-model=\"vm.service.model.defaultVariant.shipping.weight\" ui-number-mask=\"3\" required>\r\n			</md-input-container>\r\n		</div>\r\n\r\n		<p class=\"title\">Estoque</p>\r\n		\r\n		<div layout=\"column\" layout-gt-sm=\"row\">\r\n			<md-input-container>\r\n				<label>Quantidade</label>\r\n				<input type=\"number\" ng-model=\"vm.service.model.defaultVariant.qty\" required>\r\n			</md-input-container>\r\n		</div>\r\n\r\n		<p class=\"title\">{{\'PROD_FORM_FEATURED_DETAILS\' | translate}}</p>\r\n		\r\n		<div layout=\"column\" layout-gt-sm=\"row\">\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>{{\'FEATURED\' | translate}}</label>\r\n				<md-select ng-model=\"vm.service.model.featured.status\" required>\r\n					<md-option value=\"1\">\r\n						{{\'YES\' | translate}}\r\n					</md-option>\r\n					<md-option value=\"0\">\r\n						{{\'NO\' | translate}}\r\n					</md-option>\r\n				</md-select>\r\n			</md-input-container>\r\n			<md-input-container flex-xs flex-sm>\r\n				<label>{{\'ORDER\' | translate}}</label>\r\n				<input type=\"number\" ng-model=\"vm.service.model.featured.order\">\r\n			</md-input-container>\r\n		</div>\r\n		\r\n		<!-- <div class=\"variants\" layout=\"column\">\r\n				<div>\r\n						<p class=\"title\">{{\'PROD_FORM_VARIATIONS_DESC\' | translate}}</p>\r\n				</div>\r\n				<div class=\"box\" layout=\"column\">\r\n						<div variant-form ng-repeat=\"item in vm.variants\" status=\"item.status\" name=\"{{item.name}}\" title=\"{{item.name | translate}}\" entries=\"vm.factory.variants[item.name]\" model=\"vm.service.model.variants.attrs\" position=\'{{$index}}\' lang></div>\r\n				</div>\r\n		</div> -->\r\n		\r\n		<p class=\"title\">{{\'PROD_FORM_UPLOAD_DETAILS\' | translate}}</p>\r\n		\r\n		<div layout=\"column\">\r\n			<div>\r\n				<p class=\"subtitle\">{{\'PROD_FORM_UP_DESC\' | translate}}</p>\r\n			</div>\r\n			<div layout=\"column\">\r\n				<md-button\r\n					class=\"md-raised clean-left\"\r\n					ng-click=\"vm.service.chooseFile()\"\r\n					accept=\"image/*\">\r\n					{{\'CHOOSE_FILE\' | translate}}\r\n				</md-button>\r\n				\r\n				<div class=\"result-img\">\r\n					<img ng-if=\"vm.service.model.image.file && !vm.service.model.image64\" ng-src=\"{{vm.service.model.image.thumb}}\">\r\n					<img ng-if=\"vm.service.model.image64\" ng-src=\"{{vm.service.model.image64}}\">\r\n				</div>\r\n			</div>\r\n		</div>\r\n		\r\n		<div layout layout-align=\"start center\">\r\n			<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'productCreate\')\"></md-progress-circular>\r\n			<md-button ng-disabled=\"vm.service.formStatus(\'productCreate\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary clean-left\" ng-if=\"!vm.service.busy.status(\'productCreate\')\">\r\n				{{\'SAVE\' | translate}}\r\n				\r\n				<md-tooltip>\r\n				{{\'SAVE\' | translate}}\r\n				</md-tooltip>\r\n			</md-button>\r\n			<md-button ng-disabled=\"vm.service.busy.status(\'productCreate\')\" ui-sref=\"app.products\">\r\n				{{\'CANCEL\' | translate}}\r\n				\r\n				<md-tooltip>\r\n				{{\'CANCEL\' | translate}}\r\n				</md-tooltip>\r\n			</md-button>\r\n		</div>\r\n	</div>\r\n</form>\r\n<!-- /FORM -->");
$templateCache.put("core/product/productSlider/productSlider.tpl.html","<div class=\"product-slider\" layout-sm=\"row\" layout-gt-sm=\"row\">\r\n	<div class=\"pics\" layout=\"column\">\r\n		<div ng-repeat=\"item in vm.images\" class=\"pic\" ng-if=\"vm.selected === $index\">\r\n			<img ng-src=\"{{item.thumb}}\" alt=\"\">\r\n		</div>\r\n	</div>\r\n	\r\n	<md-content class=\"thumbs\" md-scroll>\r\n		<div class=\"thumb\" ng-repeat=\"item in vm.images\" ng-class=\"{\'active\' : vm.selected === $index}\" ng-click=\"vm.selected=$index\">\r\n			<img ng-src=\"{{item.img}}\" alt=\"\">\r\n		</div>\r\n	</md-content>\r\n</div>");
$templateCache.put("core/product/prodView/prodView.tpl.html","<div class=\"prod-view\" flex>\r\n	<div class=\"prod-grid\">\r\n		<div class=\"no-results\" ng-if=\"!vm.entries\">\r\n			{{\'NO_RESULTS\' | translate}}\r\n		</div>\r\n		<div class=\"prod-item\" ng-repeat=\"product in vm.entries\" ng-if=\"vm.entries\">\r\n			<a class=\"img-link\" ui-sref=\"app.product({id: product._id, slug: product.slug, category: vm.category, child: vm.child})\" alt=\"{{product.name}}\" md-ink-ripple>\r\n				<img ng-src=\"{{product.image.thumb}}\" alt=\"{{product.name}}\">\r\n			</a>\r\n			<div class=\"prod-item-details\" layout layout-align=\"start center\">\r\n				<div class=\"prod-info\" flex>\r\n					<h3>{{product.name}}</h3>\r\n					<h4>\r\n					<span class=\"price regular-price\" ng-class=\"{\'hide\': !product.priceOffer}\">{{product.price | currency}}</span>\r\n					<span class=\"price no-offer\" ng-class=\"{\'hide\': product.priceOffer}\">{{\'ONLY\' | translate}}</span>\r\n					<span class=\"price-discount\">{{product.priceOffer ? product.priceOffer : product.price | currency}}</span>\r\n					</h4>\r\n				</div>\r\n				\r\n				<md-button ui-sref=\"app.product({id: product._id, slug: product.slug, category: vm.category, child: vm.child})\" hide-xs class=\"md-raised md-primary\">\r\n				{{\'BUY\' | translate}}\r\n				</md-button>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/product/public/product.tpl.html","<div\r\n	breadcrumbs\r\n	data=\"vm.breadcrumbs\">\r\n</div>\r\n<!-- PRODUCT -->\r\n<div class=\"wrapper public-product\">\r\n	<div class=\"product\">\r\n		<!-- TITLE -->\r\n		<div class=\"title-border\">\r\n			<h1 class=\"page-title title\">{{vm.product.name}}</h1>\r\n			<div class=\"border\"></div>\r\n		</div>\r\n		\r\n		<!-- CONTENT -->\r\n		<div layout=\"column\" layout-sm=\"row\" layout-gt-sm=\"row\">\r\n			<!-- SLIDER -->\r\n			<div\r\n				product-slider\r\n				images=\"[{ img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }, { img: vm.product.image.file, thumb: vm.product.image.thumb }]\">\r\n			</div>\r\n			\r\n			<!-- ROW SEPARATOR -->\r\n			<div class=\"separator\" flex-sm flex-gt-sm></div>\r\n			\r\n			<!-- INFO -->\r\n			<div class=\"info\" flex-sm flex-gt-sm>\r\n				<div class=\"price-before\" ng-class=\"{\'hide\': product.priceOffer}\">{{vm.product.price | currency}}</div>\r\n				<div class=\"price\">{{vm.product.priceOffer ? vm.product.priceOffer : vm.product.price | currency}}</div>\r\n				\r\n				<div class=\"content\">\r\n					<md-chips class=\"custom-chips\" ng-model=\"vm.product.categories\" readonly=\"true\">\r\n						<md-chip-template>\r\n							<span>\r\n								<strong>{{$chip.title}}</strong>\r\n							</span>\r\n						</md-chip-template>\r\n					</md-chips>\r\n					\r\n					<div class=\"html\" ng-bind-html=\"vm.product.desc\"></div>\r\n					\r\n					<div cart-form product=\"vm.product\" variants=\"vm.variants\" ng-if=\"vm.product || vm.variants\"></div>\r\n\r\n					<!-- <div class=\"share\">\r\n						<a facebook class=\"facebookShare\" data-url=\'vm.url\' data-shares=\'shares\'>{{ shares }}</a>\r\n						<a twitter  data-lang=\"en\" data-count=\'horizontal\' data-url=\'vm.url\' data-via=\'ocotidiano\' data-size=\"medium\" data-text=\'{{vm.product.name}}\' ></a>\r\n						<div gplus class=\"g-plus\" data-size=\"tall\" data-annotation=\"bubble\" data-href=\'vm.url\' data-action=\'share\'></div>\r\n					</div> -->\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/product/public/products.tpl.html","<p></p>\r\n\r\n<div class=\"wrapper public category-public product-search product-view\">\r\n	<h1 class=\"page-title\"><span>{{\'SEARCH_FOR\' | translate}}:</span> {{vm.service.filters.term}}</h1>\r\n	\r\n	<form name=\"form\">\r\n		<div layout=\"row\" layout-align=\"start center\">\r\n			<md-input-container md-no-float class=\"md-block\" flex>\r\n				<md-icon md-font-set=\"material-icons\" style=\"display:inline-block;\">search</md-icon>\r\n				<input ng-model=\"vm.service.filters.term\" type=\"text\" ng-model-options=\"{ updateOn: \'blur\' }\" placeholder=\"O que você procura?\">\r\n			</md-input-container>\r\n			\r\n			<md-button class=\"md-icon-button send-btn\" aria-label=\"Buscar\">\r\n				<md-icon md-font-set=\"material-icons\">send</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</form>\r\n	<div class=\"filters\" layout=\"row\" layout-align=\"start center\">\r\n		<div layout=\"row\" layout-align=\"start center\" flex>\r\n			<div flex>\r\n				{{\'SHOWING\' | translate}}: {{vm.service.showingStart}} - {{vm.service.showingEnd}} {{\'OF\' | translate}} {{vm.service.total}} {{\'RESULT-S\' | translate}}\r\n			</div>\r\n			<div layout=\"row\" layout-align=\"end center\" flex>\r\n				<div hide-xs>Classificar por:</div>\r\n				<md-input-container>\r\n					<md-select ng-model=\"vm.service.filters.sort\" aria-label=\"classificar por\">\r\n						<md-option ng-repeat=\"sort in vm.service.sortOptions\" value=\"{{sort.value}}\">\r\n							{{sort.name}}\r\n						</md-option>\r\n					</md-select>\r\n				</md-input-container>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div layout=\"column\" layout-sm=\"row\" layout-gt-sm=\"row\">\r\n		<div class=\"category-list\" flex>\r\n			<list\r\n				params=\"vm.params\"\r\n				instance=\"vm.service\"\r\n				template-url=\"core/category/public/list.tpl.html\">\r\n			</list>\r\n		</div>\r\n	</div>\r\n</div>");
$templateCache.put("core/product/variant/list.tpl.html","<div\r\n    class=\"md-whiteframe-1dp\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref=\"app.product-form.variant-new({_id: vm.service.productId})\" ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/product/variant/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/product/variant/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content hide-loading hide-starting\">\r\n    <md-list-item ng-repeat=\"item in vm.service.entries\" ng-click=\"vm.service.primaryAction ? vm.service.primaryAction(item, $event) : \'\'\" ng-class=\"{\'no-action\': !vm.service.primaryAction}\">\r\n        <md-icon md-font-set=\"material-icons\" ng-if=\"vm.service.primaryIcon\">{{vm.service.primaryIcon}}</md-icon>\r\n        <p>{{ item.name }}</p>\r\n        <md-icon md-font-set=\"material-icons\" ng-click=\"vm.service.secondaryAction($event, item, $index)\" aria-label=\"Open Chat\" class=\"md-secondary md-hue-3\">delete</md-icon>\r\n\r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/product/variant/variantForm.tpl.html","<!-- LOADING -->\r\n<div class=\"loading\" ng-show=\"vm.factory.busy.status(\'getOptions\')\">\r\n	<p layout layout-align=\"center center\">\r\n		<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n	</p>\r\n</div>\r\n\r\n<!-- FORM -->\r\n<form name=\"form\" class=\"form form-variant content\" ng-show=\"!vm.factory.busy.status(\'getOptions\')\">\r\n	<div class=\"form-box variant\">\r\n		<div>\r\n			<p class=\"title\">{{\'VARI_PRODUCT_VARIATION\' | translate}}</p>\r\n		</div>\r\n		<div class=\"box\">\r\n			<div class=\"variant-item\" layout layout-sm=\"column\" layout-align=\"start center\" ng-repeat=\"item in vm.product.variants.attrs\">\r\n				<p class=\"variant-name\">{{item.name | translate}}</p>\r\n				\r\n				<md-input-container md-no-float flex>\r\n					<label>{{\'CHOOSE\' | translate}}..</label>\r\n					<md-select ng-model=\"vm.service.model.attrs[$index].value\" required>\r\n						<md-option ng-repeat=\"option in item.options\" value=\"{{option.value}}\">\r\n							{{option.display | translate}}\r\n						</md-option>\r\n					</md-select>\r\n				</md-input-container>\r\n			</div>\r\n			\r\n			<div layout layout-sm=\"column\" layout-align=\"start center\">\r\n				<p class=\"variant-name\">{{\'QTY\' | translate}}</p>\r\n				<md-input-container md-no-float class=\"qty\">\r\n					<input type=\"number\" ng-model=\"vm.service.model.cnt\" placeholder=\"{{\'QTY\' | translate}}\" required>\r\n				</md-input-container>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'create\')\"></md-progress-circular>\r\n	<md-button ng-disabled=\"vm.service.formStatus(\'create\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary clean-left\" ng-if=\"!vm.service.busy.status(\'create\')\">\r\n		{{\'SAVE\' | translate}}\r\n		\r\n		<md-tooltip>\r\n		{{\'SAVE\' | translate}}\r\n		</md-tooltip>\r\n	</md-button>\r\n</form>\r\n<!-- /FORM -->");
$templateCache.put("core/product/variant/variants.tpl.html","<list\r\n	params=\"vm.params\"\r\n	template-url=\"core/product/variant/list.tpl.html\"\r\n	class=\"list-default variants-list\">\r\n</list>");
$templateCache.put("core/product/variantForm/variantForm.tpl.html","<div class=\"variant-form\" layout-sm=\"row\" layout-gt-sm=\"row\" layout=\"column\" layout-align-sm=\"start center\" layout-align-gt-sm=\"start center\">\r\n	<div class=\"checkbox\">\r\n		<md-checkbox ng-model=\"vm.status\" aria-label=\"activate\">\r\n			{{vm.title | translate}}\r\n		</md-checkbox>\r\n	</div>\r\n	\r\n	<div flex>\r\n		<md-chips\r\n			ng-model=\"vm.selected\"\r\n			md-autocomplete-snap\r\n			md-transform-chip=\"vm.transformChip($chip)\"\r\n			md-require-match=\"true\">\r\n			<md-autocomplete\r\n				md-selected-item=\"vm.selectedItem\"\r\n				md-search-text=\"vm.searchText\"\r\n				md-items=\"item in vm.querySearchVariations(vm.searchText)\"\r\n				md-item-text=\"item.value\"\r\n				md-min-length=\"1\"\r\n				md-delay=\"1000\"\r\n				ng-disabled=\"!vm.status\"\r\n				placeholder=\"{{vm.status ? \'PROD_FORM_VARIATIONS_ADD\' : \'DISABLED\' | translate}}\">\r\n				<span md-highlight-text=\"vn.searchText\">{{item.display | translate}}</span>\r\n			</md-autocomplete>\r\n			<md-chip-template>\r\n				<span>\r\n					<strong>{{$chip.display | translate}}</strong>\r\n				</span>\r\n			</md-chip-template>\r\n		</md-chips>\r\n	</div>\r\n</div>");
$templateCache.put("core/user/login/login.tpl.html","<div class=\"wrapper\" layout=\"column\" layout-align=\"start center\" flex=\"no-shrink\">\n	<div class=\"login login-default md-whiteframe-1dp\">\n		<div class=\"title\">\n			<h2>{{\'LOGIN_TITLE\' | translate}}</h2>\n		</div>\n		\n		<div login-form class=\"login-form md-padding\"></div>\n\n		<div class=\"help\" layout=\"row\">\n			<a flex ui-sref=\"app.recovery\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_FORGOT_LINK\' | translate}}</a>\n			<a flex ui-sref=\"app.register\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_SIGNUP_LINK\' | translate}}</a>\n		</div>\n	</div>\n</div>");
$templateCache.put("core/user/recovery/recovery.tpl.html","<div class=\"wrapper\" layout=\"column\" layout-align=\"start center\" flex=\"no-shrink\" ng-if=\"!vm.hash\">\n	<div class=\"login-logo\">\n		<img src=\"assets/images/logo.png\" alt=\"\">\n	</div>\n	\n	<div class=\"login login-default md-whiteframe-1dp\">\n		<div class=\"title\">\n			<h2>Recuperar senha</h2>\n		</div>\n		\n		<form name=\"form\" class=\"form has-icon md-padding\" novalidate>\n			<div layout layout-xs=\"column\" class=\"email\">\n				<md-input-container flex-sm flex-gt-sm>\n					<label>{{\'EMAIL\' | translate}}</label>\n					<md-icon md-font-set=\"material-icons\">mail_outline</md-icon>\n					<input ng-model=\"vm.email\" type=\"email\" required>\n				</md-input-container>\n			</div>\n			\n			<div layout=\"row\" layout-align=\"center\" class=\"login-action-wrapper\">\n				<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.factory.busy.status(\'recovery\')\"></md-progress-circular>\n				<md-button ng-disabled=\"form.$invalid && !vm.factory.busy.status(\'recovery\')\" ng-click=\"vm.submitRecoveryLink()\" class=\"md-raised md-primary entrar\" ng-if=\"!vm.factory.busy.status(\'recovery\')\">\n					Recuperar\n					\n					<md-tooltip>\n					Recuperar\n					</md-tooltip>\n				</md-button>\n			</div>\n		</form>\n		\n		<div class=\"help\" layout=\"row\">\n			<a flex ui-sref=\"app.login\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_HAS_LINK\' | translate}}</a>\n			<a flex ui-sref=\"app.register\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_SIGNUP_LINK\' | translate}}</a>\n		</div>\n	</div>\n</div>\n\n<div class=\"wrapper\" layout=\"column\" layout-align=\"start center\" flex=\"no-shrink\" ng-if=\"vm.hash\">\n	<div class=\"login-logo\">\n		<img src=\"assets/images/logo.png\" alt=\"\">\n	</div>\n		\n	<div class=\"login login-default md-whiteframe-1dp\" layout layout-align=\"center center\" ng-if=\"!vm.confirmed\">\n		<md-progress-circular md-mode=\"indeterminate\" md-diameter=\"40px\"></md-progress-circular>\n		Validando informações..\n	</div>\n	<div class=\"login login-default md-whiteframe-1dp\" ng-if=\"vm.confirmed\">\n		<div class=\"title\">\n			<h2>Entre com sua nova senha</h2>\n		</div>\n		\n		<form name=\"form\" class=\"form has-icon md-padding\" novalidate>\n			<div layout layout-xs=\"column\" class=\"email\">\n				<md-input-container flex-sm flex-gt-sm>\n					<label>Nova senha</label>\n					<md-icon md-font-set=\"material-icons\">lock_outline</md-icon>\n					<input ng-model=\"vm.password\" type=\"password\" required>\n				</md-input-container>\n			</div>\n			\n			<div layout layout-xs=\"column\" class=\"email\">\n				<md-input-container flex-sm flex-gt-sm>\n					<label>Repetir senha</label>\n					<md-icon md-font-set=\"material-icons\">lock_outline</md-icon>\n					<input ng-model=\"vm.confirmPassword\" type=\"password\" match=\"vm.password\" required>\n				</md-input-container>\n			</div>\n			\n			<div layout=\"row\" layout-align=\"center\" class=\"login-action-wrapper\">\n				<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.factory.busy.status(\'recoveryPassword\')\"></md-progress-circular>\n				<md-button ng-disabled=\"form.$invalid && !vm.factory.busy.status(\'recoveryPassword\')\" ng-click=\"vm.submitRecoveryPassword()\" class=\"md-raised md-primary entrar\" ng-if=\"!vm.factory.busy.status(\'recoveryPassword\')\">\n					Alterar\n					\n					<md-tooltip>\n					Alterar\n					</md-tooltip>\n				</md-button>\n			</div>\n		</form>\n		\n		<div class=\"help\" layout=\"row\">\n			<a flex ui-sref=\"app.login\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_HAS_LINK\' | translate}}</a>\n			<a flex ui-sref=\"app.register\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_SIGNUP_LINK\' | translate}}</a>\n		</div>\n	</div>\n</div>\n<!-- <div class=\"wrapper\">\n							<div class=\"login-default register\">\n														<div class=\"login-icon-wrapper\" layout=\"row\" layout-align=\"center\">\n																					<md-icon md-font-set=\"material-icons\">add_circle</md-icon>\n														</div>\n														<div>\n																					<h2 class=\"text-center md-headline md-primary\">Crie sua conta</h2>\n														</div>\n														<div class=\"md-padding no-padding-right\">\n																					<form name=\"registerForm\" novalidate>\n																												<div layout=\"row\" layout-sm=\"column\" class=\"nome\">\n																																			<i hide-sm class=\"fa fa-smile-o\"></i>\n																																			<md-input-container flex>\n																																										<label>Seu nome</label>\n																																										<input ng-model=\"sign.firstName\" type=\"text\" required>\n																																			</md-input-container>\n																																			<md-input-container flex>\n																																										<label>Sobrenome</label>\n																																										<input ng-model=\"sign.lastName\" type=\"text\" required>\n																																			</md-input-container>\n																												</div>\n																												<div layout=\"row\" class=\"email\">\n																																			<i class=\"fa fa-at\"></i>\n																																			<md-input-container flex>\n																																										<label>Email</label>\n																																										<input ng-model=\"sign.email\" type=\"email\" required>\n																																			</md-input-container>\n																												</div>\n																												<div layout=\"row\" class=\"senha\">\n																																			<i class=\"fa fa-key\"></i>\n																																			<md-input-container flex>\n																																										<label>Senha</label>\n																																										<input ng-model=\"sign.password\" type=\"password\" required>\n																																			</md-input-container>\n																												</div>\n																					</form>\n														</div>\n														<div layout=\"row\" layout-align=\"start\" class=\"login-action-wrapper\">\n																					<md-button class=\"entrar md-raised md-primary\" ng-click=\"vm.login(logon)\" ng-disabled=\"logon.$invalid||app.$page.load.status\">Entrar</md-button>\n																					<facebook-login user=\"user\" template-url=\"core/login/loginFacebook.tpl.html\"></facebook-login>\n														</div>\n														\n														<div class=\"help\" layout=\"row\">\n																					<a flex ui-sref=\"app.recovery\" class=\"lost\"><i class=\"fa fa-support\"></i> Esqueci minha senha</a>\n																					<a flex ui-sref=\"app.login\" class=\"lost\"><i class=\"fa fa-support\"></i> Já tenho cadastro</a>\n														</div>\n							</div>\n</div> -->\n<!-- <md-content class=\"md-padding anim-zoom-in login\" layout=\"row\" layout-sm=\"column\" ng-if=\"!app.isAuthed()\" flex>\n										<div layout=\"column\" class=\"login\" layout-padding flex>\n																				<login-form config=\"vm.config\" user=\"app.user\"></login-form>\n										</div>\n</md-content> -->");
$templateCache.put("core/user/register/register.tpl.html","<div class=\"wrapper\" layout=\"column\" layout-align=\"start center\" flex=\"no-shrink\">\n	<div class=\"login login-default register md-whiteframe-1dp\">\n		<div class=\"title\">\n			<h2>{{\'REGISTER_TITLE\' | translate}}</h2>\n		</div>\n		\n		<div register-form class=\"register-form md-padding\" set-role=\"[\'user\']\"></div>\n		\n		<div class=\"help\" layout=\"row\">\n			<a flex ui-sref=\"app.recovery\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_FORGOT_LINK\' | translate}}</a>\n			<a flex ui-sref=\"app.login\" class=\"lost\"><i class=\"fa fa-support\"></i> {{\'LOGIN_HAS_LINK\' | translate}}</a>\n		</div>\n	</div>\n</div>");
$templateCache.put("core/user/user-list/user-list.tpl.html","<div class=\"card md-whiteframe-1dp user-table\">\r\n	<md-toolbar class=\"md-table-toolbar md-default\" ng-hide=\"list.selected.length || list.filters.show\">\r\n		<div class=\"md-toolbar-tools\">\r\n			<span>Usuários</span>\r\n			<span flex></span>\r\n			<md-button class=\"md-icon-button\" ng-click=\"list.filters.setShow(true)\">\r\n				<md-icon>filter_list</md-icon>\r\n			</md-button>\r\n			<md-button class=\"md-icon-button\" ui-sref=\"app.config.new-user\">\r\n				<md-icon>add</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</md-toolbar>\r\n	\r\n	<md-toolbar class=\"md-table-toolbar md-default\" ng-show=\"list.filters.show && !list.selected.length\">\r\n		<div class=\"md-toolbar-tools\">\r\n			<md-icon>search</md-icon>\r\n			<form flex name=\"list.filters.form\">\r\n				<input class=\"md-table-search-input\" type=\"text\" ng-model=\"list.query.filters\" ng-model-options=\"list.filters.options\" placeholder=\"Pesquisa..\">\r\n			</form>\r\n			<md-button class=\"md-icon-button\" ng-click=\"list.filters.setShow(false)\">\r\n				<md-icon>close</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</md-toolbar>\r\n	\r\n	<md-toolbar class=\"md-table-toolbar alternate\" ng-show=\"list.options.rowSelection && list.selected.length\">\r\n		<div class=\"md-toolbar-tools\">\r\n			<span>{{list.selected.length}} {{list.selected.length > 1 ? \'items selecionados\' : \'item selecionado\'}}</span>\r\n			<span flex></span>\r\n			<md-button class=\"md-icon-button\" ng-click=\"list.remove($event)\">\r\n				<md-icon>delete</md-icon>\r\n			</md-button>\r\n		</div>\r\n	</md-toolbar>\r\n	\r\n	<!-- exact table from live demo -->\r\n	<md-table-container>\r\n		<table md-table md-row-select=\"list.options.rowSelection\" multiple=\"{{list.options.multiSelect}}\" ng-model=\"list.selected\" md-progress=\"list.promise\">\r\n			<thead md-head md-order=\"list.query.order\" md-on-reorder=\"list.getAll\">\r\n				<tr md-row>\r\n					<th md-column class=\"cell-avatar\" md-numeric></th>\r\n					<th md-column md-order-by=\"name\"><span>Nome</span></th>\r\n					<th md-column class=\"cell-created-at\" md-order-by=\"createdAt\"><span>Criação</span></th>\r\n					<th md-column class=\"cell-action\"></th>\r\n				</tr>\r\n			</thead>\r\n			<tbody md-body ng-hide=\"list.data.total === 0\">\r\n				<tr md-row md-select=\"item\" md-select-id=\"name\" md-auto-select=\"false\" ng-repeat=\"item in list.data.entries\" ng-disabled=\"list.isOwner(item.role)\">\r\n					<td md-cell>\r\n						<img alt=\"{{ item.profile.fullName }}\" ng-src=\"{{item.image.thumb ? item.image.thumb : \'assets/images/user-avatar.jpg\'}}\" class=\"md-avatar\" />\r\n					</td>\r\n					<td md-cell>{{item.profile.fullName}}</td>\r\n					<td md-cell>{{item.createdAt | customMoment}}</td>\r\n					<td md-cell class=\"cell-action\">\r\n						<md-button class=\"md-icon-button\" ui-sref=\"app.config.edit-user({_id: item._id})\">\r\n							<md-icon>edit</md-icon>\r\n						</md-button>\r\n					</td>\r\n				</tr>\r\n			</tbody>\r\n			<tbody ng-show=\"list.data.total === 0\">\r\n				<tr>\r\n					<td colspan=\"6\" class=\"md-table-no-results\">\r\n						<md-icon>block</md-icon> Nenhum resultado\r\n					</td>\r\n				</tr>\r\n			</tbody>\r\n		</table>\r\n	</md-table-container>\r\n	\r\n	<md-table-pagination\r\n		md-limit=\"list.query.limit\"\r\n		md-limit-options=\"[5, 10, 15]\"\r\n		md-page=\"list.query.page\"\r\n		md-total=\"{{list.data.total}}\"\r\n		md-on-paginate=\"list.getAll\"\r\n		md-label=\"{page: \'Página:\', rowsPerPage: \'Linhas por página:\', of: \'de\'}\"\r\n		md-page-select>\r\n	</md-table-pagination>\r\n</div>");
$templateCache.put("core/user/users/dialogUserNew.tpl.html","<md-dialog aria-label=\"Novo usuário\" ng-cloak>\r\n	<form name=\"vm.form\" class=\"project-user-form-dialog\">\r\n		<md-toolbar>\r\n			<div class=\"md-toolbar-tools\">\r\n				<h2>Novo usuário</h2>\r\n				<span flex></span>\r\n				<md-button class=\"md-icon-button\" ng-click=\"vm.cancel()\">\r\n					<md-icon md-font-set=\"material-icons\" md-font-content=\"close\" aria-label=\"Fechar janela\"></md-icon>\r\n				</md-button>\r\n			</div>\r\n		</md-toolbar>\r\n		<md-dialog-content>\r\n			<div class=\"md-dialog-content\">\r\n				<!-- LOADING -->\r\n				<div class=\"loading\" ng-show=\"vm.service.busy.status(\'save\')\">\r\n					<p layout layout-align=\"center center\">\r\n						<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n					</p>\r\n				</div>\r\n				\r\n				<div ng-if=\"(!vm.edit || vm.edit && vm.service.model) && !vm.service.busy.status(\'save\')\">\r\n					<div layout layout-xs=\"column\" class=\"nome\">\r\n						<md-input-container flex-sm flex-gt-sm>\r\n							<label>{{\'NAME\' | translate}}</label>\r\n							<md-icon md-font-set=\"material-icons\">face</md-icon>\r\n							<input ng-model=\"vm.service.model.profile.firstName\" type=\"text\" required>\r\n						</md-input-container>\r\n						<md-input-container flex-sm flex-gt-sm>\r\n							<label>{{\'LASTNAME\' | translate}}</label>\r\n							<input ng-model=\"vm.service.model.profile.lastName\" type=\"text\" required>\r\n						</md-input-container>\r\n					</div>\r\n					<div layout layout-xs=\"column\" class=\"email\">\r\n						<md-input-container flex-sm flex-gt-sm>\r\n							<label>{{\'EMAIL\' | translate}}</label>\r\n							<md-icon md-font-set=\"material-icons\">mail_outline</md-icon>\r\n							<input ng-model=\"vm.service.model.email\" type=\"email\" required>\r\n						</md-input-container>\r\n					</div>\r\n					<div layout layout-xs=\"column\" class=\"senha\" ng-if=\"!vm.edit\">\r\n						<md-input-container flex-sm flex-gt-sm>\r\n							<label>{{\'PASSWORD\' | translate}}</label>\r\n							<md-icon md-font-set=\"material-icons\">lock_outline</md-icon>\r\n							<input ng-model=\"vm.service.model.password\" type=\"password\" required>\r\n						</md-input-container>\r\n					</div>\r\n					\r\n					<!-- PHONE DIRECTIVE -->\r\n					<div phone-form-content model=\"vm.service.model.profile.contact\" set-flex=\"50\"></div>\r\n					\r\n					<input type=\"hidden\" ng-model=\"vm.service.model.provider\">\r\n				</div>\r\n			</div>\r\n		</md-dialog-content>\r\n		<md-dialog-actions layout=\"row\">\r\n			<span flex></span>\r\n			<md-button ng-click=\"vm.cancel()\">\r\n				{{\'CANCEL\' | translate}}\r\n			</md-button>\r\n			<md-button ng-disabled=\"vm.service.formStatus(\'save\', vm.form.$invalid)\" ng-click=\"vm.save($event)\" class=\"clean-left md-primary\" ng-if=\"!vm.service.busy.status(\'save\')\" style=\"margin-right:20px;\">\r\n				{{\'SAVE\' | translate}}\r\n			</md-button>\r\n		</md-dialog-actions>\r\n	</form>\r\n</md-dialog>");
$templateCache.put("core/user/users/list.tpl.html","<div class=\"md-whiteframe-1dp\"\r\n    is-starting=\"{{vm.service.busy.status(\'starting\')}}\"\r\n    is-loading=\"{{vm.service.busy.status(\'loading\')}}\"\r\n    has-entries=\"{{!!vm.service.entries.length}}\">\r\n    <div layout>\r\n        <!-- Main search box -->\r\n        <div\r\n            list-search-box\r\n            service=\"vm.service\"\r\n            class=\"list-search-box\"\r\n            flex>\r\n        </div>\r\n        \r\n        <!-- Main search box -->\r\n        <div class=\"list-action-box\" layout layout-align=\"center center\">\r\n            <md-button class=\"md-icon-button md-primary\" aria-label=\"Novo(a)\" ui-sref={{vm.service.addAction}} ui-sref-opts=\"{inherit: false}\" ng-if=\"vm.service.addAction\">\r\n                <md-icon md-font-set=\"material-icons\">add_circle</md-icon>\r\n            </md-button>\r\n        </div>\r\n    </div>\r\n    \r\n    <md-divider></md-divider>\r\n    \r\n    <!-- Listing -->\r\n    <div class=\"listing\" layout=\"column\" layout-gt-md=\"row\">\r\n        \r\n        <!-- Filters -->\r\n        <!-- <div\r\n            list-filter-box\r\n            service=\"vm.list\"\r\n            class=\"list-filter-box\">\r\n        </div> -->\r\n        \r\n        <!-- Entries -->\r\n        <div\r\n            list-content\r\n            service=\"vm.service\"\r\n            template-url=\"core/user/users/listContent.tpl.html\"\r\n            class=\"list-content\"\r\n            flex-gt-md>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("core/user/users/listContent.tpl.html","<div class=\"no-results hide-entries hide-loading hide-starting\">\r\n    <p>\r\n        <md-icon md-font-set=\"material-icons\">error_outline</md-icon> Nenhum resultado\r\n    </p>\r\n</div>\r\n\r\n<div class=\"loading show-loading\">\r\n    <md-progress-linear md-mode=\"indeterminate\" class=\"md-accent\"></md-progress-linear>\r\n    <p layout layout-align=\"start center\">\r\n        <md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\" md-diameter=\"40\"></md-progress-circular> Carregando resultados..\r\n    </p>\r\n</div>\r\n\r\n<md-list class=\"list-content hide-loading hide-starting\">\r\n    <md-list-item\r\n        ng-repeat=\"item in vm.service.entries\"\r\n        ng-click=\"vm.service.primaryAction ? vm.service.primaryAction(item, $event) : \'\'\">\r\n        <div layout layout-xs=\"column\"\r\n            layout-align-sm=\"start center\" layout-align-gt-sm=\"start center\"\r\n            class=\"item\" flex>\r\n            <img alt=\"{{ item.user.fullName }}\" ng-src=\"{{item.image.thumb ? item.image.thumb : \'assets/images/user-avatar.jpg\'}}\" class=\"md-avatar\" />\r\n            <div flex-sm=\"50\" flex-gt-sm=\"50\">\r\n                <p>{{item.profile.fullName}}</p>\r\n                <p class=\"md-caption\">criado em {{item.createdAt | customMoment}}</p>\r\n            </div>\r\n            <div flex-sm flex-gt-sm layout=\"column\" layout-align-sm=\"center end\" layout-align-gt-sm=\"center end\">\r\n                <p>{{vm.service.extra.getTypeStatus(item.situation)}}</p>\r\n            </div>\r\n        </div>\r\n        <md-divider></md-divider>\r\n    </md-list-item>\r\n    <md-list-item\r\n        list-load-more\r\n        service=\"vm.service\"\r\n        ng-if=\"vm.service.loadMoreBtn\"\r\n        class=\"list-load-more\">\r\n    </md-list-item>\r\n</md-list>");
$templateCache.put("core/user/users/userForm.tpl.html","<div class=\"wrapper panel\">\r\n	<div class=\"panel-heading\">\r\n		<div class=\"panel-title\">\r\n			<md-icon md-font-set=\"material-icons\" md-font-content=\"work\"></md-icon> {{vm.edit ? \'Editar\' : \'Novo\'}} usuário\r\n		</div>\r\n	</div>\r\n	\r\n	<!-- LOADING -->\r\n	<div class=\"loading\" ng-show=\"vm.factory.busy.status(\'getType\')\">\r\n		<p layout layout-align=\"center center\">\r\n			<md-progress-circular class=\"md-accent\" md-mode=\"indeterminate\"></md-progress-circular>\r\n		</p>\r\n	</div>\r\n	\r\n	<form class=\"project-type-form has-icon\" name=\"form\" ng-if=\"!vm.edit || vm.edit && vm.service.model\">\r\n		<div class=\"panel-body\">\r\n			<div layout layout-xs=\"column\" class=\"nome\">\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>{{\'NAME\' | translate}}</label>\r\n					<md-icon md-font-set=\"material-icons\">face</md-icon>\r\n					<input ng-model=\"vm.service.model.profile.firstName\" type=\"text\" required>\r\n				</md-input-container>\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>{{\'LASTNAME\' | translate}}</label>\r\n					<input ng-model=\"vm.service.model.profile.lastName\" type=\"text\" required>\r\n				</md-input-container>\r\n			</div>\r\n			<div layout layout-xs=\"column\" class=\"email\">\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>{{\'EMAIL\' | translate}}</label>\r\n					<md-icon md-font-set=\"material-icons\">mail_outline</md-icon>\r\n					<input ng-model=\"vm.service.model.email\" type=\"email\" required>\r\n				</md-input-container>\r\n			</div>\r\n			<div layout layout-xs=\"column\" class=\"senha\" ng-if=\"!vm.edit\">\r\n				<md-input-container flex-sm flex-gt-sm>\r\n					<label>{{\'PASSWORD\' | translate}}</label>\r\n					<md-icon md-font-set=\"material-icons\">lock_outline</md-icon>\r\n					<input ng-model=\"vm.service.model.password\" type=\"password\" required>\r\n				</md-input-container>\r\n			</div>\r\n			\r\n			<!-- PHONE DIRECTIVE -->\r\n			<div phone-form-content model=\"vm.service.model.profile.contact\" set-flex=\"50\"></div>\r\n			\r\n			<div layout layout-xs=\"column\" class=\"senha\" ng-if=\"vm.edit && vm.isEditorOrUser(vm.service.model)\">\r\n				<md-input-container>\r\n					<label>Grupos</label>\r\n					<md-icon md-font-set=\"material-icons\">group</md-icon>\r\n					<md-select ng-model=\"vm.service.model.role\"\r\n						multiple>\r\n						<md-optgroup label=\"Grupos\">\r\n						<md-option ng-value=\"group.value\" ng-repeat=\"group in vm.groups |\r\n						filter:vm.searchTerm\">{{group.label}}</md-option>\r\n						</md-optgroup>\r\n					</md-select>\r\n				</md-input-container>\r\n			</div>\r\n\r\n			<input type=\"hidden\" ng-model=\"vm.service.model.provider\">\r\n			\r\n			<br>\r\n			\r\n			<div layout layout-align=\"start center\">\r\n				<md-progress-circular class=\"\" md-mode=\"indeterminate\" md-diameter=\"20px\" ng-if=\"vm.service.busy.status(\'save\')\"></md-progress-circular>\r\n				<md-button ng-disabled=\"vm.service.formStatus(\'save\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"clean-left md-primary\" ng-if=\"!vm.service.busy.status(\'save\')\">\r\n					{{\'SAVE\' | translate}}\r\n					\r\n					<md-tooltip>\r\n					{{\'SAVE\' | translate}}\r\n					</md-tooltip>\r\n				</md-button>\r\n				<md-button ng-disabled=\"vm.service.busy.status(\'save\')\" ui-sref=\"{{vm.redirectUrl}}\">\r\n					{{\'CANCEL\' | translate}}\r\n					\r\n					<md-tooltip>\r\n					{{\'CANCEL\' | translate}}\r\n					</md-tooltip>\r\n				</md-button>\r\n			</div>\r\n			\r\n		</div>\r\n	</form>\r\n</div>");
$templateCache.put("core/user/users/users.tpl.html","<div user-list></div>");
$templateCache.put("core/user/login/form/loginForm.tpl.html","<div ng-show=\"vm.user.busy.status(\'login\')\" layout layout-align=\"center center\">\r\n	<md-progress-circular md-mode=\"indeterminate\"></md-progress-circular>\r\n	Validando informações\r\n</div>\r\n<form name=\"form\" class=\"form has-icon\" ng-hide=\"vm.user.busy.status(\'login\')\" novalidate>\r\n	<div layout layout-xs=\"column\" class=\"email\">\r\n		<md-input-container flex-sm flex-gt-sm>\r\n			<label>{{\'EMAIL\' | translate}}</label>\r\n			<md-icon md-font-set=\"material-icons\">mail_outline</md-icon>\r\n			<input ng-model=\"vm.user.model.email\" type=\"email\" required>\r\n		</md-input-container>\r\n	</div>\r\n	<div layout layout-xs=\"column\" class=\"senha\">\r\n		<md-input-container flex-sm flex-gt-sm>\r\n			<label>{{\'PASSWORD\' | translate}}</label>\r\n			<md-icon md-font-set=\"material-icons\">lock_outline</md-icon>\r\n			<input ng-model=\"vm.user.model.password\" type=\"password\" required>\r\n		</md-input-container>\r\n	</div>\r\n	\r\n	<div layout=\"row\" layout-align=\"start\" class=\"login-action-wrapper\">\r\n		<md-button type=\"submit\" ng-disabled=\"vm.user.formStatus(\'login\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary entrar\" ng-if=\"!vm.user.busy.status(\'login\')\">{{\'LOGIN_BTN\' | translate}}</md-button>\r\n		<!-- <facebook-login user=\"user\" template-url=\"core/login/loginFacebook.tpl.html\"></facebook-login> -->\r\n	</div>\r\n</form>");
$templateCache.put("core/user/register/form/registerForm.tpl.html","<form name=\"form\" class=\"form has-icon\" novalidate>\r\n	\r\n	<div layout layout-xs=\"column\" class=\"nome\">\r\n		<md-input-container flex-sm flex-gt-sm>\r\n			<label>{{\'NAME\' | translate}}</label>\r\n			<md-icon md-font-set=\"material-icons\">face</md-icon>\r\n			<input ng-model=\"vm.user.model.profile.firstName\" type=\"text\" required>\r\n		</md-input-container>\r\n		<md-input-container flex-sm flex-gt-sm>\r\n			<label>{{\'LASTNAME\' | translate}}</label>\r\n			<input ng-model=\"vm.user.model.profile.lastName\" type=\"text\" required>\r\n		</md-input-container>\r\n	</div>\r\n	<div layout layout-xs=\"column\" class=\"email\">\r\n		<md-input-container flex-sm flex-gt-sm>\r\n			<label>{{\'EMAIL\' | translate}}</label>\r\n			<md-icon md-font-set=\"material-icons\">mail_outline</md-icon>\r\n			<input ng-model=\"vm.user.model.email\" type=\"email\" required>\r\n		</md-input-container>\r\n	</div>\r\n	<div layout layout-xs=\"column\" class=\"senha\">\r\n		<md-input-container flex-sm flex-gt-sm>\r\n			<label>{{\'PASSWORD\' | translate}}</label>\r\n			<md-icon md-font-set=\"material-icons\">lock_outline</md-icon>\r\n			<input ng-model=\"vm.user.model.password\" type=\"password\" required>\r\n		</md-input-container>\r\n	</div>\r\n	\r\n	<!-- PHONE DIRECTIVE -->\r\n	<div phone-form-content model=\"vm.user.model.profile.contact\" set-flex=\"50\"></div>\r\n	\r\n	<input type=\"hidden\" ng-model=\"vm.user.model.provider\">\r\n	\r\n	<div layout=\"row\" layout-align=\"start\" class=\"login-action-wrapper\">\r\n		<md-button ng-disabled=\"vm.user.formStatus(\'create\', form.$invalid)\" ng-click=\"vm.submit($event)\" class=\"md-raised md-primary entrar\" ng-if=\"!vm.user.busy.status(\'create\')\">{{\'REGISTER_BTN\' | translate}}</md-button>\r\n	</div>\r\n</form>");}]);