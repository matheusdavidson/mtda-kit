(function() {
    'use strict';
    angular.module('mtda.seed').config( /*@ngInject*/ function($stateProvider, $userProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.login-home', {
            url: '/',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.loginTemplateUrl()
                    },
                    controller: '$LoginCtrl as vm'
                }
            },
            resolve: {
                skipIfLoggedIn: $userProvider.skipIfLoggedIn
            }
        });
    })
})();