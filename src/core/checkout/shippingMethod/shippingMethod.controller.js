(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutShippingMethodCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        vm.user = $user.getCurrent();
    });
})();