(function() {
    'use strict';

    angular.module('mtda.checkout').controller('$CheckoutCtrl', /*@ngInject*/ function($log, $scope, $user, $Checkout, $checkout, lodash, $timeout, $localStorage, $cart, $state, $page, $window) {
        var vm = this;

        // 
        // Timeout vars
        var timeoutFocus = false;

        // 
        // Set factory to get cart entries
        vm.factory = $checkout;
        vm.entries = vm.factory.entries;

        // 
        // Set user
        vm.user = $user.getCurrent();

        // 
        // Side menu with steps
        vm.menu = vm.factory.steps;

        // 
        // setCurrentStep
        setCurrentStep();

        // 
        // Restore or create model from local storage
        bootstrap();

        // 
        // Actions on state change
        $scope.$on('$stateChangeStart', stateChangeStart);
        $scope.$on('$stateChangeSuccess', stateChangeSuccess);

        // 
        // Clear checkout
        vm.clear = clear;

        //
        // Indicate that checkout is done!
        vm.completed = false;
        vm.isCompleted = isCompleted;

        // 
        // Indicate that user will be redirected to paypal
        vm.paypalRedirect = false;

        // 
        // Set form
        vm.form = {
            paymentDetails: {},
            shippingDetails: {},
            shippingMethod: {},
            payment: {}
        };

        // 
        // Check to see if form is available
        vm.hasForm = hasForm;

        // 
        // Callback for login success
        vm.cbLogin = cbLogin;

        // 
        // Callback for registration success
        vm.cbRegister = cbRegister;

        // 
        // Fix for the form validation
        // Sometimes when transitioning quickly trought routes, form becomes empty
        $scope.$watch('vm.form.paymentDetails', function(nv, ov) {
            if ($state.current.innerTabGroup === 'payment-details' && !nv && ov)
                vm.form.paymentDetails = ov;
        }, true);

        $scope.$watch('vm.form.shippingDetails', function(nv, ov) {
            if ($state.current.innerTabGroup === 'shipping-details' && !nv && ov)
                vm.form.shippingDetails = ov;
        }, true);

        $scope.$watch('vm.form.shippingMethod', function(nv, ov) {
            if ($state.current.innerTabGroup === 'shipping-method' && !nv && ov)
                vm.form.shippingMethod = ov;
        }, true);

        $scope.$watch('vm.form.payment', function(nv, ov) {
            if ($state.current.innerTabGroup === 'payment' && !nv && ov)
                vm.form.payment = ov;
        }, true);

        // 
        // Option to use payment details for shipping
        $scope.$watch('vm.service.model.shipping.method', function(nv, ov) {
            if (nv && nv !== ov) {
                // 
                // Get price for this 
                var price = $checkout.shippingMethods[nv].price;

                // 
                // Set shipping price
                vm.service.model.shipping.price = price;

                // *shipping price + total must be calculated in the server
            }
        }, true);

        // 
        // Update gateway according to payment.mode
        $scope.$watch('vm.service.model.payment.mode', function(nv, ov) {
            if (nv && nv !== ov) {
                // 
                // Set gateway
                var gateway = vm.service.model.payment.mode === 'paypal_balance' || vm.service.model.payment.mode === 'paypal_cc' ? 'paypal' : 'moip';

                // 
                // Set gateway
                vm.service.model.payment.gateway = gateway;

                // 
                // Clear old value if necessary
                if (ov === 'cc' || ov === 'debito-bancario' || ov === 'boleto') vm.service.model.moip = {};
                else vm.service.model.paypal = {};

                // 
                // Set some user details we already have on mode 'cc'
                if (nv === 'cc') {
                    // 
                    // Set card
                    if (!lodash.hasIn(vm.service.model, 'moip.card')) vm.service.model.moip.card = {};

                    // 
                    // Set name
                    vm.service.model.moip.card.name = vm.user.model.profile.firstName + ' ' + vm.user.model.profile.lastName;

                    // 
                    // Set contact phone
                    if (lodash.hasIn(vm.user.model, 'profile.contact.mobile') || lodash.hasIn(vm.user.model, 'profile.contact.phone'))
                        vm.service.model.moip.card.phone = vm.user.model.profile.contact.mobile || vm.user.model.profile.contact.phone;
                }

                // 
                // Try to focus first field, when we have additional form
                if (nv === 'cc' || nv === 'paypal_cc') {
                    focusFirstInput(vm.step.innerTabGroup, '.' + nv);
                }
            }
        }, true);

        // 
        // Toggle shipping address when check/uncheck vm.service.model.payment.same_for_shipping field
        $scope.$watch('vm.service.model.payment.same_for_shipping', function(nv, ov) {
            if (nv !== ov) {
                if (nv) {
                    // 
                    // Checked
                    // Copy payment.payer to shipping.to
                    angular.extend(vm.service.model.shipping.to, vm.service.model.payment.payer);

                    // 
                    // Copy payment.address to shipping.address
                    angular.extend(vm.service.model.shipping.address, vm.service.model.payment.address);
                } else {
                    // 
                    // Unchecked
                    // clear data o shipping.to
                    vm.service.model.shipping.to = {
                        firstName: '',
                        lastName: ''
                    };

                    // 
                    // clear data o shipping.address
                    vm.service.model.shipping.address = {
                        cep: '',
                        street: '',
                        num: '',
                        bairro: '',
                        city: '',
                        state: ''
                    };
                }
            }
        }, true);

        // 
        // Auto-fill card type
        $scope.$watch('vm.service.model.moip.card.number', function(nv, ov) {
            if (nv && nv !== ov) {

                // 
                // Get card
                var card = moip.creditCard.cardType(vm.service.model.moip.card.number);

                // 
                // Validate card
                if (!card || !card.brand) return;

                var cardCheck = {
                    'MASTERCARD': 'Mastercard',
                    'VISA': 'Visa',
                    'AMEX': 'AmericanExpress',
                    'DINERS': 'Diners',
                    'HIPERCARD': 'Hipercard',
                    'ELO': 'Elo',
                    'HIPER': 'Hiper'
                };

                // 
                // Set card type
                var type = cardCheck[card.brand] ? cardCheck[card.brand] : false;

                // 
                // Set form
                if (type) vm.service.model.moip.card.type = type;


            }
        }, true);

        // 
        // Auto-fill card type
        $scope.$watch('vm.service.model.paypal.card.number', function(nv, ov) {
            if (nv && nv !== ov) {

                // 
                // Get card
                var card = moip.creditCard.cardType(vm.service.model.paypal.card.number);

                // 
                // Validate card
                if (!card || !card.brand) return;

                var cardCheck = {
                    'MASTERCARD': 'Mastercard',
                    'VISA': 'Visa',
                    'AMEX': 'Amex'
                };

                // 
                // Set card type
                var type = cardCheck[card.brand] ? cardCheck[card.brand] : false;

                // 
                // Set form
                if (type) vm.service.model.paypal.card.type = type;


            }
        }, true);

        //
        // Navigation btn
        vm.prev = prev;
        vm.next = next;

        // 
        // Bootstrap controller
        // it will set a clear service or restore a model from localstorage
        // it will check completed steps in menu
        function bootstrap(clear) {
            // 
            // Set service
            setService(clear);

            // 
            // Execute payment in case of paypal confirmation
            if (vm.step.innerTabGroup === 'confirmation') {
                // 
                // We must have a payment
                if (!vm.service.model.payment || !vm.service.model.payment.mode) {
                    // 
                    // We are in confirmation but we don´t gave a payment
                    // Usually this is because the page was reloaded in confirmation page, after a payment was already done
                    // Take user to the beginning
                    $state.go('app.checkout.identification', {}, {
                        inherit: false
                    });
                } else {
                    // 
                    // In paypal balance, we need to execute the payment to actually pay
                    if (vm.service.model.payment.mode === 'paypal_balance') {
                        // 
                        // Add payment information from url to the model
                        vm.service.model.paypal.token = $state.params.token;
                        vm.service.model.paypal.payer = $state.params.PayerID;

                        // 
                        // Execute payment
                        vm.service.execute().then(executeSuccess.bind(this), executeFail.bind(this));
                    }
                }
            }

            // 
            // Send user to the right step
            validateSteps();
        }

        // 
        // Set a new service
        function setService(clear) {

            // 
            // Checkout entry sample 
            var entry = {
                model: {
                    user: vm.user ? vm.user.model : false,
                    payment: {
                        payer: {},
                        address: {}
                    },
                    shipping: {
                        to: {},
                        address: {}
                    }
                },
                stepStatus: {
                    'identification': false,
                    'payment-details': false,
                    'shipping-details': false,
                    'shipping-method': false,
                    'payment': false,
                    'confirmation': false
                }
            };
            console.log('entry', entry);
            // 
            // Extend user data
            extendModel(entry);

            // 
            // Localstorage data
            vm.checkout = $localStorage.checkout = clear ? entry : ((lodash.hasIn($localStorage, 'checkout')) ? $localStorage.checkout : entry);

            // 
            // Set cart in model
            vm.checkout.model.total = $cart.subtotal;
            vm.checkout.model.cart = $cart.entries;

            // 
            // Restore or create model from local storage
            var model = $localStorage.checkout.model = vm.checkout.model;

            // 
            // Instantiate a new checkout
            vm.service = new $Checkout.service({
                steps: vm.menu,
                model: model
            });
        }

        // 
        // Clear form
        // It will trigger bootstrap to clear the current service
        function clear() {
            // 
            // Set a new and clear service
            bootstrap(true);

            // 
            // Validate steps
            validateSteps();

            // 
            // Send user back to the first step
            // $state.go('app.checkout.identification');
        }

        // 
        // Return the current step 
        // find entries on vm.menu with the same innerTabGroup as the route
        function currentStep() {
            // 
            // Get current state
            var obj = lodash.find(vm.menu, ['innerTabGroup', $state.current.innerTabGroup]);

            if (!obj) return false;

            //
            // Set key
            obj.index = lodash.findIndex(vm.menu, ['innerTabGroup', $state.current.innerTabGroup]);

            return obj;
        }

        // 
        // Set vm.step with currentStep()
        function setCurrentStep() {
            // setCurrentStep
            vm.step = currentStep();

            // set prev and next btn
            vm.hasPrevBtn = (vm.step.index > 1 && vm.step.index <= 5);
            vm.hasNextBtn = (vm.step.index > 0 && vm.step.index < 5);
        }

        function stateChangeStart() {
            // 
            // Mark identification as completed after user has logged or registered
            if (vm.step.innerTabGroup === 'identification') {
                var user = $user.getCurrent();

                if ($user) {
                    // 
                    // Complete identification step
                    vm.step.checked = true;

                    // 
                    // Set user
                    vm.user = user.model;
                }
            }
        }
        // 
        // Actions on state change
        function stateChangeSuccess() {
            // 
            // Change current step
            setCurrentStep();
        }

        // 
        // Go To completedStep
        function validateSteps() {

            // 
            // last completed
            var lastIsCompleted = false,
                firstIncomplete = false,
                rightStep = false,
                rightStepSlug = false;

            // 
            // Loop trought all steps to check completed and redirect user to the right place
            angular.forEach(vm.service.steps, function(menu, key) {

                // 
                // current route
                var isCurrent = ($state.current.innerTabGroup === menu.innerTabGroup);

                // 
                // Identification validation
                if (menu.innerTabGroup === 'identification') {
                    // Check if is logged in
                    menu.checked = ($user.getCurrent());

                    // This is always enabled
                    menu.disabled = false;

                    // Update local storage step validation
                    vm.checkout.stepStatus[menu.innerTabGroup] = menu.checked;

                    // 
                    // Set first incompleted
                    firstIncomplete = menu.checked ? false : true;
                } else {
                    // 
                    // Validation for all other steps
                    var completed = (vm.checkout.stepStatus[menu.innerTabGroup] === true);

                    // toogle
                    menu.checked = (completed);
                    menu.disabled = isCurrent ? false : (!completed);

                    //
                    // If we have no firstIncomplete 
                    // and this is not completed 
                    // and its not the current route
                    if (!firstIncomplete && !completed && menu.innerTabGroup !== 'confirmation') {
                        if (!isCurrent) {
                            rightStep = 'app.checkout.' + menu.innerTabGroup;
                            rightStepSlug = menu.innerTabGroup;
                        }
                        firstIncomplete = true;
                    }
                }

                // 
                // Redirect user if we have lastCompletedStep and its not the current one
                if (rightStep && vm.step.innerTabGroup !== 'confirmation' && !vm.completed) {
                    // enable state menu
                    menu.disabled = false;

                    // Go to state
                    $state.go(rightStep, {}, {
                        inherit: false
                    });

                    // 
                    // Try to focus first input
                    focusFirstInput(rightStepSlug);
                } else {
                    // 
                    // Try to focus first input
                    focusFirstInput(menu.innerTabGroup);
                }

            }.bind(this));
        }

        function next(ev) {
            var redirect = true;

            // 
            // Payment details validation
            if (vm.step.innerTabGroup === 'payment-details') {
                // validate form
                if (vm.form.paymentDetails.$invalid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);

                    // 
                    // Trigger validation
                    vm.form.paymentDetails.$setSubmitted();

                    return false;
                }
            }

            // 
            // Shipping details validation
            if (vm.step.innerTabGroup === 'shipping-details') {
                // validate form
                if (vm.form.shippingDetails.$invalid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);

                    // 
                    // Trigger validation
                    vm.form.shippingDetails.$setSubmitted();

                    return false;
                }
            }

            // 
            // Shipping method validation
            if (vm.step.innerTabGroup === 'shipping-method') {
                // validate form
                if (vm.form.shippingMethod.$invalid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);
                    return false;
                }
            }

            // 
            // Shipping method validation
            if (vm.step.innerTabGroup === 'payment') {
                var valid = validateBeforeOrderSave();

                //
                // Validate all forms
                if (vm.form.payment.$invalid || !valid) {
                    $page.showAlert(ev, 'Opss..', $page.msgFillAllFields);

                    // 
                    // Run step validation if other forms are not valid
                    if (!valid) {
                        validateSteps();
                    }

                    return false;
                } else {
                    // 
                    // Save order
                    vm.service.create().then(orderCreateSuccess.bind(this), orderCreateFail.bind(this));
                    redirect = false;
                }
            }

            // 
            // Mark current step as completed
            stepCompleted();

            // 
            // Get next step
            var nextStep = vm.menu[vm.step.index + 1],
                nextRoute = nextStep ? 'app.checkout.' + nextStep.innerTabGroup : false;

            // 
            // Redirect and enable next menu
            if (nextRoute && redirect) {
                //
                // enable next menu
                nextStep.disabled = false;

                //
                // go to next
                $state.go(nextRoute, {}, {
                    inherit: false
                });

                // 
                // Focus the first field inside checkout form
                // Use timeout to do this in the next digest
                focusFirstInput(nextStep.innerTabGroup);
            }
        }

        function prev() {
            // 
            // Get prev step
            var prevStep = vm.menu[vm.step.index - 1],
                prevRoute = prevStep ? 'app.checkout.' + prevStep.innerTabGroup : false;

            if (prevRoute) {
                //
                // go to prev
                $state.go(prevRoute, {}, {
                    inherit: false
                });

                // 
                // Focus the first field inside checkout form
                // Use timeout to do this in the next digest
                focusFirstInput(prevStep.innerTabGroup);
            }
        }

        function focusFirstInput(innerTabGroup, additionalClass) {

            if (timeoutFocus) $timeout.cancel(timeoutFocus);

            timeoutFocus = $timeout(function() {
                // 
                // Get current step form elem
                var elem = angular.element('.' + innerTabGroup);

                // 
                // Find additionalClass if provided
                if (additionalClass) elem = elem.find(additionalClass);

                // 
                // Find first input
                elem = elem.find(':input:first');

                // 
                // Focus elem
                if (elem) elem.focus();
            }, 100);
        }

        function stepCompleted() {
            // Change in menu
            vm.step.checked = true;

            // Change in localstorage
            vm.checkout.stepStatus[vm.step.innerTabGroup] = true;
        }

        function hasForm(form) {
            return (vm.form[form]);
        }

        function orderCreateSuccess(response) {
            // 
            // Update checkout with order object
            angular.extend(vm.service.model, response.data.order);

            // 
            // Already had an id, redirect to confirmation
            if (response.data.err) {
                if (vm.service.model.payment.mode === 'paypal_balance') {
                    $state.go('app.checkout.confirmation', {
                        paymentId: vm.service.model.paypal.payment,
                        token: vm.service.model.paypal.token,
                        PayerID: vm.service.model.paypal.payer
                    }, {
                        inherit: false
                    });

                    // 
                    // Execute payment
                    vm.service.execute().then(executeSuccess.bind(this), executeFail.bind(this));
                } else {
                    console.log('response.data.err', response.data.err);
                }
            } else {
                if (vm.service.model.payment.mode === 'paypal_balance') {
                    //
                    // Mark that we are reloading
                    vm.paypalRedirect = true;

                    // 
                    // Redirect user to paypal
                    $window.location = response.data.payment.redirectUrl;
                } else {
                    // 
                    // Complete checkout
                    completeCheckout(response.data.order);

                    // 
                    // Send user to confirmation
                    $state.go('app.checkout.confirmation');
                }
            }
        }

        function orderCreateFail() {}

        function executeSuccess(response) {
            // 
            // Update checkout with order object
            angular.extend(vm.service.model, response.data.order);

            completeCheckout(response.data.order);
        }

        function executeFail() {
            // 
            // Complete transaction with error
            vm.completed = {
                error: true
            };

            // 
            // Clear checkout
            finishCheckout();
        }

        function completeCheckout(order) {
            // 
            // Set as completed
            vm.completed = {
                error: false,
                order: order
            };

            // 
            // Clear checkout
            finishCheckout();
        }

        // 
        // Validate all fields before saving the order in payment route
        function validateBeforeOrderSave() {
            var valid = true;

            if (!vm.service.model.payment.payer.firstName || !vm.service.model.payment.payer.lastName || !vm.service.model.payment.address) {
                valid = false;

                // 
                // Remove completition
                vm.checkout.stepStatus['payment-details'] = false;
            }

            if (!vm.service.model.shipping.to.firstName || !vm.service.model.shipping.to.lastName || !vm.service.model.shipping.address) {
                valid = false;

                // 
                // Remove completition
                vm.checkout.stepStatus['shipping-details'] = false;
            }

            if (!vm.service.model.shipping.method || (!vm.service.model.shipping.price && vm.service.model.shipping.price !== 0)) {
                valid = false;

                // 
                // Remove completition
                vm.checkout.stepStatus['shipping-method'] = false;
            }

            if (vm.service.model.payment.mode === 'debito_bancario' && (!vm.service.model.moip || !vm.service.model.moip.bank)) {
                valid = false;
            }

            return valid;
        }

        function isCompleted() {
            return (vm.completed !== false && !vm.service.busy.status('execute'));
        }

        function finishCheckout() {
            $timeout(function() {
                // 
                // Clear checkout
                vm.checkout = false;
                delete $localStorage.checkout;

                // 
                // Set a new service
                setService(true);

                // 
                // Make all menu disable
                vm.service.steps[0].disabled = true;
                vm.service.steps[1].disabled = true;
                vm.service.steps[2].disabled = true;
                vm.service.steps[3].disabled = true;
                vm.service.steps[4].disabled = true;
                vm.service.steps[5].disabled = true;

                // 
                // Mark confirmation as completed
                vm.service.steps[5].checked = true;

                // 
                // Empty cart
                $cart.clear();
            });
        }

        // 
        // callback for login success
        // Try to add information from profile to checkout
        function cbLogin(user) {
            // 
            // Set user again
            vm.user = $user.getCurrent();

            //
            // Update service model
            vm.service.model.user = vm.user.model;

            // 
            // Extend user data
            extendUser(user);
        }

        // 
        // callback for registration success
        // Try to add information from profile to checkout
        function cbRegister(user) {
            // 
            // Set user again
            vm.user = $user.getCurrent();

            //
            // Update service model
            vm.service.model.user = vm.user.model;

            // 
            // Extend user data
            extendUser(user, true);
        }

        // 
        // Extend service with user address
        function extendUser(user, onlyName) {
            // 
            // Set user
            user = user ? user : vm.user.model;

            if (!onlyName) {
                // 
                // Extend profile data
                if (lodash.hasIn(user.profile, 'address.payment')) {
                    angular.extend(vm.service.model.payment.payer, user.profile.address.payment.payer);
                    angular.extend(vm.service.model.payment.address, user.profile.address.payment);
                }
                if (lodash.hasIn(user.profile, 'address.shipping')) {
                    angular.extend(vm.service.model.shipping.to, user.profile.address.shipping.to);
                    angular.extend(vm.service.model.shipping.address, user.profile.address.shipping);
                }
            }

            // 
            // Add first and last name if needed
            if (!vm.service.model.payment.payer.firstName) {
                vm.service.model.payment.payer.firstName = user.profile.firstName;
                vm.service.model.payment.payer.lastName = user.profile.lastName;
            }
            if (!vm.service.model.shipping.to.firstName) {
                vm.service.model.shipping.to.firstName = user.profile.firstName;
                vm.service.model.shipping.to.lastName = user.profile.lastName;
            }
        }

        // 
        // Extend entry with user address
        function extendModel(entry) {

            // User validation
            if (!vm.user) return entry;

            // 
            // Set user
            var user = vm.user.model;

            // 
            // Set user again
            if (!lodash.hasIn(user, 'profile')) {
                user = $user.getCurrent();
                user = user.model;
            }

            // 
            // Extend profile data
            if (lodash.hasIn(user.profile, 'address.payment')) {
                angular.extend(entry.model.payment.payer, user.profile.address.payment.payer);
                angular.extend(entry.model.payment.address, user.profile.address.payment);
            }
            if (lodash.hasIn(user.profile, 'address.shipping')) {
                angular.extend(entry.model.shipping.to, user.profile.address.shipping.to);
                angular.extend(entry.model.shipping.address, user.profile.address.shipping);
            }

            // 
            // Add first and last name if needed
            if (!entry.model.payment.payer.firstName) {
                entry.model.payment.payer.firstName = user.profile.firstName;
                entry.model.payment.payer.lastName = user.profile.lastName;
            }
            if (!entry.model.shipping.to.firstName) {
                entry.model.shipping.to.firstName = user.profile.firstName;
                entry.model.shipping.to.lastName = user.profile.lastName;
            }

            return entry;
        }
    });
})();