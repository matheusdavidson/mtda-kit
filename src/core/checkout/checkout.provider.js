(function() {
    'use strict';
    angular.module('mtda.checkout').provider('$checkout', $checkoutProvider);
    /**
     * @ngdoc object
     * @name mtda.checkout.$checkoutProvider
     **/
    /*@ngInject*/
    function $checkoutProvider() {

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#current
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * current checkout
         **/
        this.current = false;

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#subtotal
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * store checkout subtotal
         **/
        this.subtotal = 0;

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#checkoutTemplateUrl
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * checkout template url
         **/
        this.checkoutTemplateUrl = 'core/checkout/checkout.tpl.html';

        /**
         * @name mtda.checkout.$checkoutProvider#shippingMethods
         * @propertyOf mtda.checkout.$checkoutProvider
         * @ngdoc object
         * @description
         * shipping methods
         **/
        this.shippingMethods = {
            free: {
                slug: 'free',
                title: 'SHIP_FREE_SHIPPING',
                price: 0
            },
            get_in_store: {
                slug: 'get_in_store',
                title: 'SHIP_GET_IN_STORE',
                price: 0
            }
        };

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#gateways
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.gateways = {
            'paypal_balance': {
                status: true
            },
            'paypal_cc': {
                status: true
            },
            'boleto': {
                status: true
            },
            'debito_bancario': {
                status: true
            },
            'cc': {
                status: true
            }
        };

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#banks
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.banks = [{
            value: 'Itau',
            name: 'Itaú',
            image: '/assets/images/logo-itau.png'
        }, {
            value: 'BancoDoBrasil',
            name: 'Banco do Brasil',
            image: '/assets/images/logo-bb.png'
        }, {
            value: 'Bradesco',
            name: 'Bradesco',
            image: '/assets/images/logo-bradesco.png'
        }, {
            value: 'Banrisul',
            name: 'Banrisul',
            image: '/assets/images/logo-banrisul.png'
        }];

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#cardTypes
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.cardTypes = {
            moip: ['AmericanExpress', 'Diners', 'Mastercard', 'Hipercard', 'Hiper', 'Elo', 'Visa'],
            paypal: ['Visa', 'Mastercard', 'Discover', 'Amex']
        }

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#cardParcelas
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * shipping methods
         **/
        this.cardParcelas = [1, 2, 3];

        /**
         * @ngdoc object
         * @name mtda.checkout.$checkoutProvider#steps
         * @propertyOf mtda.checkout.$checkoutProvider
         * @description
         * steps
         **/
        this.steps = [{
            label: 'IDENTIFICATION',
            state: 'app.checkout.identification',
            innerTabGroup: 'identification',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_one'
            },
            checked: false,
            disabled: true
        }, {
            label: 'PAYMENT_DETAILS',
            state: 'app.checkout.payment-details',
            innerTabGroup: 'payment-details',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_two'
            },
            checked: false,
            disabled: true
        }, {
            label: 'SHIPPING_DETAILS',
            state: 'app.checkout.shipping-details',
            innerTabGroup: 'shipping-details',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_3'
            },
            checked: false,
            disabled: true
        }, {
            label: 'SHIPPING_METHOD',
            state: 'app.checkout.shipping-method',
            innerTabGroup: 'shipping-method',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_4'
            },
            checked: false,
            disabled: true
        }, {
            label: 'PAYMENT',
            state: 'app.checkout.payment',
            innerTabGroup: 'payment',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_5'
            },
            checked: false,
            disabled: true
        }, {
            label: 'CONFIRMATION',
            state: 'app.checkout.confirmation',
            innerTabGroup: 'confirmation',
            icon: {
                mdFontSet: 'material-icons',
                content: 'looks_6'
            },
            checked: false,
            disabled: true
        }];

        this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, lodash, $localStorage) {
            var endpoint = api.url + 'api/checkout/',
                busy = new Busy.service();

            return {
                entries: this.entries,
                subtotal: this.subtotal,
                shippingMethods: this.shippingMethods,
                gateways: this.gateways,
                cardTypes: this.cardTypes,
                cardParcelas: this.cardParcelas,
                banks: this.banks,
                steps: this.steps,
                remove: remove,
                updateSubtotal: updateSubtotal
            }

            /**
             * @ngdoc object
             * @name mtda.checkout.$checkoutProvider#remove
             * @propertyOf mtda.checkout.$checkoutProvider
             * @description
             * remove entry
             **/
            function remove(index) {
                // 
                // Remove entry from local storage
                $localStorage.checkout.splice(index, 1);

                // 
                // Notify user
                $page.toast($page.msgCartDeleted);

                // 
                // Update subtotal
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.checkout.$checkoutProvider#updateSubtotal
             * @propertyOf mtda.checkout.$checkoutProvider
             * @description
             * update subtotal
             **/
            function updateSubtotal() {
                // 
                // Calculate new subtotal
                var subtotal = _.reduce(this.entries, function(sum, obj) {
                    return sum + (obj.price * obj.qty);
                }, 0);

                // 
                // Set new subtotal
                this.subtotal = subtotal;
            };
        };
    }
})();