(function() {
    'use strict';
    angular.module('mtda.checkout').service('$Checkout', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug, $localStorage, lodash, $checkout, $mdSidenav, $timeout, $user, $filter) {

        var url = api.url + 'api/orders/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.execute = execute;
        this.service.prototype.formStatus = formStatus;

        function create() {
            // Are we busy?
            if (this.busy.start('create')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {

                        // 
                        // Remove loading
                        this.busy.end('create');

                        return response.data;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        // 
                        // Set error msg
                        var msg = (response && response.error && response.error.translate) ? $filter('translate')(response.error.translate) : $page.msgNotDone;
                        $page.showAlert(false, 'Opss..', msg);

                        // 
                        // Remove loading
                        this.busy.end('create');

                        return response.data;
                    }.bind(this));
        }

        function execute() {
            // Are we busy?
            if (this.busy.start('execute')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url + 'execute', {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {

                        // 
                        // Remove loading
                        this.busy.end('execute');

                        return response.data;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        // 
                        // Set error msg
                        var msg = (response && response.error && response.error.translate) ? $filter('translate')(response.error.translate) : $page.msgNotDone;
                        $page.showAlert(false, 'Opss..', msg);

                        // 
                        // Remove loading
                        this.busy.end('execute');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }
    });
})();