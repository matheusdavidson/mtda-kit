(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.cart
     **/
    angular.module('mtda.checkout', [
        'ui.router'
    ]);
})();