(function() {
    'use strict';
    angular.module('mtda.checkout').config( /*@ngInject*/ function($stateProvider, $checkoutProvider, $userProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.checkout', {
            url: '/checkout/',
            abstract: true,
            innerTabGroup: 'identification',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $checkoutProvider.checkoutTemplateUrl;
                    },
                    controller: '$CheckoutCtrl as vm'
                }
            }
        }).state('app.checkout.identification', {
            url: '',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'identification',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/identification/identification.tpl.html'
                    },
                    // controller: '$CheckoutIdentificationCtrl as vm'
                }
            },
            resolve: {
                skipIfLoggedInCheckout: $userProvider.skipIfLoggedInCheckout
            }
        }).state('app.checkout.payment-details', {
            url: 'payment-details/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'payment-details',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/paymentDetails/paymentDetails.tpl.html'
                    },
                    // controller: '$CheckoutPaymentDetailsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.shipping-details', {
            url: 'shipping-details/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'shipping-details',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/shippingDetails/shippingDetails.tpl.html'
                    },
                    // controller: '$CheckoutShippingDetailsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.shipping-method', {
            url: 'shipping-method/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'shipping-method',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/shippingMethod/shippingMethod.tpl.html'
                    },
                    // controller: '$CheckoutShippingMethodCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.payment', {
            url: 'payment/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'payment',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/payment/payment.tpl.html'
                    },
                    // controller: '$CheckoutPaymentCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        }).state('app.checkout.confirmation', {
            url: 'confirmation/?method?paymentId?token?PayerID',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'confirmation',
            views: {
                'checkout-form': {
                    templateUrl: /*@ngInject*/ function() {
                        return 'core/checkout/confirmation/confirmation.tpl.html'
                    },
                    // controller: '$CheckoutConfirmationCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginCheckoutRequired
            }
        });
    })
})();