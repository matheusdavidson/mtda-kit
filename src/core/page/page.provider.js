(function() {
    'use strict';
    angular.module('mtda.page').provider('$page',
        /**
         * @ngdoc object
         * @name core.page.$pageProvider
         * @description
         * Provém configurações/comportamentos/estados para página
         **/
        /*@ngInject*/
        function $pageProvider($translateProvider) {
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_config
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena configurações
             **/
            this.config = {
                // configuração para ativar/desativar a rota inicial
                'homeEnabled': true,
                set: _config.bind(this)
            };

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_title
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena o título
             **/
            this._title = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_description
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena a descrição
             **/
            this._description = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogSiteName
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph site name
             **/
            this._ogSiteName = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogTitle
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph title
             **/
            this._ogTitle = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogDescription
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph description
             **/
            this._ogDescription = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogUrl
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph url
             **/
            this._ogUrl = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogImage
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph image
             **/
            this._ogImage = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogSection
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph section
             **/
            this._ogSection = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogTag
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph tags
             **/
            this._ogTag = '';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_toolbar
             * @propertyOf core.page.$pageProvider
             * @description
             * toolbar que deve ser exibida
             **/
            this._toolbar = 'public';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_tabs
             * @propertyOf core.page.$pageProvider
             * @description
             * tabs que deve ser exibido
             **/
            this._tabs = 'public';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_footer
             * @propertyOf core.page.$pageProvider
             * @description
             * active footer
             **/
            this._footer = 'public';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_footer
             * @propertyOf core.page.$pageProvider
             * @description
             * active footer
             **/
            this.formLocale = 'pt_BR';

            this.locale = 'pt-br';

            this.protectedSlug = '/';

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#brStates
             * @propertyOf core.page.$pageProvider
             * @description
             * all br states for select
             **/
            this.brStates = [{
                value: "AC",
                name: "Acre"
            }, {
                value: "AL",
                name: "Alagoas"
            }, {
                value: "AM",
                name: "Amazonas"
            }, {
                value: "AP",
                name: "Amapá"
            }, {
                value: "BA",
                name: "Bahia"
            }, {
                value: "CE",
                name: "Ceará"
            }, {
                value: "DF",
                name: "Distrito Federal"
            }, {
                value: "ES",
                name: "Espírito Santo"
            }, {
                value: "GO",
                name: "Goiás"
            }, {
                value: "MA",
                name: "Maranhão"
            }, {
                value: "MT",
                name: "Mato Grosso"
            }, {
                value: "MS",
                name: "Mato Grosso do Sul"
            }, {
                value: "MG",
                name: "Minas Gerais"
            }, {
                value: "PA",
                name: "Pará"
            }, {
                value: "PB",
                name: "Paraíba"
            }, {
                value: "PR",
                name: "Paraná"
            }, {
                value: "PE",
                name: "Pernambuco"
            }, {
                value: "PI",
                name: "Piauí"
            }, {
                value: "RJ",
                name: "Rio de Janeiro"
            }, {
                value: "RN",
                name: "Rio Grande do Norte"
            }, {
                value: "RO",
                name: "Rondônia"
            }, {
                value: "RS",
                name: "Rio Grande do Sul"
            }, {
                value: "RR",
                name: "Roraima"
            }, {
                value: "SC",
                name: "Santa Catarina"
            }, {
                value: "SE",
                name: "Sergipe"
            }, {
                value: "SP",
                name: "São Paulo"
            }, {
                value: "TO",
                name: "Tocantins"
            }];
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#$get
             * @propertyOf core.page.$pageProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($page) {
             *      console.log($page.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($mdToast, $filter, $mdDialog, $translate) {
                return {
                    config: this.config,
                    load: load(),
                    progress: progress(),
                    toast: toast($mdToast),
                    title: title,
                    description: description,
                    ogLocale: ogLocale,
                    ogSiteName: ogSiteName,
                    ogTitle: ogTitle,
                    ogDescription: ogDescription,
                    ogUrl: ogUrl,
                    ogImage: ogImage,
                    ogSection: ogSection,
                    ogTag: ogTag,
                    protectedSlug: this.protectedSlug,
                    toolbar: this._toolbar,
                    tabs: this._tabs,
                    footer: this._footer,
                    brStates: this.brStates,
                    msgConfirmation: false,
                    msgCreated: false,
                    msgUpdated: false,
                    msgDeleted: false,
                    msgNotDone: false,
                    msgLogin: false,
                    msgCartCreated: false,
                    msgCartDeleted: false,
                    msgFillAllFields: false,
                    msgCepNotFound: false,
                    setLocale: function(locale, tmhDynamicLocale, changeForm) {
                        $translate.use(locale);
                        moment.locale(locale);
                        this.locale = locale.toLowerCase().replace('_', '-');

                        tmhDynamicLocale.set(this.locale);

                        // 
                        // Change form?
                        if (changeForm) this.formLocale = locale;

                        // 
                        // Set system msgs
                        this.setLocaleMsgs();
                    },
                    locale: this.locale,
                    formLocale: this.formLocale,
                    setLocaleMsgs: function() {
                        this.msgConfirmation = $filter('translate')('CONFIRMATION');
                        this.msgCreated = $filter('translate')('MSG_CREATED');
                        this.msgUpdated = $filter('translate')('MSG_UPDATED');
                        this.msgDeleted = $filter('translate')('MSG_DELETED');
                        this.msgNotDone = $filter('translate')('MSG_NOT_DONE');
                        this.msgLogin = $filter('translate')('MSG_LOGIN');
                        this.msgCartCreated = $filter('translate')('MSG_CART_CREATED');
                        this.msgCartDeleted = $filter('translate')('MSG_CART_DELETED');
                        this.msgFillAllFields = $filter('translate')('MSG_FILL_ALL_FIELDS');
                        this.msgCepNotFound = $filter('translate')('CEP_NOT_FOUND');
                    },
                    showAlert: function(ev, title, text) {
                        // Appending dialog to document.body to cover sidenav in docs app
                        // Modal dialogs should fully cover application
                        // to prevent interaction outside of dialog
                        $mdDialog.show(
                            $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title(title)
                            .textContent(text)
                            .ariaLabel(title)
                            .ok('Ok')
                            .targetEvent(ev)
                        );
                    },
                    showAlertConfirm: function(ev, title, text, okTitle, cancelTitle, okCb, cancelCb) {
                        // Appending dialog to document.body to cover sidenav in docs app
                        // Modal dialogs should fully cover application
                        // to prevent interaction outside of dialog
                        var confirm = $mdDialog.confirm()
                            .title(title)
                            .htmlContent(text)
                            .ariaLabel(title)
                            .targetEvent(ev)
                            .ok(okTitle)
                            .cancel(cancelTitle);

                        $mdDialog.show(confirm).then(function() {
                            okCb();
                        }, function() {
                            cancelCb();
                        });

                    }
                }
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#config
             * @methodOf core.page.$pageProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config.set(function($pageProvider) {
             *     $pageProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            function _config(key, val) {
                if (key && (val || val === false)) {
                    return this.config[key] = val
                } else if (key) {
                    return this.config[key]
                } else {
                    return this.config
                }
            }

            /**
             * @ngdoc function
             * @name core.page.$pageProvider#title
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para meta tag título
             * @param {string} str título da página
             * @return {string} título da página
             **/
            function title(value) {
                if (value) return this._title = value;
                else return this._title;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#description
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para meta tag descrição
             * @param {string} value descrição da página
             **/
            function description(value) {
                if (value) return this._description = value;
                else return this._description;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogLocale
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph locale
             * @param {string} value locale
             **/
            function ogLocale(value) {
                if (value) return this._ogLocale = value;
                else return this._ogLocale;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogSiteName
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph site name
             * @param {string} value site name
             **/
            function ogSiteName(value) {
                if (value) return this._ogSiteName = value;
                else return this._ogSiteName;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogTitle
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph title
             * @param {string} value title
             **/
            function ogTitle(value) {
                if (value) return this._ogTitle = value;
                else return this._ogTitle;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogDescription
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph description
             * @param {string} value description
             **/
            function ogDescription(value) {
                if (value) return this._ogDescription = value;
                else return this._ogDescription;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogUrl
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph url
             * @param {string} value url
             **/
            function ogUrl(value) {
                if (value) return this._ogUrl = value;
                else return this._ogUrl;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogImage
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph image
             * @param {string} value image
             **/
            function ogImage(value) {
                if (value) return this._ogImage = value;
                else return this._ogImage;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogSection
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph section
             * @param {string} value section
             **/
            function ogSection(value) {
                if (value) return this._ogSection = value;
                else return this._ogSection;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogTag
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph tag
             * @param {string} value tag
             **/
            function ogTag(value) {
                if (value) return this._ogTag = value;
                else return this._ogTag;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#toolbar
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para toolbar
             * @param {string} value tag
             **/
            function toolbar(value) {
                if (value || value === false) return this._toolbar = value;
                else return this._toolbar;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#tabs
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph tag
             * @param {string} value tag
             **/
            function tabs(value) {
                if (value || value === false) return this._tabs = value;
                else return this._tabs;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#footer
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter active footer
             * @param {string} value tag
             **/
            function footer(value) {
                if (value || value === false) return this._footer = value;
                else return this._footer;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#load
             * @methodOf core.page.$pageProvider
             * @description
             * inicia e termina o carregamento da página
             * @return {object} com metodos de inicialização (init) e finalização (done)
             **/
            function load() {
                return {
                    init: function() {
                        this.status = true;
                        //console.log('loader iniciado...' + this.status);
                    },
                    done: function() {
                        this.status = false;
                        //console.log('loader finalizado...' + this.status);
                    }
                }
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#toast
             * @methodOf core.page.$pageProvider
             * @description
             * mostra uma mensagem de aviso
             * @param {string} msg mensagem
             * @param {integer} time tempo em milisegundos
             * @param {string} position posição do alerta. default: 'bottom right'
             **/
            function toast($mdToast) {
                return function(msg, time, position) {
                    time = time ? time : 5000;
                    $mdToast.show($mdToast.simple().content(msg).position(position ? position : 'bottom right').hideDelay(time));
                }
            }
            //another type of load
            function progress() {
                return {
                    init: function() {
                        this.status = true;
                        //console.log('progress iniciado...' + this.status);
                    },
                    done: function() {
                        this.status = false;
                        //console.log('progress finalizado...' + this.status);
                    }
                }
            }
        }
    )
})();