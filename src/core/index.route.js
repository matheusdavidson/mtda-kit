(function() {
    'use strict';

    angular
        .module('mtda.app')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider, $mainProvider, $urlMatcherFactoryProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app', {
            abstract: true,
            views: {
                'app': {
                    templateUrl: /*@ngInject*/ function() {
                        return $mainProvider.layoutUrl();
                    }
                }
            }
        }).state('app.four-oh-four', {
            url: '/404/',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $mainProvider.fourOhFourUrl();
                    }
                }
            }
        });

        $urlRouterProvider.otherwise('/404/');

        //
        // Redirect Trailing Slash
        //
        // $urlMatcherFactoryProvider.strictMode(false);
        // $urlRouterProvider.otherwise(function($injector, $location) {
        //     // 
        //     // Not for iframes
        //     if ($location.hash() !== 'iframe') {

        //         // 
        //         // Inject lodash
        //         var _ = $injector.get('lodash');

        //         // 
        //         // Get current path
        //         var path = $location.url();

        //         // 
        //         // Normal cenario, but if this is still here, no route found, its a 404
        //         if (_.endsWith(path, '/') || _.endsWith(path, '/?')) {
        //             // 
        //             // If we are already /404/ or /404/? dont do nothing
        //             if (_.endsWith(path, '404/') || _.endsWith(path, '404/?')) return;

        //             // 
        //             // Send to 404 page
        //             $location.path('/404/');
        //             return;
        //         } else
        //         // 
        //         // Bad cenario, change ? for /?
        //         if (path.indexOf('?') > -1) {
        //             return path.replace('?', '/?');
        //         }

        //         // 
        //         // Bad cenario, no /, place /
        //         return path + '/';
        //     }
        // });
    }

})();