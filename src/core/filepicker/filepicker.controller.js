(function() {
    'use strict';

    angular.module('mtda.filepicker').controller('$FilepickerCtrl', /*@ngInject*/ function($scope, $log, $Filepicker, $filepicker, lodash, $timeout) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $filepicker;
        vm.entries = vm.factory.entries;

        console.log('freeAspect', vm.freeAspect);
        // 
        // Qty
        if (!vm.qty) vm.qty = 0;

        vm.service = new $Filepicker.service({
            qty: vm.qty,
            cropperOptions: vm.cropperOptions,
            result: vm.result
        });

        $scope.$watch('vm.service.result', function(nv, ov) {
            vm.result = nv;
        }, true);
    });
})();