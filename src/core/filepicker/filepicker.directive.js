(function() {
    'use strict';

    angular
        .module('mtda.filepicker')
        .directive('filepicker', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/filepicker/filepicker.tpl.html';
            },
            controller: '$FilepickerCtrl',
            controllerAs: 'vm',
            bindToController: {
                qty: '=',
                cropperOptions: '=',
                result: '=',
                bucket: '=',
                prefix: '=',
                useInstagram: '=',
                freeAspect: '='
            }
        };

        return directive;
    }

})();