(function() {
    'use strict';
    angular.module('mtda.filepicker').service('$Filepicker', /*@ngInject*/ function($q, api, $page, Busy, lodash, Upload, $http, $filter) {

        var url = api.url + 'api/files/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            this.busy = new Busy.service();
            this.files = [];
            this.crop = false;
            this.qty = 0;
            this.selected = 0;
            this.view = 'main';
            this.filesOld = [];

            //
            // Instantiate
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.processFiles = processFiles;
        this.service.prototype.convertToBase64 = convertToBase64;
        this.service.prototype.cropWrapAndPush = cropWrapAndPush;
        this.service.prototype.cropImage = cropImage;
        this.service.prototype.remove = remove;
        this.service.prototype.startCrop = startCrop;
        this.service.prototype.saveCrop = saveCrop;
        this.service.prototype.cancelCrop = cancelCrop;
        this.service.prototype.updateSelectedCnt = updateSelectedCnt;
        this.service.prototype.toggleSelected = toggleSelected;
        this.service.prototype.updateResult = updateResult;
        this.service.prototype.pickOld = pickOld;
        this.service.prototype.cancelOld = cancelOld;
        this.service.prototype.saveOld = saveOld;

        function processFiles($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event, noBlob) {
            // Start promise
            var defer = $q.defer();

            // Are we busy?
            if (this.busy.start('processFiles')) defer.reject(response);

            var files = $newFiles;

            if (files && files.length) {

                // 
                // Transform images in base64
                this.convertToBase64(files, noBlob, function(urls) {
                    // 
                    // Create cropped image
                    // Wrap into an object
                    // Push to service files
                    this.cropWrapAndPush(urls, function() {
                        // 
                        // Stop busy
                        this.busy.end('processFiles');

                        // 
                        // Update cnt
                        this.updateSelectedCnt();

                        // 
                        // Update result
                        this.updateResult();

                        // 
                        // return promise
                        defer.resolve();

                    }.bind(this));
                }.bind(this));
            } else {
                // 
                // Stop busy
                this.busy.end('processFiles');

                // 
                // return promise
                defer.reject();
            }

            // Return promise
            return defer.promise;
        }

        function convertToBase64(files, noBlob, cb) {
            if (noBlob) {
                dataUrlArray(files, function(urls) {
                    cb(urls);
                });
            } else {
                // 
                // Convert to base64 using Upload
                Upload.base64DataUrl(files).then(function(urls) {
                    cb(urls);
                });
            }
        }

        function dataUrlArray(files, cb) {
            var processedFiles = [],
                cnt = files.length,
                x = 0;

            if (!files || !files.length) return cb([]);

            angular.forEach(files, function(file) {
                //
                // Convert blob to base64
                toDataUrl(file.url, 'image/jpeg', function(dataUrl) {
                    // 
                    // Add to processedFiles
                    processedFiles.push(dataUrl);

                    // 
                    // add cnt
                    x++;

                    // 
                    // Return if is the last one
                    if (x == cnt) cb(processedFiles);
                });
            });
        }

        function toDataUrl(url, outputFormat, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';

            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
                canvas = null;
            };
            img.src = url.replace(/^https:\/\//i, 'http://');
        }

        function cropWrapAndPush(urls, cb) {
            var x = 1,
                length = urls.length;

            // 
            // Set selected cnt for comparisons
            var selectedCnt = angular.copy(this.selected);

            // 
            // Adjust images, get cropped image and add to service files
            angular.forEach(urls, function(img) {
                // 
                // We need img
                if (!img || !img.length) return;

                // 
                // Crop the image
                this.cropImage(img, function(cropped) {

                    // 
                    // Set selected
                    // select only if we have less then qty
                    var selected = ((selectedCnt + x) <= this.qty) ? true : false;

                    // 
                    // Create obj
                    var obj = {
                        img: img,
                        cropped: cropped,
                        selected: selected
                    };

                    // 
                    // Push obj to service files
                    this.files.push(obj);

                    // 
                    // Return if is the last count
                    if (x === length) return cb(true);

                    // 
                    // Add to count
                    x++;
                }.bind(this));
            }.bind(this));
        }

        // 
        // Crop image using cropper js
        function cropImage(img, cb) {
            // 
            // Create a fake element
            var elem = angular.element('<img src="' + img + '" />');

            // 
            // Cropper options
            var resultImage = (lodash.hasIn(this.cropperOptions, 'resultImage')) ? this.cropperOptions.resultImage : {
                w: 540,
                h: 540
            };

            var cropperOptions = {
                aspectRatio: 4 / 4
            };

            // 
            // Extend with user options
            angular.extend(cropperOptions, this.cropperOptions);

            // 
            // Start crop and crop image
            var crop = elem.cropper(cropperOptions);

            var cropped = crop.cropper('getCroppedCanvas', resultImage).toDataURL("image/jpeg", 0.99);
            cb(cropped);
        }

        // 
        // Remove file
        function remove(index, item) {

            // 
            // Remove from files
            if (this.files && this.files[index]) this.files.splice(index, 1);

            // 
            // Update cnt
            this.updateSelectedCnt();

            // 
            // Update result
            this.updateResult();
        }

        // 
        // Start crop
        function startCrop(index, item) {
            // 
            // Create crop obj
            var crop = {};

            // 
            // Properties
            crop.img = item.img;
            crop.model = false;
            crop.index = index;

            this.crop = crop;
        }

        // 
        // Save crop
        function saveCrop() {
            // 
            // Create new obj
            var cropped = {};

            cropped.img = this.crop.model;
            cropped.cropped = this.crop.model;

            // 
            // Extend new obj with existing in files
            angular.extend(this.files[this.crop.index], cropped);

            // 
            // Disable crop
            this.crop = false;
        }

        // 
        // Cancel crop
        function cancelCrop() {
            // 
            // Disable crop
            this.crop = false;
        }

        // 
        // Calculate qty
        function updateSelectedCnt() {
            // 
            // How many we have
            var cnt = lodash.filter(this.files, 'selected').length;

            // 
            // Set new cnt
            this.selected = cnt;

            return this.selected;
        }

        // 
        // Update result
        function updateResult() {
            // 
            // Get selected files
            var result = lodash.filter(this.files, 'selected');
            this.result = result;
        }

        // 
        // Toggle selected
        function toggleSelected(file) {

            // 
            // Don't let select if we have reached qty
            if (file.selected && this.selected === this.qty) {
                file.selected = false;

                // 
                // Toast
                $page.toast('Limite de ' + this.qty + ' atingido');

                return;
            }

            // 
            // Update selected cnt
            this.updateSelectedCnt();

            // 
            // Update result
            this.updateResult();
        }

        function pickOld() {
            this.view = 'old';

            // 
            // Get files
            this.busy.start('getOld');

            // 
            // Get old files
            return $http
                .get(url, {})
                .success(
                    //
                    // success
                    //
                    function(response) {
                        this.busy.end('getOld');

                        // 
                        // Feed old files
                        this.filesOld = response;

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getOld');

                        return response;
                    }.bind(this));
        }

        function cancelOld() {
            this.view = 'main';
        }

        function saveOld() {
            // 
            // Get selected files
            var selected = lodash.filter(this.filesOld, 'selected');

            // 
            // Process files
            this.processFiles(false, false, selected, false, false, false, true).then(function() {
                // 
                // Change view
                this.view = 'main';
            }.bind(this));
        }
    });
})();