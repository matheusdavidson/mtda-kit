(function() {
    'use strict';
    angular.module('mtda.filepicker').provider('$filepicker', $filepickerProvider);
    /**
     * @ngdoc object
     * @name mtda.filepicker.$filepickerProvider
     **/
    /*@ngInject*/
    function $filepickerProvider() {

        /**
         * @ngdoc object
         * @name mtda.filepicker.$filepickerProvider#entries
         * @propertyOf mtda.filepicker.$filepickerProvider
         * @description
         * store filepicker entries
         **/
        this.entries = [];

        /**
         * @ngdoc object
         * @name mtda.filepicker.$filepickerProvider#subtotal
         * @propertyOf mtda.filepicker.$filepickerProvider
         * @description
         * store filepicker subtotal
         **/
        this.subtotal = 0;

        /**
         * @ngdoc object
         * @name mtda.filepicker.$filepickerProvider#filepickerTemplateUrl
         * @propertyOf mtda.filepicker.$filepickerProvider
         * @description
         * filepicker template url
         **/
        this.filepickerTemplateUrl = 'core/filepicker/filepicker.tpl.html';

        this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, lodash, $localStorage) {
            var endpoint = api.url + 'api/filepickers/',
                busy = new Busy.service();

            return {
                entries: this.entries,
                subtotal: this.subtotal,
                remove: remove,
                updateSubtotal: updateSubtotal,
                clear: clear,
                hasEntries: hasEntries
            }

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#remove
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * remove entry
             **/
            function remove(index) {
                // 
                // Remove entry from local storage
                this.entries.splice(index, 1);

                // 
                // Notify user
                $page.toast($page.msgCartDeleted, 5000, 'top right');

                // 
                // Update subtotal
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#clear
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * remove all entries
             **/
            function clear() {
                // 
                // Remove all entries
                _.times(this.entries.length, function() {
                    // 
                    // Remove first element
                    this.entries.shift();
                }.bind(this));

                // 
                // Update totals
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#updateSubtotal
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * update subtotal
             **/
            function updateSubtotal() {
                // 
                // Calculate new subtotal
                var subtotal = _.reduce(this.entries, function(sum, obj) {
                    return sum + (obj.price * obj.qty);
                }, 0);

                // 
                // Set new subtotal
                this.subtotal = subtotal;
            };

            /**
             * @ngdoc object
             * @name mtda.filepicker.$filepickerProvider#hasEntries
             * @propertyOf mtda.filepicker.$filepickerProvider
             * @description
             * check if filepicker has entries
             **/
            function hasEntries() {
                return (this.entries.length);
            }
        };
    }
})();