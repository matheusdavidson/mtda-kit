(function() {
    'use strict';
    angular.module('mtda.main').provider('$menu',
        /**
         * @ngdoc object
         * @name mtda.main.$menuProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $mainProvider() {
            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#_config
             * @propertyOf mtda.main.$menuProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#site
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu do site
             **/
            this.site = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#side
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu do side
             **/
            this.side = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#avatar
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu do avatar
             **/
            this.avatar = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#admin
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu admin
             **/
            this.admin = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#owner
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu owner
             **/
            this.owner = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#currentUser
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu para o usuário
             **/
            this.currentUser = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#userLoggedOut
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu de usuário não logado
             **/
            this.userLoggedOut = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#userLoggedIn
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu de usuário logado
             **/
            this.userLoggedIn = [];

            /**
             * @ngdoc object
             * @name mtda.main.$menuProvider#ownerLoggedIn
             * @propertyOf mtda.main.$menuProvider
             * @description
             * Armazena o menu de usuário logado do role 'owner'
             **/
            this.ownerLoggedIn = [];

            /**
             * @ngdoc function
             * @name mtda.main.$menuProvider#$get
             * @propertyOf mtda.main.$menuProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($window, setting) {
                return {
                    config: this._config,
                    site: this.site,
                    side: this.side,
                    avatar: this.avatar,
                    admin: this.admin,
                    owner: this.owner,
                    currentUser: this.currentUser,
                    userLoggedOut: this.userLoggedOut,
                    userLoggedIn: this.userLoggedIn,
                    ownerLoggedIn: this.ownerLoggedIn
                }
            }

            /**
             * @ngdoc function
             * @name mtda.main.$menuProvider#config
             * @methodOf mtda.main.$menuProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.main.$menuProvider#set
             * @methodOf mtda.main.$menuProvider
             * @description
             * Adicionar um novo menu
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($menuProvider) {
             *     $menuProvider.set('admin', {
             *         name: 'Conta',
             *         type: 'link',
             *         icon: 'fa fa-at',
             *         url: '/account/',
             *         state: 'app.account'
             *     });
             * })
             * </pre>
             * @param {string} menu - slug do menu
             * @param {object} item - objeto contendo as propriedades do menu
             **/
            this.set = function(menu, items) {
                $.merge(this[menu], items);
            }
        }
    )
})();