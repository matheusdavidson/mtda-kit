(function() {
    'use strict';
    angular.module('mtda.directives').controller('$CropperCtrl', /*@ngInject*/ function($scope, $element, $filter, $timeout, Upload) {
        var vm = this;

        // Default params
        vm.imageElem = false;
        vm.dataUrl = false;
        vm.cropper = false;
        vm.cropTimeout = false;

        // Cropper default options
        vm.options = {
            aspectRatio: 16 / 9,
            resultImage: {
                w: 160,
                h: 90
            },
            viewMode: 3,
            autoCropArea: 1,
            crop: function(data) {

                // Cancel timeout
                $timeout.cancel(vm.cropTimeout);

                // Timeout for setting result image model
                vm.cropTimeout = $timeout(function() {

                    // jQuery Cropped image
                    vm.resultImage = vm.imageElem.cropper('getCroppedCanvas', {
                        width: vm.options.resultImage.w,
                        height: vm.options.resultImage.h
                    }).toDataURL("image/jpeg", 0.99);

                    // JS Cropper image
                    // vm.resultImage = vm.cropper.getCroppedCanvas({
                    //     width: vm.options.resultImage.w,
                    //     height: vm.options.resultImage.h
                    // }).toDataURL("image/jpeg", 0.99);

                }, 200);
            }
        };

        // Extend default options with user options
        angular.extend(vm.options, vm.cropperOptions);

        // Watch for changes in the image
        $scope.$watch('vm.image', function(nv, ov) {
            if (nv || nv !== ov) {
                // We need some value
                if (!nv) return false;

                // 
                // Check if element is already a base64
                if (typeof nv === 'string' && nv.indexOf('data:image') != -1) return setDataAndStart(nv);

                //
                // Convert blob to base64
                Upload.dataUrl(nv, true).then(function(dataUrl) {
                    setDataAndStart(dataUrl);
                });
            }
        });

        function setDataAndStart(dataUrl) {
            // Set dataUrl
            vm.dataUrl = dataUrl;

            // Wait next angular digest
            $timeout(function() {
                // Start cropper
                start();
            });
        }

        function start(dataUrl) {
            // Set image element
            vm.imageElem = $element.find('img');

            // Destroy the jQuery Cropper
            if (vm.cropper) vm.cropper('destroy');

            // Destroy the JS Cropper
            // if (vm.cropper) vm.cropper.destroy();

            // Start the jQuery Cropper
            vm.cropper = vm.imageElem.cropper(vm.options);

            // Start the JS Cropper
            // vm.cropper = new Cropper(vm.imageElem[0], vm.options);
        }
    })
})();