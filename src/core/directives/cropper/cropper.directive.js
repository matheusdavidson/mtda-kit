(function() {
    'use strict';
    angular.module('mtda.directives').directive('ngCropper', /*@ngInject*/ function($filter) {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/cropper/cropper.tpl.html",
            controller: '$CropperCtrl',
            controllerAs: 'vm',
            scope: {},
            bindToController: {
                image: '=',
                resultImage: '=',
                cropperOptions: '='
            }
        }
    })
})();