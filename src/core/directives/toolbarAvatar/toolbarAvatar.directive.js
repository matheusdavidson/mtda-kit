(function() {
    'use strict';

    angular.module('mtda.directives').directive('toolbarAvatar', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$ToolbarAvatarCtrl',
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/toolbarAvatar/toolbarAvatar.tpl.html';
            }
        };
    });

})();