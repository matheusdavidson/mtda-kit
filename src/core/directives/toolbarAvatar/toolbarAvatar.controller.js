(function() {
    'use strict';
    angular.module('mtda.directives').controller('$ToolbarAvatarCtrl', /*@ngInject*/ function($user, $menu) {
        var vm = this;

        vm.factory = $user;
        vm.menu = $menu;
        vm.logout = logout;

        // 
        // Current user
        vm.currentUser = vm.factory.getCurrent();

        // 
        // Show menu validation
        vm.showMenu = showMenu;

        function logout() {
            vm.factory.logout();
        }

        function showMenu(roles) {
            var invalid = false;

            // 
            // User must be logged in
            if (!vm.currentUser) return false;

            // 
            // Check each role
            if (roles) {
                angular.forEach(roles, function(val) {
                    // 
                    // This role can't be false
                    if (!vm.currentUser.model.roles[val]) invalid = true;
                });
            }

            // 
            // is invalid?
            if (invalid) return false;

            // 
            // Return true to show, since no other validation failed
            return true;
        }
    })
})();