(function() {
    'use strict';
    angular.module('mtda.directives').directive('sidenavMenu', /*@ngInject*/ function($filter) {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: "core/directives/sidenavMenu/sidenavMenu.tpl.html",
            controller: '$SidenavMenuCtrl',
            controllerAs: 'vm',
            bindToController: {
                area: '='
            }
        }
    })
})();