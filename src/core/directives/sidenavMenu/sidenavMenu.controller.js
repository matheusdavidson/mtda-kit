(function() {
    'use strict';
    angular.module('mtda.directives').controller('$SidenavMenuCtrl', controller);
    /*@ngInject*/
    function controller(lodash, $page, $user, $menu) {
        var vm = this;

        // 
        // Active menu
        vm.activeTab = activeTab;

        // 
        // set User
        vm.user = $user;

        // 
        // Set menu
        vm.menu = $menu;
        vm.sideMenu = sideMenu();
        // // 
        // // for ng-if
        // vm.showOwner = showOwner;
        // vm.showAdmin = showAdmin;

        function activeTab(tabGroup) {
            // 
            // Check against page config('tabGroup')
            return (tabGroup === $page.config.innerTabGroup);
        }

        // function toggleMenu() {
        //     vm.closed = vm.closed ? false : true;
        // }

        // function showOwner() {
        //     var user = vm.user.getCurrent();
        //     if (!user) return false;

        //     return (user.model.roles.isOwner || user.model.roles.isAdmin);
        // }

        // function showAdmin() {
        //     var user = vm.user.getCurrent();
        //     if (!user) return false;

        //     return (user.model.roles.isAdmin);
        // }

        function sideMenu() {

            // 
            // Set initial vars
            var menu = false,
                currentUser = vm.user.getCurrent();

            // 
            // Validate currentUser
            if (!currentUser) return [];

            // 
            // Add user roles
            var userRoles = currentUser.model.role;

            // 
            // Filter sidemenu and check for allowed
            menu = lodash.filter(vm.menu.side, function(value) {

                // 
                // Get difference menu.role x userRoles
                var diff = _.difference(value.role, userRoles);

                // 
                // Menu is allowed when diff length is lower then value.role length
                return (diff.length < value.role.length);
            });

            // 
            // Return filtered menu
            return menu;
        }
    }
})();