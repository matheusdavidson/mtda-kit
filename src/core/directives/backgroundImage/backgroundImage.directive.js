(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.directives.directive:backgroundImage
     * @restrict EA
     * @description
     * Setar background image
     **/
    angular.module('mtda.directives').directive('backgroundImage', /*@ngInject*/ function() {
        return function(scope, element, attrs) {
            attrs.$observe('backgroundImage', function(value) {
                if (!value) return;

                element.css({
                    'background-image': 'url(' + value + ')'
                });
            });
        };
    });
})();