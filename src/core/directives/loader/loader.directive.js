(function() {
    'use strict';
    angular.module('mtda.directives').directive('loader', /*@ngInject*/ function() {
        return {
            restrict: 'E',
            templateUrl: "core/directives/loader/loader.tpl.html"
        }
    })
})();