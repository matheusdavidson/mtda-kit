(function() {
    'use strict';
    angular.module('mtda.directives').controller('$BarAvatarCtrl', /*@ngInject*/ function($user, $menu, lodash) {
        var vm = this;

        vm.factory = $user;
        vm.menu = $menu;
        vm.logout = logout;

        // 
        // Current user
        vm.currentUser = vm.factory.getCurrent();

        // 
        // Show menu validation
        vm.avatarMenu = avatarMenu();

        function logout() {
            vm.factory.logout();
        }

        function avatarMenu() {

            // 
            // Set initial vars
            var menu = false,
                currentUser = vm.factory.getCurrent();

            // 
            // Validate currentUser
            if (!currentUser) return [];

            // 
            // Add user roles
            var userRoles = currentUser.model.role;

            // 
            // Filter sidemenu and check for allowed
            menu = lodash.filter(vm.menu.avatar, function(value) {

                // 
                // Get difference menu.role x userRoles
                var diff = _.difference(value.role, userRoles);

                // 
                // Menu is allowed when diff length is lower then value.role length
                return (diff.length < value.role.length);
            });

            // 
            // Return filtered menu
            return menu;
        }
    })
})();