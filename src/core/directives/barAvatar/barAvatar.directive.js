(function() {
    'use strict';

    angular.module('mtda.directives').directive('barAvatar', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$BarAvatarCtrl',
            controllerAs: 'vm',
            bindToController: {
                pictureOrder: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/barAvatar/barAvatar.tpl.html';
            }
        };
    });

})();