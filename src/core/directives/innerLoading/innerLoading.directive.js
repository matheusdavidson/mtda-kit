(function() {
    'use strict';
    angular.module('mtda.directives').directive('innerLoading', /*@ngInject*/ function($filter) {
        return {
            restrict: 'A',
            templateUrl: "core/directives/innerLoading/innerLoading.tpl.html",
            controller: 'InnerLoadingCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                condition: '='
            }
        }
    })
})();