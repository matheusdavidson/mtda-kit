(function() {
    'use strict';
    angular
        .module('mtda.directives')
        .directive('sidenav', sidenav);

    /** @ngInject */
    function sidenav() {
        var directive = {
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/sidenav/sidenav.tpl.html';
            },
            controller: NavController,
            controllerAs: 'vm',
            bindToController: true
        };
        return directive;

        /** @ngInject */
        function NavController($scope, $element, $mdSidenav, $state) {
            var vm = this;

            vm.adminIsLockedOpen = adminIsLockedOpen;

            // 
            // Admin is locked open
            function adminIsLockedOpen() {
                return ($state.current.toolbar === 'admin');
            }
        }
    }
})();