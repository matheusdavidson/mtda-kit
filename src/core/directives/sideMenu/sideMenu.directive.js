(function() {
    'use strict';

    angular
        .module('mtda.directives')
        .directive('sideMenu', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/sideMenu/sideMenu.tpl.html';
            },
            controller: '$SidenavMenuCtrl',
            controllerAs: 'vm',
            bindToController: {
                items: '='
            }
        };

        return directive;
    }

})();