(function() {
    'use strict';
    angular.module('mtda.directives').controller('$CeperCtrl', /*@ngInject*/ function($http, $page, api, lodash) {
        var vmCep = this;
        vmCep.busy = false;
        vmCep.get = get;

        // 
        // Set address obj if is not provided
        if (!lodash.hasIn(vmCep, 'address')) vmCep.address = {};

        function get() {
            var cep = vmCep.model;

            if (vmCep.region === 'uk') {
                getPostcode(cep);
            } else {
                getCep(cep);
            }
        }

        function getCep(cep) {
            if (cep && cep.toString().length === 8) {
                var url = api.url + 'api/cep/';

                // 
                // Set busy
                vmCep.busy = true;

                // 
                // Request addr
                request(url, cep);
            }
        }

        function getPostcode(cep) {
            var url = api.url + 'api/postcode/';

            // 
            // Set busy
            vmCep.busy = true;

            // 
            // Request addr
            request(url, cep);
        }

        function request(url, cep) {
            // 
            // Hide all elements with hide-on-cep-search
            angular.element('.hide-on-cep-search').addClass('searching-cep');
            return $http.get(url + cep, {}).then(onSuccess, onError);
        }

        function onSuccess(response) {
            var cep = response.data;

            // 
            // Unset busy
            vmCep.busy = false;

            // 
            // Unhide elements
            angular.element('.hide-on-cep-search').removeClass('searching-cep');

            // 
            // Empty current address
            vmCep.address.street = '',
            vmCep.address.num = '',
            vmCep.address.bairro = '',
            vmCep.address.city = '',
            vmCep.address.state = '',

            // 
            // Extend found cep with model
            angular.extend(vmCep.address, cep);

            // 
            // Focus on number input if we have street
            if (cep.street) angular.element('.number-input').focus();
            else angular.element('.street-input').focus();
        }

        function onError(response) {
            // 
            // Unset busy
            vmCep.busy = false;

            // 
            // Unhide elements
            angular.element('.hide-on-cep-search').removeClass('searching-cep');

            // 
            // Show msg
            $page.toast($page.msgCepNotFound, 10000, 'bottom left');
        }
    });
})();