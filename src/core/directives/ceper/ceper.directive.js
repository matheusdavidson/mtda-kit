(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.directives.directive:ceper
     * @restrict EA
     * @description
     * Input para auto busca de cep
     * @element div
     * @param {object} model - model qye representa o campo numerico do cep
     * @param {object} address - model que representa os campos de endereço (street, district, city, state)
     * @param {string} endereço do server que deverá responder o json no formato esperado
     **/
    angular.module('mtda.directives').directive('ceper', /*@ngInject*/ function() {
        return {
            restrict: 'EA',
            templateUrl: 'core/directives/ceper/ceper.tpl.html',
            controller: '$CeperCtrl',
            controllerAs: 'vmCep',
            bindToController: {
                model: '=',
                address: '=',
                required: '='
            }
        }
    });
})();