(function() {
    'use strict';
    angular.module('mtda.directives').controller('BreadcrumbsCtrl', /*@ngInject*/ function($scope) {
        var vm = this;

        $scope.$watch('vm.data', function(nv, ov) {
            bootstrap();
        }, true);

        function bootstrap() {
            vm.data.slugTitle = vm.data.slug ? vm.data.slug.split('-').join(' ') : false;
            vm.data.categoryTitle = vm.data.category ? vm.data.category.split('-').join(' ') : (vm.data.title ? vm.data.title : false);
            vm.data.childTitle = vm.data.child ? vm.data.child.split('-').join(' ') : false;

            if (!vm.data.category) vm.data.category = undefined;
        }
    })
})();