(function() {
    'use strict';
    angular.module('mtda.directives').directive('breadcrumbs', /*@ngInject*/ function($filter) {
        return {
            restrict: 'A',
            templateUrl: "core/directives/breadcrumbs/breadcrumbs.tpl.html",
            controller: 'BreadcrumbsCtrl',
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                data: '='
            }
        }
    })
})();