(function() {
    'use strict';

    angular.module('mtda.directives').directive('sidenavUserToolbar', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/directives/sidenavUserToolbar/sidenavUserToolbar.tpl.html';
            },
            controller: NavController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function NavController($scope, $user, lodash) {
            var vm = this;
            vm.user = $user;
        }
    }

})();