(function() {
    'use strict';
    angular.module('mtda.directives').controller('$ToolbarTabsCtrl', controller);
    /*@ngInject*/
    function controller($menu, $user, lodash, $page, $state, $timeout) {
        var vm = this;
        vm.menu = $menu;
        vm.user = lodash.hasIn($user, '_current') ? $user._current : false;
        vm.activeTab = activeTab;
        vm.goTo = goTo;
        vm.init = false;

        $timeout(function() {
            vm.init = true;
        }, 100);

        // 
        // Permission check
        vm.permissions = {
            admin: vm.user && lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'admin')) ? true : false,
            owner: vm.user && lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'owner')) ? true : false,
            user: vm.user && lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'user')) ? true : false,
        }

        function activeTab(tabGroup) {
            // 
            // Check against page config('tabGroup')
            return (tabGroup === $page.config.tabGroup);
        }

        function goTo(route) {
            if (!vm.init) return false;

            $state.go(route, {
                inherit: false
            });
            // // 
            // // Cancel previus timeout
            // if (vm.goToTimeout) $timeout.cancel(vm.goToTimeout);

            // // 
            // // Check if this is not the current route already
            // vm.goToTimeout = $timeout(function() {
            //     if ($state.current.name !== route)
            //         $state.go(route, {
            //             inherit: false
            //         });
            // }, 100);
        }
    }
})();