(function() {
    'use strict';
    angular.module('mtda.directives').directive('toolbarTabs', /*@ngInject*/ function($filter) {
        return {
            restrict: 'EA',
            templateUrl: "core/directives/toolbarTabs/toolbarTabs.tpl.html",
            controller: '$ToolbarTabsCtrl',
            controllerAs: 'vm',
            bindToController: true
        }
    })
})();