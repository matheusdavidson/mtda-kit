(function() {
    'use strict';

    angular.module('mtda.directives').controller('$ToolbarCtrl', controller);

    /** @ngInject */
    function controller($scope, $element, $mdSidenav, $state, $timeout, $window, $user) {
        var vm = this;

        vm.searchMode = false;
        vm.sidenav = $mdSidenav;
        vm.toogleSearch = toogleSearch;
        vm.goToSearch = goToSearch;
        vm.term = '';
        vm.user = $user;
        vm.activeTab = activeTab;

        // $scope.$watch('vm.categories', watchCategories, true);
        vm.openMenu = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        //
        // scrollTo
        //
        vm.scrollTo = scrollTo;

        //
        // Open menu
        // 
        vm.openMenu = openMenu;

        //
        // Fixed on scroll
        // 
        var wdow = angular.element('.app-content'),
            currentState = false;

        // Bind window scroll if fixedOnScroll is set
        if (vm.fixedOnScroll) wdow.bind('scroll', navbarPosition);

        // Fix / unfix navbar
        function navbarPosition() {
            var elem = angular.element('md-toolbar.toolbar-default');

            if (!elem.length) {
                // 
                // Remove fixed from toolbar and wrapper
                elem.removeClass('fixed');
                wdow.removeClass('toolbar-fixed');

                // 
                // Reset currentstate
                currentState = false;

                // 
                // Remove binding
                wdow.unbind('scroll');

                return false;
            }

            var point = angular.element(".fixed-point"),
                pointTop = point.offset().top,
                pos = (pointTop - 0);

            if (currentState === false && pos <= 0) {
                currentState = 'add';
                elem.hide().slideDown('slow').addClass('fixed');
                wdow.addClass('toolbar-fixed');
            } else if (currentState === 'add' && pos > 0) {
                currentState = false;
                elem.hide().show().removeClass('fixed');
                wdow.removeClass('toolbar-fixed');
            }
        }

        function scrollTo(view) {
            var target = angular.element('#' + view);
            if (target.length) {
                wdow.animate({
                    scrollTop: target.offset().top
                }, 800, 'swing');
            }
        }

        function openMenu($mdOpenMenu, ev) {
            $mdOpenMenu(ev);
        }

        function toogleSearch(value) {
            // Focus product search input in the page for productSearch route
            if (value && $state.current.name === 'app.productSearch') {
                // Focus search input
                focusSearchPageInput();

                return;
            }

            vm.searchMode = value;

            if (value) {
                vm.term = '';

                $timeout(function() {
                    var input = $element.find('.search');
                    input.focus();
                }, 200);
            }
        }

        function goToSearch() {
            $state.go('app.productSearch', {
                term: vm.term
            });

            // Close search and focus product search page input
            $timeout(function() {
                // Clear search
                vm.term = '';

                // Close search
                vm.toogleSearch(false);

                // Focus search input
                focusSearchPageInput();
            }, 100);
        }

        function focusSearchPageInput() {
            var input = angular.element('.product-search form').find('input');
            input.focus();
        }

        function goToBanner(url) {
            $window.location.href = url;
        }

        function activeTab(slug) {
            return (
                (
                    $state.current.name == 'app.publicCategories' ||
                    $state.current.name == 'app.publicCategoriesEmpty'
                ) &&
                $state.params.category == slug
            );
        }
    }
})();