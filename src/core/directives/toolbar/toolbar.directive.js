(function() {
    'use strict';
    angular.module('mtda.directives').directive('toolbar', /*@ngInject*/ function($main) {
        return {
            scope: {},
            controller: '$ToolbarCtrl',
            controllerAs: 'vm',
            bindToController: {
                currentState: '=',
                categories: '=',
                fixedOnScroll: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : $main.toolbarUrl;
            }
        };
    });
})();