(function() {
    'use strict';
    angular.module('mtda.user').provider('$user',
        /**
         * @ngdoc object
         * @name mtda.user.$userProvider
         **/
        /*@ngInject*/
        function $userProvider() {
            var path = 'core/user/';

            this._current = null;

            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_config
             * @propertyOf mtda.user.$userProvider
             * @description
             * armazena configurações
             **/
            this._config = {};
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_loginTemplateUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url do template para a rota
             **/
            this._loginTemplateUrl = path + 'login/login.tpl.html';
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_registerTemplateUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url do template para novos cadastros
             **/
            this._registerTemplateUrl = path + 'register/register.tpl.html';
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_recoveryTemplateUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url do template para recuperação de senha
             **/
            this._recoveryTemplateUrl = path + 'recovery/recovery.tpl.html';
            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_skipIfLoggedInUrl
             * @propertyOf mtda.user.$userProvider
             * @description
             * url para método skipIfLoggedIn
             **/
            this._skipIfLoggedInUrl = '/admin/';

            /**
             * @ngdoc object
             * @name mtda.user.$userProvider#_roles
             * @propertyOf mtda.user.$userProvider
             * @description
             * user roles
             **/
            this._roles = ['admin', 'owner', 'user', 'operator'];
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#$get
             * @propertyOf mtda.user.$userProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($login) {
             *      console.log($login.templateUrl);
             *      //prints the current templateUrl of `mtda.login`
             *      //ex.: "mtda/login/login.tpl.html"
             *      console.log($login.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = /*@ngInject*/ function(lodash) {
                return {
                    config: this._config,
                    loginTemplateUrl: this._loginTemplateUrl,
                    registerTemplateUrl: this._registerTemplateUrl,
                    recoveryTemplateUrl: this._recoveryTemplateUrl,
                    skipIfLoggedInUrl: this._skipIfLoggedInUrl,
                    roles: this._roles,
                    getCurrent: function() {
                        // 
                        // Try to get current user
                        var user = this.current() ? this.current() : false;

                        return user;
                    },
                    current: function() {
                        return this._current;
                    },
                    set: function(data) {
                        this._current = data;
                        return data;
                    },
                    logout: function() {
                        // Logout
                        this._current.logout();

                        // Clean login data
                        this._current = null;
                    }
                }
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#config
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *     $userProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             *     $userProvider.config('registerWelcome','Olá @firstName, você entrou para a @appName');
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#templateUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.templateUrl('core/login/my-login.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.loginTemplateUrl = function(val) {
                if (val) return this._loginTemplateUrl = val;
                else return this._loginTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#recoveryTemplateUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para url do template para recuperação de senha
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.recoveryTemplateUrl('core/login/my-login-recovery.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.recoveryTemplateUrl = function(val) {
                if (val) return this._recoveryTemplateUrl = val;
                else return this._recoveryTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#registerTemplateUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter para url do template de novos cadastros
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.registerTemplateUrl('core/login/my-register.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.registerTemplateUrl = function(val) {
                if (val) return this._registerTemplateUrl = val;
                else return this._registerTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#skipIfLoggedInUrl
             * @methodOf mtda.user.$userProvider
             * @description
             * setter url for skipIfLoggedIn
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.skipIfLoggedInUrl('admin/')
             * })
             * </pre>
             * @param {string} val url to redirect user if is logged in
             **/
            this.skipIfLoggedInUrl = function(val) {
                if (val) return this._skipIfLoggedInUrl = val;
                else return this._skipIfLoggedInUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.user.$userProvider#roles
             * @methodOf mtda.user.$userProvider
             * @description
             * setter user roles
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($userProvider) {
             *      $userProvider.roles(['admin'])
             * })
             * </pre>
             * @param {array} user roles
             **/
            this.roles = function(val) {
                if (val) return this._roles = val;
                else return this._roles;
            }

            this.skipIfLoggedIn = /*@ngInject*/ function($q, $location, $auth, $user) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    $location.path($user.skipIfLoggedInUrl);
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            }

            this.skipIfLoggedInCheckout = /*@ngInject*/ function($q, $location, $auth, $user) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    $location.path('/checkout/payment-details/');
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            }

            this.loginRequired = /*@ngInject*/ function($q, $location, $auth, $localStorage) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {
                    // 
                    // Gravar url para o caso de login
                    $localStorage.redirectOnLogin = $location.url();

                    $location.path('/entrar/');
                }
                return deferred.promise;
            }

            this.loginCheckoutRequired = /*@ngInject*/ function($q, $location, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {
                    $location.path('/checkout/');
                }
                return deferred.promise;
            }

            this.editorRequired = /*@ngInject*/ function($q, $location, $user) {
                var deferred = $q.defer(),
                    user = $user.getCurrent();

                if (user && (user.model.roles.isEditor || user.model.roles.isOwner || user.model.roles.isAdmin)) {
                    deferred.resolve();
                } else {
                    $location.path('/');
                }
                return deferred.promise;
            }

            this.ownerRequired = /*@ngInject*/ function($q, $location, $user) {
                var deferred = $q.defer(),
                    user = $user.getCurrent();

                if (user && (user.model.roles.isOwner || user.model.roles.isAdmin)) {
                    deferred.resolve();
                } else {
                    $location.path('/');
                }
                return deferred.promise;
            }

            this.adminRequired = /*@ngInject*/ function($q, $location, $user) {
                var deferred = $q.defer(),
                    user = $user.getCurrent();

                if (user && user.model.roles.isAdmin) {
                    deferred.resolve();
                } else {
                    $location.path('/');
                }
                return deferred.promise;
            }
        });
})();