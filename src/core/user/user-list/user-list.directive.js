(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name app.project.directive:userList
     * @restrict EA
     * @description
     * Listagem de proejtos
     * @element div
     **/
    angular.module('mtda.user').directive('userList', /*@ngInject*/ function() {
        return {
            restrict: 'EA',
            templateUrl: 'core/user/user-list/user-list.tpl.html',
            controller: '$UserListCtrl',
            controllerAs: 'list',
            bindToController: {}
        }
    });
})();