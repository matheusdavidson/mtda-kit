(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserListCtrl', /*@ngInject*/ function($scope, $userHttp, lodash, $page) {
        var list = this,
            bookmark;

        // 
        // Set factory to get entries
        list.factory = $userHttp;

        // 
        // Table options
        list.options = {
            rowSelection: true,
        };

        // 
        // Selected items
        list.selected = [];

        // 
        // Query
        list.query = {
            order: '-createdAt',
            limit: 5,
            page: 1,
            filters: '',
            owner: true
        };

        // 
        // Get all service
        list.getAll = getAll;

        // 
        // Remove service
        list.remove = remove;

        // 
        // Filters 
        list.filters = {
            show: false,
            setShow: setShow,
            options: {
                debounce: 500
            }
        };

        // 
        // is owner
        list.isOwner = isOwner;

        // 
        // Watch for changes in filter
        $scope.$watch('list.query.filters', watchQueryFilters);

        // 
        // Get entries
        list.getAll();

        // 
        // Success callback
        function success(data) {

            // 
            // Clean selected
            list.selected = [];

            // 
            // Set entries
            list.data = data;
        }

        // 
        // Get all service
        function getAll() {
            // 
            // Set promise and call
            list.promise = list.factory.getAll(list.query).then(success);
        };

        // 
        // Remove items
        function remove(e) {

            // 
            // Get array of ids
            var ids = _.map(list.selected, '_id');

            // 
            // Confirmation
            $page.showAlertConfirm(e, 'Excluir usuário', 'Deseja realmente excluir ' + ids.length + ' usuário(s)?', 'Excluir', 'Cancelar', function() {

                // 
                // Confirmed, exclude
                list.promise = list.factory.remove(e, ids).then(getAll);
            }, function() {

                // 
                // Canceled, clean selected
                list.selected = [];
            });
        }

        // 
        // Filters - setShow
        function setShow(status) {
            list.filters.show = status;
        }

        // 
        // Watch for query filter changes
        function watchQueryFilters(newValue, oldValue) {
            if (!oldValue) {
                bookmark = list.query.page;
            }

            if (newValue !== oldValue) {
                list.query.page = 1;
            }

            if (!newValue) {
                list.query.page = bookmark;
            }

            list.getAll();
        }

        // 
        // Check if is owner
        function isOwner(role) {
            return lodash.find(role, function(o) {
                return o === 'owner';
            });
        }
    });
})();