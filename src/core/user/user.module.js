(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.user
     * @requires mtda.env
     * @requires mtda.setting
     * @requires satellizer
     **/
    angular.module('mtda.user', [
        'ui.router',
        'satellizer'
    ]);
})();