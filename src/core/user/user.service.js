(function() {
    'use strict';
    angular.module('mtda.user').service('$User', /*@ngInject*/ function($auth, $http, $page, $state, $q, api, Busy, $location, $localStorage, lodash) {

        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.login = login;
        this.service.prototype.logout = logout;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;
        this.service.prototype.setRoles = setRoles;
        this.service.prototype.password = password;

        function create(provider, noLogin, redirect) {
            if (!provider) provider = 'local';

            // Are we busy?
            if (this.busy.start('save')) return this.negativePromise();

            // Default params
            var params = {
                model: {
                    provider: provider
                }
            };

            // Extend with model
            angular.extend(params.model, this.model);

            // Create user
            return $auth.signup(params).success(onSuccess.bind(this)).error(onError.bind(this));

            function onSuccess(response) {
                // 
                // Set roles
                response.user = this.setRoles(response.user);

                // 
                // Log in user after registration?
                if (!noLogin) {
                    // 
                    // Set $auth token
                    $auth.setToken(response.token);
                }

                // 
                // Confirmation msg
                $page.toast($page.msgCreated);

                // 
                // Unset busy
                this.busy.end('save');

                return response;
            }

            function onError(response) {
                $page.toast(response && response.error ? response.error : $page.msgNotDone);
                this.busy.end('save');

                return response;
            }
        }

        function update() {
            // Are we busy?
            if (this.busy.start('save')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(api.url + 'api/users/' + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('save');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('save');

                        return response;
                    }.bind(this));
        }

        function password() {
            // Are we busy?
            if (this.busy.start('password')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(api.url + 'api/users/' + model.id + '/password', {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast('Senha alterada com sucesso!');
                        this.busy.end('password');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('password');

                        return response;
                    }.bind(this));
        }

        function login(redirect) {

            // Are we busy?
            if (this.busy.start('login')) return this.negativePromise();

            return $auth.login({
                email: this.model.email,
                password: this.model.password
            }).then(onSuccess.bind(this), onError.bind(this));

            function onSuccess(response) {
                // 
                // Set roles
                response.data.user = this.setRoles(response.data.user);

                // 
                // Redirect
                if (!redirect) {
                    // 
                    // Any previus location registered?
                    if ($localStorage.redirectOnLogin) {
                        // 
                        // Redirect user to previus location
                        $location.path($localStorage.redirectOnLogin);

                        // 
                        // Delete previus location
                        delete $localStorage.redirectOnLogin;

                        // 
                        // Redirect for custmer
                    } else if (response.data.user.roles.isUser) $location.path('/customer/');

                    // 
                    // Redirect for admin
                    else if (response.data.user.roles.isAdmin) $location.path('/accounts/');

                    // 
                    // Redirect for owners
                    else if (response.data.user.roles.isOwner || response.data.user.roles.isEditor) $location.path('/admin/');

                    // 
                    // Redirect for any other situation
                    else $location.path('/customer/');
                } else {
                    // 
                    // Custom redirect from directive
                    $location.path(redirect);
                }

                // 
                // Confirmation msg
                $page.toast($page.msgLogin);

                // 
                // Unset busy
                this.busy.end('login');

                return response;
            }

            function onError(response) {
                $page.toast('Dados incorretos!');

                // 
                // Unset busy
                this.busy.end('login');

                return response;
            }
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }

        function logout() {
            // 
            // Remove user in local storage
            delete $localStorage.user;

            // 
            // Clear self model
            this.model = null;

            // 
            // Logout from $auth
            $auth.logout();

            // 
            // Redirect to login
            $state.go('app.login');
        }

        function setRoles(user) {
            var roles = {};

            // 
            // Is admin?
            roles.isAdmin = user.role.indexOf('admin') > -1 ? true : false;

            // 
            // Is owner?
            roles.isOwner = user.role.indexOf('owner') > -1 ? true : false;

            // 
            // Is editor?
            roles.isEditor = user.role.indexOf('editor') > -1 ? true : false;

            // 
            // Is operator?
            roles.isOperator = user.role.indexOf('operator') > -1 ? true : false;

            // 
            // Is user?
            roles.isUser = user.role.indexOf('user') > -1 ? true : false;

            // 
            // Set user
            user.roles = roles;

            return user;
        }
    });
})();