(function() {
    'use strict';

    angular.module('mtda.user').controller('$RecoveryCtrl', /*@ngInject*/ function($userHttp, $location, $state) {
        var vm = this;

        // 
        // Set hash
        vm.hash = $location.hash() ? $location.hash() : false;
        vm.confirmed = false;

        // 
        // Factory
        vm.factory = $userHttp;

        // 
        // submitRecoveryLink
        vm.submitRecoveryLink = submitRecoveryLink;

        // 
        // submitRecoveryPassword
        vm.submitRecoveryPassword = submitRecoveryPassword;

        // 
        // Validate hash if provided
        if (vm.hash) confirmHash();

        // 
        // Submit recovery link
        // send recovery link to email
        function submitRecoveryLink() {
            vm.factory.recoveryLink(vm.email);
        }

        // 
        // Submit recovery password
        // change password
        function submitRecoveryPassword() {
            vm.factory.recoveryPassword(vm.hash, vm.password).then(function() {

                // 
                // Send to login
                $state.go('app.login');
            });
        }

        // 
        // Confirm hash
        function confirmHash() {
            vm.factory.confirmHash(vm.hash).then(function(data) {

                // 
                // Set confirmed
                vm.confirmed = true;

            }, function(error) {

                // 
                // Set confirmed
                vm.confirmed = false;

                // 
                // Redirect user to login
                $state.go('app.login');
            });
        }
    });
})();