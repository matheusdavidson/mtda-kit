(function() {
    'use strict';
    angular.module('mtda.user').controller('$RegisterFormCtrl', /*@ngInject*/ function($User, $location, $user, $RestoreUser, $localStorage) {
        var vm = this;

        vm.sign = {};

        vm.user = new $User.service();
        vm.user.provider = 'local';

        vm.submit = submit;

        if (vm.setRole) {
            if (!vm.user.model) vm.user.model = {};
            vm.user.model.role = vm.setRole;
        }

        function submit() {
            // Send contact to server with recaptcha
            vm.user.create('local', false, vm.redirect).then(success, fail);

            function success(response) {
                // 
                // Redirect
                if (!vm.redirect) {
                    // 
                    // Any previus location registered?
                    if ($localStorage.redirectOnLogin) {
                        // 
                        // Redirect user to previus location
                        $location.path($localStorage.redirectOnLogin);

                        // 
                        // Delete previus location
                        delete $localStorage.redirectOnLogin;

                        // 
                        // Redirect for custmer
                    } else if (response.data.user.roles.isUser) $location.path('/customer/');

                    // 
                    // Redirect for admin
                    else if (response.data.user.roles.isAdmin) $location.path('/accounts/');

                    // 
                    // Redirect for owners
                    else if (response.data.user.roles.isOwner || response.data.user.roles.isEditor) $location.path('/admin/');

                    // 
                    // Redirect for any other situation
                    else $location.path('/customer/');
                } else {
                    // 
                    // Custom redirect from directive
                    $location.path(vm.redirect);
                }

                // 
                // Retora user data
                $RestoreUser.restore(response.data.user);

                // 
                // cb?
                if (vm.cb && typeof vm.cb === 'function') vm.cb(response.data.user);

                return response.data.user;
            }

            function fail() {}
        }
    })
})();