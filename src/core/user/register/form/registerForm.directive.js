(function() {
    'use strict';
    angular.module('mtda.user').directive('registerForm', /*@ngInject*/ function() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: "core/user/register/form/registerForm.tpl.html",
            controller: '$RegisterFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                redirect: '=',
                cb: '=',
                setRole: '='
            }
        }
    })
})();