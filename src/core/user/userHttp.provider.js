(function() {
    'use strict';
    angular.module('mtda.user').provider('$userHttp',
        /**
         * @ngdoc object
         * @name mtda.user.$userHttpProvider
         **/
        /*@ngInject*/
        function $userHttpProvider() {
            /**
             * @ngdoc function
             * @name mtda.user.$userHttpProvider#$get
             * @propertyOf mtda.user.$userHttpProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($login) {
             *      console.log($login.templateUrl);
             *      //prints the current templateUrl of `mtda.login`
             *      //ex.: "mtda/login/login.tpl.html"
             *      console.log($login.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = /*@ngInject*/ function(lodash, api, Busy, $http, NegativePromise, $page) {
                var endpoint = api.url + 'api/users/',
                    busy = new Busy.service();

                return {
                    busy: busy,
                    // 
                    // Index
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    recoveryLink: recoveryLink,
                    recoveryPassword: recoveryPassword,
                    confirmHash: confirmHash
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    var getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    };

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('get')) return NegativePromise.defer();

                    var getParams = {
                        user: false
                    };

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('get');
                    }
                }

                function remove(e, ids) {
                    // Are we busy?
                    if (this.busy.start('delete')) return NegativePromise.defer();

                    return $http.post(endpoint + 'destroy', {
                        ids: ids
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('delete');
                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('delete');
                    }
                }

                /**
                 * recoveryLink
                 */
                function recoveryLink(email) {

                    // 
                    // Are we busy?
                    if (this.busy.start('recovery')) return NegativePromise.defer();

                    return $http.post(endpoint + "lost", {
                        email: email
                    }).success(onSuccess.bind(this)).error(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('recovery');
                        $page.toast(response.success);

                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('recovery');
                    }
                }

                /**
                 * recoveryPassword
                 */
                function recoveryPassword(hash, password) {

                    // 
                    // Are we busy?
                    if (this.busy.start('recoveryPassword')) return NegativePromise.defer();

                    return $http.put(endpoint + "lost/" + hash, {
                        password: password
                    }).success(onSuccess.bind(this)).error(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('recoveryPassword');
                        $page.toast(response.success);

                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('recoveryPassword');
                    }
                }

                /**
                 * confirmHash
                 */
                function confirmHash(hash) {

                    // 
                    // Are we busy?
                    if (this.busy.start('confirmHash')) return NegativePromise.defer();

                    return $http.get(endpoint + "hash/" + hash).success(onSuccess.bind(this)).error(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('confirmHash');
                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('confirmHash');
                    }
                }
            }
        });
})();