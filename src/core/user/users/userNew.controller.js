(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserNewCtrl', /*@ngInject*/ function($state, $stateParams, $User, $user) {
        var vm = this;

        // 
        // Project factory
        vm.factory = $user;

        //
        // Instantiate product
        vm.service = new $User.service({
            model: {
                account: {
                    owner: $user.current().model
                },
                role: ['user']
            }
        });

        // 
        // Current url
        vm.redirectUrl = ($state.current.name === 'app.new-user') ? 'app.users' : 'app.config.users';

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // 
            // Create entry, local provider and noLogin
            vm.service.create(false, true, false).then(success, fail);

            function success() {
                // 
                // Clean model
                vm.service.model = null;

                // 
                // Redirect listing
                $state.go(vm.redirectUrl);
            }

            function fail() {}
        }
    })
})();