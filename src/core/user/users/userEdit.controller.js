(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserEditCtrl', /*@ngInject*/ function($state, $stateParams, $User, $userHttp, $element, lodash) {
        var vm = this;

        //
        // Set edit
        vm.edit = true;

        // 
        // Project factory
        vm.factory = $userHttp;

        // 
        // Current url
        vm.redirectUrl = ($state.current.name === 'app.edit-user') ? 'app.users' : 'app.config.users';

        //
        // Get and instantiate
        vm.factory.get($state.params._id).then(function(data) {
            // 
            // Check data
            if (!data) $state.go(vm.redirectUrl);

            // 
            // Service
            vm.service = new $User.service({
                model: data
            });
        });

        //
        // Submit and save
        vm.submit = submit;

        // 
        // Multiselect de grupos
        vm.groups = [{
            label: 'Usuário',
            value: 'user'
        }, {
            label: 'Editor',
            value: 'editor'
        }];

        vm.isEditorOrUser = isEditorOrUser;

        function submit() {
            // Update entry
            vm.service.update(false, true);
        }

        function isEditorOrUser(model) {
            return !model.role.length || lodash.find(model.role, function(o) {
                console.log('o', o);
                return o === 'user' || o === 'editor';
            });
        }
    })
})();