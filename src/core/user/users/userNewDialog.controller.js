(function() {
    'use strict';
    angular.module('mtda.user').controller('$UserNewDialogCtrl', /*@ngInject*/ function($state, $stateParams, $user, $User, $mdDialog, $timeout) {
        var vm = this;

        // 
        // Focus first element
        $timeout(function() {
            var elem = angular.element('.project-type-form-dialog input:first');
            elem.focus();
        }, 100);

        // 
        // Factory
        vm.factory = $user;

        //
        // Instantiate product
        vm.service = new $User.service({
            model: {
                account: {
                    owner: vm.factory.current().model
                }
            }
        });

        vm.cancel = cancel;
        vm.save = save;

        function cancel() {
            $mdDialog.cancel();
        }

        function save() {
            // 
            // Create entry, local provider and noLogin
            vm.service.create(false, true, false).then(success, fail);

            function success(response) {
                // Clean model
                vm.service.model = null;

                // Close dialog
                $mdDialog.hide(response.data);
            }

            function fail() {}
        }
    })
})();