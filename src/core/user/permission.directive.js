(function() {
    'use strict';
    angular.module('mtda.user').directive('perm', /*@ngInject*/ function($log, Authorization, PermissionMap) {
        return {
            restrict: 'A',
            link: function($scope, $element, $attributes) {
                var only = $scope.$eval($attributes.only),
                    except = $scope.$eval($attributes.except);

                $scope.$watchGroup(['only', 'except'], function() {
                    try {
                        Authorization
                            .authorize(new PermissionMap({
                                only: only,
                                except: except
                            }), null)
                            .then(function() {
                                $scope.item.visible = true;
                                $element.removeClass('ng-hide');
                            })
                            .catch(function() {
                                $scope.item.visible = false;
                                $element.addClass('ng-hide');
                            });
                    } catch (e) {
                        $scope.item.visible = false;
                        $element.addClass('ng-hide');
                        $log.error(e.message);
                    }
                });
            }
        };
    })
})();