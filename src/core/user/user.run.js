(function() {
    'use strict';

    angular.module('mtda.user').run(runBlock);

    /** @ngInject */
    function runBlock($log, PermissionStore, RoleStore, $user, $User, $RestoreUser, $auth, lodash) {
        // 
        // Restore user from local storage
        $RestoreUser.restore();

        // 
        // Permissions
        PermissionStore.definePermission('user', function(stateParams) {
            // 
            // Do we have current
            return !!$auth.isAuthenticated();
        });
        PermissionStore.definePermission('admin', function(stateParams) {
            // 
            // We need current model
            if (!lodash.hasIn($user, '_current') || !lodash.hasIn($user._current, 'model') || !lodash.hasIn($user._current.model, 'role')) return false;

            // 
            // Check for admin in user role array
            return lodash.some($user._current.model.role, lodash.partial(lodash.eq, 'admin'));
        });
    }
})();