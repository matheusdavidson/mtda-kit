(function() {
    'use strict';

    angular.module('mtda.user').service('$RestoreUser',

        /*@ngInject*/
        function($auth, $q, $user, $User, $localStorage, lodash) {
            this.restore = function(data) {
                // Create promise
                var defer = $q.defer();

                // We must have authentication
                if (!$auth.isAuthenticated()) defer.reject(false)
                    // Check for data argument
                else if (data) setUser(data)
                    // We found one, who is this? get information
                else restoreFromStorage();

                // Return promise
                return defer.promise;

                function restoreFromStorage() {
                    setUser($localStorage.user, true);
                }
            }

            function setUser(data, restore) {
                // 
                // Set current account id if is not provided
                if (!lodash.hasIn(data.owner)) {
                    // 
                    // Define owner according to role
                    data.owner = (lodash.some(data.role, lodash.partial(lodash.eq, 'admin'))) ? false : data.account.owner;
                }

                // 
                // New User service instance in user factory _current method
                $user.set(new $User.service({
                    model: data
                }));
                
                if (restore) return;

                // 
                // Double check if we have user stored, if so, delete
                if (lodash.hasIn($localStorage, 'user'))
                    delete $localStorage.user;

                // 
                // Add to storage for reload
                $localStorage.user = data;
            }
        }
    );
})();