(function() {
    'use strict';
    angular.module('mtda.user').controller('$LoginFormCtrl', /*@ngInject*/ function($auth, $page, $user, $User, setting, api, $RestoreUser) {
        var vm = this;
        vm.user = new $User.service();

        vm.submit = submit;

        function submit() {
            // Send contact to server with recaptcha
            vm.user.login(vm.redirect).then(success, fail);

            function success(response) {
                // 
                // Retora user data
                $RestoreUser.restore(response.data.user);

                // 
                // cb?
                if (vm.cb && typeof vm.cb === 'function') vm.cb(response.data.user);
            }

            function fail() {}
        }
    })
})();