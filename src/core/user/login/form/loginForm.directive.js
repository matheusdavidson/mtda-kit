(function() {
    'use strict';
    angular.module('mtda.user').directive('loginForm', /*@ngInject*/ function() {
        return {
            scope: {},
            restrict: 'EA',
            templateUrl: "core/user/login/form/loginForm.tpl.html",
            controller: '$LoginFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                redirect: '=',
                cb: '='
            }
        }
    })
})();