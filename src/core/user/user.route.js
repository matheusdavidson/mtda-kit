(function() {
    'use strict';
    angular.module('mtda.user').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $pageProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.login', {
            url: '/entrar/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.loginTemplateUrl()
                    },
                    controller: '$LoginCtrl as vm'
                }
            },
            resolve: {
                skipIfLoggedIn: $userProvider.skipIfLoggedIn
            }
        }).state('app.logout', {
            url: '/sair/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    controller: '$LogoutCtrl as vm'
                }
            }
        }).state('app.register', {
            url: '/registrar/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.registerTemplateUrl()
                    },
                    controller: '$RegisterCtrl as vm'
                },
                resolve: {
                    skipIfLoggedIn: $userProvider.skipIfLoggedIn
                }
            }
        }).state('app.recovery', {
            url: '/recuperar/',
            toolbar: 'login',
            tabs: 'login',
            footer: 'login',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $userProvider.recoveryTemplateUrl()
                    },
                    controller: '$RecoveryCtrl as vm'
                }
            },
            resolve: {
                authed: /*@ngInject*/ function($auth, $window, $user) {
                    if ($auth.isAuthenticated()) {
                        $window.location = $user.config.auth.loginSuccessRedirect //here we use $window to fix issue related with $location.hash (#) in url
                    }
                }
            }
        }).state('app.profile', {
            url: '/profile/',
            views: {
                'content': {
                    templateUrl: 'core/user/profile.tpl.html',
                    controller: '$ControllerCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        })

        // 
        // Users
        .state('app.users', {
            url: $pageProvider.protectedSlug + 'users/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'users',
            views: {
                'content': {
                    templateUrl: 'core/user/users/users.tpl.html',
                    controller: '$UsersCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.new-user', {
            url: $pageProvider.protectedSlug + 'new-user/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'users',
            views: {
                'content': {
                    templateUrl: 'core/user/users/userForm.tpl.html',
                    controller: '$UserNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.edit-user', {
            url: $pageProvider.protectedSlug + 'edit-user/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'users',
            views: {
                'content': {
                    templateUrl: 'core/user/users/userForm.tpl.html',
                    controller: '$UserEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        });
    })
})();