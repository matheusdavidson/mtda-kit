(function() {
    'use strict';
    angular.module('mtda.list').directive('list', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListCtrl',
            controllerAs: 'vm',
            bindToController: {
                params: '=',
                instance: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/list/list.tpl.html";
            }
        };
    });
})();