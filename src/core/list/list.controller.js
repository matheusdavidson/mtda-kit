(function() {
    'use strict';
    angular.module('mtda.list').controller('ListCtrl', /*@ngInject*/ function($scope, $List) {
        var vm = this;

        vm.service = vm.instance ? vm.instance : new $List.service(vm.params);

        // Watch for changes in the filter
        $scope.$watch('vm.service.filters', filterWatch, true);

        function filterWatch(nv, ov) {
            if (nv != ov) {
                vm.service.filterUpdated();
            }
        }
    })
})();