(function() {
    'use strict';
    angular.module('mtda.list').directive('listLoadMore', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListLoadMoreCtrl',
            controllerAs: 'vm',
            bindToController: {
                service: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/load-more/listLoadMore.tpl.html';
            }
        };
    });
})();