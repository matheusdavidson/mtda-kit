(function() {
    'use strict';
    angular.module('mtda.list').controller('ListContentCtrl', /*@ngInject*/ function(setting, api) {
        var vm = this;

        // Get entries
        vm.service.filterUpdated(true);
        vm.uploadFolder = api.url + 'images/uploads/';
    })
})();