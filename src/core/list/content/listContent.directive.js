(function() {
    'use strict';
    angular.module('mtda.list').directive('listContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                service: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/content/listContent.tpl.html';
            }
        };
    });
})();