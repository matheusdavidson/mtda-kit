(function() {
    'use strict';
    angular.module('mtda.list').directive('listSearchBox', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: 'ListSearchBoxCtrl',
            controllerAs: 'vm',
            bindToController: {
                service: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/list/search-box/listSearchBox.tpl.html';
            }
        };
    });
})();