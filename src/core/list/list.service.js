(function() {
    'use strict';
    angular.module('mtda.list').service('$List', /*@ngInject*/ function($http, $page, $state, $stateParams, $q, $location, api, Busy, lodash) {
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.entries = [];

            // Define if this is starting
            this.busy.start('starting');

            // Query params
            this.page = $stateParams.page && typeof $stateParams.page === 'string' ? Number($stateParams.page) : 1;
            this.limit = 12;
            this.firstQuery = true;
            this.filters = {
                term: '',
                sort: '-created'
            };

            // Route
            this.disableTransition = true;
            this.route = false;

            // Config
            this.loadMoreBtn = false;
            this.total = 0;
            this.totalDisplay = 0;
            this.searchPlaceholder = 'Pesquisar por título, descrição, etc..';
            this.showAvatar = false;
            this.primaryAction = false;
            this.primaryIcon = false;
            this.infinite = true;

            // Methods
            this.source = false;
            this.get = get;

            // Secondary action
            this.secondaryActionType = 'delete';
            this.secondaryActionSource = false;

            // Main actions
            this.addAction = false;

            // Set sort
            $stateParams.sort = this.filters.sort;

            // Set initial $stateParams on the filters
            angular.extend(this.filters, lodash.pick($stateParams, ['sort', 'term']));

            //
            // Extend filters first
            // angular.extend does not make deep extend
            //
            params.filters = angular.extend(this.filters, params.filters ? params.filters : {});

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);
        }

        this.service.prototype.search = search;
        this.service.prototype.updateTotals = updateTotals;
        this.service.prototype.updateLoadMoreBtn = updateLoadMoreBtn;
        this.service.prototype.filterUpdated = filterUpdated;
        this.service.prototype.updateQueryParams = updateQueryParams;
        this.service.prototype.entriesUpdated = entriesUpdated;
        this.service.prototype.resetList = resetList;
        this.service.prototype.goToNextPage = goToNextPage;
        this.service.prototype.goToPrevPage = goToPrevPage;

        this.service.prototype.get = get;
        this.service.prototype.getConfig = getConfig;
        this.service.prototype.getSuccess = getSuccess;

        this.service.prototype.secondaryAction = secondaryAction;

        this.service.prototype.negativePromise = negativePromise;

        function search() {
            // Reset main props
            this.resetList();

            // Update query params, silent redirect(no refresh)
            $state.go(this.route, this.updateQueryParams());
        }

        function updateTotals(total) {
            this.total = total;
            this.totalDisplay = this.entries.length;

            // var plus = this.infinite ? 0 : 1;

            // First page
            this.showingStart = this.page > 1 ? this.limit * (this.page - 1) + 1 : this.totalDisplay > 0 ? 1 : 0;
            this.showingEnd = this.page > 1 && !this.infinite ? (this.limit * (this.page - 1)) + this.totalDisplay : this.totalDisplay;

        }

        function updateLoadMoreBtn() {
            var result = (this.total > this.showingEnd);

            this.loadMoreBtn = result;
            return result;
        }

        function filterUpdated(noReset) {
            // Start loading
            if (this.busy.start('loading')) return false;

            // Reset main props
            if (!noReset) this.resetList();

            // Update query params
            var updatedQueryParams = this.updateQueryParams();

            // Silent redirect(no refresh) / required config on state: 'reloadOnSearch : false'
            if (!this.disableTransition)
                $location.search(updatedQueryParams);
            // $state.go(this.route, updatedQueryParams, {
            //     notify: false
            // });

            // Handle filter update
            this.get();
        }

        function updateQueryParams() {
            var obj = {
                page: this.page
            };

            // Add current filters
            angular.extend(obj, this.filters, {
                'category': undefined,
                'child': undefined
            }); // @todo: set this as a property

            return obj;
        }

        function entriesUpdated(data) {
            // Update totals
            this.updateTotals(data.total);

            // Update page
            this.page = data.nextPage ? data.nextPage : 1;

            // Update next button
            this.updateLoadMoreBtn();
        }

        // Verificar possibilidade de ter essas props aqui e manipular a lista por aqui mesmo,
        // no controller principal, teremos essas props tbm mas apenas para fins informativos(exibir para o usuário)
        function resetList() {
            // Reset props
            this.entries = [];
            this.total = 0;
            this.totalDisplay = 0;
            this.page = 1;
            this.loadMoreBtn = false;
        }

        function goToNextPage() {
            // Start loading
            if (this.busy.start('loadingMore')) return false;

            if (!this.disableTransition)
                $location.search(this.updateQueryParams());
            // $state.go('.', this.updateQueryParams(), {
            //     notify: false
            // });

            // Handle filter update
            this.get();
        }

        function goToPrevPage() {
            // Start loading
            if (this.busy.start('loadingMore')) return false;

            // Decrease page
            this.page = this.page > 2 ? this.page - 2 : 1;

            if (!this.disableTransition)
                $location.search(this.updateQueryParams());
            // $state.go('.', this.updateQueryParams(), {
            //     notify: false
            // });

            // Handle filter update
            this.get();
        }

        function get() {
            // Unset starting
            this.busy.end('starting');

            // Get from the source(http) and return promise
            return this.source(this.getConfig()).then(this.getSuccess.bind(this));
        }

        function getConfig() {
            // Transformar filtros em JSON
            var filters = this.filters ? angular.copy(this.filters) : false;

            return {
                user: false,
                firstQuery: this.firstQuery,
                page: this.page,
                limit: this.limit,
                filters: filters
            }
        }

        function getSuccess(data) {
            // Push new result
            this.entries = this.infinite ? this.entries.concat(data.entries) : data.entries;

            // Update totals and next button
            this.entriesUpdated(data);

            // Set to false after first time
            if (this.firstQuery) this.firstQuery = false;

            this.busy.end('loading');
            this.busy.end('loadingMore');

            // Return event
            return data;
        }

        function secondaryAction(e, item, index) {
            return this.secondaryActionSource(e, item).then(function() {
                if (this.secondaryActionType === 'delete') {
                    // Delete entry
                    this.entries.splice(index, 1);

                    // Update totals
                    this.total = this.total - 1;
                    this.totalDisplay = this.entries.length;

                    // If total displayed is 0 try to reload the query
                    if (!this.totalDisplay) this.filterUpdated();
                }
            }.bind(this), function() {}.bind(this))
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();