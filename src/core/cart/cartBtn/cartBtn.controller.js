(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartBtnCtrl', /*@ngInject*/ function($log, $mdSidenav, $Cart, $cart, lodash, $state) {
        var vm = this;

        vm.open = open;
        vm.show = show;

        vm.itemCount = function() {
            return $cart.entries.length;
        }

        function open() {
            $mdSidenav('cart').open();
        }

        function show() {
            return !($state.current.toolbar === 'admin');
        }
    });
})();