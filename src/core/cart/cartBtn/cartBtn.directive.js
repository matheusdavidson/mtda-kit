(function() {
    'use strict';
    angular.module('mtda.cart').directive('cartBtn', /*@ngInject*/ function directive() {
        return {
            scope: true,
            controller: '$CartBtnCtrl',
            controllerAs: 'vm'
        };
    });
})();