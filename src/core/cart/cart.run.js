(function() {
    'use strict';

    angular.module('mtda.cart').run(runBlock);

    /** @ngInject */
    function runBlock($log, $localStorage, $cart, lodash) {
        // 
        // Se cart, restore from localStorage or start a new one
        $cart.entries = $localStorage.cart = (lodash.hasIn($localStorage, 'cart')) ? $localStorage.cart : [];

        // 
        // Update subtotal
        $cart.updateSubtotal();
    }
})();