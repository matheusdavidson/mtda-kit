(function() {
    'use strict';
    angular.module('mtda.cart').config( /*@ngInject*/ function($stateProvider, $cartProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.cart', {
            url: '/cart/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $cartProvider.cartTemplateUrl;
                    },
                    controller: '$CartCtrl as vm'
                }
            }
        });
    })
})();