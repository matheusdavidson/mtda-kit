(function() {
    'use strict';
    angular.module('mtda.cart').provider('$cart', $cartProvider);
    /**
     * @ngdoc object
     * @name mtda.cart.$cartProvider
     **/
    /*@ngInject*/
    function $cartProvider() {

        /**
         * @ngdoc object
         * @name mtda.cart.$cartProvider#entries
         * @propertyOf mtda.cart.$cartProvider
         * @description
         * store cart entries
         **/
        this.entries = [];

        /**
         * @ngdoc object
         * @name mtda.cart.$cartProvider#subtotal
         * @propertyOf mtda.cart.$cartProvider
         * @description
         * store cart subtotal
         **/
        this.subtotal = 0;

        /**
         * @ngdoc object
         * @name mtda.cart.$cartProvider#cartTemplateUrl
         * @propertyOf mtda.cart.$cartProvider
         * @description
         * cart template url
         **/
        this.cartTemplateUrl = 'core/cart/cart.tpl.html';

        this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, lodash, $localStorage) {
            var endpoint = api.url + 'api/carts/',
                busy = new Busy.service();

            return {
                entries: this.entries,
                subtotal: this.subtotal,
                remove: remove,
                updateSubtotal: updateSubtotal,
                clear: clear,
                hasEntries: hasEntries
            }

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#remove
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * remove entry
             **/
            function remove(index) {
                // 
                // Remove entry from local storage
                this.entries.splice(index, 1);

                // 
                // Notify user
                $page.toast($page.msgCartDeleted, 5000, 'top right');

                // 
                // Update subtotal
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#clear
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * remove all entries
             **/
            function clear() {
                // 
                // Remove all entries
                _.times(this.entries.length, function() {
                    // 
                    // Remove first element
                    this.entries.shift();
                }.bind(this));

                // 
                // Update totals
                this.updateSubtotal();
            };

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#updateSubtotal
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * update subtotal
             **/
            function updateSubtotal() {
                // 
                // Calculate new subtotal
                var subtotal = _.reduce(this.entries, function(sum, obj) {
                    return sum + (obj.price * obj.qty);
                }, 0);

                // 
                // Set new subtotal
                this.subtotal = subtotal;
            };

            /**
             * @ngdoc object
             * @name mtda.cart.$cartProvider#hasEntries
             * @propertyOf mtda.cart.$cartProvider
             * @description
             * check if cart has entries
             **/
            function hasEntries() {
                return (this.entries.length);
            }
        };
    }
})();