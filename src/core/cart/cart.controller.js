(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartCtrl', /*@ngInject*/ function($log, $scope, $Cart, $cart, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $cart;
        vm.entries = vm.factory.entries;
    });
})();