(function() {
    'use strict';
    angular.module('mtda.cart').service('$Cart', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug, $localStorage, lodash, $cart, $mdSidenav, $timeout, $filter) {

        var url = api.url + 'api/carts/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.formStatus = formStatus;

        function create(ev) {
            // Start promise
            var defer = $q.defer();

            // Are we busy?
            if (this.busy.start('create')) defer.reject(response);

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;
            
            // 
            // Add entry to local storage
            $cart.entries.push(model);

            // 
            // Stop busy
            this.busy.end('create');

            // 
            // Notify user
            $page.showAlertConfirm(ev, $page.msgConfirmation, $page.msgCartCreated, $filter('translate')('KEEP_SHOPPING'), $filter('translate')('CHECKOUT'), function() {
                // 
                // Open cart
                $mdSidenav('cart').open();
            }, function() {
                // 
                // Go to checkout
                $state.go('app.checkout.identification');
            });

            // 
            // return promise
            defer.resolve();

            // Return promise
            return defer.promise;
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }
    });
})();