(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.cart
     **/
    angular.module('mtda.cart', [
        'ui.router'
    ]);
})();