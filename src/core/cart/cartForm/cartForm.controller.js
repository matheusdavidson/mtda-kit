(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartFormCtrl', /*@ngInject*/ function($log, $scope, $Cart, $cart, lodash) {
        var vm = this;

        //
        // New cart
        setCart();

        vm.variantsCompare = vm.product.variants.attrs;
        vm.submit = submit;

        //
        // Update variants quantities when attrs change
        vm.variantChange = variantChange;

        // 
        // Load directive and options
        bootstrap();

        //
        // load directive
        function bootstrap() {
            //
            // Set attrs options for the form
            angular.forEach(vm.product.variants.attrs, function(v, k) {
                // Get attr options
                var obj = v.options ? v.options : false;
                // Place in controller
                vm[v.name] = obj;
            });
            // 
            // Set product cnt
            vm.qtyAvailable = vm.product.variants.cnt;
        }
        // 
        // We need to adjust data for color
        function colorValue(value) {
            // Get color obj and set in the model
            vm.cart.model.attrs.colors = _.find(vm.colors, ['value', value]);
        }
        // 
        // We need to adjust variants quantities everytime a variant changes
        function variantChange(attrName, value, index) {
            if (attrName === 'colors') colorValue(value);

            var options = {},
                qty = 0;

            // $log.debug('variantChange', 'start');
            // $log.debug('attrName', attrName);
            // $log.debug('value', value);
            // $log.debug('index', index);
            // $log.debug('vm.variants', vm.variants);

            // 
            // Iterate over variants to make options
            _.forEach(vm.variants, function(variant, variantKey) {

                // $log.debug('variant', variant);

                // 
                // Filter variants by selected namespace
                if (!variant.name.match(attrName + ':' + value)) return;

                var skipVariant = false;

                // 
                // Get options for all attrs
                _.forEach(variant.attrs, function(attr, attrKey) {

                    // 
                    // Doublecheck variants before current
                    if (attrKey < index) {
                        var otherAttrName = vm.product.variants.attrs[attrKey].name,
                            otherAttrValue = lodash.hasIn(vm.cart.model.attrs[otherAttrName], 'value') ? vm.cart.model.attrs[otherAttrName].value : vm.cart.model.attrs[otherAttrName];

                        // $log.debug('vm.cart.model.attrs', vm.cart.model.attrs);
                        // $log.debug('variant.name', variant.name);
                        // $log.debug('otherAttrName', otherAttrName);
                        // $log.debug('otherAttrValue', otherAttrValue);

                        if (!variant.name.match(otherAttrName + ':' + otherAttrValue)) {
                            // $log.debug('skipVariant', true);

                            skipVariant = true;
                            return;
                        }
                    }

                    //
                    // attrKey must be higher then current index
                    if (attrKey <= index) return;

                    // 
                    // Create options array for this type if necessary
                    if (!_.hasIn(options, attr.name)) options[attr.name] = [];

                    // 
                    // Push option
                    options[attr.name].push(attr.value);
                });

                // 
                // Valid loop?
                if (!skipVariant) {
                    // 
                    // Increment to qty
                    qty = qty + variant.cnt;
                }
            });

            // 
            // Adjust options
            _.forEach(vm.product.variants.attrs, function(attr, attrKey) {
                //
                // attrKey must be higher then current index
                if (attrKey <= index) return;

                // $log.debug('vm.cart.model.attrs[attr.name]', vm.cart.model.attrs[attr.name]);

                // Update options in the form according to selection
                updateOptions(attr.name, options);

                // If is colors, we need to change colorValue as well
                if (attr.name === 'colors') updateOptions('colorValue', options);
            });

            // 
            // Set qty
            vm.qtyAvailable = qty;

            // $log.debug('variantChange', 'end');
        }

        function updateOptions(name, options) {
            // 
            // Reset attr model
            vm.cart.model.attrs[name] = null;

            // 
            // Empty options for attr not found
            if (!_.hasIn(options, name)) {
                _.forEach(vm[name], function(o) {
                    o.disabled = true;
                });

                return;
            }

            // 
            // Disable options not found
            _.forEach(vm[name], function(o) {
                o.disabled = (_.includes(options[name], o.value)) ? false : true;
            });
        }

        function submit(ev) {
            vm.cart.create(ev).then(onSuccess, onError);

            function onSuccess() {
                // 
                // Update subtotal
                $cart.updateSubtotal();

                // 
                // Reset the form with a new cart
                setCart();

                // 
                // Reset validation
                vm.form.$setPristine();
            }

            function onError() {}
        }

        function setCart() {
            vm.cart = new $Cart.service({
                model: {
                    product: vm.product,
                    price: vm.product.priceOffer ? vm.product.priceOffer : vm.product.price
                }
            });
        }
    });
})();