(function() {
    'use strict';
    angular.module('mtda.cart').directive('cartForm', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$CartFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                product: '=',
                variants: '=',
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/cart/cartForm/cartForm.tpl.html";
            }
        };
    });
})();