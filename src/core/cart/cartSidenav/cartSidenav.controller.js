(function() {
    'use strict';

    angular.module('mtda.cart').controller('$CartSidenavCtrl', controller);

    /*@ngInject*/
    function controller($scope, $cart, lodash) {
        var vm = this;

        // 
        // Set factory to get cart entries
        vm.factory = $cart;
        vm.entries = vm.factory.entries;
    }
})();