(function() {
    'use strict';

    angular.module('mtda.cart').directive('cartSidenav', cartSidenav);

    /** @ngInject */
    function cartSidenav() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/cart/cartSidenav/cartSidenav.tpl.html';
            },
            controller: '$CartSidenavCtrl',
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

})();