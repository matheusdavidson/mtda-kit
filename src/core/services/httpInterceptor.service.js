(function() {
    'use strict';
    angular.module('mtda.services').factory('HttpInterceptor', /*@ngInject*/ function($q, $user, $localStorage, $location) {
        return {
            // optional method
            'request': function(config) {
                // do something on success
                return config;
            },
            // optional method
            'requestError': function(rejection) {
                // do something on error
                //if (canRecover(rejection)) {
                //return responseOrNewPromise
                //}
                return $q.reject(rejection);
            },
            // optional method
            'response': function(response) {
                // do something on success
                return response;
            },
            // optional method
            'responseError': function(rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    // 
                    // Gravar url para o caso de login
                    $localStorage.redirectOnLogin = $location.url();

                    // 
                    // Try to logout user
                    $user.logout();
                }
                // do something on error
                //if (canRecover(rejection)) {
                //return responseOrNewPromise
                //}
                return $q.reject(rejection);
            }
        }
    });
})();