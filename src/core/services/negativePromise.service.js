(function() {
    'use strict';
    angular.module('mtda.services').service('NegativePromise', /*@ngInject*/ function($q) {

        this.defer = defer;

        function defer(response) {

            if(!response) response = false;

            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(response);

            // Return promise
            return defer.promise;
        }
    });
})();