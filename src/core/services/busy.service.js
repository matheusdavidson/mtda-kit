(function() {
    'use strict';
    angular.module('mtda.services').service('Busy', /*@ngInject*/ function($page) {

        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this._status = {};

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

            return this;
        }

        this.service.prototype.status = status;
        this.service.prototype.start = start;
        this.service.prototype.end = end;

        function status(method, value) {
            if (value || value === false) return this._status[method] = value;
            else return this._status[method];
        }

        function start(method, dontChangePage) {
            //
            // Are we busy? return true so consuming code can stop
            // 
            if (this.status(method)) return true;

            //
            // We're not busy, make it
            // 
            this.status(method, true);

            //
            // Enable page load?
            // 
            if (!dontChangePage) $page.load.init();

            //
            // Return false so consuming code can continue
            // 
            return false;
        }

        function end(method, dontChangePage) {
            // Disable status
            this.status(method, false);

            // Disable page loading?
            if (!dontChangePage) $page.load.done();
        }
    });
})();