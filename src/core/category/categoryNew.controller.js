(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryNewCtrl', /*@ngInject*/ function($state, $stateParams, $Category, $category) {
        var vm = this;

        vm.title = 'Nova';
        vm.subtitle = 'adicionar uma nova';

        vm.category = new $Category.service();

        vm.submit = submit;

        // Autocomplete
        vm.querySearch = $category.querySearch.bind($category);

        function submit() {

            // Send contact to server with recaptcha
            vm.category.create().then(success, fail);

            function success() {
                // Clean model
                vm.category.model = null;

                // Redirect listing
                $state.go('app.categories');
            }

            function fail() {}
        }
    })
})();