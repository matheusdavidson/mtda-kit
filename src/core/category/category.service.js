(function() {
    'use strict';
    angular.module('mtda.category').service('$Category', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug) {

        var url = api.url + 'api/categories/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;
        this.service.prototype.slugifyUrl = slugifyUrl;

        function create() {
            // Are we busy?
            if (this.busy.start('categoryCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgCreated);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this));
        }

        function update() {
            // Are we busy?
            if (this.busy.start('categoryCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(url + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('categoryCreate');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }

        function slugifyUrl(value) {
            this.model.slug = Slug.slugify(value);
        }
    });
})();