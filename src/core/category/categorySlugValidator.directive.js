(function() {
    'use strict';
    angular.module('mtda.category').directive('categorySlugValidator', /*@ngInject*/ function($category) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {

                scope.$watch(attrs.dependentModel, function (newVal, oldVal, scope) {
                    ngModel.$validate();
                });

                ngModel.$asyncValidators.slug = function(modelValue, viewValue) {
                    return $category.slugValidation(viewValue, scope.vm.category.model.id, scope.vm.category.model.parent);
                };
            }
        };
    });
})();