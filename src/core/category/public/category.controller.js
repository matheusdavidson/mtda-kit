(function() {
    'use strict';
    angular.module('mtda.category').controller('$PublicCategoryCtrl', /*@ngInject*/ function($List, $state, $location, $category, $product, $filter) {
        var vm = this;

        vm.category = $category;
        vm.changeCategory = changeCategory;
        vm.breadcrumbs = {};

        // Change breadcrumbs
        changeBreadcrumbs();

        if (!$state.params.category) {
            // $state.go('app.home');
        }

        //
        // Get all categories for menu
        //
        vm.category.getMenu($state).then(function(data) {
            vm.categoryTitle = data.title ? data.title : $filter('translate')('ALL_PRODUCTS');

            // List
            vm.params = {
                infinite: false,
                source: $product.getList,
                categoryMenu: data.entries,
                disableTransition: true,
                filters: {
                    sort: '-price',
                    category: $state.params.category
                },
                sortOptions: $product.sort
            };

            vm.service = new $List.service(vm.params);
        }).catch(function() {
            $state.go('app.home');
        });

        function primaryAction(item) {
            $state.go('app.category-edit', item);
        }

        function changeCategory(category, child, categoryTitle, childTitle) {
            // Create new title
            var title = categoryTitle;
            if (childTitle) title = childTitle + ' - ' + title;

            // Create new url
            var url = category;
            if (child) url += '/' + child;

            // Change the title
            vm.categoryTitle = title;

            // Create filter obj with new categories
            var filters = {
                category: category,
                child: child
            };

            // Change breadcrumbs
            changeBreadcrumbs(filters);

            $state.go('app.publicCategories', filters, {
                notify: false
            });

            // Reset the list
            vm.service.resetList();

            // Change the filter to trigger product entries reload
            angular.extend(vm.service.filters, filters);
        }

        function changeBreadcrumbs(filters) {
            var params = filters ? filters : $state.params;

            // Change breadcrumbs
            vm.breadcrumbs = {
                category: params.category,
                child: params.child
            }

            if (!params.category)
                vm.breadcrumbs['title'] = $filter('translate')('ALL_PRODUCTS');
        }
    })
})();