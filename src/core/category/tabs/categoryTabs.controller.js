(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryTabsCtrl', /*@ngInject*/ function($category) {
        var vm = this;

        getCategories();

        function getCategories() {
            vm.factory = $category;
            vm.factory.getAll().then(function(data) {
                vm.entries = data.entries;
            });
        }
    })
})();