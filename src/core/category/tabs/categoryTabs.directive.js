(function() {
    'use strict';
    angular.module('mtda.category').directive('categoryTabs', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$CategoryTabsCtrl',
            controllerAs: 'vm',
            bindToController: {},
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/category/tabs/categoryTabs.tpl.html";
            }
        };
    });
})();