(function() {
    'use strict';
    angular.module('mtda.category').directive('categoryItem', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: 'CategoryItemDCtrl',
            controllerAs: 'vm',
            bindToController: {
                entries: '=',
                category: '=',
                child: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/category/item/item.tpl.html";
            }
        };
    });
})();