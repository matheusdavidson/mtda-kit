(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.admin
     * @requires mtda.env
     * @requires mtda.setting
     * @requires satellizer
     **/
    angular.module('mtda.category', [
        'ui.router',
        'slugifier'
    ]);
})();