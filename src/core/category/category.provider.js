(function() {
    'use strict';
    angular.module('mtda.category').provider('$category',
        /**
         * @ngdoc object
         * @name mtda.banner.$categoryProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $categoryProvider() {
            /**
             * @ngdoc object
             * @name mtda.banner.$categoryProvider#_config
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.banner.$categoryProvider#_routePath
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc object
             * @name mtda.banner.$categoryProvider#publicCategoriesTemplateUrl
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * Url for public categories template
             **/
            this.publicCategoriesTemplateUrl = 'core/category/public/categories.tpl.html';

            /**
             * @ngdoc function
             * @name mtda.banner.$categoryProvider#$get
             * @propertyOf mtda.banner.$categoryProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, $state, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/categories/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this.config,
                    routePath: this.routePath,
                    // Factory
                    getAll: getAll,
                    get: get,
                    getMenu: getMenu,
                    remove: remove,
                    querySearch: querySearch,
                    slugValidation: slugValidation,
                    busy: busy
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (busy.start('categoryGetAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('categoryGetAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('categoryGetAll');
                    }
                }

                function getMenu(state) {
                    // Are we busy?
                    if (busy.start('getMenu')) return NegativePromise.defer();

                    return $http.get(endpoint + 'menu', {
                        params: {
                            category: state.params.category,
                            child: state.params.child
                        }
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('getMenu');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('getMenu');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (busy.start('categoryGet')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('categoryGet');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('categoryGet');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (busy.start('categoryDelete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('categoryDelete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        busy.end('categoryDelete');
                    }
                }

                function querySearch(query) {
                    if (!query) return false;

                    var deferred = $q.defer();
                    this.getAll({
                        filters: {
                            term: query,
                            owner: true
                        }
                    }).then(function(response) {
                        deferred.resolve(response.entries);
                    });

                    return deferred.promise;
                }

                function slugValidation(viewValue, id, parent) {
                    return $http.post(endpoint + 'slugValidation', {
                        slug: viewValue,
                        id: id,
                        parent: parent
                    }).then(
                        function(response) {
                            if (!response.data.valid) {
                                return $q.reject(response.data.errorMessage);
                            }

                            return true;
                        }
                    );
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$categoryProvider#config
             * @methodOf mtda.banner.$categoryProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($categoryProvider) {
             *     $categoryProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$categoryProvider#logo
             * @methodOf mtda.banner.$categoryProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($categoryProvider) {
             *     $categoryProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        }
    )
})();