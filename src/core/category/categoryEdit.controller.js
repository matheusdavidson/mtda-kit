(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryEditCtrl', /*@ngInject*/ function($state, $stateParams, $Category, $category) {
        var vm = this;

        vm.title = 'Editar';
        vm.subtitle = 'editar a';

        // Get category by id
        $category.get($state.params._id).then(function(data) {
            if (!data) $state.go('app.categories');

            vm.category = new $Category.service({
                model: data
            });
        }).catch(function() {
            $state.go('app.categories');
        });

        vm.submit = submit;

        // Autocomplete
        vm.querySearch = $category.querySearch.bind($category);

        function submit() {

            // Send contact to server with recaptcha
            vm.category.update().then(success, fail);

            function success() {}

            function fail() {}
        }
    })
})();