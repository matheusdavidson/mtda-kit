(function() {
    'use strict';
    angular.module('mtda.category').controller('$CategoryCtrl', /*@ngInject*/ function($List, $category, $state) {
        var vm = this;

        // List
        vm.params = {
            source: $category.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $category.remove,
            addAction: 'app.category-new',
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            disableTransition: true,
            filters: {
                owner: true
            }
        };

        if ($state.params.term) vm.params.filters.term = $state.params.term;

        function primaryAction(item) {
            $state.go('app.category-edit', item, {
                inherit: false
            });
        }
    })
})();