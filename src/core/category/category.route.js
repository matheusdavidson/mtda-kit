(function() {
    'use strict';
    angular.module('mtda.category').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $categoryProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.categories', {
            url: $categoryProvider.routePath() + 'categories/?term',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'category',
            views: {
                'content': {
                    templateUrl: 'core/category/categories.tpl.html',
                    controller: '$CategoryCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.category-new', {
            url: $categoryProvider.routePath() + 'categories/new/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'category',
            views: {
                'content': {
                    templateUrl: 'core/category/categoryNew.tpl.html',
                    controller: '$CategoryNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.category-edit', {
            url: $categoryProvider.routePath() + 'categories/edit/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'category',
            views: {
                'content': {
                    templateUrl: 'core/category/categoryNew.tpl.html',
                    controller: '$CategoryEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.publicCategoriesEmpty', {
            url: '/categories/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $categoryProvider.publicCategoriesTemplateUrl;
                    },
                    controller: '$PublicCategoryCtrl as vm'
                }
            }
        }).state('app.publicCategories', {
            url: '/categories/:category/:child?term?page?sort',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $categoryProvider.publicCategoriesTemplateUrl;
                    },
                    controller: '$PublicCategoryCtrl as vm'
                }
            }
        });
    })
})();