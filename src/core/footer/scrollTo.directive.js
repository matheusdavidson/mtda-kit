(function() {
    'use strict';
    angular.module('mtda.app').directive('scrollTo', /*@ngInject*/ function($location, $anchorScroll, $timeout, $state) {
        return function(scope, element, attrs) {

            element.bind('click', function(event) {
                var location = attrs.scrollTo;
                var currentState = $state.current.name;
                var state = attrs.scrollState ? attrs.scrollState : currentState;

                // Check if is the same route
                if (state == currentState) {
                    event.stopPropagation();
                    var off = scope.$on('$locationChangeStart', function(ev) {
                        off();
                        ev.preventDefault();
                    });

                    scroll(location);
                } else {
                    $state.go(state);

                    scope.$on('$stateChangeSuccess', function() {
                        $timeout(function() {
                            event.stopPropagation();
                            var off = scope.$on('$locationChangeStart', function(ev) {
                                off();
                                ev.preventDefault();
                            });

                            scroll(location);
                        }, 2000);
                    });
                }
            });

            function scroll(location) {
                $location.hash(location);
                $anchorScroll();
            }
        };
    });
})();