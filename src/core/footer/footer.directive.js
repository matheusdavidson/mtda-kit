(function() {
    'use strict';
    angular.module('mtda.app').directive('footer', /*@ngInject*/ function directive() {
        return {
            restrict: 'E',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/footer/footerAdmin.tpl.html';
            },
            controller: 'FooterCtrl',
            controllerAs: 'vm'
        }
    });
})();