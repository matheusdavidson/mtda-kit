(function() {
    'use strict';

    angular
        .module('mtda.app')
        .config(config);

    /** @ngInject */
    function config($logProvider, toastrConfig, $locationProvider, $mdThemingProvider, $authProvider, api, setting, enviroment, $httpProvider, $menuProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        // Set options third-party lib
        toastrConfig.allowHtml = true;
        toastrConfig.timeOut = 3000;
        toastrConfig.positionClass = 'toast-top-right';
        toastrConfig.preventDuplicates = true;
        toastrConfig.progressBar = true;

        $locationProvider.html5Mode(true);

        //
        // Theme options
        //
        $mdThemingProvider.theme('default').primaryPalette('indigo', {
            // 'hue-1': '600'
        }).accentPalette('red', {
            // 'hue-1': '600'
        });

        //
        // Dark theme
        //
        $mdThemingProvider.theme('darkness').primaryPalette('cyan').dark();

        //
        // Auth options
        //
        $authProvider.httpInterceptor = function() {
            return true;
        },
        $authProvider.withCredentials = false;
        $authProvider.tokenRoot = null;
        $authProvider.cordova = false;
        $authProvider.baseUrl = '/';
        $authProvider.loginUrl = api.url + 'auth/local/';
        $authProvider.signupUrl = api.url + 'api/users/';
        $authProvider.unlinkUrl = '/auth/unlink/';
        $authProvider.tokenName = 'token';
        $authProvider.tokenPrefix = setting.slug + '.session'; // Local Storage name prefix
        $authProvider.authHeader = 'Authorization';
        $authProvider.authToken = 'Bearer';
        $authProvider.storageType = 'localStorage';

        //
        // Debug handle
        //
        if (enviroment === 'production')
            $logProvider.debugEnabled(false);
        else
            $logProvider.debugEnabled(true);

        //
        // Intercept Http
        //
        $httpProvider.interceptors.push('HttpInterceptor');
    }

})();