(function() {
    'use strict';
    angular.module('mtda.banner').controller('$BannerNewCtrl', /*@ngInject*/ function($state, $user, $Banner) {
        var vm = this;
        vm.user = $user;
        vm.currentUser = vm.user.current();

        vm.banner = new $Banner.service({
            model: {
                picFile: null
            }
        });

        vm.submit = submit;

        // ng-cropper
        vm.cropperOptions = {
            aspectRatio: 12 / 5,
            resultImage: {
                w: 1320,
                h: 550
            }
        }

        function submit() {

            // Send contact to server with recaptcha
            vm.banner.create().then(success, fail);

            function success() {
                // Clean model
                vm.banner.model = null;

                // Redirect listing
                $state.go('app.banner');
            }

            function fail() {}
        }
    })
})();