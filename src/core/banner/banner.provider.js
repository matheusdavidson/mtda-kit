(function() {
    'use strict';
    angular.module('mtda.banner').provider('$banner',
        /**
         * @ngdoc object
         * @name mtda.banner.$bannerProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $bannerProvider() {
            /**
             * @ngdoc object
             * @name mtda.banner.$bannerProvider#_config
             * @propertyOf mtda.banner.$bannerProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.banner.$bannerProvider#_routePath
             * @propertyOf mtda.banner.$bannerProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc function
             * @name mtda.banner.$bannerProvider#$get
             * @propertyOf mtda.banner.$bannerProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/banners/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this.config,
                    routePath: this.routePath,
                    // Factory
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    busy: busy
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (busy.start('bannerGetAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('bannerGetAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('bannerGetAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (busy.start('bannerGet')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('bannerGet');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('bannerGet');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (busy.start('bannerDelete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('bannerDelete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        busy.end('bannerDelete');
                    }
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$bannerProvider#config
             * @methodOf mtda.banner.$bannerProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($bannerProvider) {
             *     $bannerProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$bannerProvider#logo
             * @methodOf mtda.banner.$bannerProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($bannerProvider) {
             *     $bannerProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        }
    )
})();