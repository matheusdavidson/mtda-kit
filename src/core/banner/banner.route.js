(function() {
    'use strict';
    angular.module('mtda.banner').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $bannerProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.banner', {
            url: $bannerProvider.routePath() + 'banner/?term',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'banner',
            views: {
                'content': {
                    templateUrl: 'core/banner/banner.tpl.html',
                    controller: '$BannerCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.banner-new', {
            url: $bannerProvider.routePath() + 'banner/novo/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'banner',
            views: {
                'content': {
                    templateUrl: 'core/banner/bannerNew.tpl.html',
                    controller: '$BannerNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        });
    })
})();