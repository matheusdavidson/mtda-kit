(function() {
    'use strict';
    angular.module('mtda.banner').controller('$BannerCtrl', /*@ngInject*/ function($user, $List, $banner) {
        var vm = this;
        vm.user = $user;
        vm.currentUser = vm.user.current();

        // List
        vm.params = {
            source: $banner.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $banner.remove,
            addAction: 'app.banner-new',
            showAvatar: true,
            filters: {
                owner: true
            }
        };
    })
})();