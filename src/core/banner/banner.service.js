(function() {
    'use strict';
    angular.module('mtda.banner').service('$Banner', /*@ngInject*/ function($http, $page, $state, $q, api, Busy) {

        var url = api.url + 'api/banners/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;

        function create() {
            // Are we busy?
            if (this.busy.start('create')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var captcha = this.captcha,
                model = this.model;

            return $http
                .post(url, {
                    model: model,
                    captcha: captcha
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgCreated);
                        this.busy.end('create');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('create');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid || (!this.model.image && !this.model.image64));
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();