(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.banner
     **/
    angular.module('mtda.banner', [
        'ui.router',
        'ngFileUpload',
        'ngImgCrop'
    ]);
})();