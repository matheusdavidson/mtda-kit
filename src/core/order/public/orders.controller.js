(function() {
    'use strict';
    angular.module('mtda.order').controller('$PublicOrdersCtrl', /*@ngInject*/ function($List, $order, $state, $user) {
        var vm = this;

        // 
        // Set factory
        vm.factory = $order;

        // List
        vm.params = {
            source: source,
            secondaryActionType: 'delete',
            secondaryActionSource: $order.remove,
            addAction: false,
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            filters: {
                owner: true,
                sort: '-createdAt',
                status: $state.params.status ? $state.params.status : false,
                paymode: $state.params.paymode ? $state.params.paymode : false
            },
            sortOptions: vm.factory.sort,
            filterPaymode: vm.factory.filterPaymode,
            filterStatus: vm.factory.filterStatus
        };

        function primaryAction(item) {
            $state.go('app.public-account.order', item, {
                inherit: false
            });
        }

        function source(params) {
            // 
            // Current user
            vm.user = $user.getCurrent();

            // 
            // Add user
            params.user = vm.user.model._id;

            return $order.getAll(params);
        }
    })
})();