(function() {
    'use strict';
    angular.module('mtda.order').controller('$PublicOrderCtrl', /*@ngInject*/ function($List, $order, $Order, $state, $page, $user) {
        var vm = this;

        // 
        // Set user
        vm.user = $user.getCurrent();

        // 
        // Set factory
        vm.factory = $order;

        // 
        // Set order id or redirect
        vm.order = $state.params._id;
        if (!vm.order) $state.go('app.orders');

        // 
        // Get order or redirect
        vm.factory.get(vm.order, {
            user: vm.user.model._id
        }).then(success).catch(failed);

        // 
        // Set form locale
        vm.formLocale = $page.formLocale;

        // 
        // Sucess, set service
        function success(data) {
            // 
            // User validation
            if (data.user._id !== vm.user.model._id) {
                $state.go('app.public-account.orders');
                return false;
            }

            // 
            // Set service
            vm.service = new $Order.service({
                model: data
            });

            setFilterStatus();
        }

        // 
        // No order, redirect
        function failed() {
            $state.go('app.public-account.orders');
        }

        // 
        // Remove all from status select list
        function setFilterStatus() {
            // 
            // Get array from provider, but use angular.copy to remove bindings
            var entries = angular.copy(vm.factory.filterStatus);

            // 
            // Remove "all" options which is in 0 index
            entries.splice(0, 1);

            // 
            // Set
            vm.filterStatus = entries;
        }
    })
})();