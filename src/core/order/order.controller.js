(function() {
    'use strict';
    angular.module('mtda.order').controller('$OrderCtrl', /*@ngInject*/ function($List, $order, $Order, $state, $page) {
        var vm = this;

        // 
        // Set factory
        vm.factory = $order;

        // 
        // Set order id or redirect
        vm.order = $state.params._id;
        if (!vm.order) $state.go('app.orders');

        // 
        // Get order or redirect
        vm.factory.get(vm.order).then(success, failed);

        // 
        // Set form locale
        vm.formLocale = $page.formLocale;

        // 
        // Sucess, set service
        function success(data) {
            vm.service = new $Order.service({
                model: data
            });

            setFilterStatus();
        }

        // 
        // No order, redirect
        function failed() {
            $state.go('app.orders');
        }

        // 
        // Remove all from status select list
        function setFilterStatus() {
            // 
            // Get array from provider, but use angular.copy to remove bindings
            var entries = angular.copy(vm.factory.filterStatus);

            // 
            // Remove "all" options which is in 0 index
            entries.splice(0, 1);

            // 
            // Set
            vm.filterStatus = entries;
        }
    })
})();