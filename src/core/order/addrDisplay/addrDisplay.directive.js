(function() {
    'use strict';

    angular
        .module('mtda.directives')
        .directive('addrDisplay', directive);

    /** @ngInject */
    function directive() {
        var directive = {
            scope: {},
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : 'core/order/addrDisplay/addrDisplay.tpl.html';
            },
            controller: $addrDisplayCtrl,
            controllerAs: 'vm',
            bindToController: {
                address: '=',
                person: '=',
                formLocale: '='
            }
        };

        return directive;

        function $addrDisplayCtrl() {

        }
    }

})();