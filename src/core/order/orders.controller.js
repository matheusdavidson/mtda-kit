(function() {
    'use strict';
    angular.module('mtda.order').controller('$OrdersCtrl', /*@ngInject*/ function($List, $order, $state) {
        var vm = this;

        // 
        // Set factory
        vm.factory = $order;

        // List
        vm.params = {
            source: $order.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $order.remove,
            addAction: false,
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            filters: {
                owner: true,
                sort: '-createdAt',
                status: $state.params.status ? $state.params.status : false,
                paymode: $state.params.paymode ? $state.params.paymode : false
            },
            sortOptions: vm.factory.sort,
            filterPaymode: vm.factory.filterPaymode,
            filterStatus: vm.factory.filterStatus
        };

        function primaryAction(item) {
            $state.go('app.order', item, {
                inherit: false
            });
        }
    })
})();