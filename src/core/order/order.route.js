(function() {
    'use strict';
    angular.module('mtda.order').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $orderProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.orders', {
            url: $orderProvider.routePath() + 'orders/?term?paymode?status',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'order',
            views: {
                'content': {
                    templateUrl: 'core/order/orders.tpl.html',
                    controller: '$OrdersCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.order', {
            url: $orderProvider.routePath() + 'orders/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'order',
            views: {
                'content': {
                    templateUrl: 'core/order/order.tpl.html',
                    controller: '$OrderCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.public-account.orders', {
            url: 'orders/?term?paymode?status',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'order',
            views: {
                'account-content': {
                    templateUrl: 'core/order/public/orders.tpl.html',
                    controller: '$PublicOrdersCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.public-account.order', {
            url: 'orders/:_id',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            innerTabGroup: 'order',
            views: {
                'account-content': {
                    templateUrl: 'core/order/public/order.tpl.html',
                    controller: '$PublicOrderCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        });
    })
})();