(function() {
    'use strict';
    angular.module('mtda.order').provider('$order',
        /**
         * @ngdoc object
         * @name mtda.banner.$orderProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $orderProvider() {
            /**
             * @ngdoc object
             * @name mtda.banner.$orderProvider#_config
             * @propertyOf mtda.banner.$orderProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.banner.$orderProvider#_routePath
             * @propertyOf mtda.banner.$orderProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc function
             * @name mtda.banner.$orderProvider#$get
             * @propertyOf mtda.banner.$orderProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, $state, api, Busy, NegativePromise, $filter, $timeout) {
                var endpoint = api.url + 'api/orders/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this.config,
                    routePath: this.routePath,
                    // Factory
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    querySearch: querySearch,
                    busy: busy,
                    sort: [{
                        name: $filter('translate')('SORT_NOT_RECENT'),
                        value: 'createdAt'
                    }, {
                        name: $filter('translate')('SORT_RECENT'),
                        value: '-createdAt'
                    }, {
                        name: $filter('translate')('SORT_NAME_ASC'),
                        value: 'userName'
                    }, {
                        name: $filter('translate')('SORT_NAME_DESC'),
                        value: '-userName'
                    }, {
                        name: $filter('translate')('SORT_PRICE_ASC'),
                        value: 'price'
                    }, {
                        name: $filter('translate')('SORT_PRICE_DESC'),
                        value: '-price'
                    }],
                    filterPaymode: [{
                        name: $filter('translate')('ALL'),
                        value: false
                    }, {
                        name: $filter('translate')('PAID_boleto'),
                        value: 'boleto'
                    }, {
                        name: $filter('translate')('PAID_debito_bancario'),
                        value: 'debito_bancario'
                    }, {
                        name: $filter('translate')('PAID_cc'),
                        value: 'cc'
                    }, {
                        name: $filter('translate')('PAID_paypal_balance'),
                        value: 'paypal_balance'
                    }, {
                        name: $filter('translate')('PAID_paypal_cc'),
                        value: 'paypal_cc'
                    }],
                    filterStatus: [{
                        name: $filter('translate')('ALL'),
                        value: false
                    }, {
                        name: $filter('translate')('PAY_STAT_-1'),
                        value: -1
                    }, {
                        name: $filter('translate')('PAY_STAT_0'),
                        value: 0
                    }, {
                        name: $filter('translate')('PAY_STAT_1'),
                        value: 1
                    }, {
                        name: $filter('translate')('PAY_STAT_2'),
                        value: 2
                    }, {
                        name: $filter('translate')('PAY_STAT_3'),
                        value: 3
                    }, {
                        name: $filter('translate')('PAY_STAT_4'),
                        value: 4
                    }, {
                        name: $filter('translate')('PAY_STAT_5'),
                        value: 5
                    }],
                    filterInternalStatus: [{
                        name: $filter('translate')('PROG_0'),
                        value: 0
                    }, {
                        name: $filter('translate')('PROG_1'),
                        value: 1
                    }, {
                        name: $filter('translate')('PROG_2'),
                        value: 2
                    }, {
                        name: $filter('translate')('PROG_3'),
                        value: 3
                    }]
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (busy.start('getAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this), onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (busy.start('get')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        busy.end('get');

                        return response;
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (busy.start('delete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        busy.end('delete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        busy.end('delete');
                    }
                }

                function querySearch(query) {
                    if (!query) return false;

                    var deferred = $q.defer();
                    this.getAll({
                        filters: {
                            term: query,
                            owner: true
                        }
                    }).then(function(response) {
                        deferred.resolve(response.entries);
                    });

                    return deferred.promise;
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$orderProvider#config
             * @methodOf mtda.banner.$orderProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($orderProvider) {
             *     $orderProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$orderProvider#logo
             * @methodOf mtda.banner.$orderProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($orderProvider) {
             *     $orderProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        }
    )
})();