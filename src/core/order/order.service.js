(function() {
    'use strict';
    angular.module('mtda.order').service('$Order', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug) {

        var url = api.url + 'api/orders/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = null;
            this.subtotal = 0;
            this.total = 0;

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

            // 
            // Set subtotal
            this.setSubtotal();

            // 
            // Set total
            this.setTotal();
        }

        this.service.prototype.update = update;
        this.service.prototype.setSubtotal = setSubtotal;
        this.service.prototype.setTotal = setTotal;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;

        function update(email) {
            // Are we busy?
            if (this.busy.start('update')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            var model = this.model;

            // 
            // Post data
            var post = {
                model: model
            };

            // 
            // Send email?
            if (email) post.email = true;

            return $http
                .put(url + model.id, post)
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('update');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('update');

                        return response;
                    }.bind(this));
        }

        // 
        // Set subtotal
        // (cart.item x cart.qty)
        function setSubtotal() {
            // 
            // Entries validation
            if (!this.model.cart) return this.subtotal;

            // 
            // Calculate subtotal
            var subtotal = _.reduce(this.model.cart, function(sum, obj) {
                return sum + (obj.price * obj.qty);
            }, 0);

            // 
            // Set subtotal
            this.subtotal = subtotal;

            return subtotal;
        };

        // 
        // Set total
        // (subtotal x shipping)
        function setTotal() {
            // 
            // Entries validation
            if ((!this.subtotal && this.subtotal !== 0) || (!this.model.shipping.price && this.model.shipping.price !== 0)) return this.total;

            // 
            // Calculate total
            var total = this.subtotal + this.model.shipping.price;

            // 
            // Set subtotal
            this.total = total;

            return total;
        };

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid);
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }
    });
})();