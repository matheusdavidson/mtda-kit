(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.admin
     **/
    angular.module('mtda.order', [
        'ui.router'
    ]);
})();