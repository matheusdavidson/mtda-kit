(function() {
    'use strict';
    angular.module('mtda.page').config( /** @ngInject */ function($translateProvider, $pageProvider) {
        $translateProvider.translations('en_GB', {
            // MESSAGES
            'MSG_CREATED': 'Entry created!',
            'MSG_UPDATED': 'Entry updated!',
            'MSG_DELETED': 'Entry deleted!',
            'MSG_NOT_DONE': 'Something wrong, try again!',
            'MSG_LOGIN': 'You´re logged in.',
            'MSG_CART_CREATED': 'Item added to the cart',
            'MSG_CART_DELETED': 'Item deleted from the cart',
            'MSG_FILL_ALL_FIELDS': 'Fill all required fields',
            'PAGE_NOT_FOUND': 'Page not found',
            'PAGE_NOT_FOUND_TEXT': 'The page you are looking for doesn\'t exist or an error occurred.<br /> <a ui-sref="/">Go back</a> to the main page.',

            'ITEM_DELETE': 'Delete item',
            'NO_ITEMS': 'No items',

            'CHANGE_STATUS': 'Change status',
            'BASIC_DETAILS': 'Basic info',
            'IMAGE_UPLOAD': 'Image',
            'OPEN_MENU': 'Open menu',

            'SPANISH': 'Spanish',

            'ALL': 'All',
            'TITLE': 'Title',
            'NAME': 'Name',
            'LASTNAME': 'Surname',
            'EMAIL': 'Email',
            'COMPANY': 'Company',
            'PRICE': 'Price',
            'PRICE_OFFER': 'Price offer',
            'FEATURED': 'Featured',
            'YES': 'Yes',
            'NO': 'No',
            'QTY': 'Quantity',
            'CHOOSE': 'Choose',
            'CHOOSE_FILE': 'Choose file',
            'IMAGE_SELECTED': 'Selected image',
            'CLICK_TO_CHANGE': 'Click to change',
            'SAVE': 'Save',
            'CANCEL': 'Cancel',
            'ORDER': 'Order',
            'DISABLED': 'Disabled',
            'SIZE': 'Size',
            'CEP': 'Post code',
            'STREET': 'Address line 1',
            'NUMBER': 'N#',
            'BAIRRO': 'Address line 2',
            'CITY': 'Town or City',
            'STATE': 'State',
            'PHONE': 'Phone',
            'MOBILE': 'Mobile',
            'TOTAL': 'Total',
            'IDENTIFICATION': 'Identification',
            'PAYMENT_DETAILS': 'Payment Details',
            'SHIPPING_DETAILS': 'Shipping Details',
            'SHIPPING_METHOD': 'Shipping method',
            'PAYMENT': 'Payment',
            'CONFIRMATION': 'Confirmation',
            'BACK': 'Back',
            'PREV': 'Previous',
            'NEXT': 'Next',
            'RESET': 'Reset',
            'LOADING': 'Loading..',
            'LOADING_RESULTS': 'Loading results..',
            'CLICK_ITEM_SEE_DETAILS': 'Click on the item to see more details',
            'NOT_AVAILABLE': 'Not available',
            'HOW_IT_WORKS': 'How it works',
            'ABOUT_US': 'About us',
            'NO_ENTRIES': 'No entries',
            'NO_RESULTS': 'No results',
            'SHOWING': 'Showing',
            'OF': 'of',
            'RESULT-S': 'result(s)',
            'ORDER_BY': 'Order by',
            'COPYRIGHT_MSG': 'All Right Reserved',
            'DEV_BY_MSG': 'Developed by',
            'ONLY': 'only',
            'SEARCH_FOR': 'Search for',
            'ALL_PRODUCTS': 'All products',
            'FROM': 'From',
            'FOR': 'For',
            'SHIPPING_AND_TAX': 'Shipping and taxes',
            'INTERESTED?': 'Interested?',
            'INTERESTED?_MSG': 'buy and customize right now',
            'ORDER_NOW': 'Order now',
            'ESTIMATE_SHIPPING': 'Estimate shipping costs',
            'TYPE_CEP': 'Type your postcode',
            'CALCULATE': 'Calculate',

            // CHECKOUT
            'CHECKOUT': 'Checkout',
            'KEEP_SHOPPING': 'Keep shopping',
            'WHO_IS_PAYING': 'Who is paying?',
            'BILLING_ADDRESS': 'Billing address',
            'WHO_IS_RECEIVING': 'Who is receiving?',
            'SHIPPING_ADDRESS': 'Shipping address',
            'SELECT_SHIP_METHOD': 'Select a shipping method:',
            'SHIP_FREE_SHIPPING': 'Free shipping',
            'SHIP_GET_IN_STORE': 'Collect in store',
            'SHIP_free_shipping': 'Free shipping',
            'SHIP_free': 'Free shipping',
            'SHIP_get_in_store': 'Collect in store',
            'CHOOSE_PAYMENT_METHOD': 'How do you want to pay',
            'CHECKOUT_WITH_PAYPAL': 'Pay with Paypal',
            'PAYPAL_REDIRECT_MSG': 'You\'ll be redirected to paypal website',
            'SAME_FOR_SHIPPING': 'Use the same details for shipping',
            'SHIPPING': 'Shipping',
            'CONFIRMATION_ERROR': 'Could not proccess your order',
            'CONFIRMATION_ERROR_RESTART': 'Select a product and try again',
            'BOLETO': 'Payment slip',
            'BOLETO_WARNING': 'Your order will be confirmed only after payment compensation',
            'DEBITO_BANCARIO': 'Bank transfer',
            'CREDIT_CARD': 'Credit card',
            'CHOOSE_YOUR_BANK': 'Choose your bank',
            'INSERT_CREDIT_CARD_INFORMATION': 'Insert credit card information',
            'PAY_WITH': 'Pay with',
            'CARD_NUMBER': 'Card number',
            'CARD_EXPIRY_DATE': 'Expiry (MM/YY)',
            'CARD_EXPIRY_FULLDATE': 'Expiry (MM/YYYY)',
            'CARD_SECURITY_NUMBER': 'CVV',
            'CARD_NAME': 'Name in the card',
            'CARD_BIRTHDAY': 'Birthday',
            'CARD_PHONE': 'Phone',
            'CARD_TYPE': 'Type',
            'PAY_BY_PAYPAL': 'Payment by Paypal',
            'PAY_BY_MOIP': 'Payment by MOIP',
            'CONF_ORDER_RECEIVED': 'You order has been received',
            'CONF_DEBIT_MAKE_THE_PAYMENT': 'make the payment',
            'CONF_DEBIT_DIRECTLY_BANK': 'directly through your bank',
            'CONF_DEBIT_TO_CONFIRM': 'to confirm!',
            'CONF_PAY_INVOICE': 'Pay the invoice',
            'CONF_PAY_PENDING': 'Your payment is pending, you\'ll receive an email when the payment is received.',
            'CONF_PAY_COMPLETED': 'We received your payment, your order is confirmed!',
            'CONF_ORDER_NUMBER': 'Order number',
            'CONF_ORDER_NUMBER_LABEL': 'You can keep up with your order by clicking in the order number.',
            'PRINT_INVOICE_FOR_PAYMENT': 'Invoice for payment',
            'FINISH_PAYMENT_YOUR_BANK': 'Proceed to my bank',

            // COLORS
            'COLOR': 'Color',
            'colors': 'Color',
            'sizes': 'Size',
            'BLUE': 'Blue',
            'GREEN': 'Green',
            'GREY': 'Grey',
            'YELLOW': 'Yellow',
            'ORANGE': 'Orange',
            'PURPLE': 'Purple',
            'RED': 'Red',
            'WHITE': 'White',
            'BLACK': 'Black',
            'SILVER': 'Silver',

            // CATEGORIES
            'CATEGORIES': 'Categories',
            'CAT_MENU': 'Categories',
            'CAT_FORM_PARENT_CAT': 'Parent category',

            // PRODUCTS
            'PRODUCT': 'Product',
            'PROD_MENU': 'Products',
            'PROD_LIST': 'Product list',
            'PROD_LIST_DESC': 'Here you can manage your products',
            'PROD_NEW_TITLE': 'New product',
            'PROD_EDIT_TITLE': 'Edit product',
            'PROD_NEW_SUBTITLE': 'Fill the form to add a new product',
            'PROD_EDIT_SUBTITLE': 'Manage product information',
            'PROD_FORM_CAT': 'Add categories..',
            'PROD_FORM_UP_DESC': 'Choose an image and crop it:',
            'PROD_FORM_VARIATIONS_DESC': 'Manage product variations',
            'PROD_FORM_VARIATIONS_ADD': 'Add variations..',
            'PROD_FORM_BASIC_DETAILS': 'Basic info',
            'PROD_FORM_CAT_DETAILS': 'Add categories',
            'PROD_FORM_PRICE_DETAILS': 'Prices and offers',
            'PROD_FORM_FEATURED_DETAILS': 'Featured product',
            'PROD_FORM_UPLOAD_DETAILS': 'Main product image',
            'PROD_FORM_VARIATION_STOCK': 'Variation and stock',
            'PROD_FORM_VARIATION_MANAGE': 'Manage variation',

            // VARIANTS
            'VARI_PRODUCT_VARIATION': 'Choose variations',

            'NAV_OPEN_STORE': 'open store',

            // CART
            'CART': 'Cart',
            'CART_ADD': 'Add to Cart',
            'CART_AVAI': 'available',
            'CART_AVAIS': 'available',
            'CART_AVAI_ERROR': 'limit exceeded',
            'CART_OPEN': 'Open cart',
            'CART_TOTAL': 'Total in the cart',
            'SEE_CART': 'Go to cart',

            // TABS
            'PROJ_TAB_LINK': 'My projects',
            'REP_TAB_LINK': 'Reports',
            'ACC_TAB_LINK': 'My account',
            'ACCS_TAB_LINK': 'Accounts',

            // ACCOUNT
            'ACC_FORM_COMPANY_DETAILS': 'Company details',

            // LOGIN
            'LOGIN_TITLE': 'Login',
            'LOGIN_BTN': 'Login',
            'LOGIN_FORGOT_LINK': 'Forgot my password',
            'LOGIN_SIGNUP_LINK': 'Sign up',
            'LOGIN_HAS_LINK': 'Login',
            'PASSWORD': 'Password',
            'REGISTER_TITLE': 'Sign up',
            'REGISTER_BTN': 'Register',
            'LOGOUT': 'Logout',
            'NEW_CUSTOMER': 'New customer',
            'RETURNING_CUSTOMER': 'Returning customer',

            // CEP
            'CEP_NOT_FOUND': 'No address found for this postcode, fill the form manually',

            // SERVER ERRORS
            'SERVER_FILL_FIELDS': 'Fill all required fields',
            'SERVER_ORDER_PAYMENT_ERROR': 'Error while making the payment',
            'SERVER_ORDER_EXISTING_PAYMENT': 'Payment already exists',
            'SERVER_ORDER_NO_ITEM': 'No items in the cart',
            'SERVER_ORDER_PAYMENT_NOT_DONE': 'Payment was not done, try again',
            'SERVER_ORDER_STOCK_CHECK_FAILED': 'It was not possibl to check products in stock, try again',
            'SERVER_ORDER_NO_STOCK': 'There is no stock for some items in the cart',

            // ORDER
            'ORDERS': 'Orders',
            'PAYMENT_TYPE': 'Payment type',
            'RESUME': 'Resume',
            'INTERNAL_MONITORING': 'Internal progress',
            'TRACKING_CODE': 'Tracking code',
            'CODE': 'Code',
            'INSERT_CODE': 'Insirt the code',
            'CUSTOMER': 'Customer',
            'CUSTOMERS': 'Customers',
            'PRODUCTS': 'Products',
            'PROG_0': 'Received',
            'PROG_1': 'In production',
            'PROG_2': 'Sent',
            'PROG_3': 'Delivered',

            // SORT
            'SORT_BY': 'Sort by',
            'SORT_RECENT': 'Recent',
            'SORT_NOT_RECENT': 'Not recent',
            'SORT_NAME_ASC': 'Name (A-Z)',
            'SORT_NAME_DESC': 'Name (Z-A)',
            'SORT_PRICE_ASC': 'Lower price',
            'SORT_PRICE_DESC': 'Higher price',

            // PAYMENT STATUS
            'PAY_STAT_-1': 'created',
            'PAY_STAT_0': 'pending',
            'PAY_STAT_1': 'approved',
            'PAY_STAT_2': 'failed',
            'PAY_STAT_3': 'canceled',
            'PAY_STAT_4': 'expired',
            'PAY_STAT_5': 'completed',
            'PAID_WITH': 'Paid with',
            'PAID_boleto': 'invoice',
            'PAID_debito_bancario': 'bank debit',
            'PAID_cc': 'credit card',
            'PAID_paypal_cc': 'credit card',
            'PAID_paypal_balance': 'paypal balance',

            // USER
            'USER': 'User',
            'USERS': 'Users',
            'PROFILE': 'Profile',
            'MY_ORDERS': 'My orders',
            'NAVIGATION': 'Navigation',
            'CONTACT': 'Contact',
            'YOUR_ACCOUNT': 'Your account',
            'WELCOME': 'Welcome',

            // HAPPYGRAM
            'HIW-STEP-1': 'See the categories and choose a product',
            'HIW-STEP-2': 'Import, crop and customize your pictures',
            'HIW-STEP-3': 'Make the payment and receive your products',

            // CONTACT
            'GET_IN_TOUCH': 'Get in touch',
            'GET_IN_TOUCH_MSG': 'Fill the form to get in touch with us',
            'ANTISPAN_CHECK': 'anti-spam control',
            'SUBJECT': 'Subject',
            'MESSAGE': 'Message',
            'SEND': 'Send',
            'CUSTOMER_SERVICE': 'Customer service',
            'OTHER': 'Other',
            'OTHER_CONTACTS': 'Other contacts',
            'ADDRESS': 'Address',
        });

        $translateProvider.translations('pt_BR', {
            // MESSAGES
            'MSG_CREATED': 'Criado com sucesso!',
            'MSG_UPDATED': 'Atualizado com sucesso!',
            'MSG_DELETED': 'Deletado com sucesso!',
            'MSG_NOT_DONE': 'Ação não realizada, tente novamente!',
            'MSG_LOGIN': 'Você está logado.',
            'MSG_CART_CREATED': 'O item foi adicionado ao carrinho',
            'MSG_CART_DELETED': 'O item foi removido do carrinho',
            'MSG_FILL_ALL_FIELDS': 'Preencha todos os campos obrigatórios',
            'PAGE_NOT_FOUND': 'Página não encontrada',
            'PAGE_NOT_FOUND_TEXT': 'A página que você visitou não existe ou algum erro pode ter acontecido. <br /> <a ui-sref="/">Volte</a> para a página principal.',

            'ITEM_DELETE': 'Deletar item',
            'NO_ITEMS': 'Nenhum item',

            'CHANGE_STATUS': 'Mudar status',
            'BASIC_DETAILS': 'Dados básicos',
            'IMAGE_UPLOAD': 'Imagem',
            'OPEN_MENU': 'Abrir menu',

            'SPANISH': 'Espanhol',

            'ALL': 'Todos',
            'TITLE': 'Título',
            'NAME': 'Nome',
            'LASTNAME': 'Sobrenome',
            'EMAIL': 'Email',
            'COMPANY': 'Empresa',
            'PRICE': 'Preço',
            'PRICE_OFFER': 'Preço oferta',
            'FEATURED': 'Destaque',
            'YES': 'Sim',
            'NO': 'Não',
            'QTY': 'Quantidade',
            'CHOOSE': 'Escolha',
            'CHOOSE_FILE': 'Escolher arquivo',
            'IMAGE_SELECTED': 'Imagem selecionada',
            'CLICK_TO_CHANGE': 'Clique para mudar',
            'SAVE': 'Salvar',
            'CANCEL': 'Cancelar',
            'ORDER': 'Ordem',
            'DISABLED': 'Desativado',
            'SIZE': 'Tamanho',
            'STREET': 'Logradouro',
            'NUMBER': 'Nº',
            'BAIRRO': 'Bairro',
            'CITY': 'Cidade',
            'STATE': 'UF',
            'PHONE': 'Telefone',
            'MOBILE': 'Celular',
            'TOTAL': 'Total',
            'IDENTIFICATION': 'Identificação',
            'PAYMENT_DETAILS': 'Detalhes de cobrança',
            'SHIPPING_DETAILS': 'Detalhes da entrega',
            'SHIPPING_METHOD': 'Forma de entrega',
            'PAYMENT': 'Pagamento',
            'CONFIRMATION': 'Confirmação',
            'BACK': 'Voltar',
            'PREV': 'Anterior',
            'NEXT': 'Próximo',
            'RESET': 'Resetar',
            'LOADING': 'Carregando..',
            'LOADING_RESULTS': 'Carregando resultados..',
            'CLICK_ITEM_SEE_DETAILS': 'Clique no item para ver mais detalhes',
            'NOT_AVAILABLE': 'Não disponível',
            'HOW_IT_WORKS': 'Como funciona',
            'ABOUT_US': 'Sobre nós',
            'NO_ENTRIES': 'Nenhuma entrada',
            'NO_RESULTS': 'Nenhum resultado',
            'SHOWING': 'Exibindo',
            'OF': 'de',
            'RESULT-S': 'resultado(s)',
            'ORDER_BY': 'Classificar por',
            'COPYRIGHT_MSG': 'Direitos reservados',
            'DEV_BY_MSG': 'Desenvolvido por',
            'ONLY': 'apenas',
            'SEARCH_FOR': 'Busca por',
            'ALL_PRODUCTS': 'Todos os produtos',
            'FROM': 'De',
            'FOR': 'Por',
            'SHIPPING_AND_TAX': 'Entrega e taxas',
            'INTERESTED?': 'Interessado?',
            'INTERESTED?_MSG': 'inicie a montagem agora mesmo',
            'ORDER_NOW': 'Fazer pedido',
            'ESTIMATE_SHIPPING': 'Calcular valor do frete',
            'TYPE_CEP': 'Digite seu CEP',
            'CALCULATE': 'Calcular',

            // CHECKOUT
            'CHECKOUT': 'Checkout',
            'KEEP_SHOPPING': 'Continuar comprando',
            'WHO_IS_PAYING': 'Quem está pagando?',
            'BILLING_ADDRESS': 'Endereço de cobrança',
            'WHO_IS_RECEIVING': 'Quem está recebendo?',
            'SHIPPING_ADDRESS': 'Endereço de entrega',
            'SELECT_SHIP_METHOD': 'Selecione um método de envio:',
            'SHIP_FREE_SHIPPING': 'Entrega grátis',
            'SHIP_GET_IN_STORE': 'Buscar na loja',
            'SHIP_free_shipping': 'Entrega grátis',
            'SHIP_free': 'Entrega grátis',
            'SHIP_get_in_store': 'Buscar na loja',
            'CHOOSE_PAYMENT_METHOD': 'Como gostaria de pagar?',
            'CHECKOUT_WITH_PAYPAL': 'Pagar com Paypal',
            'PAYPAL_REDIRECT_MSG': 'Você será redirecionado para o site do paypal',
            'SAME_FOR_SHIPPING': 'Usar os mesmos dados para a entrega',
            'SHIPPING': 'Entrega',
            'CONFIRMATION_ERROR': 'Houver um erro ao processar seu pagamento',
            'CONFIRMATION_ERROR_RESTART': 'Selecione um produto e tente novamente',
            'BOLETO': 'Boleto bancário',
            'BOLETO_WARNING': 'Seu pedido será liberado após a compensação do boleto',
            'DEBITO_BANCARIO': 'Débito bancário',
            'CREDIT_CARD': 'Cartão de crédito',
            'CHOOSE_YOUR_BANK': 'Selecione o seu banco',
            'INSERT_CREDIT_CARD_INFORMATION': 'Insira as informações do cartão',
            'PAY_WITH': 'Pagar pelo',
            'CARD_NUMBER': 'Número do cartão',
            'CARD_EXPIRY_DATE': 'Validade (MM/AA)',
            'CARD_EXPIRY_FULLDATE': 'Validade (MM/AAAA)',
            'CARD_SECURITY_NUMBER': 'CVC',
            'CARD_NAME': 'Nome impresso',
            'CARD_BIRTHDAY': 'Nascimento',
            'CARD_PHONE': 'Telefone',
            'CARD_TYPE': 'Instituição',
            'PAY_BY_PAYPAL': 'Pagamento feito pelo Paypal',
            'PAY_BY_MOIP': 'Pagamento feito pelo MOIP',
            'CONF_ORDER_RECEIVED': 'Seu pedido foi recebido com sucesso',
            'CONF_DEBIT_MAKE_THE_PAYMENT': 'efetue o pagamento',
            'CONF_DEBIT_DIRECTLY_BANK': 'diretamente pelo site do seu banco',
            'CONF_DEBIT_TO_CONFIRM': 'para confirmar!',
            'CONF_PAY_INVOICE': 'efetue o pagamento do boleto',
            'CONF_PAY_PENDING': 'Seu pagamento está em análise, você receberá um e-mail assim que o pagamento for recebido.',
            'CONF_PAY_COMPLETED': 'Recebemos seu pagamento, seu pedido está liberado!',
            'CONF_ORDER_NUMBER': 'Código do seu pedido',
            'CONF_ORDER_NUMBER_LABEL': 'Você pode acompanhar o seu pedido clicando no número da ordem acima.',
            'PRINT_INVOICE_FOR_PAYMENT': 'Boleto para pagamento',
            'FINISH_PAYMENT_YOUR_BANK': 'Prosseguir para o meu banco',

            // COLORS
            'COLOR': 'Cor',
            'colors': 'Cor',
            'sizes': 'Tamanho',
            'BLUE': 'Azul',
            'GREEN': 'Verde',
            'GREY': 'Cinza',
            'YELLOW': 'Amarelo',
            'ORANGE': 'Laranja',
            'PURPLE': 'Roxo',
            'RED': 'Vermelho',
            'WHITE': 'Branco',
            'BLACK': 'Preto',
            'SILVER': 'Prata',

            // CATEGORIES
            'CATEGORIES': 'Categorias',
            'CAT_MENU': 'Categorias',
            'CAT_FORM_PARENT_CAT': 'Categoria pai',
            'CART_TOTAL': 'Total do carrinho',
            'SEE_CART': 'Ver carrinho',

            // PRODUCTS
            'PRODUCT': 'Produto',
            'PROD_MENU': 'Produtos',
            'PROD_LIST': 'Lista de produtos',
            'PROD_LIST_DESC': 'Aqui você pode gerenciar seus produtos',
            'PROD_NEW_TITLE': 'Novo produto',
            'PROD_EDIT_TITLE': 'Editar produto',
            'PROD_NEW_SUBTITLE': 'Preencha o formulário abaixo para adicionar um novo produto',
            'PROD_EDIT_SUBTITLE': 'Altere os dados do produto',
            'PROD_FORM_CAT': 'Adicione categorias..',
            'PROD_FORM_UP_DESC': 'Escolha uma imagem e faça o recorte que deseja:',
            'PROD_FORM_VARIATIONS_DESC': 'Gerenciar variações do produto',
            'PROD_FORM_VARIATIONS_ADD': 'Adicione variações..',
            'PROD_FORM_BASIC_DETAILS': 'Dados básicos',
            'PROD_FORM_CAT_DETAILS': 'Adicione categorias',
            'PROD_FORM_PRICE_DETAILS': 'Valores e ofertas',
            'PROD_FORM_FEATURED_DETAILS': 'Destaque',
            'PROD_FORM_UPLOAD_DETAILS': 'Imagem principal do produto',
            'PROD_FORM_VARIATION_STOCK': 'Variações e estoque',
            'PROD_FORM_VARIATION_MANAGE': 'Gerenciar variações',

            // VARIANTS
            'VARI_PRODUCT_VARIATION': 'Escolha as variações',

            'NAV_OPEN_STORE': 'abrir loja',

            // CART
            'CART': 'Carrinho',
            'CART_ADD': 'Adicionar',
            'CART_AVAI': 'disponível',
            'CART_AVAIS': 'disponíveis',
            'CART_AVAI_ERROR': 'limite excedido',
            'CART_OPEN': 'Abrir carrinho',

            // TABS
            'PROJ_TAB_LINK': 'Meus projetos',
            'REP_TAB_LINK': 'Relatórios',
            'ACC_TAB_LINK': 'Minha conta',
            'ACCS_TAB_LINK': 'Contas',

            // ACCOUNT
            'ACC_FORM_COMPANY_DETAILS': 'Dados da empresa',

            // LOGIN
            'LOGIN_TITLE': 'acesse sua conta',
            'LOGIN_BTN': 'Entrar',
            'LOGIN_FORGOT_LINK': 'Esqueci minha senha',
            'LOGIN_SIGNUP_LINK': 'Não tenho cadastro',
            'LOGIN_HAS_LINK': 'Já tenho cadastro',
            'PASSWORD': 'Senha',
            'REGISTER_TITLE': 'crie sua conta',
            'REGISTER_BTN': 'Registrar',
            'LOGOUT': 'Sair',
            'NEW_CUSTOMER': 'Novo cliente',
            'RETURNING_CUSTOMER': 'Já sou cliente',

            // CEP
            'CEP_NOT_FOUND': 'Nenhum endereço encontrado com este cep, preencha manualmente',

            // SERVER ERRORS
            'SERVER_FILL_FIELDS': 'Preencha os campos corretamente',
            'SERVER_ORDER_PAYMENT_ERROR': 'Houve um erro no pagamento',
            'SERVER_ORDER_EXISTING_PAYMENT': 'Pagamento já existente',
            'SERVER_ORDER_NO_ITEM': 'Nenhum item no carrinho',
            'SERVER_ORDER_PAYMENT_NOT_DONE': 'O pagamento não foi realizado, tente novamente',
            'SERVER_ORDER_STOCK_CHECK_FAILED': 'Não foi possível verificar o estoque, tente novamente',
            'SERVER_ORDER_NO_STOCK': 'Não há estoque para algum dos items do carrinho',

            // ORDER
            'ORDERS': 'Pedidos',
            'PAYMENT_TYPE': 'Tipo de pagamento',
            'RESUME': 'Resumo',
            'INTERNAL_MONITORING': 'Acompanhamento interno',
            'TRACKING_CODE': 'Código de rastreamento',
            'CODE': 'Código',
            'INSERT_CODE': 'Insira o código',
            'CUSTOMER': 'Cliente',
            'CUSTOMERS': 'Clientes',
            'PRODUCTS': 'Produtos',
            'PROG_0': 'Recebido',
            'PROG_1': 'Em produção',
            'PROG_2': 'Enviado',
            'PROG_3': 'Entregue',

            // SORT
            'SORT_BY': 'Ordenar por',
            'SORT_RECENT': 'Recente',
            'SORT_NOT_RECENT': 'Não recente',
            'SORT_NAME_ASC': 'Nome (A-Z)',
            'SORT_NAME_DESC': 'Nome (Z-A)',
            'SORT_PRICE_ASC': 'Menor preço',
            'SORT_PRICE_DESC': 'Maior preço',

            // PAYMENT STATUS
            'PAY_STAT_-1': 'criado',
            'PAY_STAT_0': 'aguardando pagamento',
            'PAY_STAT_1': 'aprovado',
            'PAY_STAT_2': 'falhou',
            'PAY_STAT_3': 'cancelado',
            'PAY_STAT_4': 'expirado',
            'PAY_STAT_5': 'completo',
            'PAID_WITH': 'Pagou com',
            'PAID_boleto': 'boleto',
            'PAID_debito_bancario': 'débito bancário',
            'PAID_cc': 'cartão de crédito',
            'PAID_paypal_cc': 'cartão de crédito',
            'PAID_paypal_balance': 'saldo paypal',

            // USER
            'USER': 'Usuário',
            'USERS': 'Usuários',
            'PROFILE': 'Perfil',
            'MY_ORDERS': 'Meus pedidos',
            'NAVIGATION': 'Navegação',
            'CONTACT': 'Contato',
            'YOUR_ACCOUNT': 'Sua conta',
            'WELCOME': 'Seja bem vindo',

            // HAPPYGRAM
            'HIW-STEP-1': 'Veja as categorias e escolha um produto',
            'HIW-STEP-2': 'Importe, recorte e personalize suas fotos',
            'HIW-STEP-3': 'Faça o pagamento e receba seu produto em casa',

            // CONTACT
            'GET_IN_TOUCH': 'Entre em contato',
            'GET_IN_TOUCH_MSG': 'Preencha o formulário para falar conosco, faça seu pedido, tire dúvidas ou envie sugestões',
            'ANTISPAN_CHECK': 'controle anti-spam, marque a opção "não sou um robô" para enviar',
            'SUBJECT': 'Assunto',
            'MESSAGE': 'Mensagem',
            'SEND': 'Enviar',
            'CUSTOMER_SERVICE': 'Atendimento',
            'OTHER': 'Outro',
            'OTHER_CONTACTS': 'Outros contatos',
            'ADDRESS': 'Endereço',
        });

        $translateProvider.preferredLanguage($pageProvider.formLocale);
        // $translateProvider.useSanitizeValueStrategy('sanitize');
    });
})();