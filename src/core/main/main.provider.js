(function() {
    'use strict';
    angular.module('mtda.main').provider('$main',
        /**
         * @ngdoc object
         * @name mtda.main.$mainProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $mainProvider() {
            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_config
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_layoutUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url do template para layout
             **/
            this._layoutUrl = 'core/main/layout.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_toolbarUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url do template para toolbar
             **/
            this._toolbarUrl = 'core/directives/toolbar/toolbar.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_sidenavUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url do template para sidenav
             **/
            this._sidenavUrl = 'page/menu/sidenav.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_fourOhFourUrl
             * @propertyOf mtda.main.$mainProvider
             * @description
             * url for 404 template
             **/
            this._fourOhFourUrl = 'core/404/404.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_logo
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena logo
             **/
            this._logo = 'assets/images/logo.png';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_logoWhite
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena logo na versão branca
             **/
            this._logoWhite = 'assets/images/logo-white.png';

            /**
             * @ngdoc object
             * @name mtda.main.$mainProvider#_logoWhite
             * @propertyOf mtda.main.$mainProvider
             * @description
             * armazena logo na versão branca
             **/
            this.logoMtda = 'assets/images/mtda-yellow.png';

            this.useCart = true;
            this.useAdmin = true;

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#$get
             * @propertyOf mtda.main.$mainProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($main) {
             *      console.log($main.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/layout/layout.tpl.html"
             *      console.log($main.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($window, setting) {
                return {
                    config: this._config,
                    layoutUrl: this._layoutUrl,
                    toolbarUrl: this._toolbarUrl,
                    sidenavUrl: this._sidenavUrl,
                    fourOhFourUrl: this._fourOhFourUrl,
                    logoWhite: this._logoWhite,
                    logoMtda: this._logoMtda,
                    logo: this._logo,
                    useCart: this.useCart,
                    useAdmin: this.useAdmin,
                }
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#config
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#logo
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para o path da logo
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.logo('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logo = function(value) {
                if (value) return this._logo = value;
                else return this._logo;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#logoWhite
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para o path da logo na versão branca
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.logoWhite('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logoWhite = function(value) {
                if (value) return this._logoWhite = value;
                else return this._logoWhite;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#logoMtda
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para o path da logo na versão branca
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *     $mainProvider.logoMtda('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logoMtda = function(value) {
                if (value) return this._logoMtda = value;
                else return this._logoMtda;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#layoutUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para url do layout
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.layoutUrl('core/layout/my-layout.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.layoutUrl = function(val) {
                if (val) return this._layoutUrl = val;
                else return this._layoutUrl;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#fourOhFourUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter 404 template url
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.fourOhFourUrl('core/layout/my-layout.html')
             * })
             * </pre>
             * @param {string} val - template url
             **/
            this.fourOhFourUrl = function(val) {
                if (val) return this._fourOhFourUrl = val;
                else return this._fourOhFourUrl;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#toolbarUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para url do toolbar
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.toolbarUrl('core/layout/my-toolbar.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.toolbarUrl = function(val) {
                if (val) return this._toolbarUrl = val;
                else return this._toolbarUrl;
            }

            /**
             * @ngdoc function
             * @name mtda.main.$mainProvider#sidenavUrl
             * @methodOf mtda.main.$mainProvider
             * @description
             * getter/setter para url do sidenav
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($mainProvider) {
             *      $mainProvider.sidenavUrl('core/layout/my-sidenav.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.sidenavUrl = function(val) {
                if (val) return this._sidenavUrl = val;
                else return this._sidenavUrl;
            };
        }
    )
})();