(function() {
    angular.module("mtda.setting", []).constant("setting", {
        name: "mtda kit",
        slug: "mtda-kit",
        version: "0.0.0",
        title: "mtda kit",
        baseUrl: "",
        titleSeparator: " — ",
        description: "mtda-kit",
        copyright: "mtda-kit",
        google: {
            clientId: "",
            language: "pt-BR"
        },
        facebook: {
            scope: "email",
            appId: "",
            appSecret: "",
            language: "pt-BR"
        },
        https: [],
        redirWww: false,
        ogLocale: "pt_BR",
        ogSiteName: "mtda-kit",
        ogTitle: "mtda-kit",
        ogDescription: "mtda-kit",
        ogUrl: "",
        ogImage: "",
        ogSection: "mtda-kit",
        ogTag: "mtda-kit"
    });
})();