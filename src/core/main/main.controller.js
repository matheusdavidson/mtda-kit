(function() {
    'use strict';

    angular
        .module('mtda.main')
        .controller('$MainCtrl', MainCtrl);

    /** @ngInject */
    function MainCtrl($scope, $state, $page, $auth, setting, enviroment, $user, $timeout, $mdSidenav, $mdMedia, $main, $cart) {
        var vm = this;
        vm.enviroment = enviroment;
        
        //
        // SEO
        //
        $page.title(setting.title);
        $page.description(setting.description);

        //
        // OPEN GRAPH
        //
        $page.ogLocale(setting.ogLocale);
        $page.ogSiteName(setting.ogSiteName);
        $page.ogTitle(setting.ogTitle);
        $page.ogDescription(setting.ogDescription);
        $page.ogUrl(setting.ogUrl);
        $page.ogImage(setting.ogImage);
        $page.ogSection(setting.ogSection);
        $page.ogTag(setting.ogTag);

        //
        // Scroll Top after transition
        // 
        var wdow = false;
        $scope.$on('$stateChangeSuccess', stateChangeSuccess);

        bootstrap();

        function bootstrap() {
            vm.config = $main;
            vm.user = $user;
            vm.$page = $page;
            vm.setting = setting;
            vm.year = moment().format('YYYY');
            vm.state = $state;
            vm.auth = $auth;
            vm.year = moment().format('YYYY');
            vm.mdMedia = $mdMedia;
            vm.cart = $cart;
        }

        function stateChangeSuccess() {
            // 
            // Toolbar and tabs
            // 
            $page.toolbar = $state.current.toolbar ? $state.current.toolbar : false;
            $page.tabs = $state.current.tabs ? $state.current.tabs : false;
            $page.footer = $state.current.footer ? $state.current.footer : false;

            // 
            // Set active tab according to route
            // 
            $page.config.set('tabGroup', $state.current.tabGroup ? $state.current.tabGroup : false);
            $page.config.set('innerTabGroup', $state.current.innerTabGroup ? $state.current.innerTabGroup : false);

            //
            // Auto Scroll to Top when state changed
            //
            $timeout(function() {
                // Close sidenav
                if($main.useAdmin) $mdSidenav('admin').close();
                if($main.useCart) $mdSidenav('cart').close();

                if (!wdow) wdow = angular.element('.app-content');

                // Animate to top
                wdow.animate({
                    scrollTop: 0
                }, 100);
            });
        }
    }
})();