(function() {
    'use strict';
    angular.module('mtda.filters').filter('dotToDash', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return '';
            return value.replace(/\./g, '-');
        };
    })
})();