(function() {
    'use strict';
    angular.module('mtda.filters').filter('customMoment', /*@ngInject*/ function() {
        return function(value, format) {
            if (!value) return '';

            // 
            // Set format
            if (!format) format = 'DD/MM/YYYY HH:mm';

            // 
            // From now
            if (format === 'fromNow') {
                // 
                // Apply moment
                return moment(value).fromNow();
            } else {
                // 
                // Apply moment
                return moment(value).format(format);
            }
        };
    })
})();