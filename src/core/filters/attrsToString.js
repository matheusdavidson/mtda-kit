(function() {
    'use strict';
    angular.module('mtda.filters').filter('attrsToString', /*@ngInject*/ function(lodash, $filter) {
        return function(value) {
            if (!value) return '';

            // 
            // Array and string
            var arr = [],
                string = '';

            // 
            // Loop through attrs to translate and join values
            lodash.forEach(value[0], function(val, key) {
                if (key !== 'colorValue') {

                    // 
                    // Value can be an array
                    if (typeof val === 'object') val = val.display;

                    // 
                    // Translate key, value and push to array
                    arr.push($filter('translate')(key) + ': ' + $filter('translate')(val));
                }
            });

            // 
            // Join array and set string
            string = arr.join(', ');

            return string;
        };
    })
})();