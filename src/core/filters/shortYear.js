(function() {
    'use strict';
    angular.module('mtda.filters').filter('shortYear', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return value;

            // 
            // Remove first two digits
            value = value.slice(2, 4);

            return value;
        };
    })
})();