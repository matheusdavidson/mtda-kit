(function() {
    'use strict';
    angular.module('mtda.filters').filter('nl2Br', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return '';
            return value.replace(/\n|\r\n|\r/g, '<br/>');
        };
    })
})();