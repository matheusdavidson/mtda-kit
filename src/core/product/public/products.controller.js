(function() {
    'use strict';
    angular.module('mtda.product').controller('$PublicProductsCtrl', /*@ngInject*/ function($List, $product, $state) {
        var vm = this;

        // List
        vm.params = {
            infinite: false,
            source: $product.getList,
            filters: {
                sort: '-price',
                term: $state.params.term
            },
            sortOptions: [{
                name: 'Nome (A-Z)',
                value: 'name'
            }, {
                name: 'Nome (Z-A)',
                value: '-name'
            }, {
                name: 'Maior preço',
                value: '-price'
            }, {
                name: 'Menor preço',
                value: 'price'
            }]
        };

        vm.service = new $List.service(vm.params);
    })
})();