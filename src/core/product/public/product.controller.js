(function() {
    'use strict';
    angular.module('mtda.product').controller('$PublicProductCtrl', /*@ngInject*/ function($scope, $user, $List, $product, $stateParams, $state) {
        var vm = this;
        if (!$stateParams.id) $state.go('app.home');

        vm.breadcrumbs = {
            slug: $stateParams.slug,
            slugId: $stateParams.id,
            category: $stateParams.category,
            child: $stateParams.child
        };

        vm.factory = $product;
        vm.product = false;

        vm.url = window.location;

        vm.factory.getPublic($stateParams.id).then(function(data) {
            vm.product = data.product;
            vm.variants = data.variants;
        });
    })
})();