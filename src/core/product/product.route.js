(function() {
    'use strict';
    angular.module('mtda.product').config( /*@ngInject*/ function($stateProvider, $userProvider, $productProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.products', {
            url: $productProvider.routePath() + 'products/?term',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            views: {
                'content': {
                    templateUrl: 'core/product/products.tpl.html',
                    controller: '$ProductCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.products-new', {
            url: $productProvider.routePath() + 'products/novo',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            views: {
                'content': {
                    templateUrl: 'core/product/productNew.tpl.html',
                    controller: '$ProductNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form', {
            abstract: true,
            url: '',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            views: {
                'content': {
                    templateUrl: 'core/product/productFormPage.tpl.html',
                    controller: '$ProductFormPageCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.edit', {
            url: $productProvider.routePath() + 'products/editar/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'basic',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/productForm.tpl.html',
                    controller: '$ProductEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.variant', {
            url: $productProvider.routePath() + 'products/variant/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'variation',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/variant/variants.tpl.html',
                    controller: '$VariantsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.variant-new', {
            url: $productProvider.routePath() + 'products/variant/:_id/new',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'variation',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/variant/variantForm.tpl.html',
                    controller: '$VariantNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.product-form.variant-edit', {
            url: $productProvider.routePath() + 'products/variant/:_id/edit/:item',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'product',
            innerTabGroup: 'variation',
            views: {
                'admin-content': {
                    templateUrl: 'core/product/variant/variantForm.tpl.html',
                    controller: '$VariantEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                editorRequired: $userProvider.editorRequired
            }
        }).state('app.productSearch', {
            url: '/products/?category?child?sort?term?page',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $productProvider.publicProductsTemplateUrl;
                    },
                    controller: '$PublicProductsCtrl as vm'
                }
            }
        }).state('app.product', {
            url: '/product/:slug/:id?category?child',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $productProvider.publicProductTemplateUrl;
                    },
                    controller: '$PublicProductCtrl as vm'
                }
            }
        });
    })
})();