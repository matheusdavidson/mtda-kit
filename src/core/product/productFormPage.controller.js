(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductFormPageCtrl', /*@ngInject*/ function($state) {
        var vm = this;
        vm.edit = $state.current.name === 'app.product-form.new' ? false : true;
        vm.params = $state.params;
        vm.productId = $state.params._id ? $state.params._id : undefined;

        vm.menu = [{
            label: 'BASIC_DETAILS',
            state: 'app.product-form.edit({_id: \''+vm.productId+'\'})',
            innerTabGroup: 'basic'
        }, {
            label: 'SPANISH',
            state: 'app.product-form.edit({_id: \''+vm.productId+'\'})',
            innerTabGroup: 'spanish'
        }, {
            label: 'PROD_FORM_VARIATION_STOCK',
            state: 'app.product-form.variant({_id: \''+vm.productId+'\'})',
            innerTabGroup: 'variation'
        }];
    })
})();