(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductCtrl', /*@ngInject*/ function($List, $product, $state) {
        var vm = this;

        // List
        vm.params = {
            source: $product.getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: $product.remove,
            addAction: 'app.products-new',
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            showAvatar: true,
            filters: {
                owner: true
            }
        };

        function primaryAction(item) {
            $state.go('app.product-form.edit', item, {
                inherit: false
            });
        }
    })
})();