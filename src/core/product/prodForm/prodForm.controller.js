(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProdFormCtrl', /*@ngInject*/ function($state, $stateParams, $Product, $product, $category) {
        var vm = this;

        // 
        // Set edit
        vm.edit = $stateParams.id ? $stateParams.id : false;

        // 
        // Product factory
        vm.factory = $product;

        //
        // Instantiate product
        setService();

        //
        // Submit and save
        vm.submit = submit;

        //
        // Autocomplete
        vm.querySearch = vm.factory.querySearch.bind($category);

        // 
        // Set product service
        function setService() {
            // 
            // create mode
            if (!vm.edit) {
                // 
                // new model
                newModel();
            }
            // 
            // update mode
            else {

            }
        }

        // 
        // New model
        function newModel() {
            return vm.service = new $Product.service({
                model: {
                    categories: [],
                    variants: {
                        attrs: []
                    },
                    picFile: null
                }
            });
        }

        // 
        // Submit form
        function submit() {
            // Send contact to server with recaptcha
            vm.service.create().then(success, fail);

            function success() {
                // Clean model
                vm.service.model = null;

                // Redirect listing
                $state.go('app.products');
            }

            function fail() {}
        }
    })
})();