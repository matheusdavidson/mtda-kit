(function() {
    'use strict';
    angular.module('mtda.product').directive('prodForm', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$ProdFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                id: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/prodForm/prodForm.tpl.html";
            }
        };
    });
})();