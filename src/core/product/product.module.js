(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.product
     **/
    angular.module('mtda.product', [
        'ui.router',
        'slugifier',
        'froala',
        'ui.utils.masks',
        'mtda.cart'
    ]);
})();