(function() {
    'use strict';
    angular.module('mtda.product').directive('prodView', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$ProdViewCtrl',
            controllerAs: 'vm',
            bindToController: {
                entries: '=',
                category: '=',
                child: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/prodView/prodView.tpl.html";
            }
        };
    });
})();