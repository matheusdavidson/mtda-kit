(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductNewCtrl', /*@ngInject*/ function($state, $stateParams, $Product, $product, $category, $translate) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $product;

        //
        // Instantiate product
        vm.product = new $Product.service({
            model: {
                categories: [],
                variants: {
                    attrs: []
                },
                picFile: null
            }
        });

        vm.variants = [{
            name: 'colors',
            status: false
        }, {
            name: 'sizes',
            status: false
        }];
        
        //
        // Submit and save
        vm.submit = submit;

        //
        // Autocomplete
        vm.querySearch = vm.factory.querySearch.bind($category);

        function submit() {
            // Send contact to server with recaptcha
            vm.product.create().then(success, fail);

            function success() {
                // Clean model
                vm.product.model = null;

                // Redirect listing
                $state.go('app.products');
            }

            function fail() {}
        }
    })
})();