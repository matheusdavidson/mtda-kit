(function() {
    'use strict';
    angular.module('mtda.product').controller('$ProductEditCtrl', /*@ngInject*/ function($state, $stateParams, $Product, $product, $category, lodash) {
        var vm = this;

        //
        // Set edit
        vm.edit = true;

        //
        // Reset iamge
        vm.resetImage = resetImage;

        // 
        // Product factory
        vm.factory = $product;

        //
        // Get and instantiate product
        vm.factory.get($state.params._id).then(function(data) {
            if (!data) $state.go('app.products');

            data.variants.attrs = data.variants.attrs ? data.variants.attrs : [];
            vm.variants = [];

            lodash.forEach(data.variants.attrs, function(val){
                vm.variants.push(val);
            });

            var hasColors = lodash.find(data.variants.attrs, ['name', 'colors']);
            var hasSizes = lodash.find(data.variants.attrs, ['name', 'sizes']);

            if (!hasColors) vm.variants.push({
                name: 'colors',
                status: false
            });

            if (!hasSizes) vm.variants.push({
                name: 'sizes',
                status: false
            });

            vm.product = new $Product.service({
                model: data
            });

        }).catch(function() {
            //
            // Send to list, we need product
            $state.go('app.products');
        });

        //
        // Submit and save
        vm.submit = submit;

        //
        // Set Autocomplete
        vm.querySearch = vm.factory.querySearch.bind($category);

        function resetImage() {
            vm.product.model.image = null;
        }

        function submit() {

            // Send contact to server with recaptcha
            vm.product.update().then(success, fail);

            function success() {}

            function fail() {}
        }
    })
})();