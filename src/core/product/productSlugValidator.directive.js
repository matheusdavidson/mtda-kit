(function() {
    'use strict';
    angular.module('mtda.product').directive('productSlugValidator', /*@ngInject*/ function($product) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.slug = function(modelValue, viewValue) {
                    return $product.slugValidation(viewValue, scope.vm.service.model.id);
                };
            }
        };
    });
})();