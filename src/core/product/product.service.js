(function() {
    'use strict';
    angular.module('mtda.product').service('$Product', /*@ngInject*/ function($http, $page, $state, $q, api, Busy, Slug, $mdDialog) {

        var url = api.url + 'api/products/';
        this.service = service;

        function service(params) {
            //
            // Default Params
            // 
            this.busy = new Busy.service();
            this.model = {
                image64: false
            };

            //
            // Instantiate
            // 
            params = params ? params : {};
            angular.extend(this, params);

        }

        this.service.prototype.create = create;
        this.service.prototype.update = update;
        this.service.prototype.formStatus = formStatus;
        this.service.prototype.negativePromise = negativePromise;
        this.service.prototype.slugifyUrl = slugifyUrl;
        this.service.prototype.chooseFile = chooseFile;

        function create() {
            // Are we busy?
            if (this.busy.start('productCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .post(url, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this));
        }

        function update() {
            // Are we busy?
            if (this.busy.start('productCreate')) return this.negativePromise();

            //
            // Limpar o model e separar outros dados
            // 
            var model = this.model;

            return $http
                .put(url + model.id, {
                    model: model
                })
                .success(
                    //
                    // success
                    //
                    function(response) {
                        $page.toast($page.msgUpdated);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this))
                .error(
                    //
                    // fail
                    //
                    function(response) {
                        $page.toast(response && response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productCreate');

                        return response;
                    }.bind(this));
        }

        function formStatus(method, invalid) {
            return (this.busy.status(method) || invalid || !this.model.categories.length);
        }

        function chooseFile(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'core/filepicker/dialogFilepicker.tpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function(result) {
                console.log('confirm', result);
                this.model.image64 = result[0].cropped;
            }.bind(this), function() {
                console.log('cancel');
            }.bind(this));
        }

        function negativePromise() {
            // Start promise
            var defer = $q.defer();

            // Reject promise
            defer.reject(false);

            // Return promise
            return defer.promise;
        }

        function slugifyUrl(value) {
            this.model.slug = Slug.slugify(value);
        }

        /*@ngInject*/
        function DialogController($scope, $product) {
            $scope.qty = 1;
            $scope.cropperOptions = $product.cropperOptions;
            $scope.result = false;

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.confirm = function() {
                $mdDialog.hide($scope.result);
            };
        }
    });
})();