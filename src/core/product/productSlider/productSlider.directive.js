(function() {
    'use strict';
    angular.module('mtda.product').directive('productSlider', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$ProductSliderCtrl',
            controllerAs: 'vm',
            bindToController: {
                images: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/productSlider/productSlider.tpl.html";
            }
        };
    });
})();