(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantEditCtrl', /*@ngInject*/ function($scope, $Variant, $variant, $product, $state, lodash, $page) {
        var vm = this;

        vm.factory = $variant;
        vm.product = false;

        //
        // Submit and save
        vm.submit = submit;

        $scope.$watch('vm.service.model.attrs', watchVariants, true);

        vm.factory.getOptions($state.params._id, $state.params.item).then(function(data) {
            //
            // Need product
            if (!data.entry) {
                $page.toast('Um dos atributos não existe mais, adicione novamente ou exclua esta variação');
                $state.go('app.product-form.variant', {
                    inherit: false
                });
            }

            //
            // Set product
            vm.product = data.entry;

            // 
            // Set variant
            vm.service = new $Variant.service({
                model: data.item
            });
        });

        function watchVariants(nv, ov) {
            var name = '';
            var check = false;

            if (nv !== ov) {
                var value = lodash.chain(nv).mapKeys(function(value, key) {
                    return value.name;
                }).mapValues(function(o) {

                    if (check) {
                        name += ',';
                    } else {
                        check = true
                    }

                    name += o.name + ':' + o.value;
                    return o.value;
                }).value();

                if (lodash.hasIn(vm.service.model, 'name')) vm.service.model.name = name;
            }
        }

        function submit() {
            // Send contact to server with recaptcha
            vm.service.update().then(success, fail);

            function success() {}

            function fail() {}
        }
    })
})();