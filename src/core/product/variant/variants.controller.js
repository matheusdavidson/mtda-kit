(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantsCtrl', /*@ngInject*/ function($List, $variant, $state) {
        var vm = this;

        vm.factory = $variant;
        vm.productId = $state.params._id;

        // List
        vm.params = {
            source: getAll,
            secondaryActionType: 'delete',
            secondaryActionSource: vm.factory.remove,
            addAction: 'app.product-form.variant-new',
            primaryAction: primaryAction,
            primaryIcon: 'edit',
            showAvatar: false,
            productId: vm.productId
        };

        function primaryAction(item) {
            $state.go('app.product-form.variant-edit', {
                _id: vm.productId,
                item: item._id
            }, {
                inherit: false
            });
        }

        function getAll(params) {
            params.product = $state.params._id;
            return vm.factory.getAll(params);
        }
    })
})();