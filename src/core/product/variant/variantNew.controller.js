(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantNewCtrl', /*@ngInject*/ function($scope, $Variant, $variant, $product, $state, lodash) {
        var vm = this;

        vm.factory = $variant;
        vm.service = new $Variant.service();
        vm.product = false;

        //
        // Submit and save
        vm.submit = submit;

        $scope.$watch('vm.service.model.attrs', watchVariants, true);

        vm.factory.getOptions($state.params._id, false).then(function(data) {
            vm.product = data.entry;
            vm.service.model.product = vm.product;
            vm.service.model.name = null;

            angular.forEach(vm.product.variants.attrs, function(value, key) {
                vm.service.model.attrs.push({
                    name: value.name,
                    value: null
                });
            });
        });

        function watchVariants(nv, ov) {
            var name = '';
            var check = false;

            if (nv !== ov) {
                var value = lodash.chain(nv).mapKeys(function(value, key) {
                    return value.name;
                }).mapValues(function(o) {

                    if (check) {
                        name += ',';
                    } else {
                        check = true
                    }

                    name += o.name + ':' + o.value;
                    return o.value;
                }).value();

                if (lodash.hasIn(vm.service.model, 'name')) vm.service.model.name = name;
            }
        }

        function submit() {
            // Send contact to server with recaptcha
            vm.service.create().then(success, fail);

            function success() {
                // Clean model
                vm.service.model = null;

                // Redirect listing
                $state.go('app.product-form.variant', {
                    _id: $state.params._id
                });
            }

            function fail() {}
        }
    })
})();