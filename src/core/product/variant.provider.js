(function() {
    'use strict';
    angular.module('mtda.product').provider('$variant',

        /*@ngInject*/
        function $variantProvider() {

            this.cropperOptions = {
                aspectRatio: '11 / 12',
                resultImage: {
                    w: 352,
                    h: 384
                }
            }

            //
            // Factory
            // 
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/variants/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    };

                return {
                    getAll: getAll,
                    get: get,
                    getOptions: getOptions,
                    remove: remove,
                    busy: new Busy.service(),
                    cropperOptions: this.cropperOptions
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível selecionar as variações');
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('get')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível selecionar o produto');
                        this.busy.end('get');
                    }
                }

                function getOptions(prod, item, queryParams) {
                    // Are we busy?
                    if (this.busy.start('getOptions')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + 'options/' + prod + '/' + item, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getOptions');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível selecionar as variações do produto');
                        this.busy.end('getOptions');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (this.busy.start('delete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry.id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('delete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : 'não foi possível deletar a variação');
                        this.busy.end('delete');
                    }
                }
            }
        });
})();