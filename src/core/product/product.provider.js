(function() {
    'use strict';
    angular.module('mtda.product').provider('$product',
        /**
         * @ngdoc object
         * @name mtda.banner.$productProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $productProvider() {

            /**
             * @ngdoc object
             * @name mtda.banner.$productProvider#_routePath
             * @propertyOf mtda.product.$productProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/admin/';

            /**
             * @ngdoc object
             * @name mtda.banner.$productProvider#publicProductsTemplateUrl
             * @propertyOf mtda.product.$productProvider
             * @description
             * Url for public products template
             **/
            this.publicProductsTemplateUrl = 'core/product/public/products.tpl.html'; //'app/layout/pblicProducts.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.banner.$productProvider#publicProductTemplateUrl
             * @propertyOf mtda.product.$productProvider
             * @description
             * Url for public product template
             **/
            this.publicProductTemplateUrl = 'core/product/public/product.tpl.html'; //'app/layout/pblicProduct.tpl.html';

            this.variants = {
                colors: [{
                    value: 'blue',
                    display: 'BLUE',
                    en_GB: 'blue',
                    pt_BR: 'azul'
                }, {
                    value: 'green',
                    display: 'GREEN',
                    en_GB: 'green',
                    pt_BR: 'verde'
                }, {
                    value: 'grey',
                    display: 'GREY',
                    en_GB: 'grey',
                    pt_BR: 'cinza'
                }, {
                    value: 'yellow',
                    display: 'YELLOW',
                    en_GB: 'yellow',
                    pt_BR: 'amarelo'
                }, {
                    value: 'orange',
                    display: 'ORANGE',
                    en_GB: 'orange',
                    pt_BR: 'laranja'
                }, {
                    value: 'purple',
                    display: 'PURPLE',
                    en_GB: 'purple',
                    pt_BR: 'roxo'
                }, {
                    value: 'red',
                    display: 'RED',
                    en_GB: 'red',
                    pt_BR: 'vermelho'
                }, {
                    value: 'white',
                    display: 'WHITE',
                    en_GB: 'white',
                    pt_BR: 'branco'
                }, {
                    value: 'black',
                    display: 'BLACK',
                    en_GB: 'black',
                    pt_BR: 'preto'
                }, {
                    value: 'gold',
                    display: 'GOLD',
                    en_GB: 'gold',
                    pt_BR: 'dourado'
                }, {
                    value: 'silver',
                    display: 'SILVER',
                    en_GB: 'silver',
                    pt_BR: 'prata'
                }],
                sizes: '0, 1, 3, 5, 7, 9, 11, 13, 15, 17, xxs, xs, s, m, l, xl, xxl, xxxl, 44, 46, unique'.split(/, +/g).map(function(state) {
                    return {
                        value: state.toLowerCase(),
                        display: state
                    };
                })
            };

            this.froalaOptions = {
                toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontSize', '|', 'color', 'emoticons', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', '-', 'insertLink', 'insertVideo', 'undo', 'redo']
            };

            this.cropperOptions = {
                aspectRatio: 11.2 / 15,
                resultImage: {
                    w: 352,
                    h: 384
                },
                viewMode: 1
            }

            //
            // Factory
            // 
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise, $filter) {
                var endpoint = api.url + 'api/products/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();


                return {
                    getAll: getAll,
                    getList: getList,
                    get: get,
                    getPublic: getPublic,
                    remove: remove,
                    querySearch: querySearch,
                    slugValidation: slugValidation,
                    busy: busy,
                    variants: this.variants,
                    froalaOptions: this.froalaOptions,
                    cropperOptions: this.cropperOptions,
                    routePath: this.routePath,
                    transformChip: transformChip,
                    sort: [{
                        name: $filter('translate')('SORT_NAME_ASC'),
                        value: 'name'
                    }, {
                        name: $filter('translate')('SORT_NAME_DESC'),
                        value: '-name'
                    }, {
                        name: $filter('translate')('SORT_PRICE_ASC'),
                        value: 'price'
                    }, {
                        name: $filter('translate')('SORT_PRICE_DESC'),
                        value: '-price'
                    }]
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('productGetAll')) return NegativePromise.defer();

                    var getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    };

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('productGetAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productGetAll');
                    }
                }

                function getList(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    var getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    };

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint + 'list', {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('productGet')) return NegativePromise.defer();

                    var getParams = {
                        user: false
                    };

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('productGet');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('productGet');
                    }
                }

                function getPublic(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('getPublic')) return NegativePromise.defer();

                    var getParams = {
                        user: false
                    };

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + 'public/' + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getPublic');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getPublic');
                    }
                }

                function remove(e, entry) {
                    // Are we busy?
                    if (this.busy.start('productDelete')) return NegativePromise.defer();

                    return $http.delete(endpoint + entry._id).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('productDelete');
                        return response;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgDeleted);
                        this.busy.end('productDelete');
                    }
                }

                function querySearch(query) {
                    if (!query) return false;

                    var deferred = $q.defer();

                    this.getAll({
                        filters: {
                            term: query
                        }
                    }).then(function(response) {
                        deferred.resolve(response.entries);
                    });

                    return deferred.promise;
                }

                function slugValidation(viewValue, id) {
                    return $http.post(endpoint + 'slugValidation', {
                        slug: viewValue,
                        id: id
                    }).then(
                        function(response) {
                            if (!response.data.valid) {
                                return $q.reject(response.data.errorMessage);
                            }

                            return true;
                        }
                    );
                }
            }

            /**
             * @ngdoc function
             * @name mtda.banner.$productProvider#logo
             * @methodOf mtda.banner.$productProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($productProvider) {
             *     $productProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }

            function transformChip(chip) {
                // If it is an object, it's already a known chip
                if (angular.isObject(chip)) {
                    return chip;
                }
                // Otherwise, create a new one
                return {
                    name: chip,
                    type: 'new'
                }
            }
        });
})();