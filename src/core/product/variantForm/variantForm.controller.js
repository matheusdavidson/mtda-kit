(function() {
    'use strict';
    angular.module('mtda.product').controller('$VariantFormCtrl', /*@ngInject*/ function($scope, lodash, $translate) {
        var vm = this;

        vm.selected = [];
        vm.querySearchVariations = querySearchVariations;
        vm.transformChip = transformChip;
        vm.loading = true;

        // Load if we have data
        if (lodash.hasIn(vm.model, 0)) {
            var model = lodash.chain(vm.model).find({
                name: vm.name
            }).pick(['name', 'options', 'status']).value();

            if (model) {
                vm.status = vm.status === false ? false : true;
                vm.selected = model.options;
            }
        }

        // Set loading to false
        vm.loading = false;

        $scope.$watch('vm.status', function(nv, ov) {
            if (nv !== ov && !vm.loading) {
                // Selected
                if (nv) {
                    vm.selected = vm.selected ? vm.selected : [];
                    vm.model.splice(vm.position, 0, {
                        name: vm.name,
                        options: vm.selected
                    });
                } else {
                    vm.model.splice(vm.position, 1);
                    vm.selected = [];
                }
            }
        }, true);

        function transformChip(chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }
            // Otherwise, create a new one
            return {
                name: chip,
                type: 'new'
            }
        }

        function querySearchVariations(query) {
            var arr = vm.entries;

            if (!Array.isArray(arr)) return false;

            var results = query ? arr.filter(createFilterFor(query)) : [];
            return results;
        }

        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(value) {

                var lang = $translate.use();

                return (value.value.indexOf(lowercaseQuery) === 0) ||
                    (value.value.indexOf(lowercaseQuery) === 0) ||
                    (value[lang] && value[lang].indexOf(lowercaseQuery) === 0);
            };
        }
    })
})();