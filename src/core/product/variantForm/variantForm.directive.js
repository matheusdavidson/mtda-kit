(function() {
    'use strict';
    angular.module('mtda.product').directive('variantForm', /*@ngInject*/ function directive() {
        return {
            scope: {},
            controller: '$VariantFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                name: '@',
                entries: '=',
                model: '=',
                position: '@',
                lang: '=',
                title: '@',
                status: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/product/variantForm/variantForm.tpl.html";
            }
        };
    });
})();