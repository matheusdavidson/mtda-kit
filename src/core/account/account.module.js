(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name mtda.account
     **/
    angular.module('mtda.account', [
        'ui.router'
    ]);
})();