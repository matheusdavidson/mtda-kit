(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountPasswordCtrl', /*@ngInject*/ function($state, $User, $account, $page) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $account;

        //
        // Select account
        vm.factory.get($state.params._id).then(function(data) {
            //
            // Instantiate account
            vm.service = new $User.service({
                model: data
            });

        }, function() {
            $page.toast($page.msgNotDone);

            // 
            // Redirect user back to accounts
            vm.goToProfile();
        });

        //
        // Submit and save
        vm.submit = submit;

        // 
        // Go to profile
        vm.goToProfile = goToProfile;

        function submit() {
            // Update entry
            vm.service.password().then(function() {

                // 
                // Redirect user back to accounts
                vm.goToProfile();
            });
        }

        function goToProfile() {
            // 
            // Redirect user back to accounts
            $state.go('app.account', {
                _id: $state.params._id
            });
        }
    })
})();