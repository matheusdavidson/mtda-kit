(function() {
    'use strict';
    angular.module('mtda.account').directive('phoneFormContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$PhoneFormContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '=',
                setFlex: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/phoneFormContent/phoneFormContent.tpl.html";
            }
        };
    });
})();