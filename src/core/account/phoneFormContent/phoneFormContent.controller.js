(function() {
    'use strict';
    angular.module('mtda.account').controller('$PhoneFormContentCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // Form locale
        vm.formLocale = $page.formLocale;

        // 
        // Flex size
        vm.flex = vm.setFlex ? vm.setFlex : '30';
    });
})();