(function() {
    'use strict';
    angular.module('mtda.account').directive('companyForm', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$CompanyFormCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/companyForm/companyForm.tpl.html";
            }
        };
    });
})();