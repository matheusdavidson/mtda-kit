(function() {
    'use strict';
    angular.module('mtda.account').provider('$account',
        /**
         * @ngdoc object
         * @name mtda.account.$accountProvider
         **/
        /*@ngInject*/
        function $accountProvider() {
            var path = 'core/account/';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_config
             * @propertyOf mtda.account.$accountProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountsTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * url do template para listagem de contas
             **/
            this._accountsTemplateUrl = path + 'accounts.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountsFormTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for new account
             **/
            this._accountsFormTemplateUrl = path + 'accountsForm.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for account
             **/
            this._accountTemplateUrl = path + 'account.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountPasswordTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for account password
             **/
            this._accountPasswordTemplateUrl = path + 'password/account-password.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_publicAccountTemplateUrl
             * @propertyOf mtda.account.$accountProvider
             * @description
             * template url for public account
             **/
            this._publicAccountTemplateUrl = path + 'public/account.tpl.html';

            /**
             * @ngdoc object
             * @name mtda.account.$accountProvider#_accountsListTitle
             * @propertyOf mtda.account.$accountProvider
             * @description
             * field to be used as title in accounts listing
             **/
            this._accountsListTitle = 'email';

            /**
             * @ngdoc object
             * @name mtda.banner.$accountProvider#_routePath
             * @propertyOf mtda.banner.$accountProvider
             * @description
             * Path for routes
             **/
            this._routePath = '/';

            /**
             * @ngdoc object
             * @name mtda.banner.$accountProvider#_accountRoute
             * @propertyOf mtda.banner.$accountProvider
             * @description
             * Path for routes
             **/
            this._accountRoute = 'app.public-account.orders';

            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#$get
             * @propertyOf mtda.account.$accountProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($accounts) {
             *      console.log($accounts.accountsTemplateUrl);
             *      //prints the current accountsTemplateUrl of `mtda.account`
             *      //ex.: "mtda/account/accounts.tpl.html"
             *      console.log($accounts.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = /*@ngInject*/ function($http, $q, $page, api, Busy, NegativePromise) {
                var endpoint = api.url + 'api/accounts/',
                    getAllParams = {
                        user: false,
                        page: 0,
                        limit: 1,
                        filters: false
                    },
                    getParams = {
                        user: false
                    },
                    busy = new Busy.service();

                return {
                    // Provider
                    config: this._config,
                    accountsTemplateUrl: this._accountsTemplateUrl,
                    accountsFormTemplateUrl: this._accountsFormTemplateUrl,
                    accountTemplateUrl: this._accountTemplateUrl,
                    accountPasswordTemplateUrl: this._accountPasswordTemplateUrl,
                    publicAccountTemplateUrl: this._publicAccountTemplateUrl,
                    accountsListTitle: this._accountsListTitle,
                    accountRoute: this._accountRoute,
                    routePath: this.routePath,

                    // Factory
                    getAll: getAll,
                    get: get,
                    remove: remove,
                    busy: busy,
                }

                function getAll(queryParams) {
                    // Are we busy?
                    if (this.busy.start('getAll')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getAllParams, queryParams);

                    return $http.get(endpoint, {
                        params: getAllParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('getAll');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('getAll');
                    }
                }

                function get(id, queryParams) {
                    // Are we busy?
                    if (this.busy.start('get')) return NegativePromise.defer();

                    // Merge params
                    angular.extend(getParams, queryParams);

                    return $http.get(endpoint + id, {
                        params: getParams
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    //handle unlink success
                    function onSuccess(response) {
                        this.busy.end('get');
                        return response.data;
                    }

                    //handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('get');
                    }
                }

                function remove(e, ids) {
                    // Are we busy?
                    if (this.busy.start('delete')) return NegativePromise.defer();

                    return $http.post(endpoint + 'destroy', {
                        ids: ids
                    }).then(onSuccess.bind(this)).catch(onError.bind(this));

                    // 
                    // handle unlink success
                    function onSuccess(response) {
                        this.busy.end('delete');
                        return response;
                    }

                    // 
                    // handle unlink fail
                    function onError(response) {
                        $page.toast(response.error ? response.error : $page.msgNotDone);
                        this.busy.end('delete');
                    }
                }
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#config
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             *     $accountProvider.config('registerWelcome','Olá @firstName, você entrou para a @appName');
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountsTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.accountsTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.accountsTemplateUrl = function(val) {
                if (val) return this._accountsTemplateUrl = val;
                else return this._accountsTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountsFormTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter for update template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.accountsFormTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val template url
             **/
            this.accountsFormTemplateUrl = function(val) {
                if (val) return this._accountsFormTemplateUrl = val;
                else return this._accountsFormTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountsListTitle
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.accountsListTitle('account.company')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.accountsListTitle = function(val) {
                if (val) return this._accountsListTitle = val;
                else return this._accountsListTitle;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#accountTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.accountsTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.accountTemplateUrl = function(val) {
                if (val) return this._accountTemplateUrl = val;
                else return this._accountTemplateUrl;
            }
            this.accountPasswordTemplateUrl = function(val) {
                if (val) return this._accountPasswordTemplateUrl = val;
                else return this._accountPasswordTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.account.$accountProvider#publicAccountTemplateUrl
             * @methodOf mtda.account.$accountProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.publicAccountTemplateUrl('core/accounts/my-accounts.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.publicAccountTemplateUrl = function(val) {
                if (val) return this._publicAccountTemplateUrl = val;
                else return this._publicAccountTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name mtda.banner.$accountProvider#logo
             * @methodOf mtda.banner.$accountProvider
             * @description
             * getter/setter for route path
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.routePath('admin/')
             * })
             * </pre>
             * @param {string} value route path
             **/
            this.routePath = function(value) {
                if (value) return this._routePath = value;
                else return this._routePath;
            }
        });
})();