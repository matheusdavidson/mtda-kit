(function() {
    'use strict';
    angular.module('mtda.account').controller('$PersonFormContentCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // Form locale
        vm.formLocale = $page.formLocale;
    });
})();