(function() {
    'use strict';
    angular.module('mtda.account').directive('personFormContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$PersonFormContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/personFormContent/personFormContent.tpl.html";
            }
        };
    });
})();