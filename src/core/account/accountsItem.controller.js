(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsItemCtrl', /*@ngInject*/ function($scope, $User) {
        var vm = this;

        vm.model = $scope.item;
        vm.service = new $User.service({
            model: vm.model
        });

        vm.toggleActive = function() {
            console.log(vm.model);
            vm.service.update();
        }
    })
})();