(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name app.project.directive:accountList
     * @restrict EA
     * @description
     * Listagem de contas
     * @element div
     **/
    angular.module('mtda.user').directive('accountList', /*@ngInject*/ function() {
        return {
            restrict: 'EA',
            templateUrl: 'core/account/account-list/account-list.tpl.html',
            controller: '$AccountListCtrl',
            controllerAs: 'list',
            bindToController: {}
        }
    });
})();