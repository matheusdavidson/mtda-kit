(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsEditCtrl', /*@ngInject*/ function($state, $User, $account, $page) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $account;

        // 
        // Edit mode
        vm.edit = true;

        //
        // Select account
        vm.factory.get($state.params._id).then(function(data) {
            //
            // Instantiate account
            vm.service = new $User.service({
                model: data
            });

        }, function() {
            $page.toast($page.msgNotDone);

            // 
            // Redirect user back to accounts
            $state.go('app.accounts');
        });

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // Update entry
            vm.service.update(false, true);
        }
    })
})();