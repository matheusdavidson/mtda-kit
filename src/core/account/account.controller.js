(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountCtrl', /*@ngInject*/ function($state, $User, $account, $page, $user, activity, lodash, $mdDialog) {
        var vm = this;

        // 
        // Set logged in user
        var user = $user.getCurrent(),
            profileId = $state.params._id;

        // 
        // If no user id, add from the session
        if (!profileId) profileId = user.model._id;

        // 
        // Factory
        vm.factory = $account;
        vm.activityFactory = activity;

        // 
        // Enable editing if the logged in user is the same as the profile
        vm.editable = (profileId === user.model._id);

        // 
        // Update profile pic
        vm.updateProfilePicture = updateProfilePicture;

        //
        // Select account
        vm.factory.get(profileId).then(function(data) {

            if (!data) {
                $state.go('app.home');
            }

            //
            // Instantiate account
            vm.service = new $User.service({
                model: data
            });

        }, function() {
            $page.toast($page.msgNotDone);

            // 
            // Redirect user back to accounts
            $state.go('app.accounts');
        });

        // 
        // Feed list
        vm.params = {
            searchPlaceholder: 'Feed',
            source: getAllActivities,
            primaryIcon: 'edit',
            filters: {
                owner: true
            },
            getAnchorParams: getAnchorParams,
            disableTransition: true,
            limit: 6,
            noPoster: true,
            noPanel: true
        };

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // Update entry
            vm.service.update(false, true);
        }

        function getAllActivities(params) {
            // 
            // Add createdBy
            params.filters['createdBy'] = profileId;

            // 
            // Get entries
            return vm.activityFactory.getAll(params);
        }

        function getAnchorParams(item) {
            var params = {};

            // 
            // Project?
            if (lodash.hasIn(item, 'project._id')) {
                params['_id'] = item.project._id;
                params[item.type] = item.entry._id;
            } else {
                params['_id'] = item.entry._id;
            }

            return params;
        }

        function updateProfilePicture(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'core/filepicker/dialogFilepicker.tpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: true
            }).then(function(result) {
                vm.service.model.image = result;
            }.bind(this), function() {}.bind(this));
        }

        /*@ngInject*/
        function DialogController($scope, $user) {

            var cropperOptions = {
                aspectRatio: 4 / 4,
                resultImage: {
                    w: 420,
                    h: 420
                },
                viewMode: 1
            }

            $scope.qty = 1;
            $scope.cropperOptions = cropperOptions;
            $scope.result = false;
            $scope.bucket = 'pm-upload';
            $scope.prefix = 'user/profile/' + $user.current().model.account.owner._id + '/';
            $scope.useInstagram = false;

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.confirm = function() {
                // 
                // Get current user
                var user = $user.getCurrent();

                // 
                // Update current image
                user.model.image64 = $scope.result[0].img;

                // 
                // Save
                user.update().then(function(response) {
                    user.model.image = response.data.image;
                    $mdDialog.hide(user.model.image);
                });
            };
        }
    })
})();