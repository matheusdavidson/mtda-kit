(function() {
    'use strict';
    angular.module('mtda.account').controller('$PublicAccount', /*@ngInject*/ function($account, $state, $menu, $user) {
        var vm = this;
        vm.menu = $menu;

        // 
        // User info
        vm.currentUser = $user.getCurrent();
    })
})();