(function() {
    'use strict';
    angular.module('mtda.account').controller('$PublicProfileCtrl', /*@ngInject*/ function($account, $state, $menu, $user) {
        var vm = this;

        // 
        // User service
        vm.service = $user.getCurrent();
    })
})();