(function() {
    'use strict';
    angular.module('mtda.account').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $userProvider, $accountProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.accounts', {
            url: $accountProvider.routePath() + 'accounts/',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'accounts',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountsTemplateUrl();
                    },
                    controller: '$AccountsCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                adminRequired: $userProvider.adminRequired
            }
        }).state('app.accounts-new', {
            url: $accountProvider.routePath() + 'accounts/new',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'accounts',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountsFormTemplateUrl();
                    },
                    controller: '$AccountsNewCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                adminRequired: $userProvider.adminRequired
            }
        }).state('app.accounts-edit', {
            url: $accountProvider.routePath() + 'accounts/edit/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'accounts',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountsFormTemplateUrl();
                    },
                    controller: '$AccountsEditCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired,
                adminRequired: $userProvider.adminRequired
            }
        }).state('app.account-password', {
            url: $accountProvider.routePath() + 'accounts/password/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'account',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountPasswordTemplateUrl();
                    },
                    controller: '$AccountPasswordCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.account', {
            url: $accountProvider.routePath() + 'accounts/profile/:_id',
            toolbar: 'admin',
            tabs: 'admin',
            footer: 'admin',
            tabGroup: 'account',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.accountTemplateUrl();
                    },
                    controller: '$AccountCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.public-account', {
            abstract: true,
            url: '/account/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            tabGroup: 'account',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $accountProvider.publicAccountTemplateUrl();
                    },
                    controller: '$AccountCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        }).state('app.public-account.profile', {
            url: 'profile/',
            toolbar: 'public',
            tabs: 'public',
            footer: 'public',
            tabGroup: 'profile',
            innerTabGroup: 'profile',
            views: {
                'account-content': {
                    templateUrl: 'core/account/public/profile.tpl.html',
                    controller: '$PublicProfileCtrl as vm'
                }
            },
            resolve: {
                loginRequired: $userProvider.loginRequired
            }
        });
    })
})();