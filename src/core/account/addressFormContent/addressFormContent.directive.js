(function() {
    'use strict';
    angular.module('mtda.account').directive('addressFormContent', /*@ngInject*/ function() {
        return {
            scope: {},
            controller: '$AddressFormContentCtrl',
            controllerAs: 'vm',
            bindToController: {
                model: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/account/addressFormContent/addressFormContent.tpl.html";
            }
        };
    });
})();