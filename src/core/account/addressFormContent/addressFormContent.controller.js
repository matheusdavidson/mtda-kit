(function() {
    'use strict';
    angular.module('mtda.account').controller('$AddressFormContentCtrl', /*@ngInject*/ function($page) {
        var vm = this;

        // 
        // brState
        vm.states = $page.brStates;

        // 
        // Form locale
        vm.formLocale = $page.formLocale;
    });
})();