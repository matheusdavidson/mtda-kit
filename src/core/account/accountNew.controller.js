(function() {
    'use strict';
    angular.module('mtda.account').controller('$AccountsNewCtrl', /*@ngInject*/ function($state, $User, $user, $page) {
        var vm = this;

        // 
        // Product factory
        vm.factory = $user;

        //
        // Instantiate account
        vm.service = new $User.service({});

        //
        // Submit and save
        vm.submit = submit;

        function submit() {
            // 
            // Set email as the password
            vm.service.model.password = vm.service.model.email;

            // Create entry, local provider and noLogin
            vm.service.create(false, true).then(success, fail);

            function success() {
                // Clean model
                vm.service.model = null;

                // Redirect listing
                $state.go('app.accounts');
            }

            function fail() {}
        }
    })
})();